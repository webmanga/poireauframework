<?php

namespace PoireauFramework\Event\Resolver;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\Event\Exception\ResolverException;

/**
 * Resolve the EventType from the event type name
 */
interface EventTypeResolverInterface
{
    /**
     * Resolve the event type name
     *
     * @param string $name The event type name
     *
     * @return EventTypeInterface The event type
     *
     * @throws ResolverException When cannot resolve the event type
     */
    public function resolve(string $name): EventTypeInterface;
}
