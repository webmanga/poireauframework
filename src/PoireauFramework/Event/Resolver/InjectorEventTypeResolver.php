<?php

namespace PoireauFramework\Event\Resolver;

use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\Event\Exception\ResolverException;

/**
 * Resolver using Injector
 * @see InjectorInterface
 */
class InjectorEventTypeResolver implements EventTypeResolverInterface
{
    /**
     * @var InjectorInterface
     */
    private $injector;

    public function __construct(InjectorInterface $injector)
    {
        $this->injector = $injector;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(string $name): EventTypeInterface
    {
        try {
            $event = $this->injector->get($name);
        } catch (\Exception $ex) {
            throw new ResolverException($name, $ex);
        }

        if (!$event instanceof EventTypeInterface) {
            throw new ResolverException($name);
        }

        return $event;
    }
}
