<?php

namespace PoireauFramework\Event\Type;

/**
 * Interface for event type
 */
interface EventTypeInterface
{
    /**
     * Check if the event type supports the listener
     * @param object $listener The listener object to check
     * @return bool
     */
    public function supports($listener): bool;

    /**
     * Execute the listener
     * @param object $listener The event listener. Should implements {@link EventTypeInterface::listenerInterface()}
     * @param array $arguments The listener arguments
     */
    public function execute($listener, array $arguments): void;
}
