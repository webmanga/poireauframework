<?php

namespace PoireauFramework\Event\Dispatcher;

use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Event\Resolver\EventTypeResolverInterface;
use PoireauFramework\Event\Exception\InvalidListenerException;

/**
 * Base implementation of EventDispatcher
 */
class BaseEventDispatcher implements EventDispatcherInterface
{
    /**
     * @var EventTypeResolverInterface
     */
    private $resolver;

    /**
     * @var array
     */
    protected $listeners = [];

    public function __construct(EventTypeResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * {@inheritdoc}
     */
    public function addListener(string $event, $listener): void
    {
        $eventType = $this->resolver->resolve($event);

        if (!$eventType->supports($listener)) {
            throw new InvalidListenerException($eventType, get_class($listener));
        }

        if (!isset($this->listeners[$event])) {
            $this->listeners[$event] = [];
        }

        $this->listeners[$event][] = $listener;
    }

    /**
     * {@inheritdoc}
     */
    public function register(EventListenerInterface $listener): void
    {
        foreach ($listener->eventTypes() as $event) {
            $this->addListener($event, $listener);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(string $event, array $arguments = []): void
    {
        if (!isset($this->listeners[$event])) {
            return;
        }

        $eventType = $this->resolver->resolve($event);

        foreach ($this->listeners[$event] as $listener) {
            $eventType->execute($listener, $arguments);
        }
    }

    /**
     * Get the resolver
     * @return EventTypeResolverInterface
     */
    protected function resolver(): EventTypeResolverInterface
    {
        return $this->resolver;
    }
}
