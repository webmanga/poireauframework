<?php

namespace PoireauFramework\Event\Dispatcher;

use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Event\Exception\InvalidListenerException;

/**
 * Interface for event dispatcher
 */
interface EventDispatcherInterface
{
    /**
     * Add a new listener
     * @param string $event The event type name
     * @param object $listener The event listener
     * @throws InvalidListenerException When listener type is not compatible with event
     */
    public function addListener(string $event, $listener): void;

    /**
     * Register an event listener module
     * @param EventListenerInterface $listener
     */
    public function register(EventListenerInterface $listener): void;

    /**
     * Dispatch an event
     * @param string $event The event type name
     * @param array $arguments The event arguments
     */
    public function dispatch(string $event, array $arguments = []): void;
}
