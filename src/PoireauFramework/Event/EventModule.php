<?php

namespace PoireauFramework\Event;

use PoireauFramework\Injector\Factory\FactoryContainer;
use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Event\Resolver\EventTypeResolverInterface;
use PoireauFramework\Event\Resolver\InjectorEventTypeResolver;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Event\Dispatcher\BaseEventDispatcher;
use PoireauFramework\Event\Type\EventTypeInterface;

/**
 * Injector module for event system
 */
class EventModule implements InjectorModuleInterface
{
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(EventDispatcherInterface::class, BaseEventDispatcher::class);
        $injector->impl(EventTypeResolverInterface::class, InjectorEventTypeResolver::class);

        $injector->factory(BaseEventDispatcher::class, [$this, "createDispatcher"]);

        $injector
            ->factory(InjectorEventTypeResolver::class, [$this, "createResolver"])
            ->fork("event")
            ->useDefaultFactory([$this, "createEventType"])
        ;
    }

    public function createResolver(InjectorInterface $injector, FactoryContainer $factory): InjectorEventTypeResolver
    {
        return new InjectorEventTypeResolver($injector);
    }

    public function createDispatcher(InjectorInterface $injector, FactoryContainer $factory): BaseEventDispatcher
    {
        return new BaseEventDispatcher(
            $injector->get("PoireauFramework\\Event\\Resolver\\EventTypeResolverInterface")
        );
    }

    public function createEventType(string $className, InjectorInterface $injector, array $parameters): EventTypeInterface
    {
        if (!is_subclass_of($className, EventTypeInterface::class)) {
            return null;
        }

        return new $className();
    }
}