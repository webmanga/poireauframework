<?php

namespace PoireauFramework\Event;

/**
 * Base interface for event listeners
 */
interface EventListenerInterface
{
    /**
     * Get the list of EventType class name
     * @return string[]
     */
    public function eventTypes(): array;
}
