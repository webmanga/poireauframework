<?php

namespace PoireauFramework\Event\Exception;

/**
 * Base Exception for events
 */
class EventException extends \RuntimeException {

}