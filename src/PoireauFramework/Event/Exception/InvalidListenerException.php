<?php

namespace PoireauFramework\Event\Exception;

use PoireauFramework\Event\Type\EventTypeInterface;

/**
 * Exception for invalid listener type
 */
class InvalidListenerException extends EventException
{
    /**
     * @var EventTypeInterface
     */
    private $eventType;

    /**
     * @var string
     */
    private $listenerType;

    public function __construct(EventTypeInterface $eventType, string $listenerType)
    {
        parent::__construct("Invalid listener type for event '" . get_class($eventType) . "' : on listener '" . $listenerType . "'");

        $this->eventType = $eventType;
        $this->listenerType = $listenerType;
    }

    public function eventType(): EventTypeInterface
    {
        return $this->eventType;
    }

    /**
     * @return string
     */
    public function listenerType(): string
    {
        return $this->listenerType;
    }
}