<?php

namespace PoireauFramework\Event\Exception;

/**
 * Exception for resolving errors
 */
class ResolverException extends EventException
{
    /**
     * @var string
     */
    private $typeName;

    public function __construct(string $typeName, \Exception $previous = null)
    {
        parent::__construct("Cannot resolve event type '" . $typeName . "'", 0, $previous);

        $this->typeName = $typeName;
    }

    /**
     * @return string
     */
    public function typeName(): string
    {
        return $this->typeName;
    }
}
