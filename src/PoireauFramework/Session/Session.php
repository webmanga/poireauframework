<?php

namespace PoireauFramework\Session;

use ArrayAccess;

/**
 * The session data entity
 */
class Session implements ArrayAccess
{
    /**
     * @var string
     */
    private $sid;

    private $data = [];


    /**
     * Initialize session data
     * @param string $sid The session id
     */
    public function __construct(string $sid)
    {
        $this->sid = $sid;
    }

    /**
     * Get the session id
     * @return string
     */
    public function sid(): string
    {
        return $this->sid;
    }

    /**
     * Get all session data
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

    //============//
    // SPL Access //
    //============//

    public function offsetGet($key)
    {
        return $this->data[$key];
    }

    public function offsetSet($key, $value): void
    {
        $this->data[$key] = $value;
    }

    public function offsetExists($key): bool
    {
        return array_key_exists($key, $this->data);
    }

    public function offsetUnset($key): void
    {
        unset($this->data[$key]);
    }

    public function __get($key)
    {
        return $this->data[$key];
    }

    public function __set($key, $value): void
    {
        $this->data[$key] = $value;
    }

    public function __isset($key): bool
    {
        return array_key_exists($key, $this->data);
    }

    public function __unset($key): void
    {
        unset($this->data[$key]);
    }
}
