<?php

namespace PoireauFramework\Session\Behavior;

/**
 * Fake session behavior (for testing)
 */
class DummySessionBehavior implements SessionBehaviorInterface
{
    /**
     * @var string
     */
    protected $sid;


    /**
     *
     */
    public function __construct(string $sid = "TEST_SID")
    {
        $this->sid = $sid;
    }

    /**
     * @return $this
     */
    public function setSessionId(string $sid): DummySessionBehavior
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): string
    {
        return $this->sid;
    }
}
