<?php

namespace PoireauFramework\Session\Behavior;

/**
 *
 */
interface SessionBehaviorInterface
{
    public function getSessionId(): string;
}
