<?php

namespace PoireauFramework\Session\Behavior;

/**
 * Behavior for native sessions
 */
class NativeSessionBehavior implements SessionBehaviorInterface
{
    /**
     * {@inheritdoc}
     */
    public function getSessionId(): string
    {
        return session_id();
    }
}
