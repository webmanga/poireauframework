<?php

namespace PoireauFramework\Session\Behavior;

use PoireauFramework\Http\Cookie\CookieService;

/**
 * Behavior for using cookie for session id
 * @todo Cookie attributes
 */
class CookieSessionIdBehavior implements SessionBehaviorInterface
{
    /**
     * @var CookieService
     */
    protected $cookies;

    /**
     * @var string
     */
    protected $cookieName;

    /**
     * @var null|string
     */
    protected $sessionId;


    /**
     * Create the behavior
     *
     * @param CookieService $cookies
     * @param string $cookieName
     */
    public function __construct(CookieService $cookies, string $cookieName)
    {
        $this->cookies    = $cookies;
        $this->cookieName = $cookieName;
    }

    /**
     * {@inheritdoc}
     */
    public function getSessionId(): string
    {
        if ($this->sessionId === null) {
            $this->loadSessionId();
        }

        return $this->sessionId;
    }

    /**
     * Load the session id from the current request
     */
    protected function loadSessionId(): void
    {
        $this->sessionId = $this->cookies->get($this->cookieName);

        if ($this->sessionId === null) {
            $this->sessionId = $this->generateSessionId();

            $this->cookies
                ->create($this->cookieName, $this->sessionId)
                ->secure(false)
            ;
        }
    }

    /**
     * Generate a unique session id
     */
    protected function generateSessionId(): string
    {
        return base64_encode(strrev(uniqid()) . openssl_random_pseudo_bytes(16));
    }
}
