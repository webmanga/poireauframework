<?php

namespace PoireauFramework\Session;

use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Kernel\Event\Contract\AfterRunListenerInterface;
use PoireauFramework\Kernel\Event\Type\AfterRun;
use PoireauFramework\Kernel\KernelInterface;

/**
 * Listener events for start and stop session
 */
class SessionListener implements AfterRunListenerInterface, EventListenerInterface
{
    /**
     * @var SessionService
     */
    private $service;


    public function __construct(SessionService $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function eventTypes(): array
    {
        return [AfterRun::NAME];
    }

    /**
     * {@inheritdoc}
     */
    public function onAfterRun(KernelInterface $kernel): void
    {
        if ($this->service->isStarted()) {
            $this->service->stop();
        }
    }
}
