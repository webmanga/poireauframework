<?php

namespace PoireauFramework\Session;

use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Session\Behavior\DummySessionBehavior;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\Storage\ArraySessionStorage;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * Session module for testing usages
 */
class TestingSessionModule extends SessionModule
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector)
    {
        parent::configure($injector);

        $injector->impl(SessionBehaviorInterface::class, DummySessionBehavior::class);
        $injector->impl(SessionStorageInterface::class, ArraySessionStorage::class);
    }
}
