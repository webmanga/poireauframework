<?php

namespace PoireauFramework\Session\Storage;

use PoireauFramework\Session\Session;

/**
 * Session storage using file system for store session
 */
class FileSessionStorage implements SessionStorageInterface
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var Session[]
     */
    protected $data = [];


    /**
     * Construct the native session storage
     * @param string $path The base session directory
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * {@inheritdoc}
     */
    public function start(): void
    {
        if (!is_dir($this->path)) {
            mkdir($this->path, 0700, true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function stop(): void
    {
        $this->data = [];
    }

    /**
     * {@inheritdoc}
     */
    public function retrieve(string $sid): ?Session
    {
        if (isset($this->data[$sid])) {
            return $this->data[$sid];
        }

        $filename = $this->getSessionFile($sid);

        if (!is_file($filename)) {
            return null;
        }

        $session = unserialize(file_get_contents($filename));

        if (!$session instanceof Session) {
            return null;
        }

        return $this->data[$sid] = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function store(Session $session): void
    {
        $this->data[$session->sid()] = $session;
        file_put_contents($this->getSessionFile($session->sid()), serialize($session));
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Session $session): void
    {
        unset($this->data[$session->sid()]);
        unlink($this->getSessionFile($session->sid()));
    }

    /**
     * {@inheritdoc}
     */
    public function clean(): void
    {
        //TODO remove old sessions
    }

    /**
     * Get the session file name
     */
    protected function getSessionFile(string $sid): string
    {
        return $this->path . DIRECTORY_SEPARATOR . "session_" . rtrim(str_replace("/", "_", $sid), "=") . ".ser";
    }

    /**
     * Hide session data
     */
    public function __debugInfo(): array
    {
        return [
            "path" => $this->path
        ];
    }
}
