<?php

namespace PoireauFramework\Session\Storage;

use PoireauFramework\Session\Session;

/**
 * Session storage using native php session system
 * and $_SESSION super global
 * @todo Check session id ?
 */
class NativeSessionStorage implements SessionStorageInterface
{
    /**
     * @var string
     */
    protected $ns;


    /**
     * Construct the native session storage
     * @param string $ns The session namespace, i.e. Where the Session object is stored
     */
    public function __construct(string $ns)
    {
        $this->ns = $ns;
    }

    /**
     * {@inheritdoc}
     */
    public function start(): void
    {
        if (PHP_SAPI === "cli") {
            if (!isset($_SESSION)) {
                $_SESSION = [];
            }
            return;
        }

        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function stop(): void
    {
        session_write_close();
        $_SESSION = [];
    }

    /**
     * {@inheritdoc}
     */
    public function retrieve(string $sid): ?Session
    {
        $session = $_SESSION[$this->ns] ?? null;

        if ($session instanceof Session) {
            return $session;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function store(Session $session): void
    {
        $_SESSION[$this->ns] = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Session $session): void
    {
        unset($_SESSION[$this->ns]);
    }

    /**
     * {@inheritdoc}
     */
    public function clean(): void
    {
        //GC is run automatically
    }
}
