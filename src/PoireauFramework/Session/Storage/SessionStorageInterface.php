<?php

namespace PoireauFramework\Session\Storage;

use PoireauFramework\Session\Session;

/**
 * Interface for session storage
 */
interface SessionStorageInterface
{
    /**
     * Start and initialize the session storage
     */
    public function start(): void;

    /**
     * Stop and flush session data
     */
    public function stop(): void;

    /**
     * Retrieve the session data
     * @param string $sid The session id
     * @return Session | null
     */
    public function retrieve(string $sid): ?Session;

    /**
     * Store the session data
     * @param Session $session
     */
    public function store(Session $session): void;

    /**
     * Delete the session data
     * @param Session $session
     */
    public function delete(Session $session): void;

    /**
     * Clean all expired sessions
     */
    public function clean(): void;
}
