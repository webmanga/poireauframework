<?php

namespace PoireauFramework\Session\Storage;

use PoireauFramework\Session\Session;

/**
 * Session storage using array (for testing usage)
 */
final class ArraySessionStorage implements SessionStorageInterface
{
    /**
     * @var string
     */
    private $data = [];

    /**
     * {@inheritdoc}
     */
    public function start(): void {}

    /**
     * {@inheritdoc}
     */
    public function stop(): void {}

    /**
     * {@inheritdoc}
     */
    public function retrieve(string $sid): ?Session
    {
        $session = $this->data[$sid] ?? null;

        if ($session instanceof Session) {
            return $session;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function store(Session $session): void
    {
        $this->data[$session->sid()] = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Session $session): void
    {
        unset($this->data[$session->sid()]);
    }

    /**
     * {@inheritdoc}
     */
    public function clean(): void {}
}
