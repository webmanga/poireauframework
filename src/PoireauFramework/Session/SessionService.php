<?php

namespace PoireauFramework\Session;

use ArrayAccess;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\Exception\SessionStatusException;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * Service for sessions
 */
class SessionService implements ArrayAccess
{
    /**
     * @var SessionStorageInterface
     */
    protected $storage;

    /**
     * @var SessionBehaviorInterface
     */
    protected $behavior;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var bool
     */
    protected $isStarted = false;


    /**
     * @param SessionStorageInterface $storage
     * @param SessionBehaviorInterface $behavior
     */
    public function __construct(SessionStorageInterface $storage, SessionBehaviorInterface $behavior)
    {
        $this->storage  = $storage;
        $this->behavior = $behavior;
    }

    /**
     * Start the session
     */
    public function start(): void
    {
        if ($this->isStarted) {
            return;
        }

        $this->storage->start();

        $sid = $this->behavior->getSessionId();

        $this->session = $this->storage->retrieve($sid);

        if ($this->session === null) {
            $this->session = new Session($sid);
        }

        $this->isStarted = true;
    }

    /**
     * Stop the session
     */
    public function stop(): void
    {
        if (!$this->isStarted) {
            return;
        }

        $this->storage->store($this->session);
        $this->storage->stop();

        $this->session = null;
        $this->isStarted = false;
    }

    /**
     * Destroy (i.e. stop and remove data) the current session
     */
    public function destroy(): void
    {
        if (!$this->isStarted) {
            return;
        }

        $this->storage->delete($this->session);
        $this->storage->stop();

        $this->session = null;
        $this->isStarted = false;
    }

    /**
     * Check if the session is started
     */
    public function isStarted(): bool
    {
        return $this->isStarted;
    }

    /**
     * Get the session data
     * @return Session
     */
    public function session(): ?Session
    {
        return $this->session;
    }

    //============//
    // SPL Access //
    //============//

    public function offsetGet($key)
    {
        if (!$this->isStarted) {
            throw SessionStatusException::notStarted();
        }

        return $this->session->offsetGet(key);
    }

    public function offsetSet($key, $value): void
    {
        if (!$this->isStarted) {
            throw SessionStatusException::notStarted();
        }

        $this->session->offsetSet($key, $value);
    }

    public function offsetExists($key): bool
    {
        if (!$this->isStarted) {
            throw SessionStatusException::notStarted();
        }

        return $this->session->offsetExists($key);
    }

    public function offsetUnset($key): void
    {
        if (!$this->isStarted) {
            throw SessionStatusException::notStarted();
        }

        $this->session->offsetUnset($key);
    }
}
