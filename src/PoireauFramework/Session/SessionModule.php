<?php

namespace PoireauFramework\Session;

use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;

use PoireauFramework\Session\Behavior\DummySessionBehavior;
use PoireauFramework\Session\Behavior\NativeSessionBehavior;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\Storage\ArraySessionStorage;
use PoireauFramework\Session\Storage\NativeSessionStorage;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * Module for session
 */
class SessionModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(SessionStorageInterface::class, NativeSessionStorage::class);
        $injector->impl(SessionBehaviorInterface::class, NativeSessionBehavior::class);

        $injector->factory(SessionService::class, [$this, "createSessionService"]);
        $injector->factory(NativeSessionStorage::class, [$this, "createNativeStorage"]);
        $injector->factory(NativeSessionBehavior::class, [$this, "createNativeBehavior"]);
        $injector->factory(ArraySessionStorage::class, [$this, "createArrayStorage"]);
        $injector->factory(DummySessionBehavior::class, [$this, "createDummyBehavior"]);
    }

    public function createSessionService(InjectorInterface $injector): SessionService
    {
        $session = new SessionService(
            $injector->get(SessionStorageInterface::class),
            $injector->get(SessionBehaviorInterface::class)
        );

        if ($injector->has(EventDispatcherInterface::class)) {
            $injector->get(EventDispatcherInterface::class)->register(new SessionListener($session));
        }

        return $session;
    }

    public function createNativeBehavior(): NativeSessionBehavior
    {
        return new NativeSessionBehavior();
    }

    public function createDummyBehavior(): DummySessionBehavior
    {
        return new DummySessionBehavior();
    }

    public function createNativeStorage(InjectorInterface $injector): NativeSessionStorage
    {
        $reg = $injector->reg("session");

        return new NativeSessionStorage(
            $reg->has("namespace") ? $reg->get("namespace") : "poireauframework.session"
        );
    }

    public function createArrayStorage(): ArraySessionStorage
    {
        return new ArraySessionStorage();
    }
}
