<?php

namespace PoireauFramework\Session\Exception;

/**
 * Base exception for sessions
 */
class SessionException extends \RuntimeException {
    
}
