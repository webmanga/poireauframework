<?php

namespace PoireauFramework\Session\Exception;

/**
 * Exception raise on bad session status
 */
class SessionStatusException extends SessionException {
    static public function notStarted(): SessionStatusException
    {
        return new SessionStatusException("Cannot access to session data : the session is not started");
    }
}
