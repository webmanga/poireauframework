<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Lang;

/**
 * Item for language system
 * @author vincent
 */
class LangItem {
    protected $data;
    
    public function __construct(array $data) {
        $this->data = $data;
    }
    
    public function __get($name){
        return isset($this->data[$name]) ? $this->data[$name] : $name;
    }
    
    public function __call($name, $arguments){
        $str = $this->$name;
        
        if(isset($arguments[0]) && is_array($arguments[0]))
            $arguments = $arguments[0];
        
        return $this->format($str, $arguments);
    }
    
    public function __isset($name) {
        return isset($this->data[$name]);
    }
    
    public function format($str, array $arguments){
        foreach($arguments as $k => $v)
            $str = str_replace('{' . $k . '}', $v, $str);
        
        return $str;
    }
}
