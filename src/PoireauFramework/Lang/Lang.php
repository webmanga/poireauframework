<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Lang;

use PoireauFramework\Config;
use PoireauFramework\Loader\Creatable;
use PoireauFramework\Loader\Loader;
use PoireauFramework\Router;

/**
 * Handle language strings
 * @author vincent
 */
class Lang extends LangItem implements Creatable{
    private $locale;
    private $loaded = [];
    private $config;
    
    private $langDirs = [];
    
    public function __construct($config){
        parent::__construct([]);
        $this->config = $config;
        $this->locale = $config->locale;
        
        if(isset($config->dirs)) {
            foreach ($config->dirs as $dir) {
                $this->langDirs[] = $dir;
            }
        }
        
        $this->langDirs[] = __DIR__ . DIRECTORY_SEPARATOR;
        
        if(isset($config->preload)){
            foreach($config->preload as $file)
                $this->load($file);
        }
    }
    
    public function setLocale($locale){
        $this->locale = $locale;
        $this->reload();
    }

    /**
     * Load a new lang file
     * @param string $file The lang file name
     */
    public function load($file){
        $file = strtolower($file);
        
        if(!empty($this->loaded[$file]))
            return;
        
        foreach($this->langDirs as $dir){
            $ini = $dir . $this->locale . DIRECTORY_SEPARATOR . $file . '.ini';

            if(!is_file($ini))
                continue;

            foreach(parse_ini_file($ini, true) as $name => $value){
                if(is_array($value)){
                    $this->data[$name] = new LangItem($value);
                }else{
                    $this->data[$name] = $value;
                }
            }

            $this->loaded[$file] = true;
            return true;
        }
        
        return false;
    }
    
    /**
     * Reload all loaded lang files
     */
    public function reload(){
        foreach($this->loaded as $file => &$b){
            $b = false;
            $this->load($file);
        }
    }
    
    public static function createInstance(Loader $loader){
        $instance = new static(
            $loader->load(Config::class)->lang
        );
        $router = $loader->load(Router::class);
        $instance->load($router->getController());
        return $instance;
    }
}
