<?php

namespace PoireauFramework\Template;

use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperLocatorInterface;

/**
 * Service for templates and views
 */
class TemplateService
{
    /**
     * @var ViewEngineInterface
     */
    protected $engine;

    /**
     * @var HelperLocatorInterface
     */
    protected $helpers;

    /**
     * @var View[]
     */
    protected $parents = [];


    /**
     * Construct the templating service
     * @param ViewEngineInterface $engine
     * @param HelperLocatorInterface $helpers
     */
    public function __construct(ViewEngineInterface $engine, HelperLocatorInterface $helpers)
    {
        $this->engine  = $engine;
        $this->setHelperLocator($helpers);
    }

    /**
     * Get the engine
     * @return ViewEngineInterface
     */
    public function getEngine(): ViewEngineInterface
    {
        return $this->engine;
    }

    /**
     * Change the engine
     * @param ViewEngineInterface $engine
     */
    public function setEngine(ViewEngineInterface $engine): void
    {
        $this->engine = $engine;
    }

    /**
     * Get the helper locator
     * @return HelperLocatorInterface
     */
    public function getHelperLocator(): HelperLocatorInterface
    {
        return $this->helpers;
    }

    /**
     * Set the helper locator
     * @param HelperLocatorInterface $helpers
     */
    public function setHelperLocator(HelperLocatorInterface $helpers): void
    {
        $this->helpers = $helpers;
        $this->engine->setHelperLocator($helpers);
    }

    /**
     * Render a template
     * @param string $template The template name
     * @param array $vars Vars to pass to the view
     * @return string The renderer Template
     */
    public function render(string $template, array $vars = []): string
    {
        $view = new View($template, $vars);

        $content = $this->engine->render($view);

        return $this->renderHierarchy($view, $content);
    }

    /**
     * Render the View into its hierarchy
     * @param View view The view to render
     * @param string $content The view content
     * @return string The renderer content
     */
    protected function renderHierarchy(View $view, string $content): string
    {
        while ($view->hasParent()) {
            $view = $this->getParentView(
                $view->getParent()["template"],
                $view->getParent()["vars"]
            );

            $view->append($content);

            $content = $this->engine->render($view);
        }

        return $content;
    }

    /**
     * Get or create the parent View object
     * @param string $template The template name
     * @param array $vars The parent vars
     * @return View
     */
    protected function getParentView(string $template, array $vars): View
    {
        if (isset($this->parents[$template])) {
            return $this->parents[$template]->setVars($vars);
        }

        $this->parents[$template] = new View($template, $vars);

        return $this->parents[$template];
    }
}
