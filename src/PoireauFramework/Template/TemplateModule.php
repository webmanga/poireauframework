<?php

namespace PoireauFramework\Template;

use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Template\Engine\PhpEngine;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperLocatorInterface;
use PoireauFramework\Template\Helper\InjectorHelperLocator;
use PoireauFramework\Template\Resolver\FileTemplateResolver;

/**
 * Module for template system
 * Registry:
 * template.paths string[]
 * template.helpers_ns string[]
 */
class TemplateModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(ViewEngineInterface::class, PhpEngine::class);
        $injector->impl(HelperLocatorInterface::class, InjectorHelperLocator::class);

        $injector->factory(TemplateService::class, [$this, "createTemplateService"]);
        $injector->factory(PhpEngine::class, [$this, "createTemplateEngine"]);
        $injector->factory(InjectorHelperLocator::class, [$this, "createHelperLocator"]);
    }

    public function createTemplateService(InjectorInterface $injector): TemplateService
    {
        return new TemplateService(
            $injector->get("PoireauFramework\\Template\\Engine\\PhpEngine"),
            $injector->get("PoireauFramework\\Template\\Helper\\InjectorHelperLocator")
        );
    }

    public function createTemplateEngine(InjectorInterface $injector): PhpEngine
    {
        $registry = $injector->reg("template");

        return new PhpEngine(
            new FileTemplateResolver(
                $registry->has("paths") ? $registry->get("paths") : []
            )
        );
    }

    public function createHelperLocator(InjectorInterface $injector): InjectorHelperLocator
    {
        $registry = $injector->reg("template");

        return new InjectorHelperLocator(
            $injector,
            $registry->has("helpers_ns") ? $registry->get("helpers_ns") : []
        );
    }
}
