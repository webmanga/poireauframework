<?php

namespace PoireauFramework\Template\Exception;

use LogicException;
use Exception;

/**
 * Base exception for templates errors
 */
class TemplateException extends LogicException
{
    /**
     * @var string
     */
    private $template;


    /**
     * Construct the exception
     * @param string $template
     * @param string $message
     * @param Exception $previous
     */
    public function __construct(string $template, string $message = "", Exception $previous = null)
    {
        parent::__construct($message . " On template '" . $template . "'", 0, $previous);

        $this->template = $template;
    }

    /**
     * Get the template
     * @return string
     */
    public function template(): string
    {
        return $this->template;
    }
}
