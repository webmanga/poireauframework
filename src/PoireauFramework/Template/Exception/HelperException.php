<?php

namespace PoireauFramework\Template\Exception;

use Exception;
use RuntimeException;

/**
 * Base exception for helpers
 */
class HelperException extends RuntimeException
{
    /**
     * The helper's name
     * @var string
     */
    private $helper;

    /**
     * Construct the exception
     * @param string $helper The helper name
     * @param string $message Error message
     * @param Exception $previous The exception's cause
     */
    public function __construct(string $helper, string $message = "", Exception $previous = null)
    {
        parent::__construct("$message for helper '$helper'", 0, $previous);

        $this->helper = $helper;
    }

    /**
     * Get the helper's name
     * @return string
     */
    public function helper(): string
    {
        return $this->helper;
    }
}
