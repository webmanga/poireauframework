<?php

namespace PoireauFramework\Template\Exception;

/**
 * Exception throws when an helper cannot be found
 */
class HelperNotFoundException extends HelperException
{
    /**
     * @param string $helper The helper's name
     */
    public function __construct(string $helper)
    {
        parent::__construct($helper, "Helper not found");
    }
}
