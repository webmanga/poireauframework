<?php

namespace PoireauFramework\Template\Exception;

/**
 * Exception throws when template is not found
 */
class TemplateNotFoundException extends TemplateException
{
    /**
     * Construct exception
     * @param string $template
     */
    public function __construct(string $template)
    {
        parent::__construct($template, "Template not found");
    }
}
