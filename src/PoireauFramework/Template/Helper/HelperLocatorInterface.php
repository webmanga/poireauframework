<?php

namespace PoireauFramework\Template\Helper;

use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Exception\HelperNotFoundException;

/**
 * Locator for templates helpers
 */
interface HelperLocatorInterface
{
    /**
     * Get the template helper
     * @param string $name The Helper's name
     * @return HelperInterface The helper's instance
     * @throws HelperNotFoundException When helper cannot be found
     */
    public function get(string $name): HelperInterface;

    /**
     * Run the helper
     *
     * @param string $name The helper's name
     * @param ViewEngineInterface $engine The engine
     * @param array $arguments
     *
     * @return mixed
     */
    public function run(string $name, ViewEngineInterface $engine, array $arguments);

    /**
     * Register an Helper
     * @param HelperInterface $helper
     */
    public function register(HelperInterface $helper): void;
}
