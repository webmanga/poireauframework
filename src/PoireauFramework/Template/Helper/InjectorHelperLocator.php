<?php

namespace PoireauFramework\Template\Helper;

use Exception;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Exception\HelperException;
use PoireauFramework\Template\Exception\HelperNotFoundException;

/**
 * Helper locator implementation using Injector
 */
class InjectorHelperLocator implements HelperLocatorInterface
{
    /**
     * @var InjectorInterface
     */
    protected $injector;

    /**
     * @var string[]
     */
    protected $namespaces = [];

    /**
     * @var HelperInterface[]
     */
    protected $helpers = [];


    /**
     * Construct the locator
     * @param InjectorInterface $injector
     */
    public function __construct(InjectorInterface $injector, array $namespaces = [])
    {
        $this->injector = $injector;

        $namespaces[] = "PoireauFramework\\Template\\Helper";

        $this->namespaces = $namespaces;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name): HelperInterface
    {
        if (isset($this->helpers[$name])) {
            return $this->helpers[$name];
        }

        foreach ($this->namespaces as $ns) {
            $className = $ns . "\\" . ucfirst($name);

            if (class_exists($className)) {
                $helper = $this->injector->get($className);

                $this->register($helper);

                return $helper;
            }
        }

        throw new HelperNotFoundException($name);
    }

    /**
     * {@inheritdoc}
     */
    public function run(string $name, ViewEngineInterface $engine, array $arguments)
    {
        try {
            return $this->get($name)->run($engine, $arguments);
        } catch (Exception $e) {
            throw new HelperException($name, $e->getMessage(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function register(HelperInterface $helper): void
    {
        $this->helpers[$helper->name()] = $helper;
    }

    /**
     * Register a new namespace
     * @param string $ns
     */
    public function addNamespace(string $ns): void
    {
        $this->namespaces[] = $ns;
    }

    /**
     * Hide injector
     */
    public function __debugInfo(): array
    {
        return [
            "namespaces" => $this->namespaces,
            "helpers"    => $this->helpers
        ];
    }
}
