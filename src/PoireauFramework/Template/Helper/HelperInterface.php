<?php

namespace PoireauFramework\Template\Helper;

use PoireauFramework\Template\Engine\ViewEngineInterface;

/**
 * Interface for template helpers
 */
interface HelperInterface
{
    /**
     * Get the helper's name
     * @return string
     */
    public function name(): string;

    /**
     * Run the Helper
     * @param ViewEngineInterface $engine
     * @param array $arguments
     * @return $this|string|mixed The run result, or $this
     */
    public function run(ViewEngineInterface $engine, array $arguments);
}
