<?php

namespace PoireauFramework\Template\Resolver;

use PoireauFramework\Template\Exception\TemplateNotFoundException;

/**
 * Resolver using file
 */
class FileTemplateResolver implements TemplateResolverInterface
{
    /**
     * @var string[]
     */
    private $paths;

    /**
     * @var string[]
     */
    protected $extensions;


    /**
     * @param string[] $paths
     */
    public function __construct(array $paths)
    {
        $this->paths = $paths;
        $this->extensions = [
            "",
            ".html.php",
            ".part.php",
            ".php"
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function resolveTemplate(string $name): string
    {
        foreach ($this->paths as $path) {
            foreach ($this->extensions as $ext) {
                $file = $path . $name . $ext;

                if (file_exists($file)) {
                    return $file;
                }
            }
        }

        throw new TemplateNotFoundException($name);
    }

    /**
     * Register available file extensions for templates
     * @param array $extensions List of available extensions
     */
    public function setAvailableExtensions(array $extensions): void
    {
        $this->extensions = $extensions;
    }
}
