<?php

namespace PoireauFramework\Template\Resolver;

/**
 * Interface for resolve templates from its name
 */
interface TemplateResolverInterface
{
    /**
     * Resolve a template from its name
     * @param string $name
     * @return string
     * @throws \PoireauFramework\Template\Exception\TemplateNotFoundException When the template cannot be found
     */
    public function resolveTemplate(string $name): string;
}
