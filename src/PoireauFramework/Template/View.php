<?php

namespace PoireauFramework\Template;

/**
 * View object
 */
class View
{
    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    protected $vars;

    /**
     * @var array
     */
    protected $parent;

    /**
     * @var string
     */
    protected $content;


    /**
     * Construct the view
     * @param string $template The view template name
     * @param array $vars The view vars
     */
    public function __construct(string $template, array $vars = [])
    {
        $this->template = $template;
        $this->vars     = $vars;
        $this->parent   = [
            "template" => null,
            "vars"     => []
        ];
    }

    /**
     * Get the view template name
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * Get defined vars
     * @return array
     */
    public function getVars(): array
    {
        return $this->vars;
    }

    /**
     * Set vars
     * @param array $vars
     * @return $this
     */
    public function setVars(array $vars): View
    {
        foreach ($vars as $k => $v) {
            $this->vars[$k] = $v;
        }

        return $this;
    }

    /**
     * Get the parent template and vars
     * @return array
     */
    public function getParent(): array
    {
        return $this->parent;
    }

    /**
     * Check if the view has a parent
     * @return bool
     */
    public function hasParent(): bool
    {
        return !empty($this->parent["template"]);
    }

    /**
     * Get the view content
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Append content to the view
     * @param string $content Content to append
     * @return $this
     */
    public function append(string $content): View
    {
        $this->content .= $content;

        return $this;
    }

    /**
     * Set the parent view
     * @return $this
     */
    public function parent(string $parent = null, array $vars = []): View
    {
        $this->parent["template"] = $parent;
        $this->parent["vars"]     = array_replace($this->parent["vars"], $vars);

        return $this;
    }

    /**
     * Get a var from view
     */
    public function __get(string $name)
    {
        return $this->vars[$name];
    }

    /**
     * Set a var to the view
     */
    public function __set(string $name, $value): void
    {
        $this->vars[$name] = $value;
    }
}
