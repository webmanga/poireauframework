<?php

namespace PoireauFramework\Template\Engine;

use Exception;
use PoireauFramework\Template\Exception\TemplateException;
use PoireauFramework\Template\Helper\HelperLocatorInterface;
use PoireauFramework\Template\Resolver\TemplateResolverInterface;
use PoireauFramework\Template\View;

/**
 * View engine using PHP files
 */
class PhpEngine implements ViewEngineInterface
{
    /**
     * @var View[]
     */
    protected $views;

    /**
     * @var TemplateResolverInterface
     */
    protected $templateResolver;

    /**
     * @var HelperLocator
     */
    protected $helpers;


    /**
     * Construct PhpEngine
     * @param TemplateResolverInterface templateResolver
     */
    public function __construct(TemplateResolverInterface $templateResolver)
    {
        $this->templateResolver = $templateResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function render(View $view): string
    {
        $this->views[] = $view;

        ob_start();

        $this->part($view->getTemplate());

        array_pop($this->views);

        return ob_get_clean();
    }

    /**
     * {@inheritdoc}
     */
    public function part(string $template)
    {
        $file = $this->templateResolver->resolveTemplate($template);

        extract($this->view()->getVars());

        try {
            require $file;
        } catch (Exception $e) {
            throw new TemplateException($template, "Error during execution", $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function view(): View
    {
        return end($this->views);
    }

    /**
     * {@inheritdoc}
     */
    public function setHelperLocator(HelperLocatorInterface $helpers): void
    {
        $this->helpers = $helpers;
    }

    /**
     * Run an helper
     */
    public function __call(string $name, array $arguments)
    {
        return $this->helpers->run($name, $this, $arguments);
    }
}
