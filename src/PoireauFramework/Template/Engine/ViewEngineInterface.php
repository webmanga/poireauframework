<?php

namespace PoireauFramework\Template\Engine;

use PoireauFramework\Template\Helper\HelperLocatorInterface;
use PoireauFramework\Template\View;

/**
 * Interface for render views
 */
interface ViewEngineInterface
{
    /**
     * Render the view
     * @param View $view
     * @return string The renderer view
     */
    public function render(View $view): string;

    /**
     * include a view part
     * @param string $template The view part
     * @return mixed
     */
    public function part(string $template);

    /**
     * Get the current View instance
     * @return View
     */
    public function view(): View;

    /**
     * Set the HelperLocator
     * @param HelperLocatorInterface $helpers
     */
    public function setHelperLocator(HelperLocatorInterface $helpers): void;
}
