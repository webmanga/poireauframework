<?php

namespace PoireauFramework\Util\IO;

/**
 * Adapter for Writable classes
 */
abstract class WritableAdapter implements Writable
{
    /**
     * {@inheritdoc}
     */
    public function writeln(string $line): Writable
    {
        $this->write($line);
        $this->newLine();

        return $this;
    }
}
