<?php

namespace PoireauFramework\Util\IO;

/**
 * Interface for object that can be writing in (file, output stream...)
 */
interface Writable
{
    /**
     * Write string to the writable
     * @param string $what
     * @return $this
     */
    public function write(string $what): Writable;

    /**
     * Write a line
     * @param string $line
     * @return $this
     */
    public function writeln(string $line): Writable;

    /**
     * Add a new line
     * @return $this
     */
    public function newLine(): Writable;
}
