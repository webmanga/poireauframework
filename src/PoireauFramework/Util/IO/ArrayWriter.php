<?php

namespace PoireauFramework\Util\IO;

/**
 * Writer using array
 */
class ArrayWriter extends WritableAdapter
{
    /**
     * @var string[]
     */
    private $lines = [""];

    /**
     * {@inheritdoc}
     */
    public function write(string $what): Writable
    {
        $this->lines[count($this->lines) - 1] .= $what;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function newLine(): Writable
    {
        $this->lines[] = "";

        return $this;
    }

    /**
     * Get write lines
     * @return string[]
     */
    public function lines(): array
    {
        return $this->lines;
    }
}
