<?php

namespace PoireauFramework\Util\IO;

/**
 * Writer using php standard output (echo)
 */
class OutputWriter extends WritableAdapter
{
    /**
     * {@inheritdoc}
     */
    public function write(string $what): Writable
    {
        echo $what;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function newLine(): Writable
    {
        echo PHP_EOL;

        return $this;
    }
}
