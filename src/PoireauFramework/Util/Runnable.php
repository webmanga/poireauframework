<?php

namespace PoireauFramework\Util;

/**
 * Runnable interface
 */
interface Runnable
{
    /**
     * Perform the action
     */
    public function run(): void;
}
