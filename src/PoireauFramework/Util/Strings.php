<?php

namespace PoireauFramework\Util;

/**
 * Utilities fir strings
 */
final class Strings
{
    /** @internal */
    private function __construct() {}

    /**
     * Check if the string starts with prefix
     */
    static public function startsWith(string $str, string $prefix): bool
    {
        $len = strlen($prefix);

        if (strlen($str) < $len) {
            return false;
        }

        return substr($str, 0, $len) === $prefix;
    }

    /**
     * Check if the string ends with suffix
     */
    static public function endsWith(string $str, string $suffix): bool
    {
        $len = strlen($suffix);

        if (strlen($str) < $len) {
            return false;
        }

        return substr($str, -$len) === $suffix;
    }
}
