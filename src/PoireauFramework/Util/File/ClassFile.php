<?php

namespace PoireauFramework\Util\File;

use PoireauFramework\Util\File\Exception\FileException;
use PoireauFramework\Util\File\Exception\ParseFileException;
use SplFileInfo;

/**
 * Get class informations on a file
 */
class ClassFile extends SplFileInfo
{
    const CLASS_INFO_NAMESPACE = 0;
    const CLASS_INFO_NAME = 1;
    const CLASS_INFO_TYPE = 2;

    const CLASS_TYPE_INTERFACE = 0;
    const CLASS_TYPE_ABSTRACT = 1;
    const CLASS_TYPE_CLASS = 2;
    const CLASS_TYPE_TRAIT = 3;

    /**
     * @var array
     */
    protected $classFileInfo;


    /**
     * @param string $filename The file to load
     */
    public function __construct(string $filename)
    {
        parent::__construct($filename);

        $this->classFileInfo = [];

        $this->loadClassData();
    }

    /**
     * Get the class info
     * @param int $info The self::CLASS_INFO_* constant
     * @return mixed The info value
     */
    public function getClassInfo(int $info)
    {
        return $this->classFileInfo[$info];
    }

    /**
     * Get the declared namespace of the file
     * @return string[]
     */
    public function getNamespace(): array
    {
        return $this->classFileInfo[self::CLASS_INFO_NAMESPACE] ?? [];
    }

    /**
     * Get the class name without namespace
     * @return string
     */
    public function getClassName(): string
    {
        return $this->classFileInfo[self::CLASS_INFO_NAME];
    }

    /**
     * Check if there is a class definition into $this file
     * @return bool
     */
    public function hasClassDefinition(): bool
    {
        return isset($this->classFileInfo[self::CLASS_INFO_NAME]);
    }

    /**
     * Get the fully qualified class name
     * @return string
     */
    public function getFullClassName(): string
    {
        $parts = $this->getNamespace();
        $parts[] = $this->getClassName();

        return implode("\\", $parts);
    }

    /**
     * Check if the class is abstract
     */
    public function isAbstract(): bool
    {
        return isset($this->classFileInfo[self::CLASS_INFO_TYPE]) && $this->classFileInfo[self::CLASS_INFO_TYPE] === self::CLASS_TYPE_ABSTRACT;
    }

    /**
     * Check if the class is an interface
     */
    public function isInterface(): bool
    {
        return isset($this->classFileInfo[self::CLASS_INFO_TYPE]) && $this->classFileInfo[self::CLASS_INFO_TYPE] === self::CLASS_TYPE_INTERFACE;
    }

    /**
     * Check if the file define a trait
     */
    public function isTrait(): bool
    {
        return isset($this->classFileInfo[self::CLASS_INFO_TYPE]) && $this->classFileInfo[self::CLASS_INFO_TYPE] === self::CLASS_TYPE_TRAIT;
    }

    /**
     * Check if the file define a class
     */
    public function isClass(): bool
    {
        return isset($this->classFileInfo[self::CLASS_INFO_TYPE]) && $this->classFileInfo[self::CLASS_INFO_TYPE] === self::CLASS_TYPE_CLASS;
    }

    /**
     * Check if the class is already loaded
     * @return bool true if the class, interface, trait is loaded, or false in other cases
     */
    public function isLoaded(): bool
    {
        if (!$this->hasClassDefinition()) {
            return false;
        }

        $className = $this->getFullClassName();

        switch ($this->classFileInfo[self::CLASS_INFO_TYPE]) {
            case self::CLASS_TYPE_CLASS:
            case self::CLASS_TYPE_ABSTRACT:
                return class_exists($className, false);
            case self::CLASS_TYPE_INTERFACE:
                return interface_exists($className, false);
            case self::CLASS_TYPE_TRAIT:
                return trait_exists($className, false);

        }

        return false;
    }

    /**
     * Load the class if not already loaded
     */
    public function load(): void
    {
        if ($this->isLoaded()) {
            return;
        }

        require $this->getRealPath();
    }

    /**
     * Load class data from file
     */
    public function loadClassData(): void
    {
        if (!$this->isFile()) {
            throw new FileException($this->getPathname(), "File not found");
        }

        $code = file_get_contents($this->getRealPath());
        $tokens = token_get_all($code);

        $pos = 0;
        $last = count($tokens);

        while ($pos < $last) {
            $token = $tokens[$pos];

            if (is_array($token)) {
                $pos = $this->parseOneToken($token[0], $token[1], $token[2], $pos, $tokens);
            } else {
                ++$pos;
            }
        }
    }

    protected function parseOneToken(int $type, string $value, int $line, int $pos, array $tokens): int
    {
        switch ($type) {
            case T_NAMESPACE:
                return $this->parseNamespaceToken($pos, $tokens);
            case T_ABSTRACT:
                return $this->parseAbstractToken($pos, $tokens);
            case T_INTERFACE:
            case T_TRAIT:
            case T_CLASS:
                return $this->parseClassToken($type, $pos, $tokens);
        }

        return $pos + 1;
    }

    protected function parseNamespaceToken(int $pos, array $tokens): int
    {
        if (isset($this->classFileInfo[self::CLASS_INFO_NAMESPACE])) {
            throw new ParseFileException($this->getPathname(), "Cannot handle more than one namespace declaration");
        }

        $ns = [];

        for(;;) {
            ++$pos;

            $token = $tokens[$pos];

            if ($token === ";") {
                break;
            }

            if (!is_array($token)) {
                throw new ParseFileException($this->getPathname(), "Unexpected token '$token' after namespace keyword");
            }

            if ($token[0] === T_STRING) {
                $ns[] = $token[1];
            }
        }

        $this->classFileInfo[self::CLASS_INFO_NAMESPACE] = $ns;

        return $pos + 1;
    }

    protected function parseClassToken(int $type, int $pos, array $tokens): int
    {
        $className = null;

        // Check for ::class magic constant
        if ($pos > 0) {
            $token = $tokens[$pos - 1];

            if (is_array($token) && $token[0] === T_PAAMAYIM_NEKUDOTAYIM) {
                return $pos + 1;
            }
        }

        if (isset($this->classFileInfo[self::CLASS_INFO_NAME])) {
            throw new ParseFileException($this->getPathname(), "Cannot handle more than one class definition per file");
        }

        for(;;) {
            ++$pos;

            $token = $tokens[$pos];

            if (!is_array($token)) {
                throw new ParseFileException($this->getPathname(), "Unexpected token '" . $token . "' after class keyword");
            }

            if ($token[0] === T_STRING) {
                $className = $token[1];
                break;
            }
        }

        switch ($type) {
            case T_INTERFACE:
                $this->classFileInfo[self::CLASS_INFO_TYPE] = self::CLASS_TYPE_INTERFACE;
                break;
            case T_ABSTRACT:
                $this->classFileInfo[self::CLASS_INFO_TYPE] = self::CLASS_TYPE_ABSTRACT;
                break;
            case T_CLASS:
                $this->classFileInfo[self::CLASS_INFO_TYPE] = self::CLASS_TYPE_CLASS;
                break;
            case T_TRAIT:
                $this->classFileInfo[self::CLASS_INFO_TYPE] = self::CLASS_TYPE_TRAIT;
                break;
        }

        $this->classFileInfo[self::CLASS_INFO_NAME] = $className;

        return $pos + 1;
    }

    protected function parseAbstractToken(int $pos, array $tokens): int
    {
        for(;;) {
            ++$pos;

            $token = $tokens[$pos];

            if (!is_array($token)) {
                throw new ParseFileException($this->getPathname(), "Unexpected token '" . $token . "' after abstract keyword");
            }

            switch ($token[0]) {
                case T_CLASS:
                    return $this->parseClassToken(T_ABSTRACT, $pos, $tokens);
                case T_COMMENT:
                case T_DOC_COMMENT:
                case T_WHITESPACE:
                    continue;
                default:
                    return $pos;
            }
        }

        return $pos + 1;
    }
}
