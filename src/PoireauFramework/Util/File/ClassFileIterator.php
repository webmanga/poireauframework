<?php

namespace PoireauFramework\Util\File;

use DirectoryIterator;
use Iterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Iterator for ClassFile
 */
class ClassFileIterator implements Iterator
{
    /**
     * @var Iterator
     */
    private $innerIterator;

    /**
     * @var ClassFile
     */
    private $current;


    /**
     * @param Iterator $innerIterator The internal iterator used
     */
    public function __construct(Iterator $innerIterator)
    {
        $this->innerIterator = $innerIterator;
    }

    /**
     * {@inheritdoc}
     */
    public function current(): ClassFile
    {
        return $this->current;
    }

    /**
     * {@inheritdoc}
     */
    public function key(): string
    {
        return $this->innerIterator->key();
    }

    /**
     * {@inheritdoc}
     */
    public function next(): void
    {
        $this->current = null;

        do {
            $this->innerIterator->next();
        } while ($this->innerIterator->valid() && !$this->validateCurrent());
    }

    /**
     * {@inheritdoc}
     */
    public function rewind(): void
    {
        $this->current = null;

        $this->innerIterator->rewind();

        if (!$this->validateCurrent()) {
            $this->next();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function valid(): bool
    {
        return $this->current !== null;
    }

    protected function validateCurrent(): bool
    {
        $current = $this->innerIterator->current();

        if (!$current->isFile()) {
            return false;
        }

        try {
            /** @var ClassFile $current */
            $current = $current->getFileInfo(ClassFile::class);
        } catch (\Exception $e) {
            return false;
        }

        if (!$current->hasClassDefinition()) {
            return false;
        }

        $this->current = $current;

        return true;
    }

    /**
     * Iterate on a directory
     *
     * @param string $path
     *
     * @return ClassFileIterator
     */
    static public function directory(string $path): ClassFileIterator
    {
        return new ClassFileIterator(new DirectoryIterator($path));
    }

    /**
     * Iterate on a directory recursively
     *
     * @param string $path
     *
     * @return ClassFileIterator
     */
    static public function recursive(string $path): ClassFileIterator
    {
        return new ClassFileIterator(
            new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($path)
            )
        );
    }
}
