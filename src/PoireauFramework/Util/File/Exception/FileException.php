<?php

namespace PoireauFramework\Util\File\Exception;

use Exception;
use RuntimeException;

/**
 * Base exception for File package
 */
class FileException extends RuntimeException
{
    /**
     * @var string
     */
    private $filename;


    /**
     * @param string $filename The error file name
     * @param string $message
     * @param Exception $previous
     */
    public function __construct(string $filename, string $message = "", Exception $previous = null)
    {
        parent::__construct("On file '$filename' : $message", 0, $previous);

        $this->filename = $filename;
    }

    /**
     * Get the error file name
     */
    public function filename(): string
    {
        return $this->filename;
    }
}
