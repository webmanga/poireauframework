<?php

namespace PoireauFramework\Util\File\Exception;

/**
 * Error for parsing files
 */
class ParseFileException extends FileException
{
    
}
