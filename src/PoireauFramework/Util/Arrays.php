<?php

namespace PoireauFramework\Util;

/**
 * Array utils
 */
final class Arrays
{
    /** @internal */
    private function __construct() {}

    /**
     * Check if the array is sequential (i.e. keys are 0, 1, 2...)
     *
     * @param array $a array to check
     *
     * @return bool
     */
    static public function isSequential(array $a): bool
    {
        return array_values($a) === $a;
    }
}
