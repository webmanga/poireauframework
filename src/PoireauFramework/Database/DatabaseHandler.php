<?php

namespace PoireauFramework\Database;

/**
 * Class DatabaseHandler
 */
class DatabaseHandler
{
    /**
     * @var ConnectionFactoryInterface
     */
    private $factory;

    /**
     * @var array
     */
    protected $connections = [];


    /**
     * Construct the handler
     * @param ConnectionFactoryInterface $factory
     */
    public function __construct(ConnectionFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Get database connection
     * @return ConnectionInterface
     */
    public function getConnection(string $name): ConnectionInterface
    {
        if (isset($this->connections[$name])) {
            return $this->connections[$name];
        }

        $connection = $this->factory->createConnection($name);

        $connection->connect();

        return $this->connections[$name] = $connection;
    }

    /**
     * Get the connection factory
     * @return ConnectionFactoryInterface
     */
    public function factory(): ConnectionFactoryInterface
    {
        return $this->factory;
    }
}
