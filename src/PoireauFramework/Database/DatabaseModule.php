<?php

namespace PoireauFramework\Database;

use PoireauFramework\Config\Config;
use PoireauFramework\Database\Mapping\Resolver\MapperResolver;
use PoireauFramework\Database\Mapping\Resolver\MapperResolverInterface;
use PoireauFramework\Database\Mapping\Resolver\SqlMapperFactory;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\SimpleListType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Pdo\PdoDriver;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformRegistry;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Module for database
 *
 * Registry configuration :
 * - database.default_connection : string The default connection to use for mappers (default : "default")
 *
 * Config :
 * - database.[connection].driver : string The database driver ("pdo")
 * @see \PoireauFramework\Database\Sql\Pdo\PdoDriver
 */
class DatabaseModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(ConnectionFactoryInterface::class, ConfigConnectionFactory::class);
        $injector->impl(MapperResolverInterface::class, MapperResolver::class);

        $injector->factory(ConfigConnectionFactory::class, [$this, "createConnectionFactory"]);
        $injector->factory(DatabaseHandler::class, [$this, "createDatabaseHandler"]);
        $injector->factory(SqlPlatformRegistry::class, [$this, "createSqlPlatformRegistry"]);
        $injector->factory(TypeRegistry::class, [$this, "createTypeRegistry"]);
        $injector->factory(MapperResolver::class, [$this, "createMapperResolver"]);
        $injector->factory(SqlMapperFactory::class, [$this, "createSqlMapperFactory"]);
    }

    public function createConnectionFactory(InjectorInterface $injector): ConfigConnectionFactory
    {
        return new ConfigConnectionFactory(
            $injector->get(Config::class)->item("database"),
            $this->getConnectionDrivers($injector)
        );
    }

    public function createDatabaseHandler(InjectorInterface $injector): DatabaseHandler
    {
        return new DatabaseHandler($injector->get(ConnectionFactoryInterface::class));
    }

    public function createSqlPlatformRegistry(InjectorInterface $injector): SqlPlatformRegistry
    {
        $registry = new SqlPlatformRegistry($injector->get(TypeRegistry::class));

        $registry->set("sqlite", SQLitePlatform::class);
        $registry->set("mysql",  MySQLPlatform::class);

        return $registry;
    }

    public function createTypeRegistry(): TypeRegistry
    {
        $registry = new TypeRegistry();

        $registry->add(new StringType());
        $registry->add(new IntegerType());
        $registry->add(new SimpleListType());

        return $registry;
    }

    public function getConnectionDrivers(InjectorInterface $injector): array
    {
        $platforms = $injector->get(SqlPlatformRegistry::class);

        return [
            new PdoDriver($platforms)
        ];
    }

    public function createMapperResolver(InjectorInterface $injector): MapperResolver
    {
        return new MapperResolver(
            [
                $injector->get(SqlMapperFactory::class)
            ],
            $injector->reg("database")->has("default_connection") ? $injector->reg("database")->get("default_connection") : "default"
        );
    }

    public function createSqlMapperFactory(InjectorInterface $injector): SqlMapperFactory
    {
        return new SqlMapperFactory($injector->get(DatabaseHandler::class));
    }
}
