<?php

namespace PoireauFramework\Database\Mapping\Hydration;

use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Hydrate entities using metadata
 */
class MetadataHydrator implements HydratorInterface
{
    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var TypeRegistry
     */
    private $types;


    /**
     *
     */
    public function __construct(Metadata $metadata, TypeRegistry $types)
    {
        $this->metadata = $metadata;
        $this->types = $types;
    }

    /**
     * {@inheritdoc}
     *
     * @todo attribute injector strategy
     */
    public function fromDatabase($value)
    {
        $entityClass = $this->metadata->entity;

        $entity = new $entityClass;

        foreach ($value as $attribute => $dbValue) {
            if (!isset($this->metadata->attributes[$attribute])) {
                continue;
            }

            $field = $this->metadata->attributes[$attribute];

            $fieldName = $field->name;

            if ($entity instanceof \stdClass) {
                $entity->$fieldName = $dbValue;
            } elseif ($dbValue === null) {
                $entity->{'set'.$fieldName}(null);
            } else {
                $entity->{'set'.$fieldName}($this->types->get($field->type)->fromDatabase($dbValue));
            }
        }

        return $entity;
    }

    /**
     * {@inheritdoc}
     */
    public function toDatabase($entity)
    {
        $attributes = [];

        foreach ($this->metadata->fields as $fieldName => $field) {
            $value = $entity->$fieldName();

            if ($value === null) {
                $attributes[$field->attribute] = null;
            } else {
                $attributes[$field->attribute] = $this->types->get($field->type)->toDatabase($value);
            }
        }

        return $attributes;
    }
}
