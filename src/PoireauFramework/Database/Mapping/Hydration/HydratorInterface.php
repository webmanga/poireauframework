<?php

namespace PoireauFramework\Database\Mapping\Hydration;

/**
 * Interface for hydrate database entities
 */
interface HydratorInterface
{
    /**
     * Transform database value to PHP entity
     */
    public function fromDatabase($value);

    /**
     * Transform PHP entity to database value
     */
    public function toDatabase($entity);
}
