<?php

namespace PoireauFramework\Database\Mapping;

use PoireauFramework\Database\Mapping\Helper\SqlHelper;
use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Connection\Sql\SqlQueryConnection;
use PoireauFramework\Database\Mapping\Query\QueryFactory;
use PoireauFramework\Database\Mapping\Query\QueryFactoryInterface;
use PoireauFramework\Database\Mapping\Schema\SchemaInterface;
use PoireauFramework\Database\Mapping\Schema\SqlSchema;
use PoireauFramework\Database\Sql\SqlConnectionInterface;

/**
 * Mapper for SQL database
 */
abstract class SqlMapper implements MapperInterface
{
    /**
     * @var \PoireauFramework\Database\Sql\SqlConnectionInterface
     */
    protected $db;

    /**
     * @var \PoireauFramework\Database\Mapping\Metadata\Metadata
     */
    protected $metadata;

    /**
     * @var \PoireauFramework\Database\Mapping\Helper\SqlHelper
     */
    protected $helper;

    /**
     * @var \PoireauFramework\Database\Mapping\Hydration\HydratorInterface
     */
    protected $hydrator;

    /**
     * @var \PoireauFramework\Database\Mapping\Query\QueryFactoryInterface
     */
    protected $queries;

    /**
     * @var \PoireauFramework\Database\Mapping\Query\Connection\QueryConnectionInterface
     */
    protected $connection;


    /**
     *
     */
    public function __construct(SqlConnectionInterface $db, Metadata $metadata, HydratorInterface $hydrator)
    {
        $this->db = $db;
        $this->metadata = $metadata;
        $this->hydrator = $hydrator;
        $this->helper = new SqlHelper($this->metadata);

        $this->buildMetadata();

        $this->queries = new QueryFactory($this->metadata, $this->hydrator, $this->db->platform());
        $this->connection = new SqlQueryConnection($this->db);
    }

    /**
     * {@inheritdoc}
     */
    public function schema(): SchemaInterface
    {
        return new SqlSchema($this->metadata, $this->db);
    }

    /**
     * {@inheritdoc}
     */
    public function entity(): string
    {
        return $this->metadata->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function create($entity)
    {
        $query = $this->query()->insert();

        $this->connection
            ->prepare($query)
            ->bind($entity)
            ->execute()
        ;

        $dbEntity = clone $entity;

        foreach ($this->metadata->generated as $field) {
            $fieldName = $field->name;

            if ($dbEntity->$fieldName() !== null) {
                continue;
            }

            //TODO: faire mieux
            $dbEntity->{'set'.$fieldName}($this->db->lastInsertedId());
        }

        return $dbEntity;
    }

    /**
     * Configure the metadata (i.e. entity class and relation name)
     */
    abstract protected function configure(Metadata $metadata): void;

    /**
     * Build fields for mapper
     */
    abstract protected function buildFields(FieldsBuilder $builder): void;

    /**
     * Get the table name
     */
    protected function table(): string
    {
        return $this->helper->table();
    }

    /**
     * Get the SQL attribute
     */
    protected function attr(string $field, $table = null): string
    {
        return $this->helper->attr($field, $table);
    }

    /**
     * Get the query handler
     */
    protected function query(): QueryFactoryInterface
    {
        return $this->queries;
    }

    /**
     * Build the metadata
     */
    protected function buildMetadata(): void {
        if ($this->metadata->built) {
            return;
        }

        $this->configure($this->metadata);

        $builder = new FieldsBuilder($this->metadata);

        $this->buildFields($builder);
        $this->metadata = $builder->build();
    }
}
