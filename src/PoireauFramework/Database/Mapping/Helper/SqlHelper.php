<?php

namespace PoireauFramework\Database\Mapping\Helper;

use PoireauFramework\Database\Mapping\Metadata\Metadata;

/**
 * Helper for SQL queries
 *
 * @deprecated
 */
class SqlHelper
{
    /**
     * @var Metadata
     */
    protected $metadata;


    /**
     *
     */
    public function __construct(Metadata $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * Escape the identifier
     */
    public function identifier(string $identifier): string
    {
        return "`" . str_replace("`", "``", $identifier) . "`";
    }

    /**
     * Get the table name (for SQL query)
     */
    public function table(): string
    {
        return $this->identifier($this->metadata->schema);
    }

    /**
     * Get the SQL attribute
     */
    public function attr(string $field, $table = null): string
    {
        $attr = $this->identifier($this->metadata->fields[$field]->attribute);

        if ($table !== null) {
            return $this->identifier($table) . "." . $attr;
        }

        return $attr;
    }

    /**
     * Get the INSERT query
     */
    public function insert(): string
    {
        $cols = "";
        $vals = "";

        $first = true;

        foreach ($this->metadata->attributes as $attr => $_) {
            if ($first) {
                $first = false;
            } else {
                $cols .= ", ";
                $vals .= ", ";
            }

            $cols .= $this->identifier($attr);
            $vals .= ":" . $attr;
        }

         return "INSERT INTO {$this->table()}({$cols}) VALUES({$vals})";
    }
}
