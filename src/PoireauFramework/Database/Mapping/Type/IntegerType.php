<?php

namespace PoireauFramework\Database\Mapping\Type;

use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * Type for short string values
 */
class IntegerType implements TypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "integer";
    }

    /**
     * {@inheritdoc}
     */
    public function fromDatabase($value)
    {
        if ($value === null) {
            return null;
        }

        return (int) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toDatabase($value)
    {
        if ($value === null) {
            return null;
        }

        return (int) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function sqlDeclaration(Field $field): string
    {
        return "INTEGER";
    }
}
