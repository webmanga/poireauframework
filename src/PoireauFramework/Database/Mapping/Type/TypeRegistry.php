<?php

namespace PoireauFramework\Database\Mapping\Type;

/**
 * Registry of database types
 */
class TypeRegistry
{
    /**
     * @var TypeInterface[]
     */
    private $types = [];

    /**
     * Register a type that will be lazy created
     */
    public function lazy(string $name, string $className): void
    {
        $this->types[$name] = $className;
    }

    /**
     * Register a type
     */
    public function add(TypeInterface $type): void
    {
        $this->types[$type->name()] = $type;
    }

    /**
     * Get a registered type
     */
    public function get(string $name): TypeInterface
    {
        if (!isset($this->types[$name])) {
            throw new \LogicException("Invalid database type '{$name}'. Please register the type before use it with TypeRegistry->add()");
        }

        $type = $this->types[$name];

        if (is_string($type)) {
            $type = new $type();
            $this->types[$name] = $type;
        }

        return $type;
    }
}
