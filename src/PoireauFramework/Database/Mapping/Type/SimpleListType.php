<?php

namespace PoireauFramework\Database\Mapping\Type;

use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * Type simple CSV list of strings :
 * "foo,,,bar," => ["foo", "bar"]
 * ["foo", "bar"] => "foo,bar"
 */
class SimpleListType implements TypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "list";
    }

    /**
     * {@inheritdoc}
     */
    public function fromDatabase($value)
    {
        if ($value === null) {
            return null;
        }

        return array_values(array_filter(explode(",", $value)));
    }

    /**
     * {@inheritdoc}
     */
    public function toDatabase($value)
    {
        if ($value === null) {
            return null;
        }

        return implode(",", $value);
    }

    /**
     * {@inheritdoc}
     */
    public function sqlDeclaration(Field $field): string
    {
        return "TEXT";
    }
}
