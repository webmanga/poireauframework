<?php

namespace PoireauFramework\Database\Mapping\Type;

use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * Interface for SQL database types
 */
interface TypeInterface
{
    /**
     * Get the PHP mapper type name
     */
    public function name(): string;

    /**
     * Transform the value from database to PHP
     */
    public function fromDatabase($value);

    /**
     * Transform the value to database, from php
     */
    public function toDatabase($value);

    /**
     * Get the SQL attribute declaration
     */
    public function sqlDeclaration(Field $field): string;
}
