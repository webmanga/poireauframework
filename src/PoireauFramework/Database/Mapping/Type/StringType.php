<?php

namespace PoireauFramework\Database\Mapping\Type;

use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * Type for short string values
 */
class StringType implements TypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "string";
    }

    /**
     * {@inheritdoc}
     */
    public function fromDatabase($value)
    {
        if ($value === null) {
            return null;
        }

        return (string) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toDatabase($value)
    {
        if ($value === null) {
            return null;
        }

        return (string) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function sqlDeclaration(Field $field): string
    {
        $declaration = "VARCHAR";

        if ($field->length !== null) {
            $declaration .= "(".$field->length.")";
        }

        return $declaration;
    }
}
