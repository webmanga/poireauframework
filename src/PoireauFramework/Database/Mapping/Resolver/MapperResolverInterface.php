<?php

namespace PoireauFramework\Database\Mapping\Resolver;

use PoireauFramework\Database\Mapping\MapperInterface;

/**
 * Resolve mapper instance
 */
interface MapperResolverInterface
{
    /**
     * Get a mapper by its class name
     */
    public function get(string $className): MapperInterface;

    /**
     * Get a mapper by its handled entity class
     */
    public function getByEntity(string $entity): MapperInterface;
}
