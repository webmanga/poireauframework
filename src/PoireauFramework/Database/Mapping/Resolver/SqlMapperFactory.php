<?php

namespace PoireauFramework\Database\Mapping\Resolver;

use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\MapperInterface;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\SqlMapper;

/**
 * Factory for SQL mappers
 */
class SqlMapperFactory implements MapperFactoryInterface
{
    /**
     * @var DatabaseHandler
     */
    private $dbHandler;


    /**
     *
     */
    public function __construct(DatabaseHandler $dbHandler)
    {
        $this->dbHandler = $dbHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(string $mapperClass): bool
    {
        return is_subclass_of($mapperClass, SqlMapper::class, true);
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $mapperClass, ConfigStruct $config): MapperInterface
    {
        $connection = $this->dbHandler->getConnection($config->connection);
        $metadata = new Metadata();

        return new $mapperClass(
            $connection,
            $metadata,
            new MetadataHydrator($metadata, $connection->platform()->types())
        );
    }
}
