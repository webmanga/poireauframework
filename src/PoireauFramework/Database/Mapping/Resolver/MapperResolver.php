<?php

namespace PoireauFramework\Database\Mapping\Resolver;

use PoireauFramework\Database\Mapping\MapperInterface;
use PoireauFramework\Database\Mapping\SqlMapper;

/**
 * Resolve mapper instance
 */
class MapperResolver implements MapperResolverInterface
{
    /**
     * @var MapperInterface[]
     */
    private $mappers = [];

    /**
     * @var MapperInterface[]
     */
    private $mappersByEntity = [];

    /**
     * @var MapperFactoryInterface[]
     */
    private $factories;

    /**
     * @var string
     */
    private $defaultConnection;


    /**
     *
     */
    public function __construct(array $factories, string $defaultConnection = "default")
    {
        $this->factories = $factories;
        $this->defaultConnection = $defaultConnection;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $className): MapperInterface
    {
        if (isset($this->mappers[$className])) {
            return $this->mappers[$className];
        }

        foreach ($this->factories as $factory) {
            if ($factory->supports($className)) {
                $mapper = $factory->create($className, $this->configure($className));


                $this->mappers[$className] = $mapper;
                $this->mappersByEntity[$mapper->entity()] = $mapper;

                return $mapper;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getByEntity(string $entity): MapperInterface
    {
        return $this->mappersByEntity[$entity];
    }

    protected function configure(string $mapperClass): ConfigStruct
    {
        $config = new ConfigStruct;

        $config->connection = $this->defaultConnection;

        return $config;
    }
}
