<?php

namespace PoireauFramework\Database\Mapping\Resolver;

use PoireauFramework\Database\Mapping\MapperInterface;

/**
 * Make the mapper instance
 */
interface MapperFactoryInterface
{
    /**
     * Check if the factory supports the mapper
     */
    public function supports(string $mapperClass): bool;

    /**
     * Create the mapper instance
     */
    public function create(string $mapperClass, ConfigStruct $config): MapperInterface;
}
