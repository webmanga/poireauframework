<?php

namespace PoireauFramework\Database\Mapping\Resolver;

/**
 * Structure for mapper configuration
 */
class ConfigStruct
{
    /**
     * @var string
     */
    public $connection;

    /**
     * @var string
     */
    public $entity;
}
