<?php

namespace PoireauFramework\Database\Mapping\Query;

use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Implementation of QueryFactoryInterface
 */
class QueryFactory implements QueryFactoryInterface
{
    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var Compiled[]
     */
    private $queries = [];


    /**
     *
     */
    public function __construct(Metadata $metadata, HydratorInterface $hydrator, SqlPlatformInterface $platform)
    {
        $this->metadata = $metadata;
        $this->hydrator = $hydrator;
        $this->platform = $platform;
    }

    /**
     * {@inheritdoc}
     */
    public function select($name = null, $builder = null): Compiled
    {
        if ($builder === null) {
            if ($name === null) {
                $name = "select_all";
            } elseif (!is_string($name)) {
                $builder = $name;
                $name = null;
            }
        }

        if ($name !== null && isset($this->queries[$name])) {
            return $this->queries[$name];
        }

        $select = new Select($this->metadata, $this->hydrator);

        if (is_array($builder)) {
            $select->where($builder);
        } elseif ($builder !== null) {
            $builder($select);
        }

        $query = $select->compile($this->platform->compilers()->select());

        if ($name !== null) {
            $this->queries[$name] = $query;
        }

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function insert($name = null, $builder = null): Compiled
    {
        if ($builder === null) {
            if ($name === null) {
                $name = "insert_default";
            } elseif (!is_string($name)) {
                $builder = $name;
                $name = null;
            }
        }

        if ($name !== null && isset($this->queries[$name])) {
            return $this->queries[$name];
        }

        $insert = new Insert($this->metadata, $this->hydrator);

        if ($builder !== null) {
            $builder($insert);
        }

        $query = $insert->compile($this->platform->compilers()->insert());

        if ($name !== null) {
            $this->queries[$name] = $query;
        }

        return $query;
    }
}
