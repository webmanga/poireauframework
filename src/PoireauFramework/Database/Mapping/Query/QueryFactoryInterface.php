<?php

namespace PoireauFramework\Database\Mapping\Query;

/**
 * Make queries instance
 */
interface QueryFactoryInterface
{
    /**
     * Create a select query
     * <code>
     * $queries->select("login", function (Select $query) {
     *     $query->where([
     *         [":or" => [
     *             "username" => new Binding(),
     *             "email"    => new Binding()
     *         ]],
     *         "password" => new Binding()
     *     ]);
     * });
     * // Or
     * $queries->select("login", [
     *     [":or" => [
     *         "username" => new Binding(),
     *         "email"    => new Binding()
     *     ]],
     *     "password" => new Binding()
     * ]);
     * </code>
     *
     * @param string|callable|array name The query name (for cache purpose), or the builder callback (see parameter 2) on anonymous query
     * @param null|callable|array <p>
     *     builder The builder function.
     *     Take the Query instance as parameter, and should return void.
     *     If the query is anonymous, this parameter should be passed as first.
     *     This parameter can be an array criteria
     * </p>
     *
     * @return Compiled The compiled select query
     */
    public function select($name = null, $builder = null): Compiled;

    /**
     * Create an insert query
     * <code>
     * $queries->insert(function (Insert $query) use($values) {
     *     $query->bulk();
     *
     *     foreach ($values as $value) {
     *         $query->values($value);
     *     }
     * });
     * </code>
     *
     * @param string|callable name The query name (for cache purpose), or the builder callback (see parameter 2) on anonymous query
     * @param null|callable builder The builder function. Take the Query instance as parameter, and should return void. If the query is anonymous, this parameter should be passed as first
     *
     * @return Compiled The compiled insert query
     */
    public function insert($name = null, $builder = null): Compiled;
}
