<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler;

use PoireauFramework\Database\Mapping\Query\Bag\Criteria;

/**
 * Interface for compile criteria
 */
interface CriteriaCompilerInterface
{
    /**
     * Compile the criteria
     * @return array as [ "criteria" => [compiled criteria], "parameters" => [bindings] ]
     */
    public function compile(Criteria $criteria): array;
}
