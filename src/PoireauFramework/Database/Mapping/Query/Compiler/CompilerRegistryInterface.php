<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler;

/**
 * Registry of compilers
 */
interface CompilerRegistryInterface
{
    /**
     * Get the insert compiler
     */
    public function insert(): InsertCompilerInterface;

    /**
     * Get the select compiler
     */
    public function select(): SelectCompilerInterface;

    /**
     * Get the criteria compiler
     */
    public function criteria(): CriteriaCompilerInterface;
}
