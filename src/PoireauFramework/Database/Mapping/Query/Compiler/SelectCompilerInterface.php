<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler;

use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Select;

/**
 * Interface for compile Select
 */
interface SelectCompilerInterface
{
    /**
     * Compile the Select query
     * @return Compiled The compiled query
     */
    public function compileSelect(Select $select): Compiled;
}
