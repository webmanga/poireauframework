<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler;

use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Insert;

/**
 * Interface for compile Insert
 */
interface InsertCompilerInterface
{
    /**
     * Compile the Insert query
     * @return Compiled The compiled query
     */
    public function compileInsert(Insert $insert): Compiled;
}
