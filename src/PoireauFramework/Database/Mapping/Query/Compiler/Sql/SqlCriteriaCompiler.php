<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;

use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\Query\Compiler\CriteriaCompilerInterface;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Compile criteria to SQL
 */
class SqlCriteriaCompiler implements CriteriaCompilerInterface
{
    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var Metadata
     */
    private $metadata;


    /**
     *
     */
    public function __construct(SqlPlatformInterface $platform)
    {
        $this->platform = $platform;
    }

    /**
     * @todo à compléter
     */
    public function getDatabaseAttribute(string $field): string
    {
        if (!isset($this->metadata->fields[$field])) {
            return $field;
        }

        return $this->platform->identifier([$this->metadata->fields[$field]->attribute]);
    }

    /**
     * {@inheritdoc}
     */
    public function compile(Criteria $criteria): array
    {
        $this->metadata = $criteria->metadata();

        return $this->buildCriteria(
            $criteria->criteria(),
            $criteria->separator()
        );
    }

    /**
     * Build a criteria
     * <pre><code>
     * $criteria = [
     *     'column' => $value,
     *     ['column2', '!=', $value],
     *     ':or' => [
     *          'column3' => 5
     *     ],
     *     ':query' => [
     *          'EXISTS(SELECT * FROM USERS)',
     *          '1 IN (SELECT USER_ID FROM USER WHERE USER_NAME = ?)' => [$userName]
     *     ]
     * ];
     * </code></pre>
     * @param array $criteria
     * @param string $separator
     * @return array An array with SQL query on "criteria" index, and the parameters on "parameters" index
     */
    public function buildCriteria(array $criteria, string $separator = "AND"): array
    {
        $queryCriterias = [];
        $parameters = [];

        foreach ($criteria  as $key => $value) {
            if (is_int($key)) {
                $condition = $this->buildCriteriaLine($value);
            } elseif ($key{0} === ':') {
                $condition = $this->buildPseudoFunctionCondition($key, $value, $separator);
            } else {
                $condition = $this->buildCondition($key, "=", $value);
            }
            
            $queryCriterias[] = $condition["criteria"];

            if (isset($condition["parameters"])) {
                $parameters = array_merge($parameters, $condition["parameters"]);
            }
        }
        
        return [
            "criteria"   => implode(" " . $separator . " ", $queryCriterias),
            "parameters" => $parameters
        ];
    }

    /**
     * Build a criteria group (i.e. used to group criteria, on :or and :and conditions)
     *
     * @param array values The criteria line
     * @return array
     */
    protected function buildCriteriaLine(array $values)
    {
        if (count($values) === 3 && isset($values[0])) {
            return $this->buildCondition(...$values);
        }

        $condition = $this->buildCriteria($values);

        $condition["criteria"] = "(" . $condition["criteria"] . ")";

        return $condition;
    }

    /**
     * Build a simple condition
     *
     * @param string $column
     * @param string $operator
     * @param mixed $value
     * @return array
     * @throws SqlException
     */
    protected function buildCondition(string $column, string $operator, $value)
    {
        if (is_array($value)) {
            return $this->buildConditionWithArray($column, $operator, $value);
        }

        if ($value === null) {
            return $this->buildNullCondition($column, $operator);
        }

        if ($value instanceof Binding) {
            return $this->buildBindingCondition($column, $operator, $value);
        }

        if (is_scalar($value)) {
            return $this->buildDefaultCondition($column, $operator, $value);
        }

        throw new SqlException("", "Cannot build condition for {$column} {$operator} with " . gettype($value) . " as value");
    }

    /**
     * Build a condition on array
     *
     * @param string $column
     * @param string $operator
     * @param array $value
     * @return array
     * @throws SqlException
     */
    protected function buildConditionWithArray(string $column, string $operator, array $value)
    {
        $operator = strtoupper($operator);

        switch ($operator) {
            case "IN":
            case "NOT IN":
                return $this->buildInCondition($column, $operator, $value);
            case "BETWEEN":
            case "NOT BETWEEN":
                return $this->buildBetweenCondition($column, $operator, $value);
            default:
                throw new SqlException("", "Invalid operator {$operator} for arrays");
        }
    }

    /**
     * Build a IN or NOT IN condition
     *
     * @param string $column
     * @param string $operator IN, or NOT IN
     * @param array $values
     * @return array
     */
    protected function buildInCondition(string $column, string $operator, array $values)
    {
        $column = $this->getDatabaseAttribute($column);

        $quotedValues = "";

        foreach ($values as $index => $value) {
            if ($index > 0) {
                $quotedValues .= ", ";
            }

            $quotedValues .= $this->platform->quote($value);
        }

        return [
            "criteria" => $column . " " . $operator . " (" . $quotedValues . ")"
        ];
    }

    /**
     * Build a BETWEEN or NOT BETWEEN condition
     *
     * @param string $column
     * @param string $operator BETWEEN, or NOT BETWEEN
     * @param array $values
     * @return array
     * @throws SqlException
     */
    protected function buildBetweenCondition(string $column, string $operator, array $values)
    {
        $column = $this->getDatabaseAttribute($column);

        if (count($values) !== 2) {
            throw new SqlException("", "The operator {$operator} needs exactly 2 factors, " . count($values) . " given, for column {$column}");
        }

        return [
            "criteria"   => $column . " " . $operator . " ? AND ?",
            "parameters" => $values
        ];
    }

    /**
     * Build a IS NULL, or IS NOT NULL condition
     *
     * @param string $column
     * @param string $operator
     * @return array
     * @throws SqlException
     */
    protected function buildNullCondition(string $column, string $operator)
    {
        $criteria = $this->getDatabaseAttribute($column);

        switch ($operator) {
            case "=":
                $criteria .= " IS NULL";
                break;
            case "!=":
            case "<>":
                $criteria .= " IS NOT NULL";
                break;
            default:
                throw new SqlException("", "Invalid operator '{$operator}' for NULL value on column {$column}");
        }

        return [
            "criteria" => $criteria
        ];
    }

    /**
     *
     * @param string $column
     * @param string $operator
     * @param string $value
     * @return array
     * @throws SqlException
     */
    protected function buildDefaultCondition(string $column, string $operator, $value)
    {
        $column = $this->getDatabaseAttribute($column);

        if (strtolower($operator) === ":query") {
            if (!is_string($value)) {
                throw new SqlException("", "The operator :query needs a SQL Query as value, " . gettype($value) . " given, for column " . $column);
            }

            return [
                "criteria" => $column . " " . $value
            ];
        }

        return [
            "criteria"   => $column . " " . $operator . " ?",
            "parameters" => [$value]
        ];
    }

    /**
     *
     * @param string $field
     * @param string $operator
     * @param string $value
     * @return array
     * @throws SqlException
     */
    protected function buildBindingCondition(string $field, string $operator, Binding $value)
    {
        $column = $this->getDatabaseAttribute($field);

        if ($value->field === null) {
            $value->field = $field;
            $value->type  =  $this->metadata->fields[$field]->type;
        }

        return [
            "criteria"   => $column . " " . $operator . " ?",
            "parameters" => [$value]
        ];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param string $separator
     * @return array
     */
    protected function buildPseudoFunctionCondition(string $key, $value, string $separator)
    {
        $key = strtolower($key);

        switch ($key) {
            case ":query":
                return $this->buildSubQueryCondition(is_array($value) ? $value : [$value], $separator);
            case ":and":
            case ":or":
                $newSeparator = strtoupper(substr($key, 1));
                $subCondition = $this->buildCriteria((array) $value, $newSeparator);

                if ($newSeparator === $separator) {
                    return $subCondition;
                }

                $subCondition["criteria"] = "(" . $subCondition["criteria"] . ")";

                return $subCondition;
            default:
                throw new SqlException("", "Invalid pseudo function {$key}");
        }
    }

    protected function buildSubQueryCondition(array $value, string $separator)
    {
        $criteria   = [];
        $parameters = [];

        foreach ($value as $key => $query) {
            if (is_int($key) && is_string($query)) {
                $criteria[] = $query;
            } elseif (is_string($key) && is_array($query)) {
                $criteria[] = $key;
                $parameters = array_merge($parameters, $query);
            } else {
                throw new SqlException("", "Bad sub query criteria.");
            }
        }

        return [
            "criteria"   => implode(" " . $separator . " ", $criteria),
            "parameters" => $parameters
        ];
    }
}
