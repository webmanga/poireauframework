<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;

use PoireauFramework\Database\Mapping\Query\Compiler\CriteriaCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\CompilerRegistryInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\InsertCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\SelectCompilerInterface;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Compiler registry for SQL
 */
final class SqlCompilerRegistry implements CompilerRegistryInterface
{
    /**
     * SqlPlatformInterface
     */
    private $platform;

    private $insert;
    private $select;
    private $criteria;

    /**
     *
     */
    public function __construct(SqlPlatformInterface $platform)
    {
        $this->platform = $platform;
        $this->criteria = new SqlCriteriaCompiler($platform);
        $this->insert   = new SqlInsertCompiler($platform);
        $this->select   = new SqlSelectCompiler($platform, $this->criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function insert(): InsertCompilerInterface
    {
        return $this->insert;
    }

    /**
     * {@inheritdoc}
     */
    public function select(): SelectCompilerInterface
    {
        return $this->select;
    }

    /**
     * {@inheritdoc}
     */
    public function criteria(): CriteriaCompilerInterface
    {
        return $this->criteria;
    }
}
