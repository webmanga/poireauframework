<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;

use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainer;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainerInterface;
use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Compiler\InsertCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Exception\QueryException;
use PoireauFramework\Database\Mapping\Query\Insert;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Interface for compile Insert
 */
class SqlInsertCompiler implements InsertCompilerInterface
{
    /**
     * @var SqlPlatformInterface
     */
    private $platform;


    /**
     *
     */
    public function __construct(SqlPlatformInterface $platform)
    {
        $this->platform = $platform;
    }

    /**
     * {@inheritdoc}
     */
    public function compileInsert(Insert $insert): Compiled
    {
        $attributes = $this->extractAttributes($insert);

        if (empty($attributes)) {
            throw new QueryException("Cannot extract non-empty list of attributes. Please register attributes on Metadata, or set an attributes list with Insert->attributes([...])");
        }

        $sql = $this->compileType($insert) . " ";

        $sql .= $this->compileTable($insert) . $this->compileAttributes($insert, $attributes) . " ";
        $sql .= $this->compileValues($insert, $attributes);

        return new Compiled($sql, $this->compileBindings($insert, $attributes));
    }

    /**
     *
     */
    protected function compileType(Insert $insert): string
    {
        switch ($insert->getType()) {
            case Insert::TYPE_DEFAULT:
                return "INSERT INTO";
            case Insert::TYPE_IGNORE:
                return "INSERT IGNORE INTO";
            case Insert::TYPE_REPLACE:
                return "REPLACE INTO";
        }

        throw new QueryException("Unsupported Insert type {$insert->getType()}");
    }

    /**
     *
     */
    protected function extractAttributes(Insert $insert): array
    {
        if (!$insert->getAttributes()) {
            return array_keys($insert->getMetadata()->attributes);
        }

        $fields = $insert->getMetadata()->fields;
        $attrs = [];

        foreach ($insert->getAttributes() as $prop) {
            if (isset($fields[$prop])) {
                $attrs[] = $fields[$prop]->attribute;
            } else {
                $attrs[] = $prop;
            }
        }

        return $attrs;
    }

    /**
     *
     */
    protected function compileAttributes(Insert $insert, array $attributes): string
    {
        $list = "(";
        $first = true;

        foreach ($attributes as $attr) {
            if ($first) {
                $first = false;
            } else {
                $list .= ", ";
            }

            $list .= $this->platform->identifier([$attr]);
        }

        $list .= ")";

        return $list;
    }

    /**
     *
     */
    protected function compileTable(Insert $insert): string
    {
        return $this->platform->identifier([$insert->getMetadata()->schema]);
    }

    /**
     *
     */
    protected function compileValues(Insert $insert, array $attributes): string
    {
        return "VALUES " . ($insert->isBulk()
            ? $this->compileBulkValues($insert, $attributes)
            : $this->compilePreparedValue($insert, $attributes)
        );
    }

    /**
     *
     */
    protected function compileBulkValues(Insert $insert, array $attributes): string
    {
        $values = [];

        foreach ($insert->getValues() as $data) {
            $list = "";

            $dbData = $insert->getHydrator()->toDatabase($data);

            foreach ($attributes as $attr) {
                if (!empty($list)) {
                    $list .= ", ";
                }

                $value = $dbData[$attr] ?? null;

                if ($value === null) {
                    $list .= "NULL";
                } else {
                    $list .= $this->platform->quote($value);
                }
            }

            $values[] = "({$list})";

        }

        if (empty($values)) {
            throw new QueryException("No values given on bulk insert query. Set (at least) a value with Insert->values(), or disable bulk with Insert->bulk(false)");
        }

        return implode(" ", $values);
    }

    /**
     *
     */
    protected function compilePreparedValue(Insert $insert, array $attributes): string
    {
        return "(?" . str_repeat(", ?", count($attributes) - 1) . ")";
    }

    /**
     *
     */
    protected function compileBindings(Insert $insert, array $attributes): BindingContainerInterface
    {
        if ($insert->isBulk()) {
            return new BindingContainer([]);
        }

        $meta = $insert->getMetadata()->attributes;

        $bindings = [];

        foreach ($attributes as $attr) {
            if (isset($meta[$attr])) {
                $field = $meta[$attr];
                $bindings[] = new Binding($field->name, $field->type);
            } else {
                $bindings[] = new Binding($attr);
            }
        }

        $container = new BindingContainer($bindings);

        if (isset($insert->getValues()[0])) {
            $container->bind($insert->getValues()[0]);
        }

        return $container;
    }
}
