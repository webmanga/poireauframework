<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;

use PoireauFramework\Database\Mapping\Query\Bag\BindingContainerAggregate;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Compiler\CriteriaCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\SelectCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Select;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Compiler for SQL SELECT queries
 */
class SqlSelectCompiler implements SelectCompilerInterface
{
    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var CriteriaCompilerInterface
     */
    private $criteriaCompiler;


    /**
     *
     */
    public function __construct(SqlPlatformInterface $platform, CriteriaCompilerInterface $criteriaCompiler)
    {
        $this->platform = $platform;
        $this->criteriaCompiler = $criteriaCompiler;
    }

    /**
     * {@inheritdoc}
     */
    public function compileSelect(Select $select): Compiled
    {
        $criterias = [];
        $sql = "SELECT " . $this->compileAttributes($select) . $this->compileFrom($select);

        $where = $this->compileWhere($select);

        if ($where !== null) {
            $sql .= " WHERE " . $where->query();
            $criterias[] = $where;
        }

        return new Compiled(
            $sql,
            new BindingContainerAggregate($criterias),
            $select->getHydrator()
        );
    }

    /**
     * Compile "projection" attribute list from SELECT query
     * If the set of attributes is empty OR contains "*", it will results to ALL "*"
     * In other case, the list will be converted to DB attributes
     */
    protected function compileAttributes(Select $select): string
    {
        $attributes = $select->getAttributes();
        $fields = $select->getMetadata()->fields;

        if (empty($attributes) || in_array("*", $attributes)) {
            return "*";
        }

        $list = "";

        foreach ($attributes as $attr) {
            if (!empty($list)) {
                $list .= ", ";
            }

            if (isset($fields[$attr])) {
                $field = $fields[$attr];
                $list .= $this->platform->identifier([$field->attribute]);
            } else {
                $list .= $this->platform->identifier([$attr]);
            }
        }

        return $list;
    }

    /**
     * Compile the FROM clause
     */
    protected function compileFrom(Select $select): string
    {
        return " FROM " . $this->platform->identifier([$select->getMetadata()->schema]);
    }

    /**
     * Compile the WHERE criteria
     * @return Criteria|null Return the compile criteria or NULL if the WHERE clause os not given
     */
    protected function compileWhere(Select $select): ?Criteria
    {
        $criteria = $select->getWhere();

        if ($criteria === null) {
            return null;
        }

        return $criteria->compile($this->criteriaCompiler);
    }
}
