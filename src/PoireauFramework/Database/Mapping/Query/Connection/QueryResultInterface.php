<?php

namespace PoireauFramework\Database\Mapping\Query\Connection;

/**
 * Handle results of compiled query
 */
interface QueryResultInterface
{
    /**
     * (re)execute the query to the related connection
     * This method should be called after new bind()'ing,
     * Or after QueryConnectionInterface::prepare() call
     * @return $this
     */
    public function execute(): QueryResultInterface;

    /**
     * Bind data to the query
     * Do not forget to execute() query after
     */
    public function bind($data): QueryResultInterface;

    /**
     * Get all results of the query, in an array
     * The values will be parsed using the query hydrator
     */
    public function all(): array;

    /**
     * Fetch the current result, and convert to PHP object using the query hydrator
     * Move the cursor to the next entry
     * If there is no more results , this method will return null
     * @return object|null
     */
    public function fetch();
}
