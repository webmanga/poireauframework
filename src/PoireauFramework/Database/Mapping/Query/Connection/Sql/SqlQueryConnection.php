<?php

namespace PoireauFramework\Database\Mapping\Query\Connection\Sql;

use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Connection\QueryConnectionInterface;
use PoireauFramework\Database\Mapping\Query\Connection\QueryResultInterface;

/**
 * SQL implementation for QueryConnectionInterface
 */
final class SqlQueryConnection implements QueryConnectionInterface
{
    /**
     * @var SqlConnectionInterface
     */
    private $connection;


    /**
     *
     */
    public function __construct(SqlConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Compiled $query): QueryResultInterface
    {
        return new SqlQueryResult(
            $this->connection->query(
                $query->query(),
                $query->bindings($this->connection->platform())
            ),
            $query
        );
    }

    /**
     * {@inheritdoc}
     */
    public function prepare(Compiled $query): QueryResultInterface
    {
        return new SqlQueryResult(
            $this->connection->prepare($query->query()),
            $query
        );
    }
}
