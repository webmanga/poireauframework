<?php

namespace PoireauFramework\Database\Mapping\Query\Connection\Sql;

use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Connection\QueryResultInterface;
use PoireauFramework\Database\Sql\SqlStatementInterface;

/**
 * SQL implementation for QueryResultInterface
 */
final class SqlQueryResult implements QueryResultInterface
{
    /**
     * @var SqlStatementInterface
     */
    private $statement;

    /**
     * @var Compiled
     */
    private $query;


    /**
     *
     */
    public function __construct(SqlStatementInterface $statement, Compiled $query)
    {
        $this->statement = $statement;
        $this->query = $query;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): QueryResultInterface
    {
        $this->statement->execute();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bind($data): QueryResultInterface
    {
        $this->query->bind($data);
        $this->statement->bindAll($this->query->bindings($this->statement->connection()->platform()));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function all(): array
    {
        $result = [];

        for(;;) {
            $data = $this->fetch();

            if ($data === null) {
                break;
            }

            $result[] = $data;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch() {
        $data = $this->statement->fetch();

        if ($data === false) {
            return null;
        }

        return $this->query->convert($data);
    }
}
