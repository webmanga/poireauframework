<?php

namespace PoireauFramework\Database\Mapping\Query\Connection;

use PoireauFramework\Database\Mapping\Query\Compiled;

/**
 * Interface for execute compiled queries on a connection
 */
interface QueryConnectionInterface
{
    /**
     * Execute the compiled query
     */
    public function execute(Compiled $query): QueryResultInterface;

    /**
     * Prepare the query result, without execute-it
     * Useful for bind values after preparation
     */
    public function prepare(Compiled $query): QueryResultInterface;
}
