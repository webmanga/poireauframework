<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Aggregate multiple binding containers
 * This class is used to aggregate compiled criteria of select queries (where + join on + having)
 * /!\ The order of containers is very important for placeholder prepared queries
 */
class BindingContainerAggregate implements BindingContainerInterface
{
    /**
     * @var BindingContainerInterface[]
     */
    private $containers = [];


    /**
     * @param BindingContainerInterface[] containers
     */
    public function __construct(array $containers)
    {
        $this->containers = $containers;
    }

    /**
     * {@inheritdoc}
     */
    public function bind($data): BindingContainerInterface
    {
        foreach ($this->containers as $container) {
            $container->bind($data);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bindings(SqlPlatformInterface $platform): array
    {
        $bindings = [];

        foreach ($this->containers as $container) {
            foreach ($container->bindings($platform) as $k => $v) {
                if (is_int($k)) {
                    $bindings[] = $v;
                } else {
                    $bindings[$k] = $v;
                }
            }
        }

        return $bindings;
    }
}
