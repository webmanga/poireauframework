<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Compiler\CriteriaCompilerInterface;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Criteria for database queries
 */
class Criteria implements BindingContainerInterface
{
    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var string
     */
    private $alias = null;

    /**
     * @var string
     */
    private $separator;

    /**
     * @var array
     */
    private $criteria;

    /**
     * @var BindingContainer
     */
    private $bindings;

    /**
     * @var mixed
     */
    private $compiled;


    /**
     *
     */
    public function __construct(Metadata $metadata, array $criteria, string $separator = "AND")
    {
        $this->metadata  = $metadata;
        $this->criteria  = $criteria;
        $this->separator = $separator;
        $this->bindings  = new BindingContainer();
    }

    /**
     * Compile the criteria object
     */
    public function compile(CriteriaCompilerInterface $compiler): Criteria
    {
        $compiled = $compiler->compile($this);

        $this->bindings->setBindings($compiled["parameters"]);
        $this->compiled = $compiled["criteria"];

        return $this;
    }

    /**
     * Get the raw criteria
     */
    public function criteria(): array
    {
        return $this->criteria;
    }

    /**
     * Get the criterion separator
     */
    public function separator(): string
    {
        return $this->separator;
    }

    /**
     * Get the metadata object
     */
    public function metadata(): Metadata
    {
        return $this->metadata;
    }

    /**
     * {@inheritdoc}
     */
    public function bind($data): BindingContainerInterface
    {
        $this->bindings->bind($data);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bindings(SqlPlatformInterface $platform): array
    {
        return $this->bindings->bindings($platform);
    }

    /**
     * Get the compiled query
     */
    public function query()
    {
        return $this->compiled;
    }

    /**
     * Check if the criteria is compiled
     */
    public function isCompiled(): bool
    {
        return $this->compiled !== null;
    }
}
