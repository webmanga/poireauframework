<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Container of @see Binding
 */
class BindingContainer implements BindingContainerInterface
{
    /**
     * @var Binding[]
     */
    private $bindings = [];


    /**
     *
     */
    public function __construct(array $bindings = [])
    {
        $this->bindings = $bindings;
    }

    /**
     * @internal
     */
    public function setBindings(array $bindings): void
    {
        $this->bindings = $bindings;
    }

    /**
     * {@inheritdoc}
     */
    public function bind($data): BindingContainerInterface
    {
        foreach ($this->bindings as $binding) {
            if (!$binding instanceof Binding) {
                continue;
            }

            $binding->bind($data);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bindings(SqlPlatformInterface $platform): array
    {
        $bindings = [];

        foreach ($this->bindings as $binding) {
            if ($binding instanceof Binding) {
                $bindings[] = $binding->value($platform);
            } else {
                $bindings[] = $binding;
            }
        }

        return $bindings;
    }
}
