<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Interface for container of bindings
 */
interface BindingContainerInterface
{
    /**
     * Bind data to the criteria
     * @param array|object data Data to bind
     * @return $this
     */
    public function bind($data): BindingContainerInterface;

    /**
     * Get list of bindings
     */
    public function bindings(SqlPlatformInterface $platform): array;
}
