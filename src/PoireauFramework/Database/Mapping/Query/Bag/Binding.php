<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;
use PoireauFramework\Database\Mapping\Type\TypeInterface;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Create a binding for a criteria
 * The binding value is set AFTER criteria compilation.
 * You can specify the binding name, or null to use the associated column name
 *
 * <code>
 * $criteria = new Criteria($metadata, [
 *     "firstName" => new Binding(),
 *     "lastName"  => new Binding(),
 *     "fk.name"   => new Binding("fk")
 * ]);
 *
 * $criteria
 *    ->compile($compiler)
 *    ->bind($person) //Bind with an entity object
 *    ->bind(["fk" => "Jack"]) // Can also bind with an array
 * ;
 * </code>
 */
class Binding
{
    /**
     * @var string
     */
    public $field;

    /**
     * @var string
     */
    public $type;

    /**
     * @var mixed
     */
    private $value;


    /**
     * Create the binding
     * @param string|null $field The field name, or null to use the column name
     */
    public function __construct(string $field = null, $type = null)
    {
        $this->field = $field;
        $this->type  = $type;
    }

    /**
     * Bind the value from object / array
     */
    public function bind($obj): void
    {
        if (is_array($obj)) {
            if (isset($obj[$this->field])) {
                $this->value = $obj[$this->field];
            }
        } else {
            if (method_exists($obj, $this->field)) {
                $this->value = $obj->{$this->field}();
            } elseif (isset($obj->{$this->field})) {
                $this->value = $obj->{$this->field};
            }
        }
    }

    /**
     * Get the bind value
     */
    public function value(SqlPlatformInterface $platform)
    {
        if ($this->type === null) {
            return $this->value;
        }

        return $platform->types()->get($this->type)->toDatabase($this->value);
    }
}
