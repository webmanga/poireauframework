<?php

namespace PoireauFramework\Database\Mapping\Query\Exception;

use LogicException;

/**
 * Base exception for Queries errors
 */
class QueryException extends LogicException
{
    
}
