<?php

namespace PoireauFramework\Database\Mapping\Query;

use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Compiler\InsertCompilerInterface;

/**
 * Query for creation operations
 */
class Insert
{
    const TYPE_DEFAULT = 0;
    const TYPE_REPLACE = 1;
    const TYPE_IGNORE  = 2;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @var bool
     */
    private $bulk = false;

    /**
     * @var array|null
     */
    private $attributes = null;

    /**
     * @var array
     */
    private $values = [];

    /**
     * @var int
     */
    private $type = self::TYPE_DEFAULT;


    /**
     *
     */
    public function __construct(Metadata $metadata, HydratorInterface $hydrator)
    {
        $this->metadata = $metadata;
        $this->hydrator = $hydrator;
    }

    /**
     * Set the INSERT query as bulk
     * <code>
     * $query
     *     ->bulk()
     *     ->values(["firstName" => "John", "lastName" => "Doe"])
     *     ->values(["firstName" => "Alan", "lastName" => "Smith"])
     * ;
     * // Will results :
     * // INSERT INTO PERSON (FIRST_NAME, LAST_NAME) VALUES ("John", "Doe") ("Alan", "Smith")
     * </code>
     * @param bool flag Enable the bulk insert
     * @return $this
     */
    public function bulk(bool $flag = true): Insert
    {
        $this->bulk = $flag;

        return $this;
    }

    /**
     * Add values for the INSERT query
     * If the query is not bulk, only consider the first call of $this function
     * @param object|array data The data to insert
     * @return $this
     * @see Insert::bulk() For enable multiple values INSERT query
     */
    public function values($data): Insert
    {
        $this->values[] = $data;

        return $this;
    }

    /**
     * Set list of attributes to insert. If not provided, all attributes will be used
     * <code>
     * $query
     *     ->attributes(["firstName"])
     *     ->values(["firstName" => "John", "lastName" => "Doe"])
     * ;
     * // Will results :
     * // INSERT INTO PERSON (FIRST_NAME) VALUES ("John")
     * </code>
     * @return $this
     */
    public function attributes(array $attributes): Insert
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Set query as INSERT IGNORE
     * @return $this
     */
    public function ignore(): Insert
    {
        $this->type = self::TYPE_IGNORE;

        return $this;
    }

    /**
     * Set query as REPLACE INTO
     * @return $this
     */
    public function replace(): Insert
    {
        $this->type = self::TYPE_REPLACE;

        return $this;
    }

    /**
     * @internal
     */
    public function getMetadata(): Metadata
    {
        return $this->metadata;
    }

    /**
     * @internal
     */
    public function getHydrator(): HydratorInterface
    {
        return $this->hydrator;
    }

    /**
     * @internal
     */
    public function isBulk(): bool
    {
        return $this->bulk;
    }

    /**
     * @internal
     */
    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    /**
     * @internal
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @internal
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * Compile the query
     */
    public function compile(InsertCompilerInterface $compiler): Compiled
    {
        return $compiler->compileInsert($this);
    }
}
