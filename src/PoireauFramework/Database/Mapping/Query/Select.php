<?php

namespace PoireauFramework\Database\Mapping\Query;

use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\Query\Compiler\SelectCompilerInterface;

/**
 * Query for selection operations
 *
 * <code>
 * $query
 *     ->attributes(["name", "email"])
 *     ->where([
 *         "username" => new Binding(),
 *         "password" => new Binding()
 *     ])
 *     ->compile($compiler)
 *     ->bind($form->export())
 * ;
 * // SELECT `USER_NAME`, `USER_MAIL` FROM `USER` WHERE `USER_LOGIN` = ? AND `USER_PASSWORD` = ?
 * </code>
 */
class Select
{
    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @var array|null
     */
    private $attributes = null;

    /**
     * @var array
     */
    private $where = null;


    /**
     *
     */
    public function __construct(Metadata $metadata, HydratorInterface $hydrator)
    {
        $this->metadata = $metadata;
        $this->hydrator = $hydrator;
    }

    /**
     * Set list of attributes to retrieve. If not provided, all attributes will be used
     * <code>
     * $query
     *     ->attributes(["firstName"])
     *     ->where(["lastName" => "Doe"])
     * ;
     * // Will results :
     * // SELECT FIRST_NAME FROM PERSON WHERE LAST_NAME = "Doe"
     * </code>
     * @return $this
     */
    public function attributes(array $attributes): Select
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Add WHERE condition
     *
     * <code>
     * // All this conditions will be treated as same criteria : "WHERE `FOO` = 'BAR'"
     * $query->where("foo", "bar");
     * $query->where("foo", "=", "bar");
     * $query->where(["foo", "=", "bar"]);
     * $query->where(["foo" => "bar"]);
     * </code>
     *
     * <code>
     * // Example for OR with AND query
     * $query->where([
     *     "foo" => "bar",
     *     ":or" => [
     *         [
     *             "subentity.name"  => "foo",
     *             "subentity.value" => "bar"
     *         ],
     *         [
     *             "subentity.name"  => "bar",
     *             "subentity.value" => "foo"
     *         ]
     *     ]
     * ]);
     * // Will results of : "WHERE `foo` = ? AND ((`sub_name` = ? AND `sub_value` = ?) OR (`sub_name` = ? AND `sub_value` = ?))"
     * </code>
     *
     * <code>
     * // With bindings (i.e. bind values after query compilation)
     * $query->where("name", new Bindings());
     * $compiled = $query->compile($compiler);
     * $compiled->bind(["name" => "John"]);
     * // Will results of : "WHERE `USER_NAME` = ?" with parameter : ["John"]
     * </code>
     *
     * Multiple call of where() will merge criteria, with "AND" separator.
     * If the current criteria is a "OR" criteria (after Select->orWhere() call), the criteria will be merge with the last "OR" criteria
     *
     * @param array|string criteria <p>
     *   This parameter will choose the type of the criteria.
     *   Can be either :
     *     - The complete criteria as an array (other parameters should not be set)
     *     - The column name as string, on single column condition (at least one of the two other parameters should be set)
     *     - The single column condition as array (i.e. [column, operator, value]) (other parameters should not be set)
     * </p>
     * @param string|mixed|null operator <p>
     *   - This parameter should not be set, if the first parameter ($criteria) is an array.
     *   - If the third parameter ($value) is not set, this parameter will be the value, and the condition operator will be equals "="
     *   - In other case, this parameter is the condition operator
     * </p>
     * @param mixed value <p>
     *     - This parameter should not be set, if the first parameter ($criteria) is an array.
     *     - This operator represents the value of the condition
     *     - If this parameter is set, the second parameter ($operator) should be the condition operator
     * </p>
     *
     * @see \PoireauFramework\Database\Mapping\Query\Bag\Criteria For more information on the criteria syntax
     *
     * @return $this
     */
    public function where($criteria, $operator = null, $value = null): Select
    {
        $normalized = $this->normalizeCriteria($criteria, $operator, $value);

        if (empty($this->where)) {
            $this->where = $normalized;

            return $this;
        }

        if (isset($this->where[":or"])) {
            $oldCriteria = end($this->where[":or"]);
            $key         = key($this->where[":or"]);

            // Ensure that the "OR" condition is normalized
            if (!is_array($oldCriteria)) {
                // The condition is in form [column] => [value]
                // The key will be changed
                $tmp = [$key => $oldCriteria];
                $oldCriteria = $tmp;
                unset($this->where[":or"][$key]);
            } else {
                $oldCriteria = $this->normalizeCriteria($oldCriteria);
            }

            $newCriteria = array_merge($oldCriteria, $normalized);

            // The key is a string, do not keep it
            if (is_string($key)) {
                $this->where[":or"][] = $newCriteria;
            } else {
                $this->where[":or"][$key] = $newCriteria;
            }

            return $this;
        }

        $newCriteria = array_merge($this->where, $normalized);
        $this->where = $newCriteria;

        return $this;
    }

    /**
     * @internal
     */
    public function getMetadata(): Metadata
    {
        return $this->metadata;
    }

    /**
     * @internal
     */
    public function getHydrator(): HydratorInterface
    {
        return $this->hydrator;
    }

    /**
     * @internal
     */
    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    /**
     * Get the WHERE criteria object
     */
    public function getWhere(): ?Criteria
    {
        if (empty($this->where)) {
            return null;
        }

        if (count($this->where) === 1 && isset($this->where[":or"])) {
            return new Criteria($this->metadata, $this->where[":or"], "OR");
        }

        return new Criteria($this->metadata, $this->where);
    }

    /**
     * Compile the query
     */
    public function compile(SelectCompilerInterface $compiler): Compiled
    {
        return $compiler->compileSelect($this);
    }

    /**
     * Normalize the criteria, to get mergeable criteria array
     */
    protected function normalizeCriteria($criteria, $operator = null, $value = null): array
    {
        // Criteria is a string => single column operation
        if (is_string($criteria)) {
            if ($value === null) {
                return [$criteria => $operator];
            }

            return [[$criteria, $operator, $value]];
        }

        // Criteria is in form of [column, operator, value]
        if (count($criteria) === 3 && isset($criteria[0])) {
            return [$criteria];
        }

        // Criteria is already normalized
        return $criteria;
    }
}
