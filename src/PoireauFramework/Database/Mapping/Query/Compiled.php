<?php

namespace PoireauFramework\Database\Mapping\Query;

use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainerInterface;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Compiled and immutable query
 */
final class Compiled implements BindingContainerInterface
{
    /**
     * @var mixed
     */
    private $query;

    /**
     * @var BindingContainerInterface
     */
    private $bindings;

    /**
     * @var HydratorInterface
     */
    private $hydrator;


    /**
     *
     */
    public function __construct($query, BindingContainerInterface $bindings, HydratorInterface $hydrator = null)
    {
        $this->query    = $query;
        $this->bindings = $bindings;
        $this->hydrator = $hydrator;
    }

    /**
     * Get the compiled query
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * {@inheritdoc}
     */
    public function bind($data): BindingContainerInterface
    {
        $this->bindings->bind($data);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bindings(SqlPlatformInterface $platform): array
    {
        return $this->bindings->bindings($platform);
    }

    /**
     * Convert database data to PHP object
     */
    public function convert($dbData)
    {
        return $this->hydrator->fromDatabase($dbData);
    }
}
