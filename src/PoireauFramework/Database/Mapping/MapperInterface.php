<?php

namespace PoireauFramework\Database\Mapping;

use PoireauFramework\Database\Mapping\Schema\SchemaInterface;

/**
 * Interface for map PHP to database
 */
interface MapperInterface
{
    /**
     * Get the related schema
     */
    public function schema(): SchemaInterface;

    /**
     * Get the related entity class name
     */
    public function entity(): string;

    /**
     * Create the entity on the database
     *
     * @param object entity The entity to insert
     *
     * @return object The database entity
     */
    public function create($entity);
}
