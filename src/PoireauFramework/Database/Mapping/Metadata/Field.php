<?php

namespace PoireauFramework\Database\Mapping\Metadata;

/**
 * Field structure
 */
class Field
{
    const PRIMARY        = 1;
    const UNIQUE         = 2;
    const AUTO_INCREMENT = 4;
    const NOT_NULL       = 8;

    /**
     * The field name
     * @var string
     */
    public $name;

    /**
     * The database attribute name
     * @var string
     */
    public $attribute;

    /**
     * The type
     * @var string
     */
    public $type;

    /**
     * Properties as bit field
     * @var int
     */
    public $properties = 0;

    /**
     * The field length, or null to not specify
     * @var int|null
     */
    public $length = null;


    /**
     * Check the field property
     */
    public function is(int $prop): bool
    {
        return ($this->properties & $prop) === $prop;
    }

    /**
     * Set the property
     */
    public function set(int $prop): void
    {
        $this->properties |= $prop;
    }
}
