<?php

namespace PoireauFramework\Database\Mapping\Metadata;

/**
 * Metadata for database mapping
 */
class Metadata {
    /**
     * The name of the schema or collection
     * @var string
     */
    public $schema;

    /**
     * The entity class name
     * @var string
     */
    public $entity;

    /**
     * Array of PHP fields
     * @var Field[]
     */
    public $fields = [];

    /**
     * Array of database attributes
     * @var Field[]
     */
    public $attributes = [];

    /**
     * The primary keys
     * @var Field[]
     */
    public $primary = [];

    /**
     * List of generated attributes (i.e. autoincrement)
     * @var Field[]
     */
    public $generated = [];

    /**
     * @var bool
     */
    public $built = false;
}
