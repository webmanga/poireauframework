<?php

namespace PoireauFramework\Database\Mapping\Metadata;

/**
 * Field fields into metadata
 */
class FieldsBuilder
{
    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var Field[]
     */
    private $fields = [];

    /**
     * @var Field
     */
    private $field;


    /**
     *
     */
    public function __construct(Metadata $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * Add a new field
     * @return $this
     */
    public function add(string $name, string $type): FieldsBuilder
    {
        $field = new Field();

        $field->name = $name;
        $field->type = $type;
        $field->attribute = $name;

        $this->fields[] = $field;
        $this->field    = $field;

        return $this;
    }

    /**
     * Set the field's database attribute name
     * @return $this
     */
    public function attribute(string $name): FieldsBuilder
    {
        $this->field->attribute = $name;

        return $this;
    }

    /**
     * Set the field as primary key
     * @return $this
     */
    public function primary(): FieldsBuilder
    {
        $this->field->set(Field::PRIMARY);

        return $this;
    }

    /**
     * Set the field as unique
     * @return $this
     */
    public function unique(): FieldsBuilder
    {
        $this->field->set(Field::UNIQUE);

        return $this;
    }

    /**
     * Set the field as auto_increment
     * Do not set as primary !
     * @return $this
     */
    public function autoincrement(): FieldsBuilder
    {
        $this->field->set(Field::AUTO_INCREMENT);

        return $this;
    }

    /**
     * Set the field as not null constraint
     * @return $this
     */
    public function notNull(): FieldsBuilder
    {
        $this->field->set(Field::NOT_NULL);

        return $this;
    }

    /**
     * Set the field length
     * @return $this
     */
    public function length(int $length): FieldsBuilder
    {
        $this->field->length = $length;

        return $this;
    }

    /**
     * Build the metadata
     */
    public function build(): Metadata
    {
        $metadata = $this->metadata;

        foreach ($this->fields as $field) {
            $metadata->fields[$field->name] = $field;
            $metadata->attributes[$field->attribute] = $field;

            if ($field->is(Field::PRIMARY)) {
                $metadata->primary[] = $field;
            }

            if ($field->is(Field::AUTO_INCREMENT)) {
                $metadata->generated[] = $field;
            }
        }

        $metadata->built = true;

        return $metadata;
    }

    //================//
    // Helper methods //
    //================//

    /**
     * Create a string field
     * @return $this
     */
    public function string(string $name, int $length = null): FieldsBuilder
    {
        $this->add($name, "string");

        if ($length !== null) {
            $this->length($length);
        }

        return $this;
    }

    /**
     * Create a integer field
     * @return $this
     */
    public function integer(string $name): FieldsBuilder
    {
        return $this->add($name, "integer");
    }

    /**
     * Create a simple list field
     * @return $this
     */
    public function list(string $name): FieldsBuilder
    {
        return $this->add($name, "list");
    }
}
