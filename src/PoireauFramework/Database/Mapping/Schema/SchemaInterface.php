<?php

namespace PoireauFramework\Database\Mapping\Schema;

/**
 * Interface for handle database schemas
 */
interface SchemaInterface
{
    /**
     * Create the schema into database
     */
    public function create(): void;

    /**
     * Drop the schema from database
     */
    public function drop(): void;
}
