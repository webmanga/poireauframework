<?php

namespace PoireauFramework\Database\Mapping\Schema;

use PoireauFramework\Database\Mapping\Metadata\Field;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Sql\SqlConnectionInterface;

/**
 * Handle SQL schemas (i.e. tables)
 */
class SqlSchema implements SchemaInterface
{
    /**
     * @var Metadata
     */
    protected $metadata;

    /**
     * @var SqlConnectionInterface
     */
    protected $connection;


    /**
     *
     */
    public function __construct(Metadata $metadata, SqlConnectionInterface $connection)
    {
        $this->metadata = $metadata;
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function create(): void
    {
        $query = "CREATE TABLE `{$this->metadata->schema}` (";

        $first = true;

        foreach ($this->metadata->fields as $field) {
            if (!$first) {
                $query .= ", ";
            } else {
                $first = false;
            }

            $type = $this->connection->platform()->types()->get($field->type)->sqlDeclaration($field);
            $options = "";

            if ($field->is(Field::NOT_NULL)) {
                $options .= " NOT NULL";
            }

            if ($field->is(Field::PRIMARY)) {
                $options .= " PRIMARY KEY";
            } elseif ($field->is(Field::UNIQUE)) {
                $options .= " UNIQUE";
            }

            if ($field->is(Field::AUTO_INCREMENT)) {
                $query .= $this->connection->platform()->autoIncrementDeclaration($field->attribute, $type, ltrim($options));
            } else {
                $query .= "`{$field->attribute}` {$type}{$options}";
            }
        }

        $query .= ")";

        $this->connection->query($query);
    }

    /**
     * {@inheritdoc}
     */
    public function drop(): void
    {
        $this->connection->query("DROP TABLE `{$this->metadata->schema}`");
    }
}
