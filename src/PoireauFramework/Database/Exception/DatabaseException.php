<?php

namespace PoireauFramework\Database\Exception;

use Exception;
use RuntimeException;

/**
 * Base exception for Database package
 */
class DatabaseException extends RuntimeException
{
    /**
     * @var string
     */
    private $connection;


    public function __construct(string $connection, string $message, Exception $previous = null)
    {
        parent::__construct($message, 0, $previous);

        $this->connection = $connection;
    }

    /**
     * Get the connection name
     * @return string
     */
    public function connection(): string
    {
        return $this->connection;
    }
}
