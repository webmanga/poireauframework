<?php

namespace PoireauFramework\Database\Exception;

use Exception;

/**
 * Exception throws on SQL errors
 */
class SqlException extends DatabaseException
{
    /**
     * @var string
     */
    private $query;


    public function __construct(string $connection, string $query, Exception $previous = null)
    {
        $message = "[{$connection}] Error on query : '{$query}'";

        if ($previous !== null) {
            $message .= " : " . $previous->getMessage();
        }

        parent::__construct($connection, $message, $previous);

        $this->query = $query;
    }
}
