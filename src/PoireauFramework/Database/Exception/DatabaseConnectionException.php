<?php

namespace PoireauFramework\Database\Exception;

use Exception;

/**
 * Exception throws when an error occurs during connection
 */
class DatabaseConnectionException extends DatabaseException {
    public function __construct(string $connection, Exception $previous = null)
    {
        parent::__construct($connection, "Could not connect to database '{$connection}'", $previous);
    }
}
