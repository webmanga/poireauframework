<?php

namespace PoireauFramework\Database;

use PoireauFramework\Config\ConfigItem;

/**
 * Driver interface
 */
interface ConnectionDriverInterface
{
    /**
     * Get the driver name
     * @return string
     */
    public function name(): string;

    /**
     * Configure the connection
     * @return ConnectionInterface The new connection
     */
    public function configure(ConfigItem $config): ConnectionInterface;
}
