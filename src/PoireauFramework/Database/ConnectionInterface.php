<?php

namespace PoireauFramework\Database;

/**
 * Base interface for database connections
 */
interface ConnectionInterface
{
    /**
     * Connect to database
     * @throws \PoireauFramework\Database\Exception\DatabaseConnectionException
     */
    public function connect(): void;

    /**
     * Check if the connection is done
     * @return bool
     */
    public function isConnected(): bool;

    /**
     * Get the internal connection resource
     * @return object|resource
     */
    public function native();

    /**
     * Get the connection name
     * @return string
     */
    public function name(): string;
}
