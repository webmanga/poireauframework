<?php

namespace PoireauFramework\Database;

/**
 * Interface for database connection creation
 */
interface ConnectionFactoryInterface
{
    /**
     * Create the database connection
     *
     * @param string $name
     *
     * @return ConnectionInterface
     */
    public function createConnection(string $name): ConnectionInterface;
}
