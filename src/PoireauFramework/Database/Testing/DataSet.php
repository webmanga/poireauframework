<?php

namespace PoireauFramework\Database\Testing;

use PoireauFramework\Database\Mapping\MapperInterface;
use PoireauFramework\Database\Mapping\Resolver\MapperResolverInterface;

/**
 * handle testing data set
 */
class DataSet
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var MapperInterface[]
     */
    private $mappers = [];

    /**
     * @var MapperResolverInterface
     */
    private $resolver;


    /**
     * DataSet constructor.
     *
     * @param MapperResolverInterface $resolver
     */
    public function __construct(MapperResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * Declare a mapper into the data set
     * The related relations will be created at setUp and drop at tearDown.
     * If an entity is used, it should be declared before use it
     *
     * @param string|array|MapperInterface $mappers
     *
     * @return $this
     */
    public function declare($mappers)
    {
        if (!is_array($mappers)) {
            $mappers = [$mappers];
        }

        foreach ($mappers as $mapper) {
            if (is_string($mapper)) {
                $mapper = $this->resolver->get($mapper);
            }

            $entity = $mapper->entity();

            if (isset($this->mappers[$entity])) {
                continue;
            }

            $mapper->schema()->create();

            $this->mappers[$entity] = $mapper;
        }

        return $this;
    }

    /**
     * @param string|array $name
     * @param object|null $entity
     *
     * @return $this
     */
    public function set($name, $entity = null)
    {
        if (is_array($name)) {
            foreach ($name as $k => $v) {
                $this->set($k, $v);
            }

            return $this;
        }

        if (is_object($name)) {
            $entity = $name;
        }

        $entity = $this->mapper($entity)->create($entity);

        if (is_string($name)) {
            $this->data[$name] = $entity;
        }

        return $this;
    }

    /**
     * Get an entity from the data set
     *
     * @param string $name
     *
     * @return object
     */
    public function get($name)
    {
        return $this->data[$name];
    }

    /**
     * Get the mapper instance
     *
     * @param string|object $entity
     *
     * @return MapperInterface
     */
    public function mapper($entity)
    {
        if (is_object($entity)) {
            $entity = get_class($entity);
        }

        if (isset($this->mappers[$entity])) {
            return $this->mappers[$entity];
        }

        throw new \Exception("Cannot found mapper for $entity");
    }

    /**
     * Destroy all created entities
     */
    public function destroy()
    {
        foreach ($this->mappers as $mapper) {
            $mapper->schema()->drop();
        }

        $this->mappers = [];
        $this->data    = [];
    }
}
