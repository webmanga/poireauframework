<?php

namespace PoireauFramework\Database;

use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Database\Exception\DatabaseConfigException;

/**
 * ConnectionFactory using Config system for get connection drivers
 */
class ConfigConnectionFactory implements ConnectionFactoryInterface
{
    /**
     * @var ConfigItem
     */
    protected $config;

    /**
     * @var ConnectionDriverInterface[]
     */
    protected $drivers = [];


    /**
     * Construct the connection factory
     *
     * @param ConfigItem $config The database configuration
     * @param ConnectionDriverInterface[] $drivers
     */
    public function __construct(ConfigItem $config, array $drivers = [])
    {
        $this->config = $config;

        foreach ($drivers as $driver) {
            $this->register($driver);
        }
    }

    /**
     * Register a connection driver
     *
     * @param ConnectionDriverInterface $driver
     */
    public function register(ConnectionDriverInterface $driver): void
    {
        $this->drivers[$driver->name()] = $driver;
    }

    /**
     * {@inheritdoc}
     *
     * @todo set name
     */
    public function createConnection(string $name): ConnectionInterface
    {
        if (!$this->config->has($name)) {
            throw new DatabaseConfigException($name, "Cannot find config item");
        }

        $config = $this->config->item($name);

        if (!$config->has("driver")) {
            throw new DatabaseConfigException($name, "No driver given");
        }

        if (!isset($this->drivers[$config->get("driver")])) {
            throw new DatabaseConfigException($name, "Driver '{$config->get("driver")}' not found");
        }

        return $this->drivers[$config->get("driver")]->configure($config);
    }
}
