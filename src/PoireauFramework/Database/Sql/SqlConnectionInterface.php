<?php

namespace PoireauFramework\Database\Sql;

use PoireauFramework\Database\ConnectionInterface;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * Interface for Sql connections
 */
interface SqlConnectionInterface extends ConnectionInterface
{
    /**
     * Execute a query
     *
     * @param string $sql The SQL query
     * @param array $arguments The query arguments
     *
     * @return SqlStatementInterface The query statement
     */
    public function query(string $sql, array $arguments = []): SqlStatementInterface;

    /**
     * Prepare a query
     *
     * @param string $sql The query to prepare
     *
     * @return SqlStatementInterface The query statement
     */
    public function prepare(string $sql): SqlStatementInterface;

    /**
     * Get the last inserted ID
     *
     * @param string $name Name of the sequence
     *
     * @return string
     */
    public function lastInsertedId(string $name = null): string;

    /**
     * Get the platform
     */
    public function platform(): SqlPlatformInterface;
}
