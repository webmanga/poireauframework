<?php

namespace PoireauFramework\Database\Sql\Pdo;


use Pdo;
use PoireauFramework\Database\Exception\DatabaseConnectionException;
use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Database\Sql\SqlStatementInterface;

/**
 * Class PdoConnection
 */
class PdoConnection implements SqlConnectionInterface
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var SqlConnectionInterface
     */
    private $platform;

    /**
     * @var PdoConfigStruct
     */
    protected $config;


    public function __construct(PdoConfigStruct $config, SqlPlatformInterface $platform)
    {
        $this->config = $config;
        $this->platform = $platform;
    }

    /**
     * {@inheritdoc}
     */
    public function connect(): void
    {
        try {
            $this->pdo = new Pdo(
                $this->config->dsn,
                $this->config->username,
                $this->config->password,
                $this->config->options
            );
        } catch(\PdoException $e) {
            throw new DatabaseConnectionException($this->name(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isConnected(): bool
    {
        return $this->pdo !== null;
    }

    /**
     * {@inheritdoc}
     * @return \Pdo
     */
    public function native()
    {
        return $this->pdo;
    }

    /**
     * {@inheritdoc}
     *
     * @todo should not be null
     */
    public function name(): string
    {
        return (string) $this->config->name;
    }

    /**
     * {@inheritdoc}
     */
    public function platform(): SqlPlatformInterface
    {
        return $this->platform;
    }

    /**
     * {@inheritdoc}
     */
    public function query(string $sql, array $arguments = []): SqlStatementInterface
    {
        try {
            if (empty($arguments)) {
                $stmt = new PdoStatement($sql, $this, $this->pdo->query($sql));
            } else {
                $stmt = $this->prepare($sql);

                $stmt
                    ->bindAll($arguments)
                    ->execute()
                ;
            }
        } catch (\PdoException $e) {
            throw new SqlException($this->name(), $sql, $e);
        }

        return $stmt;
    }

    /**
     * {@inheritdoc}
     */
    public function prepare(string $sql): SqlStatementInterface
    {
        try {
            return new PdoStatement($sql, $this, $this->pdo->prepare($sql));
        } catch (\PdoException $e) {
            throw new SqlException($this->name(), $sql, $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function lastInsertedId(string $name = null): string
    {
        try {
            return $this->pdo->lastInsertId($name);
        } catch (\PdoException $e) {
            throw new SqlException($this->name(), "last inserted id", $e);
        }
    }
}
