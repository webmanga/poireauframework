<?php

namespace PoireauFramework\Database\Sql\Pdo;

use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Database\Sql\SqlStatementInterface;

/**
 * Statement wrapper for PDO
 */
class PdoStatement implements SqlStatementInterface
{
    /**
     * @var string
     */
    private $query;

    /**
     * @var PdoConnection
     */
    private $connection;

    /**
     * @var \PdoStatement
     */
    private $statement;


    public function __construct(string $query, PdoConnection $connection, \PdoStatement $statement)
    {
        $this->query = $query;
        $this->connection = $connection;
        $this->statement = $statement;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($parameters = null): bool
    {
        try {
            return $this->statement->execute($parameters);
        } catch (\PdoException $e) {
            throw new SqlException($this->connection->name(), $this->query, $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function bind($key, $value, int $type = self::TYPE_AUTO): SqlStatementInterface
    {
        if ($type === self::TYPE_AUTO) {
            $type = $this->detectBindType($value);
        }

        try {
            $this->statement->bindValue($key, $value, $type);
        } catch (\PdoException $e) {
            throw new SqlException($this->connection->name(), $this->query, $e);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bindAll(array $values): SqlStatementInterface
    {
        foreach ($values as $key => $value) {
            if (is_int($key)) {
                ++$key;
            }

            $this->bind($key, $value);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function query(): string
    {
        return $this->query;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch()
    {
        return $this->statement->fetch(\Pdo::FETCH_ASSOC);
    }

    /**
     * {@inheritdoc}
     */
    public function all(): array
    {
        return $this->statement->fetchAll(\Pdo::FETCH_ASSOC);
    }

    /**
     * {@inheritdoc}
     */
    public function connection(): SqlConnectionInterface
    {
        return $this->connection;
    }

    /**
     * Detected the bast binding type, if type is auto
     */
    protected function detectBindType($value): int
    {
        if ($value === null) {
            return self::TYPE_NULL;
        }

        if (is_int($value)) {
            return self::TYPE_INT;
        }

        if (is_bool($value)) {
            return self::TYPE_BOOL;
        }

        return self::TYPE_STR;
    }
}
