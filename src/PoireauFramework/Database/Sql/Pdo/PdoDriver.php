<?php

namespace PoireauFramework\Database\Sql\Pdo;

use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Database\ConnectionDriverInterface;
use PoireauFramework\Database\ConnectionInterface;
use PoireauFramework\Database\Exception\DatabaseConfigException;
use PoireauFramework\Database\Sql\Platform\SqlPlatformRegistry;

/**
 * Driver for PDO
 * Config (global) :
 * - dsn : string - The DSN connection string
 * - type : string - "mysql" | "sqlite"
 * - username : string - "root"
 * - password : string - ""
 *
 * MySQL :
 * - unix_socket : string
 * - host : string
 * - port : int
 * - charset : string
 * - dbname : string - required
 *
 * SQLite :
 * - memory : boolean - Use memory database
 * - path : string - Path to db file
 */
class PdoDriver implements ConnectionDriverInterface
{
    /**
     * @var SqlPlatformRegistry
     */
    private $platforms;


    /**
     *
     */
    public function __construct(SqlPlatformRegistry $platforms)
    {
        $this->platforms = $platforms;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "pdo";
    }

    /**
     * {@inheritdoc}
     *
     * @return PdoConnection
     */
    public function configure(ConfigItem $config): ConnectionInterface
    {
        $pdoConfig = new PdoConfigStruct;

        $pdoConfig->dsn = $this->resolveDsn($config);

        $pdoConfig->username = $config->get("username");
        $pdoConfig->password = $config->get("password");

        $pdoConfig->type = strstr($pdoConfig->dsn, ":", true);

        $pdoConfig->options = $this->resolveOptions($config, $pdoConfig);

        return new PdoConnection(
            $pdoConfig,
            $this->platforms->get($pdoConfig->type)
        );
    }

    /**
     * Resolve the DSN string from config
     * @return string
     */
    public function resolveDsn(ConfigItem $config): string
    {
        if ($config->has("dsn")) {
            return $config->get("dsn");
        }

        if (!$config->has("type")) {
            throw new DatabaseConfigException("???", "The configuration must have at least 'dsn' or 'type' items");
        }

        switch (strtolower($config->get("type"))) {
            case "mysql":
                return $this->resolveMySqlDsn($config);
            case "sqlite":
                return $this->resolveSqliteDsn($config);
            default:
                throw new DatabaseConfigException("???", "Unknown database type '{$config->get("type")}'");
        }
    }

    protected function resolveMySqlDsn(ConfigItem $config): string
    {
        $dsn = "mysql:";

        if ($config->has("unix_socket")) {
            $dsn .= "unix_socket=" . $config->get("unix_socket");
        } elseif ($config->has("host")) {
            $dsn .= "host=" . $config->get("host");
        } else {
            $dsn .= "host=127.0.0.1";
        }

        if ($config->has("port")) {
            $dsn .= ";port=" . $config->get("port");
        }

        if ($config->has("charset")) {
            $dsn .= ";charset=" . $config->get("charset");
        }

        if (!$config->has("dbname")) {
            throw new DatabaseConfigException("???", "You should specify a dbname for MySQL connection");
        }

        $dsn .= ";dbname=" . $config->get("dbname");

        return $dsn;
    }

    protected function resolveSqliteDsn(ConfigItem $config): string
    {
        if ($config->get("memory")) {
            return "sqlite::memory:";
        }

        if (!$config->has("path")) {
            throw new DatabaseConfigException("???", "You should specify a path (or memory) for SQLite connection");
        }

        return "sqlite:" . $config->get("path");
    }

    /**
     * Resolve the driver options
     *
     * @param ConfigItem config The database configuration
     *
     * @param PdoConfigStruct pdoConfig The internal PDO config
     */
    public function resolveOptions(ConfigItem $config, PdoConfigStruct $pdoConfig): array
    {
        $opConfig = $config->item("options");
        $options = $pdoConfig->options;

        switch ($pdoConfig->type) {
            case PdoConfigStruct::TYPE_MYSQL:
                $options = array_replace($options, $this->resolveMySqlOptions($opConfig, $pdoConfig));
                break;
            case PdoConfigStruct::TYPE_SQLITE:
                $options = array_replace($options, $this->resolveSqliteOptions($opConfig, $pdoConfig));
                break;
        }

        if ($opConfig->has("persistent")) {
            $options[\Pdo::ATTR_PERSISTENT] = (bool) $opConfig->get("persistent");
        }

        return $options;
    }

    protected function resolveMySqlOptions(ConfigItem $config, PdoConfigStruct $pdoConfig): array
    {
        return [];
    }

    protected function resolveSqliteOptions(ConfigItem $config, PdoConfigStruct $pdoConfig): array
    {
        $options = [];

        if ($pdoConfig->dsn === "sqlite::memory:") {
            $options[\Pdo::ATTR_PERSISTENT] = true;
        }

        return $options;
    }
}
