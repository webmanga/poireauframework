<?php

namespace PoireauFramework\Database\Sql\Pdo;

use Pdo;

/**
 * Class PdoConfigStruct
 * @internal
 */
class PdoConfigStruct
{
    const TYPE_MYSQL  = "mysql";
    const TYPE_SQLITE = "sqlite";

    public $name;

    public $dsn;

    public $username;

    public $password;

    public $options = [
        Pdo::ATTR_ERRMODE            => Pdo::ERRMODE_EXCEPTION,
        Pdo::ATTR_DEFAULT_FETCH_MODE => Pdo::FETCH_ASSOC
    ];

    public $type;
}
