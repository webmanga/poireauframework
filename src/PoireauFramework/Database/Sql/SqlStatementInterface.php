<?php

namespace PoireauFramework\Database\Sql;

/**
 * Interface for SQL statement
 */
interface SqlStatementInterface
{
    const TYPE_AUTO = -1;
    const TYPE_BOOL = \Pdo::PARAM_BOOL;
    const TYPE_NULL = \Pdo::PARAM_NULL;
    const TYPE_INT  = \Pdo::PARAM_INT;
    const TYPE_STR  = \Pdo::PARAM_STR;

    /**
     * Execute the query
     * @param array|null parameters The query parameters (for prepared query)
     */
    public function execute($parameters = null): bool;

    /**
     * Bind a value to the prepared statement
     *
     * @param string|int $key The binding key. If use placeholder, the key starts at 1
     * @param mixed $value The value to bind
     * @param int $type One of the SqlStatementInterface::TYPE_* constant
     *
     * @return $this
     */
    public function bind($key, $value, int $type = self::TYPE_AUTO): SqlStatementInterface;

    /**
     * Bind an array of values. The types will be automatically detected
     */
    public function bindAll(array $values): SqlStatementInterface;

    /**
     * Get the query string
     * @return string
     */
    public function query(): string;

    /**
     * Fetch the current row
     * @return array|false
     */
    public function fetch();

    /**
     * Get all rows
     * @return array
     */
    public function all(): array;

    /**
     * Get the connection
     * @return SqlConnectionInterface
     */
    public function connection(): SqlConnectionInterface;
}
