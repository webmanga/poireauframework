<?php

namespace PoireauFramework\Database\Sql\Platform;

use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Platform for SQLite
 */
class SQLitePlatform extends AbstractSqlPlatform
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "sqlite";
    }

    /**
     * {@inheritdoc}
     */
    public function autoIncrementDeclaration(string $attribute, string $type, string $options): string
    {
        return "`{$attribute}` {$type} {$options} AUTOINCREMENT";
    }
}
