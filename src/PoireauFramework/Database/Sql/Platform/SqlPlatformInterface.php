<?php

namespace PoireauFramework\Database\Sql\Platform;

use PoireauFramework\Database\Mapping\Query\Compiler\CompilerRegistryInterface;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Interface for handling sql platform specifics
 */
interface SqlPlatformInterface
{
    /**
     * Get the platform name
     */
    public function name(): string;

    /**
     * Get the auto increment SQL declaration
     *
     * @param string $attribute The attribute name
     * @param string $type The attribute type
     * @param string $options
     *
     * @return string
     */
    public function autoIncrementDeclaration(string $attribute, string $type, string $options): string;

    /**
     * Quote a value
     *
     * @param mixed $value The unsafe value
     *
     * @return mixed The quoted value
     */
    public function quote($value);

    /**
     * Quote an identifier string
     */
    public function identifier(array $parts): string;

    /**
     * Get database types
     */
    public function types(): TypeRegistry;

    /**
     * Get the platform compilers
     */
    public function compilers(): CompilerRegistryInterface;
}
