<?php

namespace PoireauFramework\Database\Sql\Platform;

use PoireauFramework\Database\Mapping\Query\Compiler\CompilerRegistryInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlCompilerRegistry;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Abstract class create sql platforms
 */
abstract class AbstractSqlPlatform implements SqlPlatformInterface
{
    /**
     * @var TypeRegistry
     */
    private $types;

    private $compilers;


    /**
     *
     */
    public function __construct(TypeRegistry $types)
    {
        $this->types = $types;
        $this->compilers = new SqlCompilerRegistry($this);
    }

    /**
     * {@inheritdoc}
     */
    public function quote($value)
    {
        if (is_numeric($value)) {
            return $value;
        }

        if ($value === null) {
            return "''";
        }

        if (is_bool($value)) {
            return $value ? 1 : 0;
        }

        return "'" . str_replace("'", "''", $value) . "'";
    }

    /**
     * {@inheritdoc}
     */
    public function identifier(array $parts): string
    {
        $out = "";

        foreach ($parts as $index => $part) {
            if ($index > 0) {
                $out .= ".";
            }

            $out .= "`" . str_replace("`", "``", $part) . "`";
        }

        return $out;
    }

    /**
     * {@inheritdoc}
     */
    public function types(): TypeRegistry
    {
        return $this->types;
    }

    /**
     * {@inheritdoc}
     */
    public function compilers(): CompilerRegistryInterface
    {
        return $this->compilers;
    }
}
