<?php

namespace PoireauFramework\Database\Sql\Platform;

use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Registry of SQL platforms
 */
class SqlPlatformRegistry
{
    /**
     * @var SqlPlatformInterface[]
     */
    private $platforms = [];

    /**
     * @var TypeRegistry
     */
    private $types;


    /**
     *
     */
    public function __construct(TypeRegistry $types)
    {
        $this->types = $types;
    }

    /**
     * Get the platform by its name
     */
    public function get(string $name): SqlPlatformInterface
    {
        if (!isset($this->platforms[$name])) {
            throw new \LogicException("Platform {$name} is not found");
        }
        
        $platform = $this->platforms[$name];

        if (is_string($platform)) {
            $platform = new $platform($this->types);
            $this->platforms[$name] = $platform;
        }

        return $platform;
    }

    /**
     * Declare a new platform class
     */
    public function set(string $name, string $platformClass): void
    {
        $this->platforms[$name] = $platformClass;
    }

    /**
     * Register a new platform
     */
    public function register(SqlPlatformInterface $platform): void
    {
        $this->platforms[$platform->name()] = $platform;
    }
}
