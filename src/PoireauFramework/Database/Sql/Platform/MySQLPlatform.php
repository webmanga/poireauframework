<?php

namespace PoireauFramework\Database\Sql\Platform;

use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * Platform for MySQL
 */
class MySQLPlatform extends AbstractSqlPlatform
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "mysql";
    }

    /**
     * {@inheritdoc}
     */
    public function autoIncrementDeclaration(string $attribute, string $type, string $options): string
    {
        return "`{$attribute}` {$type} {$options} AUTO_INCREMENT";
    }
}
