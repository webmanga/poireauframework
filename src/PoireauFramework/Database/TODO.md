# TODO

## Mapping

* Requêtes `Update` & `Delete`
* `Criteria` mutable (devient un criteria builder)
* `Select` gestion des **join**, **having**, multi-**from** et metadata alias
* Gestion des FK + **auto-join**
* Gestion des fonctions aggregative (count, avg...)
* CRUD
* Bind avec types

## Types

* Ajouter des types
* Constantes pour types
* gérer NULL

## Compiler

* `SqlCriteriaCompiler` enlever metadata des propriétés de classe

## Platform

* Interface plateforme globale (non SQL)