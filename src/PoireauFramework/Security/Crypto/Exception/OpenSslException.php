<?php

namespace PoireauFramework\Security\Crypto\Exception;

use RuntimeException;

/**
 * Exception for OpenSSL
 */
class OpenSslException extends RuntimeException
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $openSslErrors;


    /**
     * @var string method The openSSL method
     */
    public function __construct(string $method)
    {
        $this->method = $method;
        $this->openSslErrors = $this->parseOpenSslErrors();

        parent::__construct("OpenSSL error : " . $this->method . "(), info : " . PHP_EOL . implode(PHP_EOL, $this->openSslErrors));
    }

    public function method(): string
    {
        return $this->method;
    }

    public function openSslErrors(): array
    {
        return $this->openSslErrors;
    }

    protected function parseOpenSslErrors(): array
    {
        $info = [];

        for (;;) {
            $line = openssl_error_string();

            if ($line === false) {
                break;
            }

            $info[] = $line;
        }

        return $info;
    }
}
