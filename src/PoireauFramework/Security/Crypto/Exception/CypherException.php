<?php

namespace PoireauFramework\Security\Crypto\Exception;

use RuntimeException;

/**
 * Exception for Cypher
 */
class CypherException extends RuntimeException
{

}
