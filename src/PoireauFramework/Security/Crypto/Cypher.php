<?php

namespace PoireauFramework\Security\Crypto;

use Exception;
use PoireauFramework\Security\Crypto\Exception\CypherException;

/**
 * Encrypt / decrypt data using symmetric cypher
 */
class Cypher
{
    const AES256_CTR = "aes-256-ctr";

    /**
     * @var string
     */
    private $algo;

    /**
     * @var string
     */
    private $key;

    /**
     * @var int
     */
    private $ivLength;


    /**
     * @param string $algo The cypher algo
     * @param string $key The encryption key
     */
    public function __construct(string $algo, string $key) {
        $this->algo = $algo;
        $this->key  = $key;

        try {
            $this->ivLength = openssl_cipher_iv_length($algo);
        } catch (Exception $e) {
            throw new CypherException($e->getMessage(), 0, $e);
        }

        if (!$this->ivLength) {
            throw new CypherException("Invalid cypher algo $algo");
        }
    }

    /**
     * Encrypt data
     */
    public function encrypt(string $data): string
    {
        $iv = openssl_random_pseudo_bytes($this->ivLength);
        $data .= md5($data, true);

        $encrypted = openssl_encrypt($data, $this->algo, $this->key, OPENSSL_RAW_DATA, $iv) . $iv;

        return base64_encode($encrypted);
    }

    /**
     * Decrypt previously encrypted data
     * @param string $data Encrypted data via $this->encrypt()
     * @return string|bool The decrypted string, or false if an error occurs
     */
    public function decrypt(string $data)
    {
        $raw = base64_decode($data);

        if (empty($raw)) {
            return false;
        }

        $len = strlen($raw);
        $cryptLen = $len - $this->ivLength;

        if ($cryptLen <= 0) {
            return false;
        }

        $iv = substr($raw, $cryptLen);
        $decrypted = openssl_decrypt(substr($raw, 0, $cryptLen), $this->algo, $this->key, OPENSSL_RAW_DATA, $iv);

        $payloadLen = strlen($decrypted) - 16;

        if ($payloadLen < 0) {
            return false;
        }

        $checksum = substr($decrypted, $payloadLen);
        $payload  = substr($decrypted, 0, $payloadLen);

        if (!hash_equals($checksum, md5($payload, true))) {
            return false;
        }

        return $payload;
    }
}
