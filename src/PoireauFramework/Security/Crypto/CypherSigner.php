<?php

namespace PoireauFramework\Security\Crypto;

/**
 * Signer using Cypher
 */
class CypherSigner implements SignerInterface
{
    /**
     * @var Cypher
     */
    private $cypher;


    /**
     *
     */
    public function __construct(Cypher $cypher)
    {
        $this->cypher = $cypher;
    }

    /**
     * {@inheritdoc}
     */
    public function generateKeys(): void {}

    /**
     * {@inheritdoc}
     */
    public function sign(string $data): string
    {
        return $this->cypher->encrypt($data);
    }

    /**
     * {@inheritdoc}
     */
    public function unsign(string $data): ?string
    {
        $decrypted = $this->cypher->decrypt($data);

        if ($decrypted === false) {
            return null;
        }

        return $decrypted;
    }
}
