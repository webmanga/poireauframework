<?php

namespace PoireauFramework\Security\Crypto;

use PoireauFramework\Security\Crypto\Exception\OpenSslException;

/**
 * Toolbox wrapper for OpenSSL functions
 * @link http://php.net/manual/fr/ref.openssl.php
 */
final class OpenSsl
{
    /**
     * @see openssl_pkey_new()
     */
    static public function generatePrivateKey(array $config)
    {
        $key = openssl_pkey_new($config);

        if ($key === false) {
            throw new OpenSslException(__METHOD__);
        }

        return $key;
    }

    /**
     * @see openssl_pkey_export_to_file()
     */
    static public function savePrivateKey($key, string $outfilename, $passphrase = null, array $configargs = []): void
    {
        if (openssl_pkey_export_to_file($key, $outfilename, $passphrase, $configargs) === false) {
            throw new OpenSslException(__METHOD__);
        }
    }

    /**
     * @see openssl_pkey_get_details()
     */
    static public function privateKeyDetails($key): array
    {
        $details = openssl_pkey_get_details($key);

        if ($details === false) {
            throw new OpenSslException(__METHOD__);
        }

        return $details;
    }

    /**
     * @see openssl_pkey_get_private()
     */
    static public function loadPrivateKey($key, $passphrase = null)
    {
        $res = openssl_pkey_get_private($key, $passphrase);

        if ($res === false) {
            throw new OpenSslException(__METHOD__);
        }

        return $res;
    }

    /**
     * @see openssl_pkey_get_public()
     */
    static public function loadPublicKey($key)
    {
        $res = openssl_pkey_get_public($key);

        if ($res === false) {
            throw new OpenSslException(__METHOD__);
        }

        return $res;
    }

    /**
     * @see openssl_csr_new()
     */
    static public function createCsr($key, array $dn)
    {
        $res = openssl_csr_new($dn, $key);

        if ($res === false) {
            throw new OpenSslException(__METHOD__);
        }

        return $res;
    }

    /**
     * @see openssl_csr_export_to_file()
     */
    static public function saveCsr($key, string $outfilename): void
    {
        if (openssl_csr_export_to_file($key, $outfilename) === false) {
            throw new OpenSslException(__METHOD__);
        }
    }

    /**
     * @see openssl_public_decrypt()
     */
    static public function publicDecrypt($key, string $data): string
    {
        $out = "";

        if (openssl_public_decrypt($data, $out, $key) === false) {
            throw new OpenSslException(__METHOD__);
        }

        return $out;
    }
}
