<?php

namespace PoireauFramework\Security\Crypto;

use PoireauFramework\Security\Crypto\Exception\OpenSslException;

/**
 * Signer using OpenSSL
 * @link https://www.virendrachandak.com/techtalk/encryption-using-php-openssl/
 */
class OpenSslSigner implements SignerInterface
{
    /**
     * @var object
     */
    private $config;

    /**
     * @var string
     */
    private $privateKeyFile;

    /**
     * @var string
     */
    private $publicKeyFile;


    /**
     * @param object $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->privateKeyFile = $config->private_key_file;
        $this->publicKeyFile  = $config->public_key_file;
    }

    /**
     * {@inheritdoc}
     */
    public function generateKeys(): void
    {
        $privateKey = OpenSsl::generatePrivateKey([
            "private_key_bits" => (int) ($this->config->private_key_bits ?? 4096),
            "private_key_type" => OPENSSL_KEYTYPE_RSA
        ]);

        OpenSsl::savePrivateKey($privateKey, $this->privateKeyFile);

        $keyDetails = OpenSsl::privateKeyDetails($privateKey);

        file_put_contents($this->publicKeyFile, $keyDetails["key"]);

        openssl_free_key($privateKey);
    }

    /**
     * {@inheritdoc}
     */
    public function sign(string $data): string
    {
        $key = OpenSsl::loadPrivateKey("file://" . $this->privateKeyFile);

        $details = OpenSsl::privateKeyDetails($key);

        $chunkSize = (int) ($details["bits"] / 8 - 11);

        $output = "";

        foreach (str_split(md5($data, true) . $data, $chunkSize) as $chunk) {
            $encrypted = "";

            openssl_private_encrypt($chunk, $encrypted, $key);

            $output .= $encrypted;
        }

        openssl_free_key($key);

        return base64_encode($output);
    }

    /**
     * {@inheritdoc}
     */
    public function unsign(string $data): ?string
    {
        $key = OpenSsl::loadPublicKey("file://" . $this->publicKeyFile);
        $details = OpenSsl::privateKeyDetails($key);

        $chunkSize = (int) ($details["bits"] / 8);

        $output = "";
        $input = base64_decode($data);

        foreach (str_split($input, $chunkSize) as $chunk) {
            try {
                $output .= OpenSsl::publicDecrypt($key, $chunk);
            } catch (OpenSslException $e) {
                return null;
            }
        }

        $hash   = substr($output, 0, 16); //md5 hash => 16 bytes
        $output = substr($output, 16);

        openssl_free_key($key);

        if (!hash_equals($hash, md5($output, true))) {
            return null;
        }

        return $output;
    }
}
