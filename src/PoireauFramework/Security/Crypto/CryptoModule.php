<?php

namespace PoireauFramework\Security\Crypto;

use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Module for cryptography
 */
class CryptoModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(SignerInterface::class, CypherSigner::class);

        $injector->factory(CypherSigner::class, [$this, "createCypherSigner"])->persistent(false);
        $injector->factory(OpenSslSigner::class, [$this, "createOpenSslSigner"])->persistent(false);
    }

    public function createCypherSigner(InjectorInterface $injector, $config): CypherSigner
    {
        $algo = $config->algo ?? Cypher::AES256_CTR;
        $key  = $config->key;

        return new CypherSigner(new Cypher($algo, $key));
    }

    public function createOpenSslSigner(InjectorInterface $injector, $config): OpenSslSigner
    {
        return new OpenSslSigner($config);
    }
}
