<?php

namespace PoireauFramework\Security\Crypto;

/**
 * Interface for signing strings
 * Use private_encrypt
 */
interface SignerInterface
{
    /**
     * Try to generate key pair
     */
    public function generateKeys(): void;

    /**
     * sign (encrypt with private key) tha data
     * @param string $data Data to sign
     * @return string Encrypted data
     */
    public function sign(string $data): string;

    /**
     * Check and decrypt signed data
     * @param string $data Data to decrypt
     * @return string Decrypted data, or null if data is not signed
     */
    public function unsign(string $data): ?string;
}
