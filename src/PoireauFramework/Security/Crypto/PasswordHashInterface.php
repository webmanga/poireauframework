<?php

namespace PoireauFramework\Security\Crypto;

/**
 * Interface for hashing passwords
 */
interface PasswordHashInterface
{
    /**
     * Hash the password
     * @param string $password
     * @return string
     */
    public function hash(string $password): string;

    /**
     * Check if the password corresponds to the hash
     * @param string $hash The password hash
     * @param string $password The password input
     * @return bool
     */
    public function check(string $hash, string $password): bool;
}
