<?php

namespace PoireauFramework\Security\Crypto;

/**
 * PBKDF2 implementation
 * @link https://fr.wikipedia.org/wiki/PBKDF2
 */
class Pbkdf2 implements PasswordHashInterface
{
    /**
     * @var object
     */
    private $config;


    /**
     * @param object $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function hash(string $password): string
    {
        $algo    = $this->config->algo ?? "sha512";
        $saltLen = $this->config->salt_length ?? 32;
        $cost    = $this->config->cost ?? 15;

        $salt = openssl_random_pseudo_bytes($saltLen);

        $hash = $this->pbkdf2($algo, $password, $salt, $cost);

        $hash .= $salt;

        return base64_encode($hash);
    }

    /**
     * {@inheritdoc}
     */
    public function check(string $hash, string $password): bool
    {
        $algo    = $this->config->algo ?? "sha512";
        $saltLen = $this->config->salt_length ?? 32;
        $cost    = $this->config->cost ?? 15;

        $decoded = base64_decode($hash);
        $salt    = substr($decoded, -$saltLen);
        $hashed  = substr($decoded, 0, strlen($decoded) - $saltLen);

        $newHash = (string) $this->pbkdf2($algo, $password, $salt, $cost);

        return hash_equals($hashed, $newHash);
    }

    /**
     * Process the PBKDF2 hash algo
     */
    protected function pbkdf2(string $algo, string $password, string $salt, int $cost): string
    {
        return hash_pbkdf2($algo, $password, $salt, 1 << $cost, 0, true);
    }
}
