<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Security;

use PoireauFramework\Config;
use PoireauFramework\Http\Exception\HttpException;
use PoireauFramework\Http\Input;
use PoireauFramework\Http\Output;
use PoireauFramework\Http\Response\HttpForbidden;
use PoireauFramework\Http\Response\HttpMethodNotAllowed;
use PoireauFramework\Kernel\Hooks;
use PoireauFramework\Kernel\PoireauFramework;
use PoireauFramework\Loader\Creatable;
use PoireauFramework\Loader\Loader;

/**
 * Security handler
 *
 * @author vincent
 */
class Security implements Creatable{
    /**
     * @var Config
     */
    private $config;
    
    /**
     * @var Hooks
     */
    private $hooks;
    
    const SUPER_GLOBALS = ['_GET', '_POST', '_FILES', '_SERVER', 
        '_REQUEST', '_ENV', '_COOKIE'];
    
    private $unsafeGlobals = [];
    
    public function __construct(Config $config, Hooks $hooks) {
        $this->config = $config;
        $this->hooks = $hooks;
        
        if($config->remove_super_globals){
            $this->removeSuperGlobals();
        }
        
        if(!empty($config->check_referer)){
            $hooks->addHook(PoireauFramework::HOOK_BEFORE_RUN, [$this, 'checkReferer'], 10);
        }
        
        if(!empty($config->secure_headers)){
            $hooks->addHook(PoireauFramework::HOOK_BEFORE_RUN, [$this, 'secureHeaders']);
        }
    }

    public function cleanGET($get){
        $fn = isset($this->config->get) ? $this->config->get : function(&$value){
            $value = trim(filter_var($value, FILTER_SANITIZE_STRING));
        };
        
        if(is_array($get))
            array_walk_recursive($get, $fn);
        else
            $fn($get);
        
        return $get;
    }
    
    public function cleanPOST($post){
        $fn = isset($this->config->post) ? $this->config->post : function(&$value){
            $value = trim($value);
        };
        
        if(is_array($post))
            array_walk_recursive($post, $fn);
        else
            $fn($post);
        
        return $post;
    }
    
    private function removeSuperGlobals(){
        foreach(self::SUPER_GLOBALS as $global){
            $this->unsafeGlobals[$global] = $GLOBALS[$global];
            unset($GLOBALS[$global]);
        }
    }
    
    public function &unsafeGlobal($name){
        if($this->config->remove_super_globals)
            return $this->unsafeGlobals[$name];
        
        return $GLOBALS[$name];
    }
    
    /**
     * Check is a method string is a valid HTTP method
     * @param string $method
     * @return bool true if is valid, or false
     */
    public function isValidMethod($method){
        return in_array(strtoupper($method), [
            Input::METHOD_DELETE,
            Input::METHOD_POST,
            Input::METHOD_GET,
            Input::METHOD_PUT,
            Input::METHOD_HEAD,
            Input::METHOD_OPTIONS,
            Input::METHOD_TRACE
        ]);
    }
    
    public function checkMethod($method){
        if(!$this->isValidMethod($method)){
            throw new HttpException(new HttpMethodNotAllowed([
                Input::METHOD_DELETE,
                Input::METHOD_POST,
                Input::METHOD_GET,
                Input::METHOD_PUT,
                Input::METHOD_HEAD,
                Input::METHOD_OPTIONS,
                Input::METHOD_TRACE
            ]));
        }
        
        return $method;
    }
    
    /**
     * Check if a domain is safe
     * @param string $domain The domain name
     * @return boolean
     */
    public function isValidDomain($domain){
        if(!$this->config->safe_domains)
            return true;
        
        return in_array($domain, $this->config->safe_domains);
    }
    
    /**
     * Check the referer
     * @see PoireauFramework::HOOK_BEFORE_RUN
     * @param Input $input The Input instance
     * @throws HttpException
     */
    public function checkReferer(Input $input){
        $host = $input->header('host');
        
        if(!$this->isValidDomain($host))
            throw new HttpException(new HttpForbidden('Invalid host'));
        
        $referer = $input->header('referer');
        
        //No referer, or GET request => do not check
        if(!$referer || $input->method() === Input::METHOD_GET)
            return;
        
        $refererHost = parse_url($referer, PHP_URL_HOST);
        
        if(!$this->isValidDomain($refererHost))
            throw new HttpException(new HttpForbidden('Invalid referer'));
    }
    
    /**
     * Send security headers
     * @link https://wiki.mozilla.org/Security/Guidelines/Web_Security
     * @see PoireauFramework::HOOK_BEFORE_RUN
     * @param Input $input
     * @param Output $output
     */
    public function secureHeaders(Input $input, Output $output){
        $output->removeHeader('Server');
        $output->removeHeader('X-Powered-By');
        
        if(isset($this->config->headers)){
            foreach($this->config->headers as $name => $value){
                $output->setHeader($name, $value);
            }
        }
    }

    public static function createInstance(Loader $loader) {
        return new static(
            $loader->load(Config::class)->security,
            $loader->load(Hooks::class)
        );
    }
}
