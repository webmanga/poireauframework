<?php

namespace PoireauFramework\Http;

use PoireauFramework\Http\Cookie\CookieService;
use PoireauFramework\Http\Cookie\Parser\ParserInterface;
use PoireauFramework\Http\Cookie\Parser\ParserResolverInterface;
use PoireauFramework\Http\Cookie\Parser\PlainParser;
use PoireauFramework\Http\Cookie\Parser\SignedParser;
use PoireauFramework\Http\Cookie\Parser\JsonParser;
use PoireauFramework\Http\Cookie\Parser\SerializeParser;
use PoireauFramework\Http\Cookie\Parser\SingleParserResolver;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\HttpResponseSender;
use PoireauFramework\Http\Response\ResponseSenderInterface;
use PoireauFramework\Http\Response\ResponseStack;
use PoireauFramework\Http\Response\ResponseTransformer;
use PoireauFramework\Http\Response\ResponseTransformerInterface;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Security\Crypto\SignerInterface;

/**
 * Provide module for HTTP package
 */
class HttpModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(ResponseTransformerInterface::class, ResponseTransformer::class);
        $injector->impl(ResponseSenderInterface::class, HttpResponseSender::class);
        $injector->impl(ParserResolverInterface::class, SingleParserResolver::class);

        $injector->factory(RequestInterface::class, [$this, "createRequest"]);
        $injector->factory(RequestStack::class, [$this, "createRequestStack"]);
        $injector->factory(ResponseTransformer::class, [$this, "createResponseTransformer"]);
        $injector->factory(HttpResponseSender::class, [$this, "createResponseSender"]);
        $injector->factory(ResponseStack::class, [$this, "createResponseStack"]);
        $injector->factory(CookieService::class, [$this, "createCookieService"]);
        $injector->factory(PlainParser::class, [PlainParser::class, "instance"]);

        $injector->factory(SignedParser::class, [$this, "createSignedParser"])->persistent(false);
        $injector->factory(JsonParser::class, [$this, "createJsonParser"])->persistent(false);
        $injector->factory(SerializeParser::class, [$this, "createSerializeParser"])->persistent(false);

        $injector->factory(SingleParserResolver::class, [$this, "createSingleParserResolver"]);
    }

    public function createRequest(InjectorInterface $injector): RequestInterface
    {
        return $injector->get(RequestStack::class)->current();
    }

    public function createRequestStack(InjectorInterface $injector): RequestStack
    {
        return new RequestStack(
            $this->createBaseRequest()
        );
    }

    public function createBaseRequest(): RequestInterface
    {
        return RequestBuilder::fromGlobals()->build();
    }

    public function createResponseTransformer(): ResponseTransformer
    {
        return new ResponseTransformer();
    }

    public function createResponseSender(): HttpResponseSender
    {
        return new HttpResponseSender();
    }

    public function createResponseStack(): ResponseStack
    {
        return new ResponseStack();
    }

    public function createSingleParserResolver(): SingleParserResolver
    {
        return new SingleParserResolver();
    }

    public function createCookieService(InjectorInterface $injector): CookieService
    {
        return new CookieService(
            $injector->get(RequestStack::class),
            $injector->get(ResponseStack::class),
            $injector->get(ParserResolverInterface::class)
        );
    }

    public function createSignedParser(InjectorInterface $injector, ParserInterface $parser, $config): SignedParser
    {
        return new SignedParser(
            $injector->create(SignerInterface::class, [$config]),
            $parser
        );
    }

    public function createJsonParser(InjectorInterface $injector, $config = null): JsonParser
    {
        $parser = new JsonParser();

        if (is_object($config) && !empty($config->useAssoc)) {
            $parser->useAssoc();
        }

        return $parser;
    }

    public function createSerializeParser(): SerializeParser
    {
        return new SerializeParser();
    }
}
