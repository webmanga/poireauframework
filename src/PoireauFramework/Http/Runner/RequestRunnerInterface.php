<?php

namespace PoireauFramework\Http\Runner;

use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Interface for running requests
 */
interface RequestRunnerInterface
{
    /**
     * Run the request
     * @param RequestInterface $request The request
     * @return ResponseInterface The response
     */
    public function run(RequestInterface $request): ResponseInterface;
}
