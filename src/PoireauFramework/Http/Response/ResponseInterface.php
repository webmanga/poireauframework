<?php

namespace PoireauFramework\Http\Response;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\ResponseHeadersBag;

/**
 * Interface for HTTP response
 */
interface ResponseInterface
{
    /**
     * Set http response code
     * @param int $code
     * @return $this
     */
    public function code(int $code): ResponseInterface;

    /**
     * Get the HTTP response code
     * @return int
     */
    public function getCode(): int;

    /**
     * Get headers
     * @return ResponseHeadersBag
     */
    public function headers(): ResponseHeadersBag;

    /**
     * Set headers
     * @param ResponseHeadersBag $headers
     * @return $this
     */
    public function setHeaders(ResponseHeadersBag $headers): ResponseInterface;

    /**
     * Set a response body
     * @param BagInterface $body
     * @return $this
     */
    public function body(BagInterface $body): ResponseInterface;

    /**
     * Get the response body
     * @return BagInterface
     */
    public function getBody(): ?BagInterface;
}
