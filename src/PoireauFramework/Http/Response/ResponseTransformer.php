<?php

namespace PoireauFramework\Http\Response;

use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\TextBag;

/**
 * Class ResponseTransformer
 */
class ResponseTransformer implements ResponseTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform(ResponseInterface $response, $value): ResponseInterface
    {
        if ($value === null) {
            return $response;
        }

        if ($value instanceof ResponseInterface) {
            return $value;
        }

        if ($value instanceof BagInterface) {
            return $response->body($value);
        }

        return $response->body(
            $this->createBagFromValue($value)
        );
    }

    /**
     * Create a Bag from a value
     * @param mixed $value Value to transform
     * @return BagInterface
     */
    public function createBagFromValue($value): BagInterface
    {
        if (is_array($value)) {
            return new ArrayBag($value);
        }

        return new TextBag($value);
    }
}
