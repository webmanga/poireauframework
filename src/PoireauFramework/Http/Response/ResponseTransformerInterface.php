<?php

namespace PoireauFramework\Http\Response;

/**
 * Transform a response object with a return value
 */
interface ResponseTransformerInterface
{
    /**
     * Transform the response
     *
     * @param ResponseInterface $response The response to transform
     * @param mixed $value The transform value
     *
     * @return ResponseInterface The transformed response
     */
    public function transform(ResponseInterface $response, $value): ResponseInterface;
}
