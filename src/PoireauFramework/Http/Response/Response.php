<?php

namespace PoireauFramework\Http\Response;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\ResponseHeadersBag;

/**
 * Class Response
 */
class Response implements ResponseInterface
{
    const CODE_OK           = 200;
    const CODE_CREATED      = 201;

    const CODE_MOVED_PERM   = 301;
    const CODE_MOVED_TMP    = 302;
    const CODE_SEE_OTHER    = 303;
    const CODE_REDIR_TMP    = 307;

    const CODE_BAD_REQUEST  = 400;
    const CODE_UNAUTHORIZED = 401;
    const CODE_FORBIDDEN    = 403;
    const CODE_NOT_FOUND    = 404;
    const CODE_INVAL_METHOD = 405;

    const CODE_INT_SRV_ERR  = 500;

    /**
     * @var int
     */
    private $code;

    /**
     * @var ResponseHeadersBag
     */
    private $headers;

    /**
     * @var BagInterface
     */
    private $body;


    /**
     * Construct response
     * @param int $code
     * @param ResponseHeadersBag $headers
     * @param BagInterface $body
     */
    public function __construct(int $code = 200, ResponseHeadersBag $headers = null, BagInterface $body = null)
    {
        $this->code = $code;
        $this->headers = $headers ?: new ResponseHeadersBag([]);
        $this->body = $body;
    }

     /**
      * {@inheritdoc}
      */
     public function code(int $code): ResponseInterface
     {
        $this->code = $code;

        return $this;
     }

    /**
     * {@inheritdoc}
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function headers(): ResponseHeadersBag
    {
        return $this->headers;
    }

     /**
      * {@inheritdoc}
      */
     public function setHeaders(ResponseHeadersBag $headers): ResponseInterface
    {
        $this->headers = $headers;

        return $this;
     }

     /**
      * {@inheritdoc}
      */
     public function body(BagInterface $body): ResponseInterface
    {
        $this->body = $body;

        return $this;
     }

    /**
     * {@inheritdoc}
     */
    public function getBody(): ?BagInterface
    {
        return $this->body;
    }
}
