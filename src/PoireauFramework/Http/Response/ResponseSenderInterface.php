<?php

namespace PoireauFramework\Http\Response;

/**
 * Interface for send response to client
 */
interface ResponseSenderInterface
{
    /**
     * Send a response object to the client
     * @param ResponseInterface $response Response to send
     */
    public function send(ResponseInterface $response): void;
}
