<?php

namespace PoireauFramework\Http\Response;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\ResponseHeadersBag;

/**
 * Stack for response objects
 */
class ResponseStack implements ResponseInterface
{
    /**
     * @var ResponseInterface[]
     */
    protected $stack;


    /**
     * Construct ResponseStack
     */
    public function __construct() {
        $this->stack = [];
    }

    /**
     * Push a new response to the stack
     * @param ResponseInterface response
     */
    public function push(ResponseInterface $response): void
    {
        $this->stack[] = $response;
    }

    /**
     * Remove the last Response from the stack
     * @return ResponseInterface The last response
     */
    public function pop(): ResponseInterface
    {
        return array_pop($this->stack);
    }

    /**
     * Get the current response
     * @return ResponseInterface
     */
    public function current(): ResponseInterface
    {
        return end($this->stack);
    }

    /**
     * {@inheritdoc}
     */
    public function code(int $code): ResponseInterface
    {
        $this->current()->code($code);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode(): int
    {
        return $this->current()->getCode();
    }

    /**
     * {@inheritdoc}
     */
    public function headers(): ResponseHeadersBag
    {
        return $this->current()->headers();
    }

    /**
     * {@inheritdoc}
     */
    public function setHeaders(ResponseHeadersBag $headers): ResponseInterface
    {
        $this->current()->setHeaders($headers);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function body(BagInterface $body): ResponseInterface
    {
        $this->current()->body($body);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(): ?BagInterface
    {
        return $this->current()->getBody();
    }
}
