<?php

namespace PoireauFramework\Http\Response;

/**
 * Response for redirection
 */
class RedirectResponse extends Response
{
    /**
     * Construct the redirect response
     * @param string $url The target url
     * @param int $code The http response code
     */
    public function __construct(string $url, int $code = Response::CODE_MOVED_TMP) {
        parent::__construct($code);

        $this->headers()->setLocation($url);
    }
}
