<?php

namespace PoireauFramework\Http\Response;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\HeadersBag;

/**
 * Send response to client using HTTP functions
 */
class HttpResponseSender implements ResponseSenderInterface
{
    /**
     * {@inheritdoc}
     */
    public function send(ResponseInterface $response): void
    {
        $this->sendCode($response->getCode());
        $this->sendHeaders($response->headers());
        $this->sendBody($response->getBody());
    }

    /**
     * Send HTTP code
     * @var int code
     */
    protected function sendCode(int $code): void
    {
        http_response_code($code);
    }

    /**
     * Send headers
     * @param HeadersBag $headers
     */
    protected function sendHeaders(HeadersBag $headers): void
    {
        foreach ($headers->raw() as $name => $value) {
            if (is_array($value)) {
                foreach ($value as $val) {
                    header($name . ": " . $val, false);
                }
            } else {
                header($name . ": " . $value);
            }
        }
    }

    /**
     * Send response body
     * @param BagInterface $body
     */
    protected function sendBody(BagInterface $body): void
    {
        if ($body === null) {
            return;
        }

        echo $body->raw();
    }
}
