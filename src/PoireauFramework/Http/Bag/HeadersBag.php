<?php

namespace PoireauFramework\Http\Bag;

/**
 * Bag for headers.
 * Provide normalization and simple accessors
 */
class HeadersBag extends ArrayBag
{
    /**
     * Create and normalize headers bag
     * @param array $data Headers $data
     */
    public function __construct(array $data = []) {
        parent::__construct(self::normalizeArray($data));
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $name, $value): BagInterface
    {
        return parent::set(self::normalizeName($name), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $name, $value): BagInterface
    {
        return parent::add(self::normalizeName($name), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name)
    {
        return parent::get(self::normalizeName($name));
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $name): bool
    {
        return parent::has(self::normalizeName($name));
    }

    /**
     * Get the Content-Type (mime-type)
     * @return string
     */
    public function contentType(): string
    {
        $type = parent::get("Content-Type");

        return trim(explode(";", $type, 2)[0]);
    }

    /**
     * Set the response content type
     * @return $this
     */
    public function setContentType(string $contentType, array $attributes = []): HeadersBag
    {
        $line = $contentType;

        foreach ($attributes as $k => $v) {
            $line .= "; " . $k .  "=" . $v;
        }

        parent::set("Content-Type", $line);

        return $this;
    }

    /**
     * Set the Location header (for redirection)
     * @return $this
     */
    public function setLocation(string $url): HeadersBag
    {
        $this->set("Location", $url);

        return $this;
    }

    /**
     * Normalize an headers array
     * @param array $a The headers to normalize
     * @return array Normalized array
     */
    static public function normalizeArray(array $a): array
    {
        $normalized = [];

        foreach ($a as $name => $value)  {
            if (is_string($name)) {
                $normalized[self::normalizeName($name)] = $value;
            } else {
                $data = explode(":", $value, 2);

                $lineName = trim(self::normalizeName($data[0]));
                $lineValue = trim($data[1]);

                if (!isset($normalized[$lineName])) {
                    $normalized[$lineName] = $lineValue;
                } elseif (is_array($normalized[$lineName])) {
                    $normalized[$lineName][] = $lineValue;
                } else {
                    $normalized[$lineName] = [
                        $normalized[$lineName],
                        $lineValue
                    ];
                }
            }
        }

        return $normalized;
    }

    /**
     * Normalize header name.
     * content_type => Content-Type
     * @param string $name Name to normalize
     * @return string Normalized name
     */
    static public function normalizeName(string $name): string
    {
        $normalized = "";
        $b = false;

        foreach (explode("-", str_replace("_", "-", $name)) as $part) {
            if ($b) {
                $normalized .= "-";
            } else {
                $b = true;
            }

            $normalized .= ucfirst(strtolower($part));
        }

        return $normalized;
    }
}
