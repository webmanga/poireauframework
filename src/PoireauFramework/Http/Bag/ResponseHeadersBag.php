<?php

namespace PoireauFramework\Http\Bag;

use PoireauFramework\Http\Cookie\CookieBuilder;

/**
 * HeadersBag for Response object
 * Adds Set-Cookie header handling
 */
class ResponseHeadersBag extends HeadersBag
{
    /**
     * Get response cookies (Set-Cookie)
     * @return array|\PoireauFramework\Http\Cookie\CookieBuilder[]
     */
    public function getCookies(): array
    {
        if (!$this->has("Set-Cookie")) {
            return [];
        }

        $cookies = $this->get("Set-Cookie");

        if (!is_array($cookies)) {
            return [$cookies];
        }

        return $cookies;
    }

    /**
     * Add a new cookie in the response headers
     * @param string|\PoireauFramework\Http\Cookie\CookieBuilder cookie The cookie to add. If string should be the complete cookie string
     * @return $this
     */
    public function addCookie($cookie): ResponseHeadersBag
    {
        $this->add("Set-Cookie", $cookie);

        return $this;
    }
}
