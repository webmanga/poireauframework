<?php

namespace PoireauFramework\Http\Bag;

/**
 * Handle Bag with simple PHP types (float, string, array, int)
 * @todo Move to "Util" package ?
 */
interface TypedBagInterface
{
    /**
     * Get a scalar value from the bag
     * The value will not be converted
     * If the value is not found, will return $defaultValue
     */
    public function scalar(string $name, $defaultValue = null);

    /**
     * Get a string value from the bag
     * Do the same behavior than TypedBagInterface::scalar() But ensure that the returned value is a string
     */
    public function string(string $name, string $defaultValue = ""): string;

    /**
     * Get a integer value from the bag
     */
    public function integer(string $name, int $defaultValue = 0): int;

    /**
     * Get a float value from the bag
     */
    public function float(string $name, float $defaultValue = 0.0): float;

    /**
     * Get an array value from the bag
     * If the found value is not an array, this method will return an empty array (value not found)
     */
    public function array(string $name): array;
}

