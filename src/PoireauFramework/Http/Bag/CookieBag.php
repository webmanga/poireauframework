<?php

namespace PoireauFramework\Http\Bag;

/**
 * Bag for handling request cookies
 * Adds prefixes handling
 * @link https://tools.ietf.org/html/draft-ietf-httpbis-cookie-prefixes-00
 */
class CookieBag extends ArrayBag
{
    const PREFIX_SECURE = "__Secure-";
    const PREFIX_HOST   = "__Host-";

    /**
     * Get a secure cookie (PREFIX_SECURE)
     * @return string
     */
    public function secure(string $name)
    {
        return $this->get(self::PREFIX_SECURE . $name);
    }

    /**
     * Get an host cookie (PREFIX_HOST)
     * @return string
     */
    public function host(string $name)
    {
        return $this->get(self::PREFIX_HOST . $name);
    }

    /**
     * Set a PREFIX_SECURE cookie
     * @param string $name The cookie name
     * @param mixed $value
     * @return $this
     */
    public function setSecure(string $name, $value): self
    {
        return $this->set(self::PREFIX_SECURE . $name, $value);
    }

    /**
     * Set a PREFIX_HOST cookie
     * @param string $name The cookie name
     * @param mixed $value
     * @return $this
     */
    public function setHost(string $name, $value): self
    {
        return $this->set(self::PREFIX_HOST . $name, $value);
    }
}
