<?php

namespace PoireauFramework\Http\Bag;

/**
 * Bag for request query
 */
class QueryBag extends ArrayBag implements TypedBagInterface
{
    /**
     * {@inheritdoc}
     */
    public function scalar(string $name, $defaultValue = null)
    {
        if (!$this->has($name)) {
            return $defaultValue;
        }

        $value = $this->get($name);

        if (is_array($value)) {
            return $defaultValue;
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function string(string $name, string $defaultValue = ""): string
    {
        return (string) $this->scalar($name, $defaultValue);
    }

    /**
     * {@inheritdoc}
     */
    public function integer(string $name, int $defaultValue = 0): int
    {
        return (int) $this->scalar($name, $defaultValue);
    }

    /**
     * {@inheritdoc}
     */
    public function float(string $name, float $defaultValue = 0.0): float
    {
        return (float) $this->scalar($name, $defaultValue);
    }

    /**
     * {@inheritdoc}
     */
    public function array(string $name): array
    {
        if (!$this->has($name)) {
            return [];
        }

        $value = $this->get($name);

        return is_array($value) ? $value : [];
    }
}
