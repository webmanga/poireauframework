<?php

namespace PoireauFramework\Http\Bag;

/**
 * Interface for bag objects
 */
interface BagInterface
{
    /**
     * Get a value from the bag
     *
     * @param string $name The parameter name
     *
     * @return mixed The value, or null if not found
     */
    public function get(string $name);

    /**
     * Check if the bag has a value
     * @param string $name The parameter name
     * @return bool
     */
    public function has(string $name): bool;

    /**
     * Get raw values of the bag
     * @return mixed
     */
    public function raw();

    /**
     * Set (override) a value into the bag
     * @param string $name The parameter name
     * @param mixed $value The value
     * @return $this
     */
    public function set(string $name, $value): self;

    /**
     * Add a value into the bag.
     * - If the value if not an array, cast to array an push
     * - If the value do not exists, set the value
     * - If the value is an array, push
     * @param string $name The parameter name
     * @param mixed $value The value
     * @return $this
     */
    public function add(string $name, $value): self;
}