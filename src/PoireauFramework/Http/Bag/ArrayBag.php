<?php

namespace PoireauFramework\Http\Bag;

/**
 * Bag using array as data container
 */
class ArrayBag implements BagInterface
{
    /**
     * The bag datas
     * @var array
     */
    protected $data;

    /**
     * Construct the bag
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $name): bool
    {
        return isset($this->data[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function raw()
    {
        return $this->data;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $name, $value): BagInterface
    {
        $this->data[$name] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $name, $value): BagInterface
    {
        if (!isset($this->data[$name])) {
            $this->data[$name] = $value;
        } else {
            if (is_array($this->data[$name])) {
                $this->data[$name][] = $value;
            } else {
                $this->data[$name] = [$this->data[$name], $value];
            }
        }

        return $this;
    }
}
