<?php

namespace PoireauFramework\Http\Bag;

/**
 * Class TextBag
 */
class TextBag implements BagInterface
{
    /**
     * @var string
     */
    private $content;


    /**
     * Create new TextBag
     * @param string $content
     */
    public function __construct(string $content = "") {
        $this->content = $content;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $name): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function raw()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $name, $value): BagInterface
    {
        $this->content = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $name, $value): BagInterface
    {
        $this->content .= $value;

        return $this;
    }
}
