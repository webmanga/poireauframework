<?php

namespace PoireauFramework\Http\Exception;

use RuntimeException;

/**
 * Base exception class for HTTP
 */
class HttpException extends RuntimeException {

}
