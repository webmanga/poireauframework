<?php

namespace PoireauFramework\Http\Exception;

use PoireauFramework\Http\Response\Response;

/**
 * Unauthorized 401
 */
class UnauthorizedHttpException extends HttpException
{
    public function __construct(string $message = "Authentification needed to access the page")
    {
        parent::__construct($message, Response::CODE_UNAUTHORIZED);
    }
}
