<?php

namespace PoireauFramework\Http\Exception;

use PoireauFramework\Http\Response\Response;

/**
 * Not found 404
 */
class NotFoundHttpException extends HttpException
{
    public function __construct(string $message = "Resource not found")
    {
        parent::__construct($message, Response::CODE_NOT_FOUND);
    }
}
