<?php

namespace PoireauFramework\Http\Exception;

use PoireauFramework\Http\Response\Response;

/**
 * Forbidden 403
 */
class ForbiddenHttpException extends HttpException
{
    public function __construct(string $message = "You don't have access to this page")
    {
        parent::__construct($message, Response::CODE_FORBIDDEN);
    }
}
