<?php

namespace PoireauFramework\Http\Exception;

use PoireauFramework\Http\Response\Response;

/**
 * Bad request 400
 */
class BadRequestHttpException extends HttpException
{
    public function __construct(string $message = "Bad request")
    {
        parent::__construct($message, Response::CODE_BAD_REQUEST);
    }
}
