<?php

namespace PoireauFramework\Http\Exception;

use PoireauFramework\Http\Response\Response;

/**
 * Method not allowed 405
 */
class MethodNotAllowedHttpException extends HttpException
{
    /**
     * @var string[]
     */
    private $methods;

    public function __construct($methods, string $message = "Method not allowed")
    {
        parent::__construct($message, Response::CODE_NOT_FOUND);

        $this->methods = (array) $methods;
    }

    /**
     * Get allowed methods
     * @return string[]
     */
    public function methods(): array
    {
        return $this->methods;
    }
}
