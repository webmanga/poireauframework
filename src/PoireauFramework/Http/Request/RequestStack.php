<?php

namespace PoireauFramework\Http\Request;

/**
 * Stack of requests
 */
class RequestStack
{
    /**
     * @var RequestInterface[]
     */
    protected $stack;


    /**
     * Construct RequestStack
     */
    public function __construct(RequestInterface $first)
    {
        $this->stack = [$first];
    }

    /**
     * Push a new request to the stack
     */
    public function push(RequestInterface $request): void
    {
        $this->stack[] = $request;
    }

    /**
     * Remove and get the last request.
     * Do not remove the first request
     * @return RequestInterface
     */
    public function pop(): RequestInterface
    {
        if ($this->isFirst())  {
            return $this->stack[0];
        }

        return array_pop($this->stack);
    }

    /**
     * Get the current (last) request
     * @return RequestInterface
     */
    public function current(): RequestInterface
    {
        return end($this->stack);
    }

    /**
     * Get the first request
     * @return RequestInterface
     */
    public function first(): RequestInterface
    {
        return $this->stack[0];
    }

    /**
     * Check if the stack only contains the first request
     * @return bool
     */
    public function isFirst(): bool
    {
        return count($this->stack) === 1;
    }
}
