<?php

namespace PoireauFramework\Http\Request;

use PoireauFramework\Http\Bag\QueryBag;
use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\CookieBag;
use PoireauFramework\Http\Bag\HeadersBag;

/**
 * Interface for HTTP requests
 */
interface RequestInterface
{
    /**
     * Get the http method name
     * @return string
     */
    public function method(): string;

    /**
     * Get the query path info
     * ex: http://example.com/my/root/index.php/my/path?query=value => /my/path
     * Always starts with '/'
     *
     * @return string
     */
    public function path(): string;

    /**
     * Get the running script file path
     * @return string
     */
    public function script(): ?string;

    /**
     * Get the query parameters ($_GET)
     * @return QueryBag
     */
    public function query(): QueryBag;

    /**
     * Get the request headers
     * @return HeadersBag
     */
    public function headers(): HeadersBag;

    /**
     * Get the request body.
     * Can be either $_POST, or php://input
     * The returns type depends of the request Content-Type
     * @return BagInterface
     */
    public function body(): ?BagInterface;

    /**
     * Get the request cookies
     * @return CookieBag
     */
    public function cookies(): CookieBag;

    /**
     * Attach a value to the request
     * @param string $key The attachment key
     * @param mixed $value The attachment value
     */
    public function attach(string $key, $value): void;

    /**
     * Get the attachment
     * @param string $key The attachment key
     * @return mixed The attachment value
     */
    public function attachment(string $key);

    /**
     * Check if the request has the attachment
     * @param string $key The attachment key
     * @return bool
     */
    public function has(string $key): bool;
}