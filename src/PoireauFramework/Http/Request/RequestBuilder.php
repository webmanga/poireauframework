<?php

namespace PoireauFramework\Http\Request;

use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Bag\QueryBag;
use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\CookieBag;
use PoireauFramework\Http\Bag\HeadersBag;

/**
 * Builder for {@link Request}
 */
class RequestBuilder
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $script;

    /**
     * @var QueryBag
     */
    private $query;

    /**
     * @var HeadersBag
     */
    private $headers;

    /**
     * @var BagInterface
     */
    private $body;

    /**
     * @var CookieBag
     */
    private $cookies;


    public function __construct() {
        $this->method  = Request::METHOD_GET;
        $this->path    = "/";
        $this->script  = null;
        $this->query   = new QueryBag();
        $this->headers = new HeadersBag();
        $this->body    = null;
        $this->cookies = new CookieBag();
    }

    /**
     * @param string $method
     * @return $this
     */
    public function method(string $method): RequestBuilder
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function path(string $path): RequestBuilder
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param string $script
     * @return $this
     */
    public function script(string $script): RequestBuilder
    {
        $this->script = $script;
        return $this;
    }

    /**
     * @return QueryBag
     */
    public function query(): QueryBag
    {
        return $this->query;
    }

    /**
     * @return HeadersBag
     */
    public function headers(): HeadersBag
    {
        return $this->headers;
    }

    /**
     * @param BagInterface $body
     * @return $this
     */
    public function body(BagInterface $body): RequestBuilder
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return CookieBag
     */
    public function cookies(): CookieBag
    {
        return $this->cookies;
    }

    /**
     * Build the Request object
     * @return RequestInterface
     */
    public function build(): RequestInterface
    {
        return new Request(
            $this->method,
            $this->path,
            $this->script,
            $this->query,
            $this->headers,
            $this->body,
            $this->cookies
        );
    }

    /**
     * Initialize RequestBuilder using PHP globals
     * @return RequestBuilder
     */
    static public function fromGlobals(): RequestBuilder
    {
        $builder = new RequestBuilder();

        $builder->method   = isset($_SERVER["REQUEST_METHOD"]) ? strtoupper($_SERVER["REQUEST_METHOD"]) : Request::METHOD_INTERNAL;
        $builder->path     = $_SERVER["PATH_INFO"] ?? "/";
        $builder->script   = $_SERVER["SCRIPT_NAME"];
        $builder->query    = new QueryBag($_GET);
        $builder->cookies  = new CookieBag($_COOKIE);

        if (!empty($_POST)) {
            $builder->body = new ArrayBag($_POST);
        }

        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === "HTTP_") {
                $builder->headers->set(substr($key, 5), $value);
            }
        }

        return $builder;
    }
}
