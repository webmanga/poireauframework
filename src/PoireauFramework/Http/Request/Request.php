<?php

namespace PoireauFramework\Http\Request;

use PoireauFramework\Http\Bag\QueryBag;
use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Bag\CookieBag;
use PoireauFramework\Http\Bag\HeadersBag;

/**
 * Request object implementation
 */
class Request implements RequestInterface
{
    const METHOD_GET      = "GET";
    const METHOD_POST     = "POST";
    const METHOD_PUT      = "PUT";
    const METHOD_DELETE   = "DELETE";
    const METHOD_PATCH    = "PATCH";
    const METHOD_OPTION   = "OPTION";
    const METHOD_TRACE    = "TRACE";
    const METHOD_HEAD     = "HEAD";
    const METHOD_INTERNAL = "__INTERNAL__";


    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $script;

    /**
     * @var QueryBag
     */
    private $query;

    /**
     * @var HeadersBag
     */
    private $headers;

    /**
     * @var BagInterface
     */
    private $body;

    /**
     * @var CookieBag
     */
    private $cookies;

    /**
     * @var array
     */
    protected $attachments = [];


    /**
     * Construct the request
     * @param string $method
     * @param string $path
     * @param string $script
     * @param QueryBag $query
     * @param HeadersBag $headers
     * @param BagInterface $body
     * @param CookieBag $cookies
     */
    public function __construct(string $method, string $path, ?string $script, QueryBag $query, HeadersBag $headers, ?BagInterface $body, CookieBag $cookies)
    {
        $this->method  = $method;
        $this->path    = $path;
        $this->script  = $script;
        $this->query   = $query;
        $this->headers = $headers;
        $this->body    = $body;
        $this->cookies = $cookies;
    }

    /**
     * {@inheritdoc}
     */
    public function method(): string
    {
        return $this->method;
    }

    /**
     * {@inheritdoc}
     */
    public function path(): string
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function script(): ?string
    {
        return $this->script;
    }

    /**
     * {@inheritdoc}
     */
    public function query(): QueryBag
    {
        return $this->query;
    }

    /**
     * {@inheritdoc}
     */
    public function headers(): HeadersBag
    {
        return $this->headers;
    }

    /**
     * {@inheritdoc}
     */
    public function body(): ?BagInterface
    {
        return $this->body;
    }

    /**
     * {@inheritdoc}
     */
    public function cookies(): CookieBag
    {
        return $this->cookies;
    }

    /**
     * {@inheritdoc}
     */
    public function attach(string $key, $value): void
    {
        $this->attachments[$key] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function attachment(string $key)
    {
        return $this->attachments[$key];
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $key): bool
    {
        return isset($this->attachments[$key]);
    }
}
