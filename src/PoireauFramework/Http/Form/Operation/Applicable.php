<?php

namespace PoireauFramework\Http\Form\Operation;

/**
 * Interface that represents element which operation can be applicated on it
 */
interface Applicable
{
    /**
     * Apply operation
     * @param FormOperationInterface $operation Operation to perform
     * @return FormOperationInterface The operation
     */
    public function apply(FormOperationInterface $operation): FormOperationInterface;
}
