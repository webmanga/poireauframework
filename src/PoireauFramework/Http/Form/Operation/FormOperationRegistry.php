<?php

namespace PoireauFramework\Http\Form\Operation;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Validation\ValidationContext;

/**
 * Registry for standard form operations
 */
class FormOperationRegistry
{
    /**
     * Get @see FormInterface::bind() operation
     */
    public function bind(BagInterface $bag): FormOperationInterface
    {
        return new Bind($bag);
    }

    /**
     * Get @see FormInterface::validate() operation
     */
    public function validate(FormInterface $form): FormOperationInterface
    {
        return new Validate(new ValidationContext($form));
    }

    /**
     * Get @see FormInterface::toArray() operation
     */
    public function toArray(): FormOperationInterface
    {
        return new ToArray();
    }

    /**
     * Get @see FormInterface::export() operation
     */
    public function export($target): FormOperationInterface
    {
        return new Export($target);
    }
}
