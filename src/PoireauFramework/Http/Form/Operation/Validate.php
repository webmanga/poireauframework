<?php

namespace PoireauFramework\Http\Form\Operation;

use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Validation\Error;
use PoireauFramework\Http\Form\Validation\ValidationContext;

/**
 * Class Validate
 */
class Validate implements FormOperationInterface
{
    /**
     * @var ValidationContext
     */
    private $context;


    /**
     * Construct the operation with context
     */
    public function __construct(ValidationContext $context)
    {
        $this->context = $context;
    }

    /**
     * Get the validation context
     */
    public function context(): ValidationContext
    {
        return $this->context;
    }

    /**
     * {@inheritdoc}
     */
    public function applyOnField(Field $field): void
    {
        $this->context->setCurrent($field);

        $result = $field->validate($this->context);

        // Field is not valid, but no reason given
        if (!$result && !$this->context->hasError($field)) {
            $this->context->setError(Error::undefined());
        }
    }
}
