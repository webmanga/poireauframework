<?php

namespace PoireauFramework\Http\Form\Operation;

use PoireauFramework\Http\Form\Element\Field;

/**
 * Perform operation on form elements
 * @see Applicable
 */
interface FormOperationInterface
{
    public function applyOnField(Field $field): void;
}
