<?php

namespace PoireauFramework\Http\Form\Operation;

use PoireauFramework\Http\Form\Element\Field;

/**
 * Bind data to form elements
 * @see FormOperationRegistry::toArray()
 * @see FormInterface::toArray()
 */
class ToArray implements FormOperationInterface
{
    /**
     * @var array
     */
    private $data = [];


    /**
     * {@inheritdoc}
     */
    public function applyOnField(Field $field): void
    {
        $this->data[$field->name()] = $field->value();
    }

    /**
     * Get the form data as array
     */
    public function data(): array
    {
        return $this->data;
    }
}
