<?php

namespace PoireauFramework\Http\Form\Operation;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Form\Element\Field;

/**
 * Bind data to form elements
 * @see FormOperationRegistry::bind()
 * @see FormInterface::bind()
 */
class Bind implements FormOperationInterface
{
    /**
     * @var BagInterface
     */
    protected $data;


    /**
     *
     */
    public function __construct(BagInterface $data)
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function applyOnField(Field $field): void
    {
        if ($this->data->has($field->name())) {
            $field->setValue(
                $this->data->get($field->name())
            );
        }
    }
}
