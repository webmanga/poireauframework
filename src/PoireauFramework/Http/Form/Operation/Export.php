<?php

namespace PoireauFramework\Http\Form\Operation;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Form\Element\Field;

/**
 * Export for data to an object
 * @see FormOperationRegistry::export()
 * @see FormInterface::export()
 */
class Export implements FormOperationInterface
{
    /**
     * @var object
     */
    private $target;


    /**
     *
     */
    public function __construct($target)
    {
        $this->target = $target;
    }

    /**
     * {@inheritdoc}
     */
    public function applyOnField(Field $field): void
    {
        $field->hydrate($this->target);
    }

    /**
     * Get the form data
     */
    public function target()
    {
        return $this->target;
    }
}