<?php

namespace PoireauFramework\Http\Form;

use PoireauFramework\Http\Form\Operation\Applicable;
use PoireauFramework\Http\Form\Operation\FormOperationInterface;

/**
 * Interface for aggregate of FormElementInterface
 */
interface ElementAggregateInterface extends Applicable
{

}
