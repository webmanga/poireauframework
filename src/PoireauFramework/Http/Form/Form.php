<?php

namespace PoireauFramework\Http\Form;

use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Form\Operation\FormOperationInterface;
use PoireauFramework\Http\Form\Operation\FormOperationRegistry;

/**
 * Form class
 */
class Form implements FormInterface
{
    /**
     * @var \PoireauFramework\Http\Form\FormElementInterface[]
     */
    protected $elements = [];

    /**
     * @var FormOperationRegistry
     */
    protected $operations;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var object|string
     */
    protected $attachment = null;


    /**
     *
     */
    public function __construct(FormOperationRegistry $operations, array $elements)
    {
        $this->operations = $operations;
        $this->elements   = $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name): FormElementInterface
    {
        return $this->elements[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    /**
     * {@inheritdoc}
     */
    public function error(string $name)
    {
        return $this->errors[$name] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function hasError(string $name): bool
    {
        return isset($this->errors[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * {@inheritdoc}
     */
    public function attach($attachment): FormInterface
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bind(BagInterface $bag): FormInterface
    {
        $this->apply($this->operations->bind($bag));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(): bool
    {
        $operation = $this->apply($this->operations->validate($this));

        $this->errors = $operation->context()->errors();

        return !$operation->context()->hasErrors();
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return $this->apply($this->operations->toArray())->data();
    }

    /**
     * {@inheritdoc}
     */
    public function export($target = null)
    {
        if ($target === null) {
            $target = $this->attachment;
        }

        if ($target === null) {
            throw new \LogicException("No target or attachment given. Cannot export the form data.");
        }

        if (is_string($target)) {
            $target = new $target();
        }

        return $this->apply($this->operations->export($target))->target();
    }

    /**
     * {@inheritdoc}
     */
    public function apply(FormOperationInterface $operation): FormOperationInterface
    {
        foreach ($this->elements as $element) {
            $element->apply($operation);
        }

        return $operation;
    }
}
