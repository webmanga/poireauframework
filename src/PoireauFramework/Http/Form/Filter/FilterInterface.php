<?php

namespace PoireauFramework\Http\Form\Filter;

/**
 * Interface for filtering form values
 */
interface FilterInterface
{
    /**
     * Filter the input value and parse to a valid PHP value
     * @param string|array value The input value
     * @return mixed
     */
    public function fromHttp($value);

    /**
     * Convert the PHP value from field
     * to a valid HTTP value (i.e. can be used into html form, or as request)
     * @param mixed $value The PHP value
     * @return string|array
     */
    public function toHttp($value);
}
