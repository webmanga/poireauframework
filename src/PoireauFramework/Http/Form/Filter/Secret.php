<?php

namespace PoireauFramework\Http\Form\Filter;

/**
 * Filter for secret value (i.e. do not display value on field)
 */
class Secret implements FilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function fromHttp($value)
    {
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toHttp($value)
    {
        return "";
    }
}
