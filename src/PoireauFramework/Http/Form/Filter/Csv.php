<?php

namespace PoireauFramework\Http\Form\Filter;

/**
 * Filter for CSV input strings
 * @see http://php.net/manual/fr/function.str-getcsv.php
 */
class Csv implements FilterInterface
{
    /**
     * @var string
     */
    private $delimiter = ",";

    /**
     * @var string
     */
    private $enclosure = "\"";

    /**
     * @var string
     */
    private $escape = "\\";


    /**
     * Set the delimiter
     * @return $this
     */
    public function delimiter(string $delimiter): Csv
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    /**
     * Set the escape char
     * @return $this
     */
    public function escape(string $escape): Csv
    {
        $this->escape = $escape;

        return $this;
    }

    /**
     * Set the enclosure char
     * @return $this
     */
    public function enclosure(string $enclosure): Csv
    {
        $this->enclosure = $enclosure;

        return $this;
    }

     /**
      * {@inheritdoc}
      */
     public function fromHttp($value)
     {
        $out = [];
        $replacements = [
            $this->escape . $this->enclosure => $this->enclosure,
            $this->escape . $this->escape    => $this->escape,
        ];

        $raw = str_getcsv(
            $value,
            $this->delimiter,
            $this->enclosure,
            $this->escape
        );

        foreach ($raw as $line) {
            $out[] = strtr($line, $replacements);
        }

        return $out;
     }

     /**
      * {@inheritdoc}
      */
    public function toHttp($value)
    {
        $out = [];

        $replacements = [
            $this->enclosure => $this->escape . $this->enclosure,
            $this->escape    => $this->escape . $this->escape,
        ];

        foreach ((array) $value as $val) {
            $newVal = strtr($val, $replacements);

            if (strpos($val, $this->delimiter) !== false) {
                $out[] = $this->enclosure . $newVal . $this->enclosure;
            } else {
                $out[] = $newVal;
            }
        }

        return implode($this->delimiter, $out);
    }
}
