<?php

namespace PoireauFramework\Http\Form\Filter;

/**
 * Filter using closure
 */
class Closure implements FilterInterface
{
    /**
     * @var callable
     */
    private $from;

    /**
     * @var callable
     */
    private $to;


    /**
     * Create the filter
     * @param callable $from The fromHttp function. Get the input value as parameter
     * @param callable $to The toHttp function. Get the PHP value as parameter
     */
    public function __construct(?callable $from, callable $to = null)
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * {@inheritdoc}
     */
    public function fromHttp($value)
    {
        if ($this->from !== null) {
            return call_user_func($this->from, $value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toHttp($value)
    {
        if ($this->to !== null) {
            return call_user_func($this->to, $value);
        }

        return $value;
    }
}
