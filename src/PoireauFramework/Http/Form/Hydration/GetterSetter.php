<?php

namespace PoireauFramework\Http\Form\Hydration;

use PoireauFramework\Http\Form\FormElementInterface;

/**
 * Hydrator using getter and setter
 */
class GetterSetter implements HydratorInterface, ExtractorInterface
{
    /**
     * @var string|null
     */
    private $propertyName;

    /**
     * @var string|null
     */
    private $getterPrefix;

    /**
     * @var string|null
     */
    private $setterPrefix;


    /**
     *
     */
    public function __construct(string $propertyName = null, string $getterPrefix = null, string $setterPrefix = "set") {
        $this->propertyName = $propertyName;
        $this->getterPrefix = $getterPrefix;
        $this->setterPrefix = $setterPrefix;
    }

    /**
     * {@inheritdoc}
     */
    public function hydrate($target, FormElementInterface $element): void
    {
        $method = $this->propertyName ?: $element->name();

        if ($this->setterPrefix !== null) {
            $method = $this->setterPrefix . ucfirst($method);
        }

        $target->$method($element->value());
    }

    /**
     * {@inheritdoc}
     */
    public function extract($target, FormElementInterface $element): void
    {
        $method = $this->propertyName ?: $element->name();

        if ($this->getterPrefix !== null) {
           $method = $this->getterPrefix . ucfirst($method);
        }

        $element->setValue($target->$method());
    }
}
