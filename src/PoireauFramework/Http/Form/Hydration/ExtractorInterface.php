<?php

namespace PoireauFramework\Http\Form\Hydration;

use PoireauFramework\Http\Form\FormElementInterface;

/**
 * Interface for extract data from an object and set to a form element
 */
interface ExtractorInterface
{
    /**
     * Extract from the target object
     * @param object $target Object to hydrate
     * @param FormElementInterface $element The form element
     */
    public function extract($target, FormElementInterface $element): void;
}
