<?php

namespace PoireauFramework\Http\Form\Hydration;

use PoireauFramework\Http\Form\FormElementInterface;

/**
 * Interface for hydrate an object from a form element
 */
interface HydratorInterface
{
    /**
     * Hydrate the target object
     * @param object $target Object to hydrate
     * @param FormElementInterface $element The form element
     */
    public function hydrate($target, FormElementInterface $element): void;
}
