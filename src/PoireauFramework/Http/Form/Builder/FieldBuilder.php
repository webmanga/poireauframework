<?php

namespace PoireauFramework\Http\Form\Builder;

use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Filter\Closure as ClosureFilter;
use PoireauFramework\Http\Form\Filter\FilterInterface;
use PoireauFramework\Http\Form\Hydration\ExtractorInterface;
use PoireauFramework\Http\Form\Hydration\GetterSetter;
use PoireauFramework\Http\Form\Hydration\HydratorInterface;
use PoireauFramework\Http\Form\Validation\Closure as ClosureRule;
use PoireauFramework\Http\Form\Validation\RuleInterface;

/**
 * Builder for @see \PoireauFramework\Http\Form\Element\Field
 */
class FieldBuilder
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string|bool
     */
    private $required = false;

    /**
     * @var FilterInterface
     */
    private $filters = [];

    /**
     * @var RuleInterface
     */
    private $rules = [];

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @var ExtractorInterface
     */
    private $extractor;


    /**
     * Construct the builder
     * @param string $name The field name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Set the field name
     * @return $this
     */
    public function name(string $name): FieldBuilder
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the field as required
     * @param bool|string required false for optional, true for required, string for required message
     * @return $this
     */
    public function required($required = true): FieldBuilder
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Add a new rule
     * <code>
     * $builder->rule(new Length(4, 12)); // Add Length rule
     * $builder->rule(function ($value, ValidationContext $context) {
     *     if (!$service->check($context->form()->get("other"), $value)) {
     *         $context->setError("My error");
     *         return false;
     *     }
     *     return true;
     * }); //Add a custom rule with closure
     * $builder->rule([$this, "myRule"]); // Add a custom rule with method
     * </code>
     * @param callable|\PoireauFramework\Http\Form\Validation\RuleInterface rule The rule to add
     * @return $this
     */
    public function rule($rule): FieldBuilder
    {
        if (!$rule instanceof RuleInterface) {
            return $this->rule(new ClosureRule($rule));
        }

        $this->rules[] = $rule;

        return $this;
    }

    /**
     * Add a new filter
     * <code>
     * $builder->filter(new Csv()); // Add Csv filter
     * $builder->filter(function ($value) {
     *     return array_unshift($value, "Hello");
     * }); //Add a custom filter with closure
     * $builder->rule([$this, "myRule"]); // Add a custom filter with method
     * </code>
     * @param callable|\PoireauFramework\Http\Form\Filter\FilterInterface filter The filter to add
     * @return $this
     */
    public function filter($filter): FieldBuilder
    {
        if (!$filter instanceof FilterInterface) {
            return $this->filter(new ClosureFilter($filter));
        }

        $this->filters[] = $filter;

        return $this;
    }

    /**
     * Set extractor and/or hydrator
     * @return $this
     */
    public function set($obj): FieldBuilder
{
        if ($obj instanceof HydratorInterface) {
            $this->hydrator = $obj;
        }

        if ($obj instanceof ExtractorInterface) {
            $this->extractor = $obj;
        }

        return $this;
    }

    /**
     * Set the GetterSetter hydrator / extractor
     * @see GetterSetter for parameters
     * @return $this
     */
    public function getSet($propertyName = null, $get = null, $set = "set"): FieldBuilder
    {
        $gs = new GetterSetter($propertyName, $get, $set);

        $this->extractor = $gs;
        $this->hydrator = $gs;

        return $this;
    }

    /**
     * Build the field
     */
    public function build(): Field
    {
        return new Field(
            $this->name,
            $this->filters,
            $this->required,
            $this->rules,
            $this->hydrator,
            $this->extractor
        );
    }
}