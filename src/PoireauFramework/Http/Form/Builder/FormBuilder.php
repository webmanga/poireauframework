<?php

namespace PoireauFramework\Http\Form\Builder;

use PoireauFramework\Http\Form\Form;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Operation\FormOperationRegistry;

/**
 * Builder for @see \PoireauFramework\Http\Form\FormInterface
 */
class FormBuilder
{
    /**
     * @var FormOperationRegistry
     */
    private $operations;

    /**
     * @var FieldBuilder[]
     */
    private $elements = [];

    /**
     * @var object|string
     */
    private $attachment;


    /**
     * Set custom form operations
     * @return $this
     */
    public function operations(FormOperationRegistry $operations): FormBuilder
    {
        $this->operations = $operations;

        return $this;
    }

    /**
     * Add a field into the form
     * @param string $name The Field name
     */
    public function field(string $name): FieldBuilder
    {
        return $this->elements[] = new FieldBuilder($name);
    }

    /**
     * Attach an object to the form.
     * The attachment can ben a target object (i.e. instance), or class name
     *
     * @param string|object attachment
     * @return $this
     *
     * @see FormInterface::attach()
     */
    public function attach($attachment): FormBuilder
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * Build the form
     */
    public function build(): FormInterface
    {
        $elements = [];

        foreach ($this->elements as $builder) {
            $element = $builder->build();
            $elements[$element->name()] = $element;
        }

        $form = new Form(
            $this->operations ? $this->operations : new FormOperationRegistry(),
            $elements
        );

        if ($this->attachment !== null) {
            $form->attach($this->attachment);
        }

        return $form;
    }
}
