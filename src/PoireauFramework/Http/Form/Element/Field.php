<?php

namespace PoireauFramework\Http\Form\Element;

use PoireauFramework\Http\Form\FormElementInterface;
use PoireauFramework\Http\Form\Filter\FilterInterface;
use PoireauFramework\Http\Form\Hydration\ExtractorInterface;
use PoireauFramework\Http\Form\Hydration\HydratorInterface;
use PoireauFramework\Http\Form\Operation\FormOperationInterface;
use PoireauFramework\Http\Form\Validation\Error;
use PoireauFramework\Http\Form\Validation\ValidationContext;
use PoireauFramework\Http\Form\Validation\RuleInterface;

/**
 * Represents a simple Form field
 */
class Field implements FormElementInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var FilterInterface[]
     */
    private $filters;

    /**
     * @var bool|string
     */
    private $required;

    /**
     * @var RuleInterface[]
     */
    private $rules;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @var ExtractorInterface
     */
    private $extractor;


    /**
     *
     */
    public function __construct(string $name, array $filters = [], $required = false, array $rules = [], HydratorInterface $hydrator = null, ExtractorInterface $extractor = null)
    {
        $this->name = $name;
        $this->filters = $filters;
        $this->required = $required;
        $this->rules = $rules;
        $this->hydrator = $hydrator;
        $this->extractor = $extractor;
    }

    /**
     * {@inheritdoc}
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function httpValue()
    {
        $value = $this->value;

        foreach ($this->filters as $filter) {
            $value = $filter->toHttp($value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * Set the field value
     * @internal
     */
    public function setValue($value): void
    {
        foreach ($this->filters as $filter) {
            $value = $filter->fromHttp($value);
        }

        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function extract($target): void
    {
        if ($this->extractor !== null) {
            $this->extractor->extract($target, $this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hydrate($target): void
    {
        if ($this->hydrator !== null) {
            $this->hydrator->hydrate($target, $this);
        }
    }

    /**
     * Validate the field
     * @internal
     */
    public function validate(ValidationContext $context): bool
    {
        if ($this->required !== false && empty($this->value)) {
            $context->setError(Error::required($this->required));
            return false;
        }

        foreach ($this->rules as $rule) {
            if (!$rule->validate($this->value, $context)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(FormOperationInterface $operation): FormOperationInterface
    {
        $operation->applyOnField($this);

        return $operation;
    }

    /**
     * Get list of filters
     * @internal
     * @return FilterInterface[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * Get list of rules
     * @internal
     * @return RuleInterface[]
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * Check if the field is marked as required
     * @internal
     */
    public function isRequired(): bool
    {
        return $this->required;
    }
}
