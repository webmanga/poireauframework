<?php

namespace PoireauFramework\Http\Form\Validation;

use PoireauFramework\Http\Form\FormElementInterface;
use PoireauFramework\Http\Form\FormInterface;

/**
 * Class ValidationContext
 */
class ValidationContext
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var FormElementInterface
     */
    private $current;

    /**
     * @var array
     */
    private $errors = [];


    /**
     * Create the context
     */
    public function __construct(FormInterface $form)
    {
        $this->form = $form;
    }

    /**
     * Get the form instance
     */
    public function form(): FormInterface
    {
        return $this->form;
    }

    /**
     * Get the element in validation
     */
    public function current(): FormElementInterface
    {
        return $this->current;
    }

    /**
     * Set the current element
     * @internal
     */
    public function setCurrent(FormElementInterface $element): void
    {
        $this->current = $element;
    }

    /**
     * Check is errors occurs
     */
    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    /**
     * Set error on current element
     */
    public function setError($error): void
    {
        $this->errors[$this->current->name()] = $error;
    }

    /**
     * Get error from an element
     */
    public function getError(FormElementInterface $element)
    {
        return $this->errors[$element->name()] ?? null;
    }

    /**
     * Check is an element has error
     */
    public function hasError(FormElementInterface $element): bool
    {
        return isset($this->errors[$element->name()]);
    }

    /**
     * Get all errors
     */
    public function errors(): array
    {
        return $this->errors;
    }
}
