<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Rule for check string length
 */
class Length implements RuleInterface
{
    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $max;

    /**
     * @var string
     */
    private $minMessage = "The value should have at least {min} characters";

    /**
     * @var string
     */
    private $maxMessage = "The value should have at most {max} characters";


    /**
     *
     */
    public function __construct($min, int $max = null)
    {
        if (is_array($min)) {
            foreach ($min as $k => $v) {
                $this->$k = $v;
            }
        } else {
            $this->min = $min;
            $this->max = $max;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, ValidationContext $context): bool
    {
        if (!is_string($value)) {
            $context->setError(Error::rule($this, "The value should be a string"));
            return false;
        }

        $len = strlen($value);

        if ($this->min !== null && $len < $this->min) {
            $context->setError(Error::rule($this, $this->minMessage));
            return false;
        }

        if ($this->max !== null && $len > $this->max) {
            $context->setError(Error::rule($this, $this->maxMessage));
            return false;
        }

        return true;
    }

    /**
     * Get the min range
     */
    public function getMin(): ?int
    {
        return $this->min;
    }

    /**
     * Get the max range
     */
    public function getMax(): ?int
    {
        return $this->max;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters(): array
    {
        return [
            "min" => $this->min,
            "max" => $this->max
        ];
    }
}
