<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Validate an email address
 */
class Email extends FilterVar
{
    public function __construct(string $message = "The value should be a valid email address")
    {
        parent::__construct(FILTER_VALIDATE_EMAIL, null, $message);
    }
}
