<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Rule using @see filter_var()
 * @link http://php.net/manual/fr/function.filter-var.php
 */
class FilterVar implements RuleInterface
{
    /**
     * @var int
     */
    private $filter;

    /**
     * @var mixed
     */
    private $options;

    /**
     * @var string
     */
    private $message;


    /**
     * Construct the Regex rule
     * @param int $filter One of the FILTER_VALIDATE_* constant
     * @param mixed $options The filter_var options
     * @param string $message The error message
     */
    public function __construct(int $filter, $options = null, string $message = "The value is not valid")
    {
        $this->filter = $filter;
        $this->options = $options;
        $this->message = $message;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, ValidationContext $context): bool
    {
        if (filter_var($value, $this->filter, $this->options) === false) {
            $context->setError(Error::rule($this));
            return false;
        }

        return true;
    }

    /**
     * Get the filter
     */
    public function getFilter(): int
    {
        return $this->filter;
    }

    /**
     * Get the filter options
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Get the message
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters(): array
    {
        return [
            "filter"  => $this->filter,
            "options" => $this->options
        ];
    }
}
