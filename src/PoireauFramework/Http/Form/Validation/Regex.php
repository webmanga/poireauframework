<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Rule for regular expression
 * Works only with string value
 */
class Regex implements RuleInterface
{
    /**
     * @var string
     */
    private $pattern;

    /**
     * @var string
     */
    private $message;


    /**
     * Construct the Regex rule
     * @param string $pattern The regex
     * @param string $message The error message
     */
    public function __construct(string $pattern, string $message = "The value is not valid") {
        $this->pattern = $pattern;
        $this->message = $message;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, ValidationContext $context): bool
    {
        if (!is_string($value) || !preg_match($this->pattern, $value)) {
            $context->setError(Error::rule($this));
            return false;
        }

        return true;
    }

    /**
     * Get the Regex pattern
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * Get the message
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters(): array
    {
        return [
            "pattern" => $this->pattern
        ];
    }
}
