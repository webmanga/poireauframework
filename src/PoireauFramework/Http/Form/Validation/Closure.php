<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Register a callable as a rule
 */
class Closure implements RuleInterface
{
    /**
     * @var callable
     */
    private $validator;


    /**
     * Construct the rule
     */
    public function __construct(callable $validator)
    {
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, ValidationContext $context): bool
    {
        return call_user_func($this->validator, $value, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters(): array
    {
        return [];
    }
}
