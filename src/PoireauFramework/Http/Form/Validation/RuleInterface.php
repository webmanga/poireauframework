<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Interface for validation form element value
 */
interface RuleInterface
{
    /**
     * Validate the value
     * @param mixed $value The field value
     * @param ValidationContext $context The context
     * @return bool
     */
    public function validate($value, ValidationContext $context): bool;

    /**
     * Get the rule parameters as array
     */
    public function getParameters(): array;
}
