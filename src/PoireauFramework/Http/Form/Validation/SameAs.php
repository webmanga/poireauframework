<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Check is the value is same as an other field
 */
class SameAs implements RuleInterface
{
    /**
     * @var string
     */
    private $field;

    /**
     * @var string
     */
    private $message;


    /**
     * Construct the Regex rule
     * @param string $field The other field name
     * @param string $message The error message
     */
    public function __construct(string $field, string $message = "The value should be same as '{field}'")
    {
        $this->field = $field;
        $this->message = $message;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, ValidationContext $context): bool
    {
        if ($value != $context->form()->get($this->field)->value()) {
            $context->setError(Error::rule($this));
            return false;
        }

        return true;
    }

    /**
     * Get the other field name
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * Get the message
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters(): array
    {
        return [
            "field" => $this->field
        ];
    }
}
