<?php

namespace PoireauFramework\Http\Form\Validation;

/**
 * Class Error
 */
class Error
{
    const TYPE_REQUIRED  = "required";
    const TYPE_UNDEFINED = "undefined";

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string[]
     */
    private $parameters;


    /**
     * Construct the error
     */
    public function __construct(string $type, string $message, array $parameters = [])
    {
        $this->type = $type;
        $this->message = $message;
        $this->parameters = $parameters;
    }

    /**
     * Get the message
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * Get the error parameters
     */
    public function parameters(): array
    {
        return $this->parameters;
    }

    /**
     * Get the error type
     * Can be the Rule class name or "required"
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * Render the error message as string
     */
    public function __toString(): string
    {
        $message = $this->message;

        foreach ($this->parameters as $p => $v) {
            $message = str_replace("{" . $p . "}", $v, $message);
        }

        return $message;
    }

    /**
     * Create error from a rule
     */
    static public function rule(RuleInterface $rule, $message = null): Error
    {
        return new Error(
            get_class($rule),
            $message ?: $rule->getMessage(),
            $rule->getParameters()
        );
    }

    /**
     * Create error for required field
     */
    static public function required($message = null): Error
    {
        return new Error(
            self::TYPE_REQUIRED,
            is_string($message) ? $message : "The value is required",
            []
        );
    }

    /**
     * Create an undefined error
     */
    static public function undefined($message = null): Error
    {
        return new Error(
            self::TYPE_UNDEFINED,
            is_string($message) ? $message : "Invalid value",
            []
        );
    }
}
