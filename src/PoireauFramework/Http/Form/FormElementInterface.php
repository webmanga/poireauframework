<?php

namespace PoireauFramework\Http\Form;

use PoireauFramework\Http\Form\Operation\Applicable;

/**
 * Interface for forms elements
 */
interface FormElementInterface extends Applicable
{
    /**
     * Get the element value
     * @return mixed
     */
    public function value();

    /**
     * Get the HTTP value of the element
     * @return mixed
     */
    public function httpValue();

    /**
     * Get the element's name
     */
    public function name(): string;

    /**
     * Extract the value from the object
     */
    public function extract($target): void;

    /**
     * Hydrate the target object
     */
    public function hydrate($target): void;
}
