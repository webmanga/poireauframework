<?php

namespace PoireauFramework\Http\Form;

use PoireauFramework\Http\Bag\BagInterface;

/**
 * Interface for handling forms
 */
interface FormInterface extends ElementAggregateInterface
{
    /**
     * Get a form element
     * @param string $name The element's name
     */
    public function get(string $name): FormElementInterface;

    /**
     * Check if the form has errors
     */
    public function hasErrors(): bool;

    /**
     * Get error on an element
     */
    public function error(string $name);

    /**
     * Check is an element has an error
     */
    public function hasError(string $name): bool;

    /**
     * Get all errors
     */
    public function errors(): array;

    /**
     * Attach an object to the form
     * The attachment will be used by default on export()
     * @param string|object $attachment
     * @return $this
     */
    public function attach($attachment): FormInterface;

    /**
     * Bind data to the form
     * @param BagInterface $bag Data to bind
     * @return $this
     */
    public function bind(BagInterface $bag): FormInterface;

    /**
     * Validate the form data
     * @return bool true if data is valid
     */
    public function validate(): bool;

    /**
     * Get tha array value of the form
     */
    public function toArray(): array;

    /**
     * Export the form data to object
     * @param object|string|null target The target object, class name, or null to use default form target
     */
    public function export($target = null);
}
