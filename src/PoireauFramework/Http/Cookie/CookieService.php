<?php

namespace PoireauFramework\Http\Cookie;

use PoireauFramework\Http\Bag\CookieBag;
use PoireauFramework\Http\Cookie\Parser\ParserInterface;
use PoireauFramework\Http\Cookie\Parser\ParserResolverInterface;
use PoireauFramework\Http\Cookie\Parser\SingleParserResolver;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseStack;

/**
 * Service for handling cookies
 */
class CookieService
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ResponseStack
     */
    private $responseStack;

    /**
     * @var ParserResolverInterface
     */
    private $parsers;


    /**
     *
     */
    public function __construct(RequestStack $requestStack, ResponseStack $responseStack, ParserResolverInterface $parsers = null)
    {
        $this->requestStack  = $requestStack;
        $this->responseStack = $responseStack;
        $this->parsers = $parsers ?: new SingleParserResolver();
    }

    /**
     * Create a new cookie
     *
     * @param string $name The cookie name (without prefix)
     * @param mixed $value
     *
     * @return CookieBuilder
     */
    public function create(string $name, $value): CookieBuilder
    {
        $cookie = new CookieBuilder($name, $value);

        $cookie->parser($this->parsers->parser($name));

        $this->responseStack->headers()->addCookie($cookie);

        return $cookie;
    }

    /**
     * Create an __Secure- cookie
     *
     * @param string $name The cookie name (without prefix)
     * @param mixed $value
     *
     * @return CookieBuilder
     */
    public function createSecure(string $name, $value): CookieBuilder
    {
        $cookie = $this->create(CookieBag::PREFIX_SECURE . $name, $value);

        $cookie->secure(true);

        return $cookie;
    }

    /**
     * Create an __Host- cookie
     *
     * @param string $name The cookie name (without prefix)
     * @param mixed $value
     *
     * @return CookieBuilder
     */
    public function createHost(string $name, $value): CookieBuilder
    {
        $cookie = $this->create(CookieBag::PREFIX_HOST . $name, $value);

        $cookie->domain(null);
        $cookie->secure(true);
        $cookie->path("/");

        return $cookie;
    }

    /**
     * Get a cookie value
     */
    public function get(string $name)
    {
        $request = $this->requestStack->current();

        if (!$request->cookies()->has($name)) {
            return null;
        }

        return $this->parsers->parser($name)->parse($request->cookies()->get($name));
    }
}
