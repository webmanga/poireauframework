<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Parser for cookie values
 */
interface ParserInterface
{
    /**
     * Transform cookie value to string (encrypt)
     * @param mixed $value
     * @return string
     */
    public function stringify($value): string;

    /**
     * Parse encrypted $value
     * @param string $value Encrypted value
     * @return mixed
     */
    public function parse(string $value);
}
