<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PoireauFramework\Security\Crypto\SignerInterface;

/**
 * Cookie parser for sign and check values
 * This is a decorator
 * @see \PoireauFramework\Security\Crypto\SignerInterface
 */
class SignedParser implements ParserInterface
{
    /**
     * @var SignerInterface
     */
    private $signer;

    /**
     * @var ParserInterface
     */
    private $parser;


    /**
     * Create a signed parser
     * @param SignerInterface $signer The signer instance
     * @param ParserInterface $parser The decorated parser. If not provided, will use PlainParser
     */
    public function __construct(SignerInterface $signer, ParserInterface $parser = null)
    {
        $this->signer = $signer;
        $this->parser = $parser ?: PlainParser::instance();
    }

    /**
     * {@inheritdoc}
     */
    public function stringify($value): string
    {
        return $this->signer->sign(
            $this->parser->stringify($value)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function parse(string $value)
    {
        $safe = $this->signer->unsign($value);

        if ($safe === null) {
            return null;
        }

        return $this->parser->parse($safe);
    }
}
