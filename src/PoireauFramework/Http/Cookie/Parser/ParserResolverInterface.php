<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Resolve parser from a cookie name
 */
interface ParserResolverInterface {
    /**
     * Get the parser instance
     */
    public function parser(string $cookieName): ParserInterface;

    /**
     * Get the default parser
     */
    public function defaultParser(): ParserInterface;
}
