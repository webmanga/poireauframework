<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Handle JSON format for cookies
 */
class JsonParser implements ParserInterface
{
    /**
     * @var bool
     */
    private $useAssoc = false;

    /**
     * {@inheritdoc}
     */
    public function stringify($value): string
    {
        return json_encode($value);
    }

    /**
     * {@inheritdoc}
     */
    public function parse(string $value)
    {
        return json_decode($value, $this->useAssoc);
    }

    /**
     * Use associative array instead of stdClass
     */
    public function useAssoc(bool $flag = true): void
    {
        $this->useAssoc = $flag;
    }
}
