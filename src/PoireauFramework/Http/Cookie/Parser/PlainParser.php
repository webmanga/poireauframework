<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Cookie Parser for handling plain text values
 */
class PlainParser implements ParserInterface
{
    static private $instance;

    /**
     * {@inheritdoc}
     */
    public function stringify($value): string
    {
        return (string) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function parse(string $value)
    {
        return $value;
    }

    /**
     * Get the parser instance
     */
    static public function instance(): PlainParser
    {
        if (self::$instance === null) {
            self::$instance = new PlainParser();
        }

        return self::$instance;
    }
}
