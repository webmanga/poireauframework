<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Resolver using cookie name mapping
 */
class MapParserResolver implements ParserResolverInterface
{
    /**
     * @var ParserInterface
     */
    private $defaultParser;

    /**
     * @var ParserInterface[]
     */
    private $map;

    /**
     * Create the resolver with parser
     *
     * @param ParserInterface|null $defaultParser
     * @param ParserInterface[] $map
     */
    public function __construct(ParserInterface $defaultParser = null, array $map = [])
    {
        $this->map = $map;
        $this->defaultParser = $defaultParser ?: PlainParser::instance();
    }

    /**
     * {@inheritdoc}
     */
    public function parser(string $cookieName): ParserInterface
    {
        return $this->map[$cookieName] ?? $this->defaultParser;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultParser(): ParserInterface
    {
        return $this->defaultParser;
    }
}
