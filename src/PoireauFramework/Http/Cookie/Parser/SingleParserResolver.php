<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Resolver with only on inner parser
 */
class SingleParserResolver implements ParserResolverInterface
{
    /**
     * @var ParserInterface
     */
    private $parser;


    /**
     * Create the resolver with parser
     */
    public function __construct(ParserInterface $parser = null)
    {
        $this->parser = $parser ?: PlainParser::instance();
    }

    /**
     * {@inheritdoc}
     */
    public function parser(string $cookieName): ParserInterface
    {
        return $this->parser;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultParser(): ParserInterface
    {
        return $this->parser;
    }
}
