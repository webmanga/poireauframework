<?php

namespace PoireauFramework\Http\Cookie\Parser;

/**
 * Serialize php values for cookies
 */
class SerializeParser implements ParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function stringify($value): string
    {
        return serialize($value);
    }

    /**
     * {@inheritdoc}
     */
    public function parse(string $value)
    {
        return unserialize($value);
    }
}
