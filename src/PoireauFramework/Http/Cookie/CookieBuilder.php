<?php

namespace PoireauFramework\Http\Cookie;

use DateTime;
use PoireauFramework\Http\Cookie\Parser\ParserInterface;
use PoireauFramework\Http\Cookie\Parser\PlainParser;

/**
 *
 */
class CookieBuilder
{
    const SAME_SITE_STRICT = "Strict";
    const SAME_SITE_LAX    = "Lax";

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @var DateTime
     */
    private $expires;

    /**
     * @var int
     */
    private $maxAge;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $path;

    /**
     * @var boolean
     */
    private $httpOnly = true;

    /**
     * @var boolean
     */
    private $secure = true;

    /**
     * @var string
     */
    private $sameSite;

    /**
     * @var ParserInterface
     */
    private $parser;


    /**
     * Cookie constructor
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, $value = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->parser = PlainParser::instance();
    }

    /**
     * @return $this
     */
    public function value($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return $this
     */
    public function expires(?DateTime $expires): self
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * @return $this
     */
    public function maxAge(?int $maxAge): self
    {
        $this->maxAge = $maxAge;

        return $this;
    }

    /**
     * @return $this
     */
    public function domain(?string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return $this
     */
    public function path(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return $this
     */
    public function httpOnly(bool $httpOnly = true): self
    {
        $this->httpOnly = $httpOnly;

        return $this;
    }

    /**
     * @return $this
     */
    public function secure(bool $secure = true): self
    {
        $this->secure = $secure;

        return $this;
    }

    /**
     * @return $this
     */
    public function sameSite(?string $sameSite): self
    {
        $this->sameSite = $sameSite;

        return $this;
    }

    /**
     * @return $this
     */
    public function parser(ParserInterface $parser): self
    {
        $this->parser = $parser;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getExpires(): ?DateTime
    {
        return $this->expires;
    }

    public function getMaxAge(): ?int
    {
        return $this->maxAge;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getSameSite(): ?string
    {
        return $this->sameSite;
    }

    public function isHttpOnly(): bool
    {
        return $this->httpOnly;
    }

    public function isSecure(): bool
    {
        return $this->secure;
    }

    public function __toString(): string
    {
        $header = "";

        $header .= rawurlencode($this->name) . "=" . rawurlencode($this->parser->stringify($this->value));

        if ($this->expires !== null) {
            $header .= "; Expires=" . $this->expires->format(DateTime::COOKIE);
        }

        if ($this->maxAge !== null) {
            $header .= "; Max-Age=" . $this->maxAge;
        }

        if ($this->domain !== null) {
            $header .= "; Domain=" . $this->domain;
        }

        if ($this->path !== null) {
            $header .= "; Path=" . $this->path;
        }

        if ($this->sameSite !== null) {
            $header .= "; SameSite=" . $this->sameSite;
        }

        if ($this->httpOnly === true) {
            $header .= "; HttpOnly";
        }

        if ($this->secure === true) {
            $header .= "; Secure";
        }

        return $header;
    }
}
