<?php

namespace PoireauFramework\Registry;

use PoireauFramework\Util\Arrays;

/**
 * Factory methods for initialize registry
 */
class RegistryFactory
{
    /**
     * Recursively create Registry instance from an array
     * Create a sub registry when an associative array is found
     *
     * @param array $data
     *
     * @return Registry
     */
    static public function createFromArray(array $data): Registry
    {
        $normalized = [];

        foreach ($data as $key => $value) {
            if (is_array($value) && !Arrays::isSequential($value)) {
                $normalized[$key] = self::createFromArray($value);
            } else {
                $normalized[$key] = $value;
            }
        }

        return new Registry($normalized);
    }
}
