<?php

namespace PoireauFramework\Registry\Exception;

/**
 * Base exception for registry
 */
class RegistryException extends \RuntimeException
{
    
}