<?php

namespace PoireauFramework\Registry\Exception;

/**
 * Exception throws when trying to set an immutable value
 */
class ImmutableValueException extends RegistryException
{
    /**
     * @var string
     */
    private $itemName;

    public function __construct(string $itemName)
    {
        parent::__construct("The value of '" . $itemName . "' cannot be modified");

        $this->itemName = $itemName;
    }

    /**
     * @return string
     */
    public function itemName(): string
    {
        return $this->itemName;
    }
}
