<?php

namespace PoireauFramework\Registry\Exception;

/**
 * Throw when trying to get an inexistant value
 */
class ValueNotFoundException extends RegistryException
{
    /**
     * @var string
     */
    private $itemName;

    public function __construct(string $itemName)
    {
        parent::__construct("Trying to get value of '" . $itemName . "', but it's not already registered");

        $this->itemName = $itemName;
    }

    /**
     * @return string
     */
    public function itemName(): string
    {
        return $this->itemName;
    }
}