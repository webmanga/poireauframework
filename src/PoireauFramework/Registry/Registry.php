<?php

namespace PoireauFramework\Registry;

use PoireauFramework\Registry\Exception\ValueNotFoundException;
use PoireauFramework\Registry\Exception\RegistryException;

/**
 * Implementation of RegistryInterface
 */
class Registry implements RegistryInterface
{
    /**
     * @var array
     */
    protected $data = [];


    /**
     * Construct the Registry
     * @param array $data The default registry data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name)
    {
        if (!isset($this->data[$name])) {
            throw new ValueNotFoundException($name);
        }

        return $this->data[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $name, $value): void
    {
        $this->data[$name] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $name): bool
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * {@inheritdoc}
     */
    public function sub(string $name): RegistryInterface
    {
        if ($this->has($name)) {
            $value = $this->get($name);

            if (!is_object($value) || !($value instanceof RegistryInterface)) {
                throw new RegistryException("The registry item '" . $name . "' is already defined, but it's not a RegistryInterface instance");
            }

            return $value;
        }

        $value = new Registry();
        $this->set($name, $value);

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function keys(): array
    {
        return array_keys($this->data);
    }
}
