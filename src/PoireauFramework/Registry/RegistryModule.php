<?php

namespace PoireauFramework\Registry;

use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Injector\InjectorConfigurable;

/**
 * Module for register the registry into injector
 */
class RegistryModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(RegistryInterface::class, Registry::class);
        $injector->instance(new Registry());
    }
}
