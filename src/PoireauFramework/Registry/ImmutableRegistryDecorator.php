<?php

namespace PoireauFramework\Registry;

use PoireauFramework\Registry\Exception\ImmutableValueException;
use PoireauFramework\Registry\Exception\RegistryException;

/**
 * Set a registry immutable
 */
class ImmutableRegistryDecorator implements RegistryInterface
{
    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var RegistryInterface[]
     */
    protected $subs = [];

    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name)
    {
        if (isset($this->subs[$name])) {
            return $this->subs[$name];
        }

        $value = $this->registry->get($name);

        if ($value instanceof RegistryInterface) {
            return $this->subs[$name] = new ImmutableRegistryDecorator($value);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $name, $value, bool $immutable = false): void
    {
        throw new ImmutableValueException($name);
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $name): bool
    {
        return $this->registry->has($name);
    }

    /**
     * {@inheritdoc}
     */
    public function sub(string $name): RegistryInterface
    {
        if (!$this->has($name)) {
            throw new ImmutableValueException($name);
        }

        if (isset($this->subs[$name])) {
            return $this->subs[$name];
        }

        $value = $this->registry->get($name);

        if (!$value instanceof RegistryInterface) {
            throw new RegistryException("The registry item '" . $name . "' is already defined, but it's not a RegistryInterface instance");
        }

        return $this->subs[$name] = new ImmutableRegistryDecorator($value);
    }

    /**
     * {@inheritdoc}
     */
    public function keys(): array
    {
        return $this->registry->keys();
    }
}
