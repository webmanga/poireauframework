<?php

namespace PoireauFramework\Registry;

use PoireauFramework\Registry\Exception\ImmutableValueException;
use PoireauFramework\Registry\Exception\ValueNotFoundException;
use PoireauFramework\Registry\Exception\RegistryException;

/**
 * Interface for registry
 */
interface RegistryInterface
{
    /**
     * Get the registered value
     * @param string $name The item key
     * @return mixed
     * @throws ValueNotFoundException When the value is not already set
     */
    public function get(string $name);

    /**
     * Alias of {@link RegistryInterface::get(name)}
     */
    public function __get(string $name);

    /**
     * Set a value the registry
     * @param string $name The key
     * @param mixed $value The value
     */
    public function set(string $name, $value): void;

    /**
     * Check if the registry contains a certain value
     * @param string $name The key
     * @return bool
     */
    public function has(string $name): bool;

    /**
     * get or create a sub-registry.
     * If the sub-registry do not exists, create a new instance, and set to the current registry as an immutable value
     * @param string $name The item name
     * @return RegistryInterface
     * @throws RegistryException When the registry already has a value, but not a Registry
     */
    public function sub(string $name): RegistryInterface;

    /**
     * Get all registry keys
     * @return string[]
     */
    public function keys(): array;
}