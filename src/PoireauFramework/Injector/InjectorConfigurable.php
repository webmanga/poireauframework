<?php

namespace PoireauFramework\Injector;

use PoireauFramework\Injector\Factory\FactoryContainer;

/**
 * Interface for configuration methods of Injector
 * @see InjectorInterface
 * @see InjectorModuleInterface
 */
interface InjectorConfigurable {
    /**
     * Register a new instance into injector
     * @param object $instance The instance to register
     */
    public function instance($instance);

    /**
     * Set a new couple <interface / implementation>
     *
     * @param string $base The interface / parent class
     * @param string $impl The implemantion class
     *
     * @return void
     */
    public function impl(string $base, string $impl);

    /**
     * Register a new factory into the injector
     *
     * @param string $className The class to create
     * @param callable $factory The factory
     *
     * @return FactoryContainer The factory container
     */
    public function factory(string $className, callable $factory): FactoryContainer;

    /**
     * Register a new default factory (i.e. called when no factories are found)
     *
     * @param callable $factory <p> The factory.
     * Should have 3 parameters :
     *  - The class name
     *  - The injector
     *  - The arguments as array
     *
     *     <pre><code>
     *         function(string $className, InjectorInterface $injector, array $arguments) {
     *             return new $className($injector->get($arguments[0]));
     *         }
     *     </code></pre>
     *   The factory should return the instance if possible, or NULL is this factory cannot handle the class
     * </p>
     *
     * @return void
     */
    public function addDefaultFactory(callable $factory);
}
