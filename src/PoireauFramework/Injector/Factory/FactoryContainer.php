<?php

namespace PoireauFramework\Injector\Factory;

use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;

/**
 * Container for injector factories
 * <pre><code>
 * $injector->factory(MyClass::class, function(InjectorInterface $injector) {
 *     return new MyClass(
 *         $injector->get(MyDep::class), $injector->get(MyRegistry::class)
 *     );
 * })
 * ->fork()
 * ->useImpl(MyDep::class, MyCustomDep::class)
 * ->useFactory(MyCustomDep::class, function() { return new MyCustomDep(); })
 * ->useInstance($registry);
 * </code></pre>
 */
class FactoryContainer
{
    /**
     * The owner (declarer) injector
     * @var InjectorInterface
     */
    private $owner;

    /**
     * @var string
     */
    private $className;

    /**
     * The factory
     * @var callable
     */
    private $factory;

    /**
     * The used injector instance
     * @var InjectorInterface|InjectorConfigurable
     */
    private $injector;

    /**
     * Does the instance should be saved ?
     * @var bool
     */
    private $persistent;


    /**
     * Construct the container
     * @param InjectorInterface $owner The declarer injector
     * @param string $className Class to construct
     * @param callable $factory The factory
     */
    public function __construct(InjectorInterface $owner, string $className, callable $factory)
    {
        $this->owner = $owner;
        $this->className = $className;
        $this->factory = $factory;

        $this->injector = $owner;
        $this->persistent = true;
    }

    /**
     * Get the class name to construct
     * @return string
     */
    public function getClass(): string
    {
        return $this->className;
    }

    /**
     * Set a custom injector
     * @param InjectorInterface $injector
     * @return $this
     */
    public function injector(InjectorInterface $injector): self
    {
        $this->injector = $injector;

        return $this;
    }

    /**
     * Get the current injector instance
     * @return InjectorInterface
     */
    public function getInjector(): InjectorInterface
    {
        return $this->injector;
    }

    /**
     * Fork the injector instance
     * @see InjectorInterface::fork()
     * @return $this
     */
    public function fork(string $key): self
    {
        $this->injector = $this->owner->fork($key);

        return $this;
    }

    /**
     * Does the instance should be keeped or not ?
     * @param bool $persistent
     * @return $this
     */
    public function persistent(bool $persistent = true): self
    {
        $this->persistent = $persistent;

        return $this;
    }

    /**
     * Check if the instance should be keeped
     * @return bool
     */
    public function isPersistent(): bool
    {
        return $this->persistent;
    }

    /**
     * Change the factory
     * @internal Must not be called externally of Injector
     */
    public function setFactory(callable $factory): void
    {
        $this->factory = $factory;
    }

    /**
     * Use custom implementation on injector
     *
     * @see InjectorInterface::impl()
     *
     * @param string $interfaceName The interface class name
     * @param string $implementation The implementation class name
     *
     * @return $this
     */
    public function useImpl(string $interfaceName, string $implementation): self
    {
        $this->injector->impl($interfaceName, $implementation);

        return $this;
    }

    /**
     * Use a custom factory on injector
     * /!\ The returns FactoryContainer is the current instance. To get the sub-factory container instance, use $this->getInjector()->factory()
     *
     * @see InjectorInterface::factory()
     *
     * @param string $className
     * @param callable $factory
     *
     * @return $this
     */
    public function useFactory(string $className, callable $factory): self
    {
        $this->injector->factory($className, $factory);

        return $this;
    }

    /**
     * Use a custom instance on injector
     * @param object $instance The instance to register
     * @return $this
     */
    public function useInstance($instance): self
    {
        $this->injector->instance($instance);

        return $this;
    }

    /**
     * Add a default factory on injector
     * @param callable $factory The default factory
     * @return $this
     */
    public function useDefaultFactory(callable $factory): self
    {
        $this->injector->addDefaultFactory($factory);

        return $this;
    }

    /**
     * Make the instance
     *
     * @return object
     */
    public function make()
    {
        return call_user_func($this->factory, $this->injector, $this);
    }

    /**
     * Make the instance with custom arguments
     */
    public function makeCustom(array $arguments)
    {
        $factory = $this->factory;

        return $factory($this->injector, ...$arguments);
    }
}
