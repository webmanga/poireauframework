<?php

namespace PoireauFramework\Injector\Factory;

use PoireauFramework\Injector\InjectorInterface;

/**
 * Interface for classes that can be auto-instantiated
 */
interface InjectorInstantiable {
    /**
     * Create the instance
     *
     * @param InjectorInterface $injector The injector
     * @param array $parameters Extras parameters
     *
     * @return self The instance
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): self;
}
