<?php

namespace PoireauFramework\Injector;

use PoireauFramework\Injector\Exception\InjectorException;
use PoireauFramework\Injector\Factory\InjectorInstantiable;

/**
 *
 */
class BaseInjector extends AbstractInjector {
    /**
     * {@inheritdoc}
     */
    public function parent(): InjectorInterface
    {
        throw new InjectorException("BaseInjector do not have parent");
    }

    /**
     * {@inheritdoc}
     */
    public function isRoot(): bool
    {
        return true;
    }
}
