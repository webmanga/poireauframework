<?php

namespace PoireauFramework\Injector\Exception;

/**
 * Bad implementation error
 */
class BadImplementationException extends InjectorException
{
    public function __construct(string $exceptedClass, string $currentClass)
    {
        parent::__construct("Excepted instance of " . $exceptedClass . " but found " . $currentClass);
    }
}
