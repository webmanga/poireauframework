<?php

namespace PoireauFramework\Injector\Exception;

/**
 * Base exception for injector
 */
class InjectorException extends \Exception
{
}
