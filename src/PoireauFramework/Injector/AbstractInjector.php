<?php

namespace PoireauFramework\Injector;

use PoireauFramework\Injector\Exception\InjectorException;
use PoireauFramework\Injector\Factory\FactoryContainer;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryInterface;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;

/**
 * Abstract implementation of InjectorInterface and InjectorConfigurable
 */
abstract class AbstractInjector implements InjectorInterface, InjectorConfigurable
{
    /**
     * @var RegistryInterface
     */
    protected $registry;

    /**
     * @var callable[]
     */
    protected $defaultFactories = [];


    /**
     * Construct the injector and save itself instance
     *
     * @param RegistryInterface $registry The injector registry
     */
    public function __construct(RegistryInterface $registry = null)
    {
        $this->registry = $registry ?: new Registry();

        $this->instance($this);
        $this->impl(InjectorInterface::class, get_class($this));
    }

    /**
     * {@inheritdoc}
     */
    public function instance($instance)
    {
        $this->registry->set(get_class($instance), $instance);
    }

    /**
     * {@inheritdoc}
     */
    public function impl(string $base, string $impl)
    {
        $this->registry->set($base, $impl);
    }

    /**
     * {@inheritdoc}
     */
    public function factory(string $className, callable $factory): FactoryContainer
    {
        if ($this->registry->has($className)) {
            $container = $this->registry->get($className);
            $container->setFactory($factory);
        } else {
            $container = new FactoryContainer($this, $className, $factory);
            $this->registry->set($className, $container);
        }

        return $container;
    }

    /**
     * {@inheritdoc}
     */
    public function addDefaultFactory(callable $factory)
    {
        $this->defaultFactories[] = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function raw(string $key)
    {
        return $this->registry->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function reg(string $key): RegistryInterface
    {
        return $this->registry->sub($key);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $className)
    {
        $realClassName = $this->resolveClassName($className);

        if (!$this->registry->has($realClassName)) {
            $instance = $this->getDefault($realClassName);
            $this->registry->set($realClassName, $instance);

            return $instance;
        }

        $factory = $this->registry->get($realClassName);

        if ($factory instanceof FactoryContainer) {
            $instance = $factory->make();

            if ($factory->isPersistent()) {
                $this->registry->set($realClassName, $instance);
            }
        } else {
            $instance = $factory;
        }

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $key): bool
    {
        return $this->registry->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $className, array $parameters = [])
    {
        $realClassName = $this->resolveClassName($className);

        if ($this->registry->has($realClassName)) {
            $factory = $this->registry->get($realClassName);

            if ($factory instanceof FactoryContainer) {
                return $factory->makeCustom($parameters);
            }
        }

        return $this->createDefault($realClassName, $parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function register(InjectorModuleInterface $module)
    {
        $module->configure($this);
    }

    /**
     * {@inheritdoc}
     */
    public function fork(string $key): InjectorInterface
    {
        return new ChildInjector($this, $this->registry->sub($key));
    }

    ///==========///
    /// Internal ///
    ///==========///

    /**
     * Get the real class name from interface name
     * @internal
     * @return string
     */
    protected function resolveClassName(string $className): string
    {
        $className = ltrim($className, '\\');

        if ($this->registry->has($className)) {
            $registered = $this->registry->get($className);

            if (is_string($registered)) {
                return $registered;
            }
        }

        return $className;
    }

    /**
     * Default action for get()
     * @param string $className The class to get or create
     * @return object the class instance
     * @internal
     */
    protected function getDefault(string $className)
    {
        return $this->createDefault($className);
    }

    /**
     * Try to create the instance using default factories
     * @param string $className The class to create
     * @param array $parameters The constructor parameters
     * @return object | null The instance, or null if default factories cannot create the instance
     */
    protected function createWithDefaultFactories(string $className, array $parameters = [])
    {
        if (is_subclass_of($className, InjectorInstantiable::class)) {
            return $className::createInstance($this, $parameters);
        }

        foreach ($this->defaultFactories as $factory)  {
            $instance = $factory($className, $this, $parameters);

            if ($instance !== null) {
                return $instance;
            }
        }

        return null;
    }

    /**
     * Default action for create()
     * @param string $className The class to create
     * @param array $parameters Factory parameters
     * @return object The new instance
     * @internal
     */
    protected function createDefault(string $className, array $parameters = [])
    {
        $instance = $this->createWithDefaultFactories($className, $parameters);

        if ($instance === null) {
            throw new InjectorException("No factory found for " . $className);
        }

        return $instance;
    }
}
