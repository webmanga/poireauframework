<?php

namespace PoireauFramework\Injector;

use PoireauFramework\Injector\Exception\InjectorException;
use PoireauFramework\Injector\Exception\BadImplementationException;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Interface for injectors
 */
interface InjectorInterface {
    /**
     * Create a new instance of the class $className
     * @param string $className The class to instanciate
     * @param array $parameters The optional $parameters
     * @return object The new instance
     * @throws BadImplementationException When the factory do not return instance of $className
     */
    public function create(string $className, array $parameters = []);

    /**
     * Get (or create) instance of $className
     * @param string $className The class to get
     * @return object The instance of the class
     * @throws BadImplementationException When the factory do not return instance of $className
     */
    public function get(string $className);

    /**
     * Check if the injector has the class registerer
     * @param string $className The class name
     * @return bool
     */
    public function has(string $className): bool;

    /**
     * Get raw value from the registry
     * @param string $key
     * @return mixed
     */
    public function raw(string $key);

    /**
     * Get a sub registry
     * @param string $key
     * @return RegistryInterface
     */
    public function reg(string $key): RegistryInterface;

    /**
     * Register a new injector module
     * @param InjectorModuleInterface $module The module to register
     */
    public function register(InjectorModuleInterface $module);

    /**
     * Fork the current Injector and return related child injector
     * @param string $key The registry key for the child injector
     * @return InjectorInterface The child injector
     */
    public function fork(string $key): InjectorInterface;

    /**
     * Get the parent injector
     * @return InjectorInterface
     * @throws InjectorException When the injector do not have a parent (root)
     * @see InjectorInterface::isRoot()
     */
    public function parent(): InjectorInterface;

    /**
     * Check if the current injector is root (i.e. do not have a parent)
     * @see InjectorInterface::parent()
     * @return bool true if this injector do not have a parent
     */
    public function isRoot(): bool;
}