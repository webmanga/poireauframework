<?php

namespace PoireauFramework\Injector;

/**
 * Register a module into the injector
 *
 * @see InjectorInterface::register(module)
 */
interface InjectorModuleInterface {
    /**
     * Configure the injector
     *
     * @param InjectorConfigurable $injector The injector
     */
    public function configure(InjectorConfigurable $injector): void;
}
