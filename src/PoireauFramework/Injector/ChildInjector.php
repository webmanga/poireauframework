<?php

namespace PoireauFramework\Injector;

use PoireauFramework\Registry\RegistryInterface;

/**
 *
 */
class ChildInjector extends AbstractInjector {
    /**
     * @var InjectorInterface
     */
    private $parentInjector;

    /**
     * Construct the child injector
     * @param InjectorInterface $parentInjector
     * @param RegistryInterface $registry The registry
     */
    public function __construct(InjectorInterface $parentInjector, RegistryInterface $registry)
    {
        parent::__construct($registry);

        $this->parentInjector = $parentInjector;
    }

    /**
     * {@inheritdoc}
     */
    public function parent(): InjectorInterface
    {
        return $this->parentInjector;
    }

    /**
     * {@inheritdoc}
     */
    public function isRoot(): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefault(string $className)
    {
        $instance = $this->createWithDefaultFactories($className);

        if ($instance !== null) {
            return $instance;
        }

        return $this->parent()->get($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function createDefault(string $className, array $parameters = [])
    {
        $instance = $this->createWithDefaultFactories($className, $parameters);

        if ($instance !== null) {
            return $instance;
        }

        return $this->parent()->create($className, $parameters);
    }
}
