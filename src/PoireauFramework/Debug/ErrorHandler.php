<?php

namespace PoireauFramework\Debug;

use Exception;
use PoireauFramework\Debug\Exception\ExtendedErrorException;
use PoireauFramework\Debug\Exception\SeverityInterface;

/**
 * Class for handling PHP errors
 */
class ErrorHandler
{
    /**
     * @var self
     */
    static private $instance;

    /**
     * @var callable[]
     */
    protected $exceptionHandlers = [];

    /**
     * @var bool
     */
    protected $showErrors = false;

    /**
     * @var callable
     */
    protected $hideErrorAction;

    /**
     * @var callable
     */
    protected $lastErrorHandler;

    /**
     * @var callable
     */
    protected $lastExceptionHandler;


    /**
     *
     */
    public function __construct() {
        $this->hideErrorAction = [$this, "defaultHideErrorAction"];
    }

    /**
     * Errors are shown ?
     * @return boolean
     */
    public function isShowErrors(): bool
    {
        return $this->showErrors;
    }

    /**
     * Set the show errors status
     * @param boolean $b
     */
    public function showErrors(bool $b = true): void
    {
        $this->showErrors = $b;
    }

    /**
     * The default action for hiding errors
     */
    public function defaultHideErrorAction(): void
    {
        http_response_code(500);

        echo "<!DOCTYPE html>
        <html>
            <head>
                <title>Internal server error</title>
                <meta charset=\"UTF-8\"/>
                <meta name=\"robots\" content=\"none\"/>
            </head>
            <body>
                <h1>Internal server error</h1>
                <p>An internal server error has been detected. Please contact the server admin.</p>
            </body>
        </html>";

        exit();
    }

    /**
     * Register a new exception handler
     * @param callable $handler The handle, Should have the Exception to handle as first param, and return true if the Exception is handled, or false
     * <code>
     *  handler = function(Exception ex){
     *      if(!(ex instanceof MyException))
     *          return false;
     *
     *      //Handle the exception
     *      return true;
     *  };
     * </code>
     */
    public function addExceptionHandler(callable $handler): void
    {
        array_unshift($this->exceptionHandlers, $handler);
    }

    /**
     * Convert all php errors to ExtendedErrorException
     * @param int $type One of the E_ constants
     * @param string $msg The error message
     * @param string|null $file The error file
     * @param int $line The error line
     * @param array $context The error context
     * @throws ExtendedErrorException
     */
    public function handleError(int $type, string $msg, string $file, int $line, array $context = []): void
    {
        if ((error_reporting() & $type) !== $type) {
            return;
        }

        $trace = debug_backtrace();

        throw new ExtendedErrorException($msg, $type, $file, $line, $context, $trace);
    }

    /**
     * Handle an exception
     * @param Exception $exception The exception to handle
     */
    public function handleException(Exception $exception): void
    {
        if (!$this->showErrors) {
            call_user_func($this->hideErrorAction);
            return;
        }

        foreach ($this->exceptionHandlers as $handler) {
            if ($handler($exception)) {
                return;
            }
        }

        var_dump($exception);
    }

    /**
     * Register exception and error handlers
     * as PHP handler
     */
    public function register(): void
    {
        $this->lastErrorHandler     = set_error_handler([$this, "handleError"]);
        $this->lastExceptionHandler = set_exception_handler([$this, "handleException"]);
    }

    /**
     * Restore last error handler
     */
    public function restore(): void
    {
        set_error_handler($this->lastErrorHandler);
        set_exception_handler($this->lastExceptionHandler);
    }

    /**
     * Get the ErrorHandler instance
     * @return ErrorHandler
     */
    static public function instance(): ErrorHandler
    {
        if (self::$instance === null) {
            self::$instance = new ErrorHandler();
        }

        return self::$instance;
    }
}
