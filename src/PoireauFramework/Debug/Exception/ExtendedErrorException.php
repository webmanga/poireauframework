<?php

namespace PoireauFramework\Debug\Exception;

use ErrorException;

/**
 * ErrorException with context
 */
class ExtendedErrorException extends ErrorException implements SeverityInterface
{
    /**
     * @var array
     */
    private $context;


    public function __construct(string $message, int $severity, string $filename, int $lineno, array $context = [], array $stacktrace = [])
    {
        parent::__construct($message, 500, $severity, $filename, $lineno);

        $this->context = $context;

        if (!empty($stacktrace)) {
            $reflection = new \ReflectionProperty(\Exception::class, "trace");
            $reflection->setAccessible(true);
            $reflection->setValue($this, $stacktrace);
        }
    }

    /**
     * Get the error context
     * @return array
     */
    public function context(): array
    {
        return $this->context;
    }

    /**
     * {@inheritdoc}
     */
    public function severity(): int
    {
        return $this->getSeverity();
    }

    /**
     * {@inheritdoc}
     */
    public function isFatal(): bool
    {
        return false;
    }

    /**
     * Create the Exception object from an error array
     * @see error_get_last()
     * @param array $error
     * @return self
     */
    static public function createFromArray(array $error, array $stacktrace = []): ExtendedErrorException
    {
        return new ExtendedErrorException(
            $error["message"] ?? "",
            $error["type"]    ?? 1,
            $error["file"]    ?? null,
            $error["line"]    ?? -1,
            $error["context"] ?? [],
            $stacktrace
        );
    }
}
