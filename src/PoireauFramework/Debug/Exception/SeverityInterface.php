<?php

namespace PoireauFramework\Debug\Exception;

/**
 * Provides methods to get severity of an error or exception
 */
interface SeverityInterface
{
    /**
     * Get the error severity
     * @return int
     */
    public function severity(): int;

    /**
     * Check if the exception is fatal
     * @return bool
     */
    public function isFatal(): bool;
}
