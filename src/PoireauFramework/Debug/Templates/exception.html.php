<section class="debug error">
    <?php if($error instanceof \ErrorException):?>
    <h1><?= self::getSeverityString($error->getSeverity())?></h1>
    <?php else:?>
    <h1>Exception : <?= (new ReflectionClass($error))->getShortName()?></h1>
    <?php endif?>
    
    <?php for ($exception = $error, $i = 0; $exception != null && $i < 10; $exception = $exception->getPrevious(), ++$i): ?>
    <article>
        <h3>Message :</h3>
        <p class="debug info"><?= $exception->getMessage()?></p>
    </article>

    <article>
        <h3>Debug :</h3>
        <div class="debug info">
            <p>On file <em><?= $exception->getFile()?></em> line <strong><?= $exception->getLine()?></strong> :</p>
            <?php self::showErrorFile($exception->getFile(), $exception->getLine())?>
        </div>
    </article>
    <?php endfor?>

    <?php if(!empty($error->getTrace())):?>
    <article>
        <h3>Stack trace :</h3>
        <div class="debug info">
            <?php self::showTrace($error->getTrace())?>
        </div>
    </article>
    <?php endif?>
    
    <?php if(method_exists($error, 'getContext') && !empty($error->getContext())):?>
    <article>
        <h3>Context :</h3>
        <div class="debug info">
            <?php self::showContext($error->getContext())?>
        </div>
    </article>
    <?php endif?>
</section>