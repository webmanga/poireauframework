<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <meta charset="utf-8"/>
        <meta name="robots" content="noindex,nofollow"/>
        <style><?= self::getErrorCSS()?></style>
    </head>
    <body>
        <div id="content"><?= $contents?></div>
        <footer>
            &copy; <a href="http://webmanga.fr" target="_blank">WebManga</a> - <?= date('Y')?>
        </footer>
    </body>
</html>