<?php

namespace PoireauFramework\Debug;

use PoireauFramework\Kernel\BootableModuleInterface;
use PoireauFramework\Kernel\KernelInterface;

/**
 * Debug module for PoireauFramework
 *
 * @author vincent
 */
final class Debug implements BootableModuleInterface  {
    /**
     * {@inheritdoc}
     */
    public function boot(KernelInterface $kernel): void
    {
        $kernel->injector()->get(ErrorHandler::class)->addExceptionHandler(function ($e) {
            static::showExceptionPage($e);

            return true;
        });
    }

    /**
     * Show a php code, with highlighted line
     * @param string $code Code to show
     * @param int $errorLine The line to highlight
     */
    static private function showCode($code, $errorLine) {
        $openTag = strpos($code, '<?');
        $closeTag = false;
        $openTagAdded = false;

        if ($openTag !== false) {
            $closeTag = strpos($code, '?>');
        }

        //There is not open tag or there is a close tag before the open (i.e. The open tag is missing)
        if ($openTag === false || ($closeTag !== false && $closeTag < $openTag)) {
            $code = '<?php' . PHP_EOL . $code; //Add the open tag
            $openTagAdded = true;
        }

        $str = highlight_string($code, true);

        $str = preg_replace('#' . $errorLine . '\..*<br\s*/?>#iU', '<strong style="background: rgba(0,0,0,.1);">$0</strong>', $str);

        if ($openTagAdded)
            $str = str_replace('&lt;?php<br />', '', $str);

        echo $str;
    }

    /**
     * Show a code from error file and line
     * @param string $errorFile
     * @param int $errorLine
     */
    static public function showErrorFile($errorFile, $errorLine) {
        if (strpos($errorFile, "eval()'d code") !== false) {
            return self::showEvalCode($errorFile, $errorLine);
        }

        if (substr($errorFile, -4) === ".zep") {
            $errorFile = __DIR__ . "/../../../ext/$errorFile";
        }

        $code = '';
        $file = file($errorFile);
        $lineStart = $errorLine - 3;

        if ($lineStart < 0)
            $lineStart = 0;

        for ($line = $lineStart; $line < $errorLine + 2 && $line < count($file); ++$line) {
            $code .= ($line + 1) . '. ' . $file[$line];
        }

        self::showCode($code, $errorLine);
    }

    static private function showEvalCode($errorFile, $errorLine) {
        $matches = [];

        if (!preg_match("#(.*)\((\d+)\) : eval\(\)'d#iU", $errorFile, $matches)) //not match ? 
            return;

        $realFile = $matches[1];
        $realLine = $matches[2];

        self::showErrorFile($realFile, $realLine);
    }

    /**
     * Display the stack trace as html table
     * @param array $trace
     */
    static public function showTrace(array $trace) {
        echo '<table class="debug trace">';

        echo '<tr><th>#</th><th>File</th><th>Function</th></tr>';

        foreach ($trace as $i => $line) {
            echo '<tr>';

            echo "<td>#{$i}</td>";

            if (isset($line['file'], $line['line'])) {
                echo '<td class="debug file">', htmlentities($line['file']), ':', $line['line'];

                echo '<div class="debug code">', self::showErrorFile($line['file'], $line['line']), '</div>';

                echo '</td>';
            } else {
                echo '<td>[PHP]</td>';
            }

            echo '<td class="debug function">';

            if (isset($line['class'], $line['type']))
                echo htmlentities($line['class']), $line['type'];

            echo $line['function'], '<span class="debug args">(';

            if (isset($line['args']))
                echo self::formatArgs($line['args']);

            echo ')</span></td>';

            echo '</tr>';
        }

        echo '</table>';
    }

    /**
     * Format function args
     * @param array $args
     * @return string
     */
    static public function formatArgs(array $args) {
        $params = [];
        foreach ($args as $k => $arg) {
            $val = '';
            switch (gettype($arg)) {
                case 'boolean':
                    $val = '<em>bool</em>(<span class="debug arg-bool">' . ($arg ? 'true' : 'false') . '</span>)';
                    break;
                case 'string':
                    $val = '<em>string</em>(<span class="debug arg-string">"' . htmlentities($arg) . '"</span>)';
                    break;
                case 'integer':
                    $val = '<em>int</em>(<span class="debug arg-int">' . $arg . '</span>)';
                    break;
                case 'float':
                case 'double':
                    $val = '<em>float</em>(<span class="debug arg-float">' . $arg . '</span>)';
                    break;
                case 'object':
                    $val = '<em>object</em>(<span class="debug arg-object">' . get_class($arg) . '</span>)';
                    break;
                case 'array':
                    $val = '<em>array</em>{' . self::formatArgs($arg) . '}';
                    break;
                case 'NULL':
                    $val = '<span class="debug arg-null">NULL</span>';
                    break;
                default:
                    $val = var_export($arg, true);
            }

            $params[] = is_int($k) ? $val : '"' . htmlentities($k) . '" => ' . $val;
        }

        return implode(', ', $params);
    }

    /**
     * @param \Exception $error
     * @param string $template
     */
    static public function showException($error, $template = 'exception.html.php') {
        require self::templatesDir() . $template;
    }

    /**
     * @param \Exception $error
     * @param string $contents
     */
    static public function decorateError($error, $contents) {
        require self::templatesDir() . 'layout.html.php';
    }

    /**
     * @param \Exception $error
     * @param string $template
     */
    static public function showExceptionPage($error, $template = 'exception.html.php') {
        while(ob_get_level() > 0)
            ob_end_clean();
        
        ob_start();
        self::showException($error, $template);
        self::decorateError($error, ob_get_clean());
    }

    static public function getErrorCSS() {
        return file_get_contents(self::templatesDir() . 'error.css');
    }

    static public function templatesDir() {
        return __DIR__ . DIRECTORY_SEPARATOR . 'Templates' . DIRECTORY_SEPARATOR;
    }

    static public function showContext(array $context) {
        echo <<<'EOD'
    <table class="debug context">
        <tr>
            <th>Var name</th><th>Value</th>
        </tr>
EOD;

        foreach ($context as $name => $value) {
            echo '<tr><td>', $name, '</td><td>';
            var_dump($value);
            echo '</td></tr>';
        }

        echo '</table>';
    }

    static public function getSeverityString($code) {
        switch ($code) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_USER_ERROR:
                return 'Fatal error';
            case E_COMPILE_ERROR:
                return 'Compile error';
            case E_USER_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_WARNING:
                return 'Warning';
            case E_NOTICE:
            case E_USER_NOTICE:
                return 'Notice';
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                return 'Deprecated';
            case E_STRICT:
                return 'Strict standards';
            case E_PARSE:
                return 'Parse error';
            case E_RECOVERABLE_ERROR:
                return 'Catchable error';
            default:
                return 'Undefined error';
        }
    }

    static public function isFatal($severity){
        return in_array($severity, [E_COMPILE_ERROR, E_PARSE, E_ERROR, E_CORE_ERROR]);
    }
}
