<?php

namespace PoireauFramework\Cache;

use Psr\Cache\CacheItemInterface;

/**
 * Cache implementation using file system
 * @todo handle file errors
 * @todo check key chars
 * @todo deep directory level files
 */
final class FileCache extends AbstractCachePool
{
    const VERSION = 1;

    private $path;
    private $prefix;
    private $suffix;

    /**
     *
     */
    public function __construct($config = null)
    {
        if (is_string($config)) {
            $config = (object) ['path' => $config];
        } elseif ($config === null) {
            $config = new \stdClass;
        }

        $this->path   = $config->path ?? sys_get_temp_dir();
        $this->prefix = $config->prefix ?? "pw-cache-";
        $this->suffix = $config->suffix ?? ".cache";
    }

    /**
     * {@inheritdoc}
     */
    public function getItem($key): CacheItemInterface
    {
        if (isset($this->deferred[$key])) {
            return clone $this->deferred[$key];
        }

        $file = $this->fileName($key);
        $item = new CacheItem($key);

        if (is_file($file)) {
            $item->import(file_get_contents($file));
        }

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function hasItem($key): bool
    {
        return isset($this->deferred[$key]) || is_file($this->fileName($key));
    }

    /**
     * {@inheritdoc}
     */
    public function clear(): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteItem($key): bool
    {
        unset($this->deferred[$key]);

        $file = $this->fileName($key);

        if (!is_file($file)) {
            return true;
        }

        return unlink($file);
    }

    /**
     * {@inheritdoc}
     */
    public function save(CacheItemInterface $item): bool
    {
        if (!$item->isHit()) {
            return false;
        }

        $file = $this->fileName($item->getKey());
        $dir = dirname($file);

        if (!is_dir($dir)) {
            mkdir($dir, 0770, true);
        }

        return file_put_contents($file, $item->export()) !== false;
    }

    /**
     *
     */
    private function fileName(string $key): string
    {
        $encoded = strtr(base64_encode($key), "/", "_");
        return $this->path . DIRECTORY_SEPARATOR . $this->prefix . $encoded . $this->suffix;
    }
}
