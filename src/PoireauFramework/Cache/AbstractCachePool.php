<?php

namespace PoireauFramework\Cache;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Abstract implementation for cache item pool
 */
abstract class AbstractCachePool implements CacheItemPoolInterface
{
    /**
     * @var CacheItemInterface[]
     */
    protected $deferred = [];


    /**
     * {@inheritdoc}
     */
    public function getItems(array $keys = []): array
    {
        $items = [];

        foreach ($keys as $key) {
            $items[$key] = $this->getItem($key);
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteItems(array $keys): bool
    {
        $result = true;

        foreach ($keys as $key) {
            $result = $result & $this->deleteItem($key);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function saveDeferred(CacheItemInterface $item): bool
    {
        $this->deferred[$item->getKey()] = $item;

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function commit(): bool
    {
        $result = true;

        foreach ($this->deferred as $item) {
            $result = $result & $this->save($item);
        }

        $this->deferred = [];

        return $result;
    }

    /**
     * Perform commit on destruct
     */
    public function __destruct()
    {
        $this->commit();
    }
}
