<?php

namespace PoireauFramework\Cache;

use DateInterval;
use DateTime;
use DateTimeInterface;
use Psr\Cache\CacheItemInterface;

/**
 * Basic implementation of cache item
 * @internal
 */
final class CacheItem implements CacheItemInterface
{
    const DEFAULT_INTERVAL = "PT1M";
    const VERSION = 1;

    /**
     * @var string
     */
    private $key;

    /**
     * @var mixed
     */
    private $value = null;

    /**
     * @var bool
     */
    private $hit = false;

    /**
     * @var DateTimeInterface
     */
    private $expiration = null;


    /**
     * Create the item
     * @internal Should NEVER be called externally
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * {@inheritdoc}
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * {@inheritdoc}
     */
    public function get() {
        if (!$this->isHit()) {
            return null;
        }

        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function isHit(): bool
    {
        if (!$this->hit) {
            return false;
        }

        if ($this->expiration === null) {
            return true;
        }

        return new DateTime() < $this->expiration;
    }

    /**
     * {@inheritdoc}
     */
    public function set($value): CacheItemInterface
    {
        $this->hit   = true;
        $this->value = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function expiresAt($expiration = null): CacheItemInterface
    {
        $this->expiration = $expiration;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function expiresAfter($time): CacheItemInterface
    {
        $now = new DateTime();

        if (is_int($time)) {
            $interval = new DateInterval("PT" . $time . "S");
        } elseif ($time === null) {
             $interval = new DateInterval(self::DEFAULT_INTERVAL);
         } elseif ($time instanceof DateInterval) {
            $interval = $time;
        }

        $this->expiration = $now->add($interval);

        return $this;
    }

    /**
     * Export the item to string
     * @internal
     */
    public function export(): string
    {
        $expiration = -1;

        if ($this->expiration !== null) {
            $expiration = $this->expiration->getTimestamp();
        }

        return pack("cq", self::VERSION, $expiration) . serialize($this->value);
    }

    /**
     * Parse string data to get a new item
     *
     * @param string $data
     *
     * @return $this
     * @internal
     */
    public function import(string $data): CacheItem
    {
        if (empty($data) || strlen($data) < 11) { // version(1) + expiration(8) + min[serialize](2)
            return $this;
        }

        if (ord($data[0]) !== self::VERSION) {
            return $this;
        }

        $tmp = unpack("q", substr($data, 1, 8));
        $timestamp = (int) $tmp[1];

        if ($timestamp !== -1) {
            $this->expiration = new DateTime();
            $this->expiration->setTimestamp($timestamp);
        }

        $this->set(unserialize(substr($data, 9)));

        return $this;
    }
}
