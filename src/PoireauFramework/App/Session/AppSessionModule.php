<?php

namespace PoireauFramework\App\Session;

use PoireauFramework\Http\Cookie\CookieService;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Session\Behavior\CookieSessionIdBehavior;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\SessionModule;
use PoireauFramework\Session\Storage\FileSessionStorage;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * Module for session
 */
class AppSessionModule extends SessionModule
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        parent::configure($injector);

        $injector->impl(SessionBehaviorInterface::class, CookieSessionIdBehavior::class);
        $injector->impl(SessionStorageInterface::class, FileSessionStorage::class);

        $injector->factory(CookieSessionIdBehavior::class, [$this, "createCookieSessionIdBehavior"]);
        $injector->factory(FileSessionStorage::class, [$this, "createFileStorage"]);
    }

    public function createCookieSessionIdBehavior(InjectorInterface $injector): CookieSessionIdBehavior
    {
        $registry = $injector->reg("session");

        return new CookieSessionIdBehavior(
            $injector->get(CookieService::class),
            $registry->has("cookie_name") ? $registry->get("cookie_name") : "sid"
        );
    }

    public function createFileStorage(InjectorInterface $injector): FileSessionStorage
    {
        $registry = $injector->reg("session")->sub("storage");

        return new FileSessionStorage(
            $registry->has("path") ? $registry->get("path") : sys_get_temp_dir()
        );
    }
}
