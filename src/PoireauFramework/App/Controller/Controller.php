<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Injector\InjectorInterface;

/**
 * Class Controller
 */
class Controller
{
    /**
     * @var Application
     */
    private $application;

    /**
     * @var \PoireauFramework\Http\Request\RequestInterface;
     */
    protected $request;

    /**
     * @var \PoireauFramework\Http\Response\ResponseInterface
     */
    protected $response;


    /**
     * Default controller's constructor
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;

        $this->inject($application->injector());
    }

    /**
     * Set the request object
     * @param RequestInterface $request
     */
    public function setRequest(RequestInterface $request): void
    {
        $this->request = $request;
    }

    /**
     * Set the response object
     * @param ResponseInterface $response
     */
    public function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;
    }

    /**
     * @return Application
     */
    public function application(): Application
    {
        return $this->application;
    }

    /**
     * @return RequestInterface
     */
    public function request(): RequestInterface
    {
        return $this->request;
    }

    /**
     * @return ResponseInterface
     */
    public function response(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * Get from registry
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->application->$key;
    }

    /**
     * Hide application
     */
    public function __debugInfo(): array
    {
        return [];
    }

    /**
     * Inject dependencies
     * To override
     */
    protected function inject(InjectorInterface $injector): void {}
}
