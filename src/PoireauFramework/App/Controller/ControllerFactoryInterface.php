<?php

namespace PoireauFramework\App\Controller;

/**
 * Class ControllerFactoryInterface
 */
interface ControllerFactoryInterface
{
    /**
     * Create the controller instance
     * @return Controller The controller
     */
    public function create(string $controllerClass): ?Controller;
}
