<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\App\Kernel\Application;

/**
 * Class BaseControllerFactory
 */
class BaseControllerFactory implements ControllerFactoryInterface
{
    /**
     * @var Application
     */
    private $application;


    /**
     * Construct base controller factory
     * @param Application application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $controllerClass): ?Controller
    {
        if (!class_exists($controllerClass)) {
            return null;
        }

        return new $controllerClass($this->application);
    }
}
