<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\App\Router\Route;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseStack;

/**
 * Resolve controller class from a Route object
 */
class RouterControllerResolver implements ControllerResolverInterface
{
    /**
     * @var object
     */
    protected $config;

    /**
     * @var ControllerFactoryInterface
     */
    protected $factory;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var ResponseStack
     */
    protected $responseStack;


    /**
     * Construct RouterControllerResolver
     */
    public function __construct($config, ControllerFactoryInterface $factory, RequestStack $requestStack, ResponseStack $responseStack)
    {
        $this->config = $config;
        $this->factory = $factory;
        $this->requestStack = $requestStack;
        $this->responseStack = $responseStack;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveController(Route $route): ?callable
    {
        $controllerClass = $this->config->controller_prefix . ucfirst(strtolower($route->controller())) . $this->config->controller_suffix;
        $methodName      = $this->config->method_prefix     . strtolower($route->action())              . $this->config->method_suffix;

        $controller = $this->factory->create($controllerClass);

        if ($controller === null || !method_exists($controller, $methodName)) {
            return null;
        }

        $controller->setRequest($this->requestStack->current());
        $controller->setResponse($this->responseStack->current());

        return [$controller, $methodName];
    }

    /**
     * {@inheritdoc}
     */
    public function resolveArguments(callable $controller, Route $route, RequestInterface $request): array
    {
        return $route->arguments();
    }
}
