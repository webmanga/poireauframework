<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Template\TemplateService;

/**
 * Controller object for front end
 * Do not forget to register @see PoireauFramework\Template\TemplateModule
 */
class FrontEndController extends Controller
{
    /**
     * @var \PoireauFramework\Template\TemplateService
     */
    protected $template;


    /**
     * Initialize The controller
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->template = $application->injector()->get(TemplateService::class);
    }

    /**
     * Render the template
     * @param string $template The template name
     * @param array $vars Vars to pass to the view
     * @return string
     * @see \PoireauFramework\Template\TemplateService::render()
     */
    public function render(string $template, array $vars = []): string
    {
        return $this->template->render($template, $vars);
    }
}
