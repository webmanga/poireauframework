<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\App\Router\Route;
use PoireauFramework\Http\Request\RequestInterface;

/**
 * Resolve a controller from a request
 */
interface ControllerResolverInterface
{
    /**
     * Resolve the controller from a route
     * @param Route $route The route object
     * @return callable|null The controller callable or null if not found
     */
    public function resolveController(Route $route): ?callable;

    /**
     * Resolve the controller's arguments
     * @param callable $controller The controller
     * @param Route $route The route object
     * @param RequestInterface $request
     * @return array List of arguments
     */
    public function resolveArguments(callable $controller, Route $route, RequestInterface $request): array;
}
