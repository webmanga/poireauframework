<?php

namespace PoireauFramework\App\Database;

use PoireauFramework\Database\Mapping\Resolver\MapperResolverInterface;
use PoireauFramework\Database\Mapping\SqlMapper;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;

/**
 * Class SqlModel
 */
abstract class SqlModel extends SqlMapper implements InjectorInstantiable
{
    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return $injector->get(MapperResolverInterface::class)->get(get_called_class());
    }
}
