<?php

namespace PoireauFramework\App\Database\Console;

use PoireauFramework\Console\Command\Command;
use PoireauFramework\Console\Command\CommandArgument;
use PoireauFramework\Console\Command\CommandInput;
use PoireauFramework\Console\Command\CommandOptions;

use PoireauFramework\Database\Mapping\MapperInterface;
use PoireauFramework\Util\File\ClassFile;
use PoireauFramework\Util\File\ClassFileIterator;

/**
 * Command for creating mapper schemas
 */
class CreateSchemasCommand extends Command
{
    /**
     * {@inhertidoc}
     */
    public function name(): string
    {
        return "db:create";
    }

    /**
     * {@inheritdoc}
     */
    public function shortDescription(): string
    {
        return "Create mappers databases schemas";
    }

    /**
     * {@inheridoc}
     */
    protected function configure(CommandOptions $options): void
    {
        $options
            ->addArgument("paths")
            ->setArray()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(CommandInput $input): void
    {
        foreach ($input->argument("paths") as $path) {
            $it = ClassFileIterator::recursive($path);

            $it->rewind();

            // @todo foreach
            while ($it->valid()) {
                $classFile = $it->current();

                if (!$classFile->isClass()) {
                    $it->next();
                    continue;
                }

                $classFile->load();

                if (!is_subclass_of($classFile->getFullClassName(), MapperInterface::class)) {
                    $it->next();
                    continue;
                }

                $this->output->writeln("Found mapper '{$classFile->getFullClassName()}'");

                $mapper = $this->console->injector()->get($classFile->getFullClassName());

                $mapper->schema()->create();

                $it->next();
            }
        }
    }
}
