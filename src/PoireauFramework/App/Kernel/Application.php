<?php

namespace PoireauFramework\App\Kernel;

use PoireauFramework\App\AppModule;
use PoireauFramework\App\Http\AppHttpModule;
use PoireauFramework\App\Http\RequestRunner;
use PoireauFramework\Config\Config;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Event\EventModule;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Kernel\AbstractKernel;
use PoireauFramework\Kernel\RunnerInterface;
use PoireauFramework\Registry\RegistryFactory;

/**
 * Class Application
 */
class Application extends AbstractKernel
{
    /**
     * @var Runner
     */
    protected $runner;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var array
     */
    private $defaultModules;


    /**
     * Create the Application
     * @param array $defaults The application defaults
     */
    public function __construct(array $defaults)
    {
        $registry = RegistryFactory::createFromArray($defaults);

        $this->config = new Config($registry->sub("config"));
        $this->runner = new Runner();


        $injector = new BaseInjector($registry);

        $injector->instance($this);
        $injector->instance($this->config);
        $injector->instance($registry);
        $injector->register(new EventModule());

        $eventDispatcher = $injector->get(EventDispatcherInterface::class);

        $this->initDefaultModules($defaults);

        parent::__construct($injector, $registry, $eventDispatcher);
    }

    private function initDefaultModules(array $config): void
    {
        $this->defaultModules = $this->kernelDefaultModules();

        if (isset($config["kernel_modules"])) {
            foreach ($config["kernel_modules"] as $name => $module) {
                $this->defaultModules[$name] = $module;
            }
        }
    }

    /**
     * Get the application configuration
     * @return Config
     */
    public function config(): Config
    {
        return $this->config;
    }

    /**
     * Get the request stack
     * @return RequestStack
     */
    public function requestStack(): RequestStack
    {
        return $this->injector()->get(RequestStack::class);
    }

    /**
     * Get the current request
     * @return RequestInterface
     */
    public function request(): RequestInterface
    {
        return $this->requestStack()->current();
    }

    /**
     * Get the base request
     * @return RequestInterface
     */
    public function baseRequest(): RequestInterface
    {
        return $this->requestStack()->first();
    }

    /**
     * Get the request runner
     * @return RequestRunner
     */
    public function requestRunner(): RequestRunner
    {
        return $this->injector()->get(RequestRunner::class);
    }

    /**
     * Get the current response
     * @return ResponseInterface
     */
    public function response(): ResponseInterface
    {
        return $this->injector()->get(ResponseStack::class)->current();
    }

    /**
     * Get a registry var
     */
    public function __get(string $key)
    {
        return $this->registry()->get($key);
    }

    /**
     * {@inehritdoc}
     */
    protected function runner(): RunnerInterface
    {
        return $this->runner;
    }

    /**
     * {@inehritdoc}
     */
    final protected function defaultModules(): array
    {
        return $this->defaultModules;
    }

    /**
     * Get kernel default modules
     * To override for adding modules
     * @return array
     */
    protected function kernelDefaultModules(): array
    {
        return [
            "http" => new AppHttpModule(),
            "app"  => new AppModule()
        ];
    }
}
