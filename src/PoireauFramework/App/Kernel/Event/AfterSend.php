<?php

namespace PoireauFramework\App\Kernel\Event;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\App\Kernel\Event\AfterSendListenerInterface;

/**
 * Event dispatched after response send
 * {@link AfterSendListenerInterface}
 */
class AfterSend implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\App\\Kernel\\Event\\AfterSend";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof AfterSendListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onAfterSend($arguments[0]);
    }
}