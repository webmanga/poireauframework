<?php

namespace PoireauFramework\App\Kernel\Event;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\App\Kernel\Event\BeforeSendListenerInterface;

/**
 * Event dispatched before send a response.
 * Used to transform the response object
 * @see BeforeSendListenerInterface
 */
class BeforeSend implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\App\\Kernel\\Event\\BeforeSend";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof BeforeSendListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onBeforeSend($arguments[0], $arguments[1]);
    }
}