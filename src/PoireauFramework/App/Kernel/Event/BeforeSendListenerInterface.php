<?php

namespace PoireauFramework\App\Kernel\Event;

use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Kernel\KernelInterface;

/**
 * Listener for AfterRun event
 * @see \PoireauFramework\App\Kernel\Event\BeforeSend
 */
interface BeforeSendListenerInterface
{
    public function onBeforeSend(ResponseInterface $response, RequestInterface $request): void;
}