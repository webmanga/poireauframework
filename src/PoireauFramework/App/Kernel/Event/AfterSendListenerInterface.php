<?php

namespace PoireauFramework\App\Kernel\Event;

use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Kernel\KernelInterface;

/**
 * Listener for AfterSend event
 * @see \PoireauFramework\App\Kernel\Event\AfterSend
 */
interface AfterSendListenerInterface
{
    public function onAfterSend(ResponseInterface $response): void;
}