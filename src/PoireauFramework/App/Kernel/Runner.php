<?php

namespace PoireauFramework\App\Kernel;

use PoireauFramework\App\Kernel\Event\AfterSend;
use PoireauFramework\App\Kernel\Event\BeforeSend;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Http\Response\ResponseSenderInterface;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Kernel\KernelInterface;
use PoireauFramework\Kernel\RunnerInterface;

/**
 * Class Runner
 */
class Runner implements RunnerInterface
{
    /**
     * {@inehritdoc}
     */
    public function run(KernelInterface $kernel): void
    {
        $request = $kernel->baseRequest();

        $response = $kernel->requestRunner()->run($request);
        $sender   = $kernel->injector()->get(ResponseSenderInterface::class);

        $kernel->events()->dispatch(BeforeSend::NAME, [$response, $request]);

        $sender->send($response);

        $kernel->events()->dispatch(AfterSend::NAME, [$response]);
    }
}
