<?php

namespace PoireauFramework\App\Template\Html;

/**
 * Utilities for HTML
 */
class HtmlUtils
{
    /**
     * @var int
     */
    static private $idCount = 0;


    /**
     * Render element attributes as string
     */
    static public function renderAttributes(HtmlElementInterface $element): string
    {
        $out = "";

        if ($element->getId()) {
            $out .= "id=\"" . htmlentities($element->getId()) . "\" ";
        }

        $classes = $element->getClasses();

        if (!empty($classes)) {
            $out .= "class=\"" . htmlentities(implode(" ", $classes)) . "\" ";
        }

        foreach ($element->getAttributes() as $attr => $val) {
            $out .= self::cleanAttributeName($attr) . "=\"" . htmlentities($val) . "\" ";
        }

        foreach ($element->getProperties() as $attr) {
            $out .= self::cleanAttributeName($attr) . " ";
        }

        return $out;
    }

    /**
     * Clean an attribute name
     */
    static public function cleanAttributeName(string $name): string
    {
        $clean = "";
        $len = strlen($name);

        for ($i = 0; $i < $len; ++$i) {
            $c = $name[$i];

            if (
                ($c >= 'a' && $c <= 'z')
                || ($c >= 'A' && $c <= 'Z')
                || ($c >= '0' && $c <= '9')
                || $c === '-' || $c === ':'
                || $c === '_' || $c === '.'
            ) {
                $clean .= $c;
            }
        }

        return $clean;
    }

    /**
     * Generate an ID for the element
     */
    static public function generateId(HtmlElementInterface $element): string
    {
        $id = "__id_";

        $id .= strtolower(substr(strrchr(get_class($element), "\\"), 1));
        $id .= "_" . self::$idCount++ . "__";

        $element->id($id);

        return $id;
    }

    /**
     * @internal For testing usage only
     */
    static public function setIdCount(int $count): void
    {
        self::$idCount = $count;
    }
}
