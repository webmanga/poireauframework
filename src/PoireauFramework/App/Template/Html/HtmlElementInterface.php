<?php

namespace PoireauFramework\App\Template\Html;

/**
 * Interface for build HTML elements in PHP
 */
interface HtmlElementInterface
{
    /**
     * Set an attribute on the element
     * @return $this
     */
    public function attribute(string $name, $value): HtmlElementInterface;

    /**
     * Get all registered attributes
     * @return string[]
     */
    public function getAttributes(): array;

    /**
     * Add a new class
     * @return $this
     */
    public function addClass(string $className): HtmlElementInterface;

    /**
     * Get all classes
     * @return string[]
     */
    public function getClasses(): array;

    /**
     * Add a property
     * @return $this
     */
    public function property(string $name, bool $active = true): HtmlElementInterface;

    /**
     * Get list of active properties
     * @return string[]
     */
    public function getProperties(): array;

    /**
     * Set the element id
     * @return $this
     */
    public function id(string $id): HtmlElementInterface;

    /**
     * Get the element id
     */
    public function getId(): ?string;
}
