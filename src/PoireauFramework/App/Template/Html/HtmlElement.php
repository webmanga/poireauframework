<?php

namespace PoireauFramework\App\Template\Html;

/**
 * Simple implementation for create Html element adapters
 */
class HtmlElement implements HtmlElementInterface
{
    protected $attributes = [];
    protected $classes = [];
    protected $properties = [];
    protected $id;


    /**
     * {@inheritdoc}
     */
    public function attribute(string $name, $value): HtmlElementInterface
    {
        $this->attributes[$name] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function addClass(string $className): HtmlElementInterface
    {
        $this->classes[] = $className;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getClasses(): array
    {
        return $this->classes;
    }

    /**
     * {@inheritdoc}
     */
    public function property(string $name, bool $active = true): HtmlElementInterface
    {
        $this->properties[$name] = $active;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProperties(): array
    {
        $properties = [];

        foreach ($this->properties as $k => $v) {
            if ($v) {
                $properties[] = $k;
            }
        }

        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public function id(string $id): HtmlElementInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?string
    {
        return $this->id;
    }
}
