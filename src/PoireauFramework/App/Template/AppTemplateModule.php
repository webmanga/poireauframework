<?php

namespace PoireauFramework\App\Template;

use PoireauFramework\App\Template\Listener\ViewResolverListener;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Kernel\BootableModuleInterface;
use PoireauFramework\Kernel\KernelInterface;
use PoireauFramework\Template\Helper\InjectorHelperLocator;
use PoireauFramework\Template\TemplateModule;
use PoireauFramework\Template\TemplateService;

/**
 * Template module for App package
 */
class AppTemplateModule extends TemplateModule implements BootableModuleInterface
{
    /**
     * @var bool
     */
    protected $useResolver;


    /**
     * @param bool $useResolver Use the view resolver listener ?
     */
    public function __construct(bool $useResolver = false)
    {
        $this->useResolver = $useResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function boot(KernelInterface $kernel): void
    {
        if ($this->useResolver) {
            $kernel->register($kernel->injector()->get(ViewResolverListener::class));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        parent::configure($injector);

        $injector->factory(ViewResolverListener::class, [$this, "createViewResolverListener"]);
    }

    public function createHelperLocator(InjectorInterface $injector): InjectorHelperLocator
    {
        $helpers = parent::createHelperLocator($injector);

        $helpers->addNamespace("PoireauFramework\\App\\Template\\Helper");

        return $helpers;
    }

    public function createViewResolverListener(InjectorInterface $injector): ViewResolverListener
    {
        return new ViewResolverListener($injector->get(TemplateService::class));
    }
}
