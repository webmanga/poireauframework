<?php

namespace PoireauFramework\App\Template\Helper;

use PoireauFramework\App\Security\SecurityService;
use PoireauFramework\App\Security\UserInterface;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;

/**
 * Helper for security purpose (session and access check)
 */
class Security implements HelperInterface, InjectorInstantiable
{
    /**
     * @var SecurityService
     */
    protected $service;


    /**
     *
     */
    public function __construct(SecurityService $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "security";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        return $this;
    }

    /**
     * Check if the client is logged
     */
    public function logged(): bool
    {
        return $this->service->isLogged();
    }

    /**
     * Get the logged user.
     * If the client is not logged, will return null
     */
    public function user(): UserInterface
    {
        return $this->service->user();
    }

    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new Security($injector->get(SecurityService::class));
    }
}
