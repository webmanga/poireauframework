<?php

namespace PoireauFramework\App\Template\Helper;

use PoireauFramework\Config\Config;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;

/**
 * Helper for urls
 */
class Url implements HelperInterface, InjectorInstantiable
{
    /**
     * @var ConfigItem
     */
    protected $config;

    /**
     * @var RequestStack
     */
    protected $requestStack;


    /**
     * @param ConfigItem $config
     */
    public function __construct(ConfigItem $config, RequestStack $requestStack)
    {
        $this->config = $config;
        $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "url";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        if (!empty($arguments)) {
            return $this->page(...$arguments);
        }

        return $this;
    }

    /**
     * Get the base URL (without le last "/")
     * @return string
     */
    public function base(): string
    {
        return $this->config->get("base") . dirname($this->requestStack->current()->script());
    }

    /**
     * Get page URL
     * @param string $page The path to the page
     * @param array $get The GET parameters
     * @return string The URL
     */
    public function page(string $page = "/", array $get = []): string
    {
        $url = $this->config->get("base");
        $url .= $this->config->get("rewriting") ? dirname($this->requestStack->current()->script()) : $this->requestStack->current()->script();
        $url .= $page;

        if (!empty($get)) {
            $url .= "?" . http_build_query($get);
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new Url(
            $injector->get(Config::class)->item("url"),
            $injector->get(RequestStack::class)
        );
    }
}
