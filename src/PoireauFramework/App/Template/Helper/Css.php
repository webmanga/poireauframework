<?php

namespace PoireauFramework\App\Template\Helper;

use PoireauFramework\Config\Config;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;

/**
 * Helper for handling Css
 */
class Css implements HelperInterface, InjectorInstantiable
{
    /**
     * @var ConfigItem
     */
    private $config;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var string[]
     */
    private $sources = [];


    /**
     * @param Url $url
     */
    public function __construct(ConfigItem $config, Url $url)
    {
        $this->config = $config;
        $this->url = $url;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "css";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        if (!empty($arguments)) {
            return $this->add(...$arguments);
        }

        return $this;
    }

    /**
     * Get script url
     * @param string $file The script name
     * @return string The absolute URL of script
     */
    public function url(string $file): string
    {
        return $this->url->base() . $this->config->css . $file;
    }

    /**
     * Add a new css file
     * @param string css The css file
     * @param bool $isAbsolute true to use absolute css URL
     * @return $this
     */
    public function add(string $file, bool $isAbsolute = false): Css
    {
        $this->sources[] = $isAbsolute ? $file : $this->url($file);

        return $this;
    }

    /**
     * Render css tags
     */
    public function __toString(): string
    {
        $out = "";

        foreach ($this->sources as $url) {
            $out .= '<link rel="stylesheet" lang="text/css" href="' . htmlentities($url) . '"/>';
        }

        return $out;
    }

    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new Css(
            $injector->get(Config::class)->item("asset"),
            $injector->get(Url::class)
        );
    }
}
