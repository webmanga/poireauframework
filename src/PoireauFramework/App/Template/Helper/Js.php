<?php

namespace PoireauFramework\App\Template\Helper;

use PoireauFramework\Config\Config;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;

/**
 * Helper for handling Javascript
 */
class Js implements HelperInterface, InjectorInstantiable
{
    /**
     * @var ConfigItem
     */
    private $config;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var string[]
     */
    private $sources = [];


    /**
     * @param ConfigItem $config
     * @param Url url
     */
    public function __construct(ConfigItem $config, Url $url)
    {
        $this->config = $config;
        $this->url = $url;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "js";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        return $this;
    }

    /**
     * Get script url
     * @param string $script The script name
     * @return string The absolute URL of script
     */
    public function url(string $script): string
    {
        return $this->url->base() . $this->config->js . $script;
    }

    /**
     * Add a new javascript source
     * @param string $script The script name, or script absolute URL
     * @param bool $isAbsolute true to use absolute script URL
     * @return $this
     */
    public function src(string $script, bool $isAbsolute = false): Js
    {
        $this->sources[] = $isAbsolute ? $script : $this->url($script);

        return $this;
    }

    /**
     * Render js tags
     */
    public function __toString(): string
    {
        $out = "";

        foreach ($this->sources as $url) {
            $out .= '<script lang="text/javascript" src="' . htmlentities($url) . '"></script>';
        }

        return $out;
    }

    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new Js(
            $injector->get(Config::class)->item("asset"),
            $injector->get(Url::class)
        );
    }
}
