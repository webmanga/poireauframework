<?php

namespace PoireauFramework\App\Template\Helper;

use PoireauFramework\App\Router\RouterInterface;
use PoireauFramework\App\Security\SecurityService;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;
use PoireauFramework\Util\Strings;

/**
 * Helper for make navigation menu
 * @todo selected (setter + resolver)
 */
class Menu implements HelperInterface, InjectorInstantiable
{
    const TYPE_SEPARATOR = "separator";
    const TYPE_DROPDOWN  = "dropdown";
    const TYPE_LINK      = "link";

    /**
     * @var Url
     */
    private $url;

    /**
     * @var RequestStack
     */
    private $requests;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SecurityService
     */
    private $security;

    /**
     * @var array
     */
    private $menus = [];

    /**
     * @var string
     */
    private $current;


    /**
     * @param Url url
     */
    public function __construct(Url $url, RequestStack $requests, RouterInterface $router, SecurityService $security = null)
    {
        $this->url = $url;
        $this->requests = $requests;
        $this->router = $router;
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "menu";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        switch (count($arguments)) {
            case 1:
                $this->current = $arguments[0];
                break;

            case 2:
                $this->merge($arguments[0], $arguments[1]);
                break;
        }

        return $this;
    }

    /**
     * Merge or create menu
     *
     * @param string|array $name The menu name (should be unique). If the name is already configured, you can directly pass the second parameter
     * @param array $data The menu parameters
     * @param bool $prepend If true, the menu data will be added at the begin of the menu
     *
     * @return $this
     */
    public function merge($name, $data = null, bool $prepend = false): Menu
    {
        if (is_array($name)) {
            $prepend = (bool) $data;
            $data = $name;
            $name = $this->current;
        } else {
            $this->current = $name;
        }

        if (!isset($this->menus[$name])) {
            $prepend = false;
            $this->menus[$name] = [];
        }

        $normalized = $this->normalize($data);

        if ($prepend) {
            $this->menus[$name] = array_merge($normalized, $this->menus[$name]);
        } else {
            foreach ($normalized as $k => $v) {
                $this->menus[$name][$k] = $v;
            }
        }

        return $this;
    }

    /**
     * Set the menu css class
     *
     * @param string $menu The menu name.
     * @param string $cssClass The CSS class. If not set, the first argument will be used
     *
     * @return $this
     */
    public function menuClass(string $menu, string $cssClass = null): Menu
    {
        if (empty($cssClass)) {
            $cssClass = $menu;
            $menu     = $this->current;
        }

        $this->menus[$menu]["_metadata"]["menu.class"] = $cssClass;

        return $this;
    }

    /**
     * Render a menu as HTML string
     */
    public function render(string $name = null): string
    {
        if (empty($name)) {
            $name = $this->current;
        }

        return $this->renderMenu($this->menus[$name]);
    }

    /**
     * Render a menu as HTML string
     */
    public function renderMenu(array $menu): string
    {
        $metadata = (array) $menu["_metadata"];
        $html = '<ul class="' . $metadata["menu.class"] . '">';

        foreach ($menu as $name => $data) {
            if ($name === "_metadata") {
                continue;
            }

            if (!$this->hasAccess($data)) {
                continue;
            }

            $html .= $this->renderItem($name, $data);
        }

        $html .= "</ul>";

        return $html;
    }

    /**
     *
     */
    protected function renderItem(string $name, array $data): string
    {
        switch ($data["type"]) {
            case self::TYPE_SEPARATOR:
                return $this->renderSeparator($data);
            case self::TYPE_LINK:
                return $this->renderLink($name, $data);
            case self::TYPE_DROPDOWN:
                return $this->renderDropdown($name, $data);
        }

        throw new \Exception("Invalid item type " . $data["type"]);
    }

    /**
     * Render a separator
     */
    protected function renderSeparator(array $item): string
    {
        return '<li class="divider"></li>';
    }

    /**
     * Render a link
     */
    protected function renderLink(string $name, array $item): string
    {
        $html = "<li";

        if ($item["active"]) {
            $html .= " class=\"active\"";
        }

        $html .= "><a href=\"" . $this->url($item["url"]) . "\">" . htmlentities($name) . "</a></li>";

        return $html;
    }

    /**
     * Render a dropdown
     */
    protected function renderDropdown(string $name, array $item): string
    {
        $html = "<li class=\"dropdown";

        if ($item["active"]) {
            $html .= " active";
        }

        $html .= "\"><a href=\"" . $this->url($item["url"]) . "\"";
        $html .= " class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        $html .= htmlentities($name) . "</a>";
        $html .= $this->renderMenu($item["menu"]);
        $html .= "</li>";

        return $html;
    }

    /**
     * Normalize the menu data
     */
    protected function normalize(array $menu, bool $dropdown = false): array
    {
        $normalized = [];

        foreach ($menu as $k => $v) {
            $normalized[$k] = $this->normalizeItem($v);
        }

        if (!isset($normalized["_metadata"])) {
            $normalized["_metadata"] = [];
        }

        if (!isset($normalized["_metadata"]["menu.class"])) {
            $normalized["_metadata"]["menu.class"] = $dropdown ? "dropdown-menu" : "nav";
        }

        return $normalized;
    }

    /**
     * Normal one menu item
     */
    protected function normalizeItem($item): array
    {
        if ($item === "separator") {
            return ["type" => self::TYPE_SEPARATOR];
        }

        if (is_string($item)) {
            return [
                "type"   => self::TYPE_LINK,
                "url"    => $item,
                "active" => false
            ];
        } else {
            $normalized = (array) $item;
        }

        if (isset($normalized["menu"])) {
            $normalized["menu"] = $this->normalize($normalized["menu"], true);
            $normalized["type"] = self::TYPE_DROPDOWN;

            if (!isset($normalized["active"])) {
                $normalized["active"] = $this->isDropdownActive($normalized["menu"]);
            }
        } elseif (!isset($normalized["type"])) {
            $normalized["type"] = self::TYPE_LINK;
        }

        if (!isset($normalized["url"])) {
            $normalized["url"] = "#";
        }

        if (
            is_array($normalized["url"])
            && isset($normalized["url"]["route"])
        ) {
            $route = $normalized["url"]["route"];

            if (!isset($normalized["active"])) {
                $normalized["active"] = $this->isActiveRoute($route);
            }

            $normalized["url"]["route"] = $this->router->route($route);
        }

        return $normalized;
    }

    /**
     * Make URL
     */
    protected function url($url): string
    {
        if (is_string($url)) {
            if (
                $url[0] === '#'
                || Strings::startsWith($url, "http://")
                || Strings::startsWith($url, "https://")
            ) {
                return $url;
            }

            return $this->url->page($url);
        }

        $get = $url["get"] ?? [];

        if (isset($url["route"])) {
            return $this->url->page(
                $this->router->makeUrl($url["route"], $get)
            );
        }

        return $this->url->page($url["page"], $get);
    }

    /**
     * Check if the user can access to the item
     */
    protected function hasAccess(array $data): bool
    {
        if ($this->security === null) {
            return true;
        }

        if (
            isset($data["url"])
            && is_array($data["url"])
            && isset($data["url"]["route"])
        ) {
            return $this->security->hasAccess($data["url"]["route"]);
        }

        return true;
    }

    /**
     * Check if the item is current URL
     */
    protected function isActiveRoute(array $params): bool
    {
        $request = $this->requests->current();

        if (!$request->has("__route")) {
            return false;
        }

        $route = $request->attachment("__route");

        return $this->router->match($params, $route);
    }

    /**
     * Check if a dropown menu is active
     */
    protected function isDropdownActive(array $menu): bool
    {
        foreach ($menu as $item) {
            if (isset($item["active"]) && $item["active"] === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new Menu(
            $injector->get(Url::class),
            $injector->get(RequestStack::class),
            $injector->get(RouterInterface::class),
            $injector->has(SecurityService::class)
                ? $injector->get(SecurityService::class)
                : null
        );
    }
}
