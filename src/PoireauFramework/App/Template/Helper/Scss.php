<?php

namespace PoireauFramework\App\Template\Helper;

use PoireauFramework\App\Asset\Scss as ScssCompiler;
use PoireauFramework\App\Asset\ScssHandler;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;

/**
 * Helper for Scss
 */
class Scss implements HelperInterface, InjectorInstantiable
{
    /**
     * @var Css
     */
    private $css;

    /**
     * @var ScssCompiler
     */
    private $compiler;

    /**
     * @var ScssHandler
     */
    private $handler;


    /**
     * Scss constructor.
     *
     * @param Css $css
     * @param ScssCompiler $compiler
     * @param ScssHandler $handler
     */
    public function __construct(Css $css, ScssCompiler $compiler, ScssHandler $handler = null)
    {
        $this->css = $css;
        $this->compiler = $compiler;
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "scss";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        if ($arguments) {
            return $this->add($arguments[0]);
        }

        return $this;
    }

    /**
     * Compile scss and add css file
     *
     * @param string $file the SCSS file
     *
     * @return $this
     */
    public function add($file)
    {
        if ($this->handler === null) {
            $this->compiler->compile($file);
        }

        $this->css->add("$file.css");

        return $this;
    }

    /**
     * Get the CSS helper instance
     *
     * @return Css
     */
    public function css()
    {
        return $this->css;
    }

    /**
     * {@inheritdoc}
     */
    static public function createInstance(InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new self(
            $injector->get(Css::class),
            $injector->get(ScssCompiler::class),
            $injector->has(ScssHandler::class) ? $injector->get(ScssHandler::class) : null
        );
    }
}
