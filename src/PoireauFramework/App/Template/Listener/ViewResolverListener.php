<?php

namespace PoireauFramework\App\Template\Listener;

use PoireauFramework\App\Http\RequestRunner;
use PoireauFramework\App\Kernel\Event\BeforeSend;
use PoireauFramework\App\Kernel\Event\BeforeSendListenerInterface;
use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Template\TemplateService;

/**
 * Resolve the view template using the route
 */
class ViewResolverListener implements BeforeSendListenerInterface, EventListenerInterface
{
    /**
     * @var TemplateService
     */
    private $service;


    /**
     *
     */
    public function __construct(TemplateService $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function onBeforeSend(ResponseInterface $response, RequestInterface $request): void
    {
        if (!$request->has(RequestRunner::ROUTE_ATTACHMENT_KEY)) {
            return;
        }

        if ($response->getBody() === null || !($response->getBody() instanceof ArrayBag)) {
            return;
        }

        $route = $request->attachment(RequestRunner::ROUTE_ATTACHMENT_KEY);

        $view = $this->service->render(
            $route->controller() . "/" . $route->action(),
            $response->getBody()->raw()
        );

        $response->body(new TextBag($view));
    }

    /**
     * {@inheritdoc}
     */
    public function eventTypes(): array
    {
        return [BeforeSend::NAME];
    }
}
