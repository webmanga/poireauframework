<?php

namespace PoireauFramework\App\Asset;

use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Handle an asset type
 */
interface HandlerInterface
{
    /**
     * Get the request base path
     */
    public function basePath(): string;

    /**
     * Check if the handle supports the request
     */
    public function supports(string $path, RequestInterface $request): bool;

    /**
     * Handle the request and create the response
     */
    public function handle(string $path, RequestInterface $request): ResponseInterface;
}
