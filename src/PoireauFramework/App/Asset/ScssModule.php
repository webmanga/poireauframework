<?php

namespace PoireauFramework\App\Asset;

use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Formatter\Compact;
use Leafo\ScssPhp\Formatter\Compressed;
use Leafo\ScssPhp\Formatter\Crunched;
use Leafo\ScssPhp\Formatter\Expanded;
use Leafo\ScssPhp\Formatter\Nested;
use PoireauFramework\App\Asset\Cache\AlwaysRecompile;
use PoireauFramework\App\Asset\Cache\CacheStrategyInterface;
use PoireauFramework\App\Asset\Cache\TimeToLiveCache;
use PoireauFramework\App\Asset\Cache\VersionCheck;
use PoireauFramework\Cache\FileCache;
use PoireauFramework\Config\Config;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Kernel\BootableModuleInterface;
use PoireauFramework\Kernel\KernelInterface;

/**
 * Configure ScssPhp
 * @link http://leafo.github.io/scssphp/docs/
 *
 * Config :
 * - asset.scss.src : string - The path for scss sources
 * - asset.scss.out : string - The path for css output
 * - asset.scss.formatter : string - The formatter name
 * - asset.scss.strategy : string - The cache strategy. Can be "recompile", "ttl", "version"
 * - asset.scss.ttl : int - For "ttl" strategy : the time to live in seconds (if not set, or negative, no expiration given)
 * - asset.scss.version : string - For "version" strategy : the SCSS version string
 */
class ScssModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->factory(Compiler::class, [$this, "createScssCompiler"]);
        $injector->factory(Scss::class, [$this, "createScss"]);
        $injector->factory(ScssHandler::class, [$this, "createScssHandler"]);
        $injector->factory(CacheStrategyInterface::class, [$this, "createCacheStrategy"]);
    }

    /**
     * @param InjectorInterface $injector
     *
     * @return Compiler
     */
    public function createScssCompiler(InjectorInterface $injector)
    {
        $config = $injector->get(Config::class)->item("asset")->item("scss");

        $compiler = new Compiler();

        $compiler->setImportPaths(
            $config->get("src")
        );

        switch ($config->get("formatter")) {
            case "expanded":
                $compiler->setFormatter(Expanded::class);
                break;
            case "nested":
                $compiler->setFormatter(Nested::class);
                break;
            case "compact":
                $compiler->setFormatter(Compact::class);
                break;
            case "runched":
                $compiler->setFormatter(Crunched::class);
                break;
            case "compressed":
            default:
                $compiler->setFormatter(Compressed::class);
                break;
        }

        return $compiler;
    }

    /**
     * @param InjectorInterface $injector
     *
     * @return Scss
     */
    public function createScss(InjectorInterface $injector)
    {
        return new Scss(
            $injector->get(Config::class)->item("asset")->item("scss"),
            $injector->get(Compiler::class),
            $injector->get(CacheStrategyInterface::class)
        );
    }

    public function createScssHandler(InjectorInterface $injector, $path)
    {
        return new ScssHandler(
            $injector->get(Scss::class),
            $path
        );
    }

    public function createCacheStrategy(InjectorInterface $injector)
    {
        $config = $injector->get(Config::class)->item("asset")->item("scss");

        switch ($config->get("strategy")) {
            case "ttl":
                $ttl = $config->has("ttl") ? (int) $config->get("ttl") : null;
                return new TimeToLiveCache(new FileCache(), $ttl);

            case "version":
                return new VersionCheck(new FileCache(), $config->get("version"));

            case "recompile":
            case null:
                return new AlwaysRecompile();
        }
    }
}
