<?php

namespace PoireauFramework\App\Asset;

use Leafo\ScssPhp\Compiler;
use PoireauFramework\App\Asset\Cache\AlwaysRecompile;
use PoireauFramework\App\Asset\Cache\CacheStrategyInterface;
use PoireauFramework\Cache\DummyCache;
use PoireauFramework\Cache\Psr\CacheItemPoolInterface;
use PoireauFramework\Config\ConfigItem;

/**
 * Class for handling Scss files
 */
class Scss
{
    /**
     * @var ConfigItem
     */
    protected $config;

    /**
     * @var Compiler
     */
    protected $compiler;

    /**
     * @var CacheStrategyInterface
     */
    protected $cache;


    /**
     * Scss constructor.
     *
     * @param ConfigItem $config
     * @param Compiler $compiler
     * @param CacheStrategyInterface $cache
     */
    public function __construct(ConfigItem $config, Compiler $compiler, CacheStrategyInterface $cache = null)
    {
        $this->config = $config;
        $this->compiler = $compiler;
        $this->cache = $cache ?: new AlwaysRecompile();
    }

    /**
     * Compile a SCSS file to CSS file
     *
     * @param string $file The file name
     */
    public function compile($file)
    {
        $out = $this->config->get("out") . $file . ".css";

        $css = $this->generate($file);

        if (!is_dir(dirname($out))) {
            mkdir(dirname($out), 0755, true);
        }

        file_put_contents($out, $css);
        chmod($out, 0644);
    }

    /**
     * Generate CSS from a sass file
     *
     * @param string $file The scss file name
     *
     * @return string The compiled CSS
     */
    public function generate($file)
    {
        $in = $this->scssFile($file);

        if ($this->cache->needsCompile($in)) {
            $this->compiler->addImportPath(dirname($in));

            $scss = file_get_contents($in);

            $css = $this->compiler->compile($scss, $in);
            $this->cache->save($in, $css);

            return $css;
        }

        return $this->cache->get($in);
    }

    /**
     * Get the scss file name
     *
     * @param string $file
     *
     * @return string
     */
    public function scssFile($file)
    {
        return $this->config->get("src") . $file;
    }
}
