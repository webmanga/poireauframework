<?php

namespace PoireauFramework\App\Asset;

use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Util\Strings;

/**
 * Handler for scss
 */
class ScssHandler implements HandlerInterface
{
    /**
     * @var Scss
     */
    private $scss;

    /**
     * @var string
     */
    private $path;


    /**
     * ScssHandler constructor.
     *
     * @param Scss $scss
     * @param string $path
     */
    public function __construct(Scss $scss, $path)
    {
        $this->scss = $scss;
        $this->path = $path;
    }

    /**
     * {@inheritdoc}
     */
    public function basePath(): string
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(string $path, RequestInterface $request): bool
    {
        if (!Strings::endsWith($path, ".css")) {
            return false;
        }

        return is_file($this->realScssFile($path));
    }

    /**
     * {@inheritdoc}
     */
    public function handle(string $path, RequestInterface $request): ResponseInterface
    {
        $response = new Response();

        $response->headers()->setContentType("text/css");
        $response->body(new TextBag($this->scss->generate(substr($path, 0, -4) . ".scss")));

        return $response;
    }

    /**
     * Get the real scss file
     *
     * @param string $path
     *
     * @return string
     */
    protected function realScssFile($path)
    {
        return $this->scss->scssFile(substr($path, 0, -4) . ".scss");
    }
}
