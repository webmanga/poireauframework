<?php

namespace PoireauFramework\App\Asset;

use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Simple handler for file fowarding
 */
class FileHandler implements HandlerInterface
{
    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string[]
     */
    private $mapping = [];


    /**
     *
     */
    public function __construct(string $basePath, array $mapping)
    {
        $this->basePath = $basePath;
        $this->mapping  = $mapping;
    }

    /**
     * {@inheritdoc}
     */
    public function basePath(): string
    {
        return $this->basePath;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(string $path, RequestInterface $request): bool
    {
        $path = $this->getRealPath($path);

        return $path !== "" && is_file($path);
    }

    /**
     * {@inheritdoc}
     */
    public function handle(string $path, RequestInterface $request): ResponseInterface
    {
        $response = new Response();

        $response->body(new TextBag(file_get_contents($this->getRealPath($path))));

        return $response;
    }

    /**
     * Get the real path from requested path
     */
    protected function getRealPath(string $path): string
    {
        $parts = explode("/", ltrim($path, "/"), 2);

        if (!isset($this->mapping[$parts[0]])) {
            return "";
        }

        return realpath($this->mapping[$parts[0]] . DIRECTORY_SEPARATOR . $parts[1]);
    }
}
