<?php


namespace PoireauFramework\App\Asset\Cache;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Strategy using cache TTL for store and recompile SCSS
 * You can set NULL TTL for definitive cache
 */
class TimeToLiveCache implements CacheStrategyInterface
{
    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var \DateInterval|int|null
     */
    private $ttl;

    /**
     * @var CacheItemInterface[]
     */
    private $items = [];


    /**
     * TimeToLiveCache constructor.
     *
     * @param CacheItemPoolInterface $cache
     * @param int|\DateInterval|null $ttl
     */
    public function __construct(CacheItemPoolInterface $cache, $ttl)
    {
        $this->cache = $cache;
        $this->ttl   = $ttl;
    }

    /**
     * {@inheritdoc}
     */
    public function needsCompile($source)
    {
        $key = md5($source);

        if (!$this->cache->hasItem($key)) {
            return true;
        }

        $this->items[$source] = $item = $this->cache->getItem($key);

        return !$item->isHit();
    }

    /**
     * {@inheritdoc}
     */
    public function get($source)
    {
        if (isset($this->items[$source])) {
            return $this->items[$source]->get();
        }

        return $this->cache->getItem(md5($source))->get();
    }

    /**
     * {@inheritdoc}
     */
    public function save($source, $css)
    {
        $item = $this->cache->getItem(md5($source))->set($css);

        if ($this->ttl !== null) {
            $item->expiresAfter($this->ttl);
        }

        $this->cache->save($item);
    }
}
