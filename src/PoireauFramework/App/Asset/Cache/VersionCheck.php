<?php

namespace PoireauFramework\App\Asset\Cache;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;


/**
 * Check the CSS version for invalidate the cached style
 */
class VersionCheck implements CacheStrategyInterface
{
    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var string
     */
    private $version;

    /**
     * @var CacheItemInterface[]
     */
    private $items = [];


    /**
     * VersionCheck constructor.
     *
     * @param CacheItemPoolInterface $cache
     * @param string $version
     */
    public function __construct(CacheItemPoolInterface $cache, $version)
    {
        $this->cache   = $cache;
        $this->version = $version;
    }

    /**
     * {@inheritdoc}
     */
    public function needsCompile($source)
    {
        $key = md5($source);

        if (!$this->cache->hasItem($key)) {
            return true;
        }

        $this->items[$source] = $item = $this->cache->getItem($key);

        return !$item->isHit() || $item->get()["version"] !== $this->version;
    }

    /**
     * {@inheritdoc}
     */
    public function get($source)
    {
        if (isset($this->items[$source])) {
            return $this->items[$source]->get()["css"];
        }

        return $this->cache->getItem(md5($source))->get()["css"];
    }

    /**
     * {@inheritdoc}
     */
    public function save($source, $css)
    {
        $item = $this->cache->getItem(md5($source))->set([
            "version" => $this->version,
            "css"     => $css
        ]);

        $this->cache->save($item);
    }
}
