<?php

namespace PoireauFramework\App\Asset\Cache;

/**
 * Strategy for SCSS cache
 */
interface CacheStrategyInterface
{
    /**
     * Check if the SCSS file needs compilation
     *
     * @param string $source The SCSS source file
     *
     * @return bool
     */
    public function needsCompile($source);

    /**
     * Get the cached source
     *
     * @param string $source The SCSS source file
     *
     * @return string
     */
    public function get($source);

    /**
     * Save into the cache the CSS
     *
     * @param string $source The SCSS source file
     * @param string $css The compiled CSS
     *
     * @return void
     */
    public function save($source, $css);
}
