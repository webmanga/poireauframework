<?php


namespace PoireauFramework\App\Asset\Cache;

/**
 * Always recompile the SCSS
 */
class AlwaysRecompile implements CacheStrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function needsCompile($source)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function get($source)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function save($source, $css) {}
}
