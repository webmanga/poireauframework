<?php

namespace PoireauFramework\App\Asset;

use PoireauFramework\App\Http\Event\Prepare;
use PoireauFramework\App\Http\Event\PrepareArgument;
use PoireauFramework\App\Http\Event\PrepareListenerInterface;
use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Util\Strings;

/**
 * Class ServerListener
 */
class ServerListener implements PrepareListenerInterface, EventListenerInterface
{
    /**
     * AssetServer
     */
    private $server;


    /**
     *
     */
    public function __construct(AssetServer $server)
    {
        $this->server = $server;
    }

    /**
     * {@inheritdoc}
     */
    public function onPrepare(PrepareArgument $argument): void
    {
        if (!Strings::startsWith($argument->request()->path(), "/" . $this->server->path() . "/")) {
            return;
        }

        $argument->setResult(
            $this->server->handle($argument->request())
        );
    }

    /**
     * {@inheritdoc}
     */
    public function eventTypes(): array
    {
        return [Prepare::NAME];
    }
}
