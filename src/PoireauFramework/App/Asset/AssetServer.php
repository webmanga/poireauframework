<?php

namespace PoireauFramework\App\Asset;

use PoireauFramework\Http\Exception\MethodNotAllowedHttpException;
use PoireauFramework\Http\Exception\NotFoundHttpException;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Util\Strings;

/**
 * Server for handle assets
 */
class AssetServer
{
    /**
     * @var HandlerInterface[]
     */
    private $handlers = [];

    /**
     * @var string
     */
    private $path;


    /**
     * @param string $path The listen path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * Register a new handler to the server
     * @return $this
     */
    public function register(HandlerInterface $handler): AssetServer
    {
        $this->handlers[$handler->basePath()] = $handler;

        return $this;
    }

    /**
     * Get the server path
     */
    public function path(): string
    {
        return $this->path;
    }

    /**
     * Handle the request
     */
    public function handle(RequestInterface $request): ResponseInterface
    {
        if ($request->method() !== Request::METHOD_GET) {
            throw new MethodNotAllowedHttpException(Request::METHOD_GET);
        }

        $path = explode("/", $request->path(), 4);

        if (count($path) !== 4) {
            throw new NotFoundHttpException();
        }

        if (!isset($this->handlers[$path[2]])) {
            throw new NotFoundHttpException();
        }

        $handler = $this->handlers[$path[2]];

        if (!$handler->supports($path[3], $request)) {
            throw new NotFoundHttpException();
        }

        return $handler->handle($path[3], $request);
    }
}
