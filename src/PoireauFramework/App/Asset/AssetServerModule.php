<?php

namespace PoireauFramework\App\Asset;

use PoireauFramework\Config\Config;
use PoireauFramework\Event\EventDispatcherInterface;
use PoireauFramework\Injector\Factory\FactoryContainer;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Kernel\BootableModuleInterface;
use PoireauFramework\Kernel\KernelInterface;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Register the asset server module
 */
class AssetServerModule implements InjectorModuleInterface, BootableModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->factory(AssetServer::class, [$this, "createServer"]);
        $injector->factory(FileHandler::class, [$this, "createFileHandler"])->persistent(false);
    }

    /**
     * {@inheritdoc}
     */
    public function boot(KernelInterface $kernel): void
    {
        $kernel->register(new ServerListener(
            $kernel->injector()->get(AssetServer::class)
        ));
    }

    public function createServer(InjectorInterface $injector): AssetServer
    {
        $server = new AssetServer($injector->get(Config::class)->item("asset")->get("server_path"));

        if ($injector->reg("asset")->has("handlers")) {
            foreach ($injector->reg("asset")->get("handlers") as $handler) {
                $handlerClass = array_shift($handler);

                $server->register($injector->create($handlerClass, $handler));
            }
        }

        return $server;
    }

    public function createFileHandler(InjectorInterface $injector, string $path, array $mapping): FileHandler
    {
        return new FileHandler($path, $mapping);
    }
}
