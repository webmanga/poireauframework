# Assets

* Cache / no-reload

# Template

* Testing template engine (get last view)

# Security

* Move to "global" package
* Add interfaces
* Make service as final
* refactor
* getters on service