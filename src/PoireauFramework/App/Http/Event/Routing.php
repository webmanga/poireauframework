<?php

namespace PoireauFramework\App\Http\Event;

use PoireauFramework\Event\Type\EventTypeInterface;

/**
 * Event dispatched after the route is resolved
 * {@link RoutingListenerInterface}
 */
class Routing implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\App\\Http\\Event\\Routing";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof RoutingListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onRouting($arguments[0]);
    }
}
