<?php

namespace PoireauFramework\App\Http\Event;

use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Argument class for PrepareListenerInterface::onPrepare()
 */
class PrepareArgument
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var mixed
     */
    private $result;


    /**
     *
     */
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Get the request object
     */
    public function request(): RequestInterface
    {
        return $this->request;
    }

    /**
     * Get the result
     */
    public function result()
    {
        return $this->result;
    }

    /**
     * Set the result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }
}
