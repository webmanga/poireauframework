<?php

namespace PoireauFramework\App\Http\Event;

use PoireauFramework\App\Router\Route;
use PoireauFramework\Http\Request\RequestInterface;

/**
 * Argument class for RoutingListenerInterface::onRouting()
 */
final class RoutingArgument
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Route
     */
    private $route;

    /**
     * @var mixed
     */
    private $result;


    /**
     *
     */
    public function __construct(RequestInterface $request, Route $route)
    {
        $this->request = $request;
        $this->route   = $route;
    }

    /**
     * Get the request object
     */
    public function request(): RequestInterface
    {
        return $this->request;
    }

    /**
     * Get the route
     */
    public function route(): Route
    {
        return $this->route;
    }

    /**
     * Get the result
     */
    public function result()
    {
        return $this->result;
    }

    /**
     * Set the result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }
}
