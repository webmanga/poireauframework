<?php

namespace PoireauFramework\App\Http\Event;

/**
 * Listener for Prepare event
 * @see \PoireauFramework\App\Http\Event\Prepare
 */
interface PrepareListenerInterface
{
    /**
     *
     */
    public function onPrepare(PrepareArgument $argument): void;
}
