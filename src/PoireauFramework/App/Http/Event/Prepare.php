<?php

namespace PoireauFramework\App\Http\Event;

use PoireauFramework\Event\Type\EventTypeInterface;

/**
 * Event dispatched before run the request
 * {@link PrepareListenerInterface}
 */
class Prepare implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\App\\Http\\Event\\Prepare";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof PrepareListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onPrepare($arguments[0]);
    }
}
