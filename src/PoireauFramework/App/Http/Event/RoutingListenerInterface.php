<?php

namespace PoireauFramework\App\Http\Event;

/**
 * Listener for Routing event
 * @see \PoireauFramework\App\Http\Event\Routing
 */
interface RoutingListenerInterface
{
    /**
     *
     */
    public function onRouting(RoutingArgument $argument): void;
}
