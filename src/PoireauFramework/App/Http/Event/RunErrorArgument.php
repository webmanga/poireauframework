<?php

namespace PoireauFramework\App\Http\Event;

use Exception;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Argument for RunErrorListenerInterface::onRunError()
 */
class RunErrorArgument
{
    /**
     * @var Exception
     */
    private $exception;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * @var mixed
     */
    private $result;


    public function __construct(Exception $exception, RequestInterface $request, ResponseInterface $response)
    {
        $this->exception = $exception;
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @return Exception
     */
    public function exception(): Exception
    {
        return $this->exception;
    }

    /**
     * @return RequestInterface
     */
    public function request(): RequestInterface
    {
        return $this->request;
    }

    /**
     * @return ResponseInterface
     */
    public function response(): ResponseInterface
    {
        return $this->response;
    }

    public function result()
    {
        return $this->result;
    }

    public function setResult($result): void
    {
        $this->result = $result;
    }
}