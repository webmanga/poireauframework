<?php

namespace PoireauFramework\App\Http\Event;

/**
 * Listener for RunError event
 * @see \PoireauFramework\App\Http\Event\RunError
 */
interface RunErrorListenerInterface
{
    /**
     *
     */
    public function onRunError(RunErrorArgument $argument): void;
}