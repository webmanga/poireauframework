<?php

namespace PoireauFramework\App\Http\Event;

use PoireauFramework\Event\Type\EventTypeInterface;

/**
 * Event dispatched when RequestRunner encounter an exception
 * {@link RunErrorListenerInterface}
 */
class RunError implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\App\\Http\\Event\\RunError";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof RunErrorListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onRunError($arguments[0]);
    }
}