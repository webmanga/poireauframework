<?php

namespace PoireauFramework\App\Http;

use PoireauFramework\App\Controller\ControllerResolverInterface;
use PoireauFramework\App\Http\Event\Prepare;
use PoireauFramework\App\Http\Event\PrepareArgument;
use PoireauFramework\App\Http\Event\RunError;
use PoireauFramework\App\Http\Event\RunErrorArgument;
use PoireauFramework\App\Http\Event\Routing;
use PoireauFramework\App\Http\Event\RoutingArgument;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Router\RouterInterface;

use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;

use PoireauFramework\Http\Exception\NotFoundHttpException;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Http\Response\ResponseStack;
use PoireauFramework\Http\Response\ResponseTransformerInterface;
use PoireauFramework\Http\Runner\RequestRunnerInterface;

/**
 * RequestRunner for local requests
 */
final class RequestRunner implements RequestRunnerInterface
{
    const ROUTE_ATTACHMENT_KEY = "__route";

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ResponseStack
     */
    private $responseStack;

    /**
     * @var ControllerResolverInterface
     */
    private $controllerResolver;

    /**
     * @var ResponseTransformerInterface
     */
    private $responseTransformer;

    /**
     * @var RouterInterface
     */
    private $router;


    /**
     * Construct RequestRunner
     * @param RequestStack requestStack
     * @param ControllerResolverInterface controllerResolver
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, RequestStack $requestStack, ResponseStack $responseStack, ControllerResolverInterface $controllerResolver, ResponseTransformerInterface $responseTransformer, RouterInterface $router)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack = $requestStack;
        $this->responseStack = $responseStack;
        $this->controllerResolver = $controllerResolver;
        $this->responseTransformer = $responseTransformer;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function run(RequestInterface $request): ResponseInterface
    {
        $response = new Response();

        $this->requestStack->push($request);
        $this->responseStack->push($response);

        try {
            $result = $this->process($request, $response);
        } catch (\Exception $e) {
            $argument = new RunErrorArgument($e, $request, $response);
            $this->eventDispatcher->dispatch(RunError::NAME, [$argument]);

            // Not result : rethrow
            if ($argument->result() === null) {
                throw $e;
            }

            $result = $argument->result();
        }

        $this->requestStack->pop();
        $this->responseStack->pop();

        return $this->responseTransformer->transform($response, $result);
    }

    /**
     * Process the request
     */
    private function process(RequestInterface $request, ResponseInterface $response)
    {
        $argument = new PrepareArgument($request);
        $this->eventDispatcher->dispatch(Prepare::NAME, [$argument]);

        // The preparation return a result
        if ($argument->result() !== null) {
            return $argument->result();
        }

        $route = $this->getRoute($request);

        $argument = new RoutingArgument($request, $route);
        $this->eventDispatcher->dispatch(Routing::NAME, [$argument]);

        // The routing return a result
        if ($argument->result() !== null) {
            return $argument->result();
        }

        $controller = $this->controllerResolver->resolveController($route);

        if ($controller === null) {
            throw new NotFoundHttpException("Controller not found");
        }

        return call_user_func_array(
            $controller,
            $this->controllerResolver->resolveArguments(
                $controller,
                $route,
                $request
            )
        );
    }

    /**
     * get the route from the request
     */
    private function getRoute(RequestInterface $request): Route
    {
        if ($request->has(self::ROUTE_ATTACHMENT_KEY)) {
            return $request->attachment(self::ROUTE_ATTACHMENT_KEY);
        }

        $route = $this->router->resolveRoute($request);

        $request->attach(self::ROUTE_ATTACHMENT_KEY, $route);

        return $route;
    }
}
