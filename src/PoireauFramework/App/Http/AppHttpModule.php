<?php

namespace PoireauFramework\App\Http;

use PoireauFramework\Config\Config;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Http\Cookie\Parser\MapParserResolver;
use PoireauFramework\Http\Cookie\Parser\ParserResolverInterface;
use PoireauFramework\Http\Cookie\Parser\PlainParser;
use PoireauFramework\Http\Cookie\Parser\SignedParser;
use PoireauFramework\Http\HttpModule;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;

/**
 * Http application module
 */
class AppHttpModule extends HttpModule
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        parent::configure($injector);

        $injector->impl(ParserResolverInterface::class, MapParserResolver::class);
        $injector->factory(MapParserResolver::class, [$this, "createMapParserResolver"]);
    }

    public function createMapParserResolver(InjectorInterface $injector): MapParserResolver
    {
        /** @var ConfigItem $config */
        $config = $injector->get(Config::class)->item("http")->item("cookies");

        $parsers = [];

        foreach ($config->getIterator() as $name => $sub) {
            if ($sub->has("class")) {
                $parser = $injector->create($sub->get("class"), [$sub]);
            } else {
                $parser = PlainParser::instance();
            }

            if ($sub->has("signed") && $sub->get("signed")) {
                $parser = $injector->create(SignedParser::class, [$parser, $sub]);
            }

            $parsers[$name] = $parser;
        }

        return new MapParserResolver(
            $parsers["default"] ?? PlainParser::instance(),
            $parsers
        );
    }
}
