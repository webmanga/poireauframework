<?php

namespace PoireauFramework\App\Exception;

use LogicException;

/**
 * Base exception for misconfigured application
 */
class AppException extends LogicException {
    
}
