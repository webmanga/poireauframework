<?php

namespace PoireauFramework\App\Testing;

use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;

/**
 * Request stack for testing purpose
 */
class TestingRequestStack extends RequestStack
{
    /**
     * TestingRequestStack constructor.
     */
    public function __construct()
    {
        parent::__construct((new RequestBuilder())->build());
    }

    /**
     * @param RequestInterface $request
     */
    public function setFirst(RequestInterface $request)
    {
        $this->stack[0] = $request;
    }
}
