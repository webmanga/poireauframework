<?php

namespace PoireauFramework\App\Testing\Extension;

use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Response\RedirectResponse;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Add assertions for @see Response
 */
trait ResponseAssertion
{
    /**
     * @return ResponseInterface
     */
    abstract protected function lastResponse();

    /**
     *
     */
    public function assertResponseSuccess()
    {
        $this->assertEquals(2, (int) ($this->lastResponse()->getCode() / 100), "Expected a successful response (i.e. code 2xx) : actual {$this->lastResponse()->getCode()}");
    }

    /**
     *
     */
    public function assertResponseCode($code)
    {
        $this->assertEquals($code, $this->lastResponse()->getCode(), "Expected a response code {$code} : actual {$this->lastResponse()->getCode()}");
    }

    /**
     * @param string $location
     */
    public function assertRedirect($location)
    {
        $response = $this->lastResponse();

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals($location, $response->headers()->get("location"));
    }

    /**
     *
     */
    public function assertResponseHtmlContent($content)
    {
        $this->assertInstanceOf(TextBag::class, $this->lastResponse()->getBody(), "Expect the response is a string");
        $this->assertXmlStringEqualsXmlString($content, $this->lastResponse()->getBody()->raw());
    }

    /**
     *
     */
    public function assertResponseEquals($content)
    {
        if (is_string($content)) {
            $this->assertInstanceOf(TextBag::class, $this->lastResponse()->getBody(), "Expect the response is a string");
        }

        $this->assertEquals($content, $this->lastResponse()->getBody()->raw());
    }
}