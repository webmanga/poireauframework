<?php

namespace PoireauFramework\App\Testing\Extension;

use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Testing\TestingAppModule;
use PoireauFramework\Http\Bag\BagInterface;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Http\Response\ResponseSenderInterface;
use PoireauFramework\Http\Response\ResponseTransformer;

/**
 * Configure @see Application
 */
trait ApplicationConfiguration
{
    /**
     * @var Application
     */
    protected $app;


    /**
     * @param array $defaults
     *
     * @return Application
     */
    protected function createApplication(array $defaults = [])
    {
        return new Application($defaults);
    }

    /**
     * @param Application $application
     * @param array $modules
     */
    protected function configureApplication(Application $application = null, array $modules = [])
    {
        if ($application === null) {
            $application = $this->createApplication();
        }

        foreach ($modules as $module) {
            $application->register($module);
        }

        $application->register(new TestingAppModule());
        $application->load();

        $this->app = $application;
    }

    /**
     * Inject object to the injector
     *
     * @param string|object $class The class name or the instance
     * @param object|null $object The instance if class name is given
     *
     * @return $this
     */
    protected function inject($class, $object = null)
    {
        if (is_object($class)) {
            $this->app->injector()->instance($class);
        } else {
            $this->app->injector()->instance($object);
            $this->app->injector()->impl($class, get_class($object));
        }

        return $this;
    }

    /**
     * @param string $method
     * @param string $path
     * @param mixed $parameters
     *
     * @return ResponseInterface
     */
    protected function call($method, $path = '/', $parameters = [])
    {
        $method = strtoupper($method);
        $builder = (new RequestBuilder());

        if ($path{0} !== "/") {
            $path = "/$path";
        }

        $builder
            ->method($method)
            ->path($path)
        ;

        switch ($method) {
            case Request::METHOD_GET:
            case Request::METHOD_HEAD:
            case Request::METHOD_OPTION:
            case Request::METHOD_TRACE:
                foreach ($parameters as $k => $v) {
                    $builder->query()->set($k, $v);
                }
                break;
            default:
                if (!($parameters instanceof BagInterface)) {
                    $parameters = (new ResponseTransformer())->createBagFromValue($parameters);
                }

                $builder->body($parameters);
        }

        return $this->execute($builder->build());
    }

    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    protected function execute(RequestInterface $request)
    {
        $this->app->requestStack()->setFirst($request);
        $this->app->run();

        return $this->lastResponse();
    }

    /**
     * @return ResponseInterface
     */
    protected function lastResponse()
    {
        return $this->app->injector()->get(ResponseSenderInterface::class)->lastResponse;
    }
}
