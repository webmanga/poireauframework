<?php

namespace PoireauFramework\App\Testing;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Testing\Extension\ApplicationConfiguration;
use PoireauFramework\App\Testing\Extension\ResponseAssertion;
use PoireauFramework\Debug\ErrorHandler;

/**
 * Test case for application
 */
class AppTestCase extends TestCase
{
    use ApplicationConfiguration;
    use ResponseAssertion;


    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->app = $this->createApplication();
        $this->configureApplication($this->app);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        ErrorHandler::instance()->restore();
    }
}
