<?php

namespace PoireauFramework\App\Testing;

use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseSenderInterface;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Class TestingAppModule
 */
class TestingAppModule implements InjectorModuleInterface
{
    private $overrideStack;

    /**
     * TestingAppModule constructor.
     *
     * @param $overrideStack
     */
    public function __construct($overrideStack = true)
    {
        $this->overrideStack = $overrideStack;
    }


    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(
            ResponseSenderInterface::class,
            TestingResponseSender::class
        );

        $injector->factory(TestingResponseSender::class, function () {
            return TestingResponseSender::instance();
        });

        if ($this->overrideStack) {
            $injector->impl(
                RequestStack::class,
                TestingRequestStack::class
            );

            $injector->factory(TestingRequestStack::class, function () {
                return new TestingRequestStack();
            });
        }
    }
}
