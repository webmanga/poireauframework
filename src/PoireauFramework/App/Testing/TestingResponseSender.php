<?php

namespace PoireauFramework\App\Testing;

use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Http\Response\ResponseSenderInterface;

/**
 * Class TestingResponseSender
 */
class TestingResponseSender implements ResponseSenderInterface
{
    /**
     * @var TestingResponseSender
     */
    static protected $instance;

    private function __construct() {}

    /**
     * @var Response
     */
    public $lastResponse;


    /**
     * {@inheritdoc}
     */
    public function send(ResponseInterface $response): void
    {
        $this->lastResponse = $response;
    }

    /**
     * @return TestingResponseSender
     */
    static public function instance()
    {
        if (self::$instance === null) {
            self::$instance = new TestingResponseSender();
        }

        return self::$instance;
    }
}
