<?php

namespace PoireauFramework\App\Form\Template\Theme;

use PoireauFramework\App\Form\Template\Renderer\FieldRendererInterface;
use PoireauFramework\App\Form\Template\Renderer\FormRendererInterface;

/**
 * Interface for handling form themes
 */
interface ThemeInterface
{
    /**
     * Get renderer for FormView
     */
    public function forForm(): FormRendererInterface;

    /**
     * Get render for FieldView
     */
    public function forField(string $type = "text"): FieldRendererInterface;
}
