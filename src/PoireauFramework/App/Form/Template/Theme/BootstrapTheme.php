<?php

namespace PoireauFramework\App\Form\Template\Theme;

use PoireauFramework\App\Form\Template\Renderer\BootstrapFormRenderer;
use PoireauFramework\App\Form\Template\Renderer\BootstrapFieldRenderer;
use PoireauFramework\App\Form\Template\Renderer\FormRendererInterface;
use PoireauFramework\App\Form\Template\Renderer\FieldRendererInterface;

/**
 * Theme for Bootstrap
 */
class BootstrapTheme implements ThemeInterface
{
    /**
     * @var ThemeInterface
     */
    private $base;

    private $form;
    private $fields = [];


    /**
     *
     */
    public function __construct(ThemeInterface $theme = null)
    {
        $this->base = $theme ?: new DefaultTheme();
    }

    /**
     * {@inheritdoc}
     */
    public function forForm(): FormRendererInterface
    {
        if ($this->form === null) {
            $this->form = new BootstrapFormRenderer($this->base->forForm());
        }

        return $this->form;
    }

    /**
     * {@inheritdoc}
     */
    public function forField(string $type = "text"): FieldRendererInterface
    {
        if (isset($this->fields[$type])) {
            $this->fields[$type];
        }

        $renderer = new BootstrapFieldRenderer($this->base->forField($type));
        $this->fields[$type] = $renderer;

        return $renderer;
    }
}
