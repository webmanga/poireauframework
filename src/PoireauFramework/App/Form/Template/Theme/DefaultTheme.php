<?php

namespace PoireauFramework\App\Form\Template\Theme;

use PoireauFramework\App\Form\Template\Renderer\FormRenderer;
use PoireauFramework\App\Form\Template\Renderer\FormRendererInterface;
use PoireauFramework\App\Form\Template\Renderer\FieldRenderer;
use PoireauFramework\App\Form\Template\Renderer\FieldRendererInterface;
use PoireauFramework\App\Form\Template\Renderer\TextAreaRenderer;

/**
 * Default HTML theme
 */
class DefaultTheme implements ThemeInterface
{
    private $fieldRenderer;
    private $textareaRenderer;
    private $formRenderer;


    /**
     *
     */
    public function __construct() {
        $this->fieldRenderer = new FieldRenderer();
        $this->formRenderer  = new FormRenderer();
        $this->textareaRenderer  = new TextAreaRenderer();
    }

    /**
     * {@inheritdoc}
     */
    public function forForm(): FormRendererInterface
    {
        return $this->formRenderer;
    }

    /**
     * {@inheritdoc}
     */
    public function forField(string $type = "text"): FieldRendererInterface
    {
        switch ($type) {
            case "textarea":
                return $this->textareaRenderer;
            default:
                return $this->fieldRenderer;
        }

        return $this->fieldRenderer;
    }
}
