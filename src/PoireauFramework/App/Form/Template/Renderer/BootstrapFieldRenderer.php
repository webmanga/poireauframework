<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FieldView;

/**
 * renderer for bootstrap fields
 */
class BootstrapFieldRenderer implements FieldRendererInterface
{
    /**
     * @var FieldRendererInterface
     */
    private $base;


    /**
     *
     */
    public function __construct(FieldRendererInterface $base)
    {
        $this->base = $base;
    }

    /**
     * {@inheritdoc}
     */
    public function render(FieldView $field): string
    {
        $html = "";

        $field->addClass("form-control");

        if ($field->getParameter("bs-label", true)) {
            $html .= $field->label();
        }

        $html .= $this->base->render($field);

        if ($field->getParameter("bs-feedback", true)) {
            $html .= <<<TPL
<div class="help-block">{$this->e($field->getError())}</div>
TPL;
        }

        if ($field->getParameter("bs-wrap", true)) {
            $groupClasses = "form-group";

            if ($field->hasError()) {
                $groupClasses .= " has-error";
            }

            $html = <<<TPL
<div class="{$groupClasses}">{$html}</div>
TPL;
        }

        return $html;
    }

    /**
     * {@inheritdoc}
     * @todo Add class control-label
     */
    public function label(FieldView $field, string $value): string
    {
        $field->param("label-class", "control-label");

        return $this->base->label($field, $value);
    }

    private function e(?string $html): string
    {
        return htmlentities($html);
    }
}
