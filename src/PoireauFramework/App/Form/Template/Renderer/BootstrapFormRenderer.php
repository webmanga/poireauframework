<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FormView;

/**
 * Renderer for bootstrap form tag
 */
class BootstrapFormRenderer implements FormRendererInterface
{
    /**
     * @var FormRendererInterface
     */
    private $base;


    /**
     * Decorate a base renderer
     */
    public function __construct(FormRendererInterface $base)
    {
        $this->base = $base;
    }

    /**
     * {@inheritdoc}
     */
    public function renderOpenTag(FormView $view): string
    {
        return $this->base->renderOpenTag($view);
    }

    /**
     * {@inheritdoc}
     */
    public function renderCloseTag(FormView $view): string
    {
        return $this->base->renderCloseTag($view);
    }
}
