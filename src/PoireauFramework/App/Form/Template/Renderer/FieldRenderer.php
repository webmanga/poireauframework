<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FieldView;
use PoireauFramework\App\Template\Html\HtmlUtils;

/**
 * Default renderer for fields
 */
class FieldRenderer implements FieldRendererInterface
{
    /**
     * {@inheritdoc}
     */
    public function render(FieldView $field): string
    {
        $attributes = HtmlUtils::renderAttributes($field);

        return <<<TPL
<input type="{$this->e($field->getType())}" name="{$this->e($field->getName())}" value="{$this->e($field->getValue())}" {$attributes}/>
TPL;
    }

    /**
     * {@inheritdoc}
     */
    public function label(FieldView $field, ?string $value): string
    {
        if (empty($value)) {
            $value = $field->getParameter("label-value", $field->getName());
        }

        if ($field->getParameter("label-escape", true)) {
            $value = $this->e($value);
        }

        if ($field->getId() == "") {
            HtmlUtils::generateId($field);
        }

        $attributes = 'for="' . $this->e($field->getId()) . '"';

        if ($field->getParameter("label-class")) {
            $attributes .= ' class="' . $this->e($field->getParameter("label-class")) . '"';
        }

        return "<label {$attributes}>{$value}</label>";
    }

    private function e(?string $html): ?string
    {
        return htmlentities($html);
    }
}
