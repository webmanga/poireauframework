<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FormView;

/**
 * Render for @see FormView
 */
interface FormRendererInterface
{
    /**
     * Render the <form> open tag
     */
    public function renderOpenTag(FormView $view): string;

    /**
     * Render the </form> close tag
     */
    public function renderCloseTag(FormView $view): string;
}
