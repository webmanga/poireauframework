<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FieldView;

/**
 * Interface for render @see FieldView
 */
interface FieldRendererInterface
{
    /**
     * Render the field tag
     */
    public function render(FieldView $field): string;

    /**
     * Render the label tag
     */
    public function label(FieldView $field, string $value): string;
}
