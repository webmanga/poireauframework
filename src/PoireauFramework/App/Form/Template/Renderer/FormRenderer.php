<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FormView;
use PoireauFramework\App\Template\Html\HtmlUtils;

/**
 * Default render for @see FormView
 */
class FormRenderer implements FormRendererInterface
{
    /**
     * {@inheritdoc}
     */
    public function renderOpenTag(FormView $view): string
    {
        $attributes = HtmlUtils::renderAttributes($view);

        return <<<TPL
<form action="{$this->e($view->getAction())}" method="{$this->e($view->getMethod())}" {$attributes}>
TPL;
    }

    /**
     * {@inheritdoc}
     */
    public function renderCloseTag(FormView $view): string
    {
        return "</form>";
    }

    private function e(string $html): string
    {
        return htmlentities($html);
    }
}
