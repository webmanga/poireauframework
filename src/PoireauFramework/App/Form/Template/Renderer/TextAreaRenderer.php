<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PoireauFramework\App\Form\Template\FieldView;
use PoireauFramework\App\Template\Html\HtmlUtils;

/**
 * renderer for <textarea> fields
 */
class TextAreaRenderer extends FieldRenderer
{
    /**
     * {@inheritdoc}
     */
    public function render(FieldView $field): string
    {
        $attributes = HtmlUtils::renderAttributes($field);

        return <<<TPL
<textarea name="{$this->e($field->getName())}" {$attributes}>{$this->e($field->getValue())}</textarea>
TPL;
    }

    private function e(string $html): string
    {
        return htmlentities($html);
    }
}
