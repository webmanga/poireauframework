<?php

namespace PoireauFramework\App\Form\Template;

use ArrayAccess;
use ArrayIterator;
use BadMethodCallException;
use Iterator;
use IteratorAggregate;
use PoireauFramework\App\Form\Template\Renderer\FormRenderer;
use PoireauFramework\App\Form\Template\Renderer\FormRendererInterface;
use PoireauFramework\App\Template\Html\HtmlElement;

/**
 * View for Form objects
 */
class FormView extends HtmlElement implements ArrayAccess, IteratorAggregate
{
    /**
     * @var FormRendererInterface
     */
    private $renderer;

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $method = "GET";

    /**
     * @var array
     */
    private $elements = [];


    /**
     * Create the form view
     * @param FormRendererInterface renderer The form renderer
     */
    public function __construct(FormRendererInterface $renderer = null)
    {
        $this->renderer = $renderer ?: new FormRenderer();
    }

    /**
     * Set the action URL
     * @return $this
     */
    public function action(string $url): FormView
    {
        $this->action = $url;

        return $this;
    }

    /**
     * Get the action URL
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Set the form method
     * @param string $method "GET" | "POST"
     * @return $this
     */
    public function method(string $method): FormView
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get the form method
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Add a new element into the form
     * @return $this
     */
    public function add(FieldView $field): FormView
    {
        $this->elements[$field->getName()] = $field;

        return $this;
    }

    /**
     * Get one form element
     * @return FieldView
     */
    public function offsetGet($name): FieldView
    {
        return $this->elements[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($name): bool
    {
        return isset($this->elements[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($name): void
    {
        unset($this->elements[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($name, $value): void
    {
        throw new BadMethodCallException("Cannot set an element on form view");
    }

    /**
     * Iterator through elements
     * @return FieldView[]
     */
    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->elements);
    }

    /**
     * Open the <form> tag
     */
    public function open(): string
    {
        return $this->renderer->renderOpenTag($this);
    }

    /**
     * Close the </form> tag
     */
    public function close(): string
    {
        return $this->renderer->renderCloseTag($this);
    }
}
