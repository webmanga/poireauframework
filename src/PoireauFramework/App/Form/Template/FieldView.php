<?php

namespace PoireauFramework\App\Form\Template;

use PoireauFramework\App\Template\Html\HtmlElement;
use PoireauFramework\App\Form\Template\Renderer\FieldRenderer;
use PoireauFramework\App\Form\Template\Renderer\FieldRendererInterface;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;
use PoireauFramework\App\Form\Template\Theme\ThemeInterface;

/**
 * View object for Field
 */
class FieldView extends HtmlElement
{
    /**
     * @var ThemeInterface
     */
    private $theme;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type = "text";

    /**
     * @var string
     */
    private $value;

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var string|null
     */
    private $error;


    /**
     *
     */
    public function __construct(ThemeInterface $theme, array $parameters = [])
    {
        $this->theme = $theme;
        $this->parameters = $parameters;
    }

    /**
     * Set the field name
     * @return $this
     */
    public function name(string $name): FieldView
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the field name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the field type
     * @return $this
     */
    public function type(string $type): FieldView
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the field type
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set the field value
     * @return $this
     */
    public function value($value): FieldView
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the field value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set a parameter
     * @return $this
     */
    public function param(string $key, $value): FieldView
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    /**
     * Get a parameter value
     */
    public function getParameter(string $key, $defaultValue = null)
    {
        return $this->parameters[$key] ?? $defaultValue;
    }

    /**
     * Set an error on the field
     * @return $this
     */
    public function error($value): FieldView
    {
        $this->error = $value;

        return $this;
    }

    /**
     * Get error on $this field
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * Check if the field have an error
     */
    public function hasError(): bool
    {
        return $this->error !== null;
    }

    /**
     * Render a <label> tag related to $this field
     */
    public function label(string $value = null): string
    {
        return $this->theme->forField($this->type)->label($this, $value);
    }

    /**
     * Render the field tag
     */
    public function __toString(): string
    {
        return $this->theme->forField($this->type)->render($this);
    }
}
