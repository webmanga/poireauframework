<?php

namespace PoireauFramework\App\Form\Template;

use PoireauFramework\App\Form\Template\Theme\DefaultTheme;
use PoireauFramework\App\Form\Template\Theme\ThemeInterface;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Filter\Secret;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Operation\FormOperationInterface;
use PoireauFramework\Http\Form\Validation\Email;
use PoireauFramework\Http\Form\Validation\Length;

/**
 * Operation for get view from an http form
 */
class GetView implements FormOperationInterface
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @var FormView
     */
    private $formView;

    /**
     * @var ThemeInterface
     */
    private $theme;

    /**
     * @var array
     */
    private $parameters;

    /**
     *
     */
    public function __construct(FormInterface $form, ThemeInterface $theme = null, array $parameters = [])
    {
        $this->form = $form;
        $this->theme = $theme ?: new DefaultTheme();
        $this->formView = new FormView($this->theme->forForm());
        $this->parameters = $parameters;
    }

    /**
     * {@inheritdoc}
     * @todo Handle toHttp() filter
     */
    public function applyOnField(Field $field): void
    {
        $parameters = $this->parameters[$field->name()] ?? [];
        $fieldView = new FieldView($this->theme, $parameters);

        $fieldView
            ->name($field->name())
            ->value($field->httpValue())
            ->error(
                $this->form->error($field->name())
            )
        ;

        foreach ($field->getFilters() as $filter) {
            if ($filter instanceof Secret) {
                $fieldView->type("password");
                break; //Break, no other filters ?
            }
        }

        foreach ($field->getRules() as $rule) {
            if ($rule instanceof Email) {
                $fieldView->type("email");
            }

            if ($rule instanceof Length && $rule->getMax() !== null) {
                $fieldView->attribute("maxlength", $rule->getMax());
            }
        }

        if ($field->isRequired()) {
            $fieldView->property("required");
        }

        $this->formView->add($fieldView);
    }

    /**
     * Get the form view
     */
    public function get(): FormView
    {
        return $this->formView;
    }
}
