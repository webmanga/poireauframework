<?php

namespace PoireauFramework\App\Form;

use PoireauFramework\Http\Form\Builder\FormBuilder;
use PoireauFramework\Http\Form\FormInterface;

/**
 * Factory class for Http Forms
 */
abstract class FormFactory
{
    /**
     * Create the form instance
     */
    public function create(): FormInterface
    {
        $builder = $this->builder();
        $this->configure($builder);

        return $builder->build();
    }

    /**
     * Configure the form
     */
    abstract public function configure(FormBuilder $builder): void;

    /**
     * Get the builder
     */
    protected function builder(): FormBuilder
    {
        return new FormBuilder();
    }
}
