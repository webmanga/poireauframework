<?php

namespace PoireauFramework\App\Form;

use PoireauFramework\App\Form\Exception\FormHttpException;
use PoireauFramework\App\Form\Exception\FrontFormHttpException;
use PoireauFramework\App\Form\Template\FormView;
use PoireauFramework\App\Form\Template\GetView;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;
use PoireauFramework\App\Form\Template\Theme\ThemeInterface;
use PoireauFramework\Http\Exception\BadRequestHttpException;
use PoireauFramework\Http\Exception\MethodNotAllowedHttpException;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\Request;

/**
 * Form class for handle front end
 */
abstract class FrontForm extends FormFactory
{
    /**
     * @var \PoireauFramework\Http\Form\FormInterface
     */
    protected $form;


    /**
     * The form action URL
     * @return string
     */
    abstract public function url(): string;

    /**
     * The form view URL
     */
    abstract public function viewUrl(): string;

    /**
     * The HTTP method
     * @return string
     */
    abstract public function method(): string;

    /**
     * Create the view object
     */
    public function view(): FormView
    {
        $view = $this->form()->apply(new GetView($this->form(), $this->theme(), $this->viewParameters()))->get();

        $view
            ->action($this->url())
            ->method($this->method())
        ;

        return $view;
    }

    /**
     * Get the form instance, or create it
     */
    public function form(): FormInterface
    {
        if ($this->form === null) {
            $this->form = $this->create();
        }

        return $this->form;
    }

    /**
     * Validate form data
     */
    public function validate(RequestInterface $request): FrontForm
    {
        if ($request->method() !== $this->method()) {
            throw new MethodNotAllowedHttpException($this->method());
        }

        switch ($this->method()) {
            case Request::METHOD_HEAD:
            case Request::METHOD_GET:
                $bag = $request->query();
                break;
            default:
                $bag = $request->body();
        }

        if ($bag === null) {
            throw new BadRequestHttpException("No data found");
        }

        $this->form()->bind($bag);

        if (!$this->form()->validate()) {
            throw new FrontFormHttpException($this);
        }

        return $this;
    }

    /**
     * Get a field form value
     */
    public function __get(string $name)
    {
        return $this->form()->get($name)->value();
    }

    /**
     * Export the form data to object
     * @param object|string|null target The target object, class name, or null to use default form target
     */
    public function export($target = null)
    {
        return $this->form()->export($target);
    }

    /**
     * Get array of form labels
     */
    protected function labels(): array
    {
        return [];
    }

    /**
     * Get view parameters
     */
    protected function viewParameters(): array
    {
        $parameters = [];

        foreach ($this->labels() as $field => $label) {
            $parameters[$field] = ["label-value" => $label];
        }

        return $parameters;
    }

    /**
     * Get the form view theme
     */
    protected function theme(): ThemeInterface
    {
        return new DefaultTheme();
    }
}
