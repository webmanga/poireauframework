<?php

namespace PoireauFramework\App\Form\Exception;

use PoireauFramework\App\Form\FrontForm;
use PoireauFramework\Http\Form\FormInterface;

/**
 * Exception for front form errors
 */
class FrontFormHttpException extends FormHttpException
{
    /**
     * @var FrontForm
     */
    private $form;

    public function __construct(FrontForm $form, string $message = "Invalid form data")
    {
        parent::__construct($form->form()->errors(), $message);

        $this->form = $form;
    }

    /**
     * Get the form
     */
    public function form(): FrontForm
    {
        return $this->form;
    }
}
