<?php

namespace PoireauFramework\App\Form\Exception;

use PoireauFramework\Http\Exception\BadRequestHttpException;

/**
 * Exception for form errors
 */
class FormHttpException extends BadRequestHttpException
{
    /**
     * @var array
     */
    private $errors;

    public function __construct(array $errors, string $message = "Invalid form data")
    {
        parent::__construct($message);

        $this->errors = $errors;
    }

    /**
     * Get the fields errors
     */
    public function errors(): array
    {
        return $this->errors;
    }
}
