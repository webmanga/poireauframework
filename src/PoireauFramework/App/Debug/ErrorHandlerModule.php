<?php

namespace PoireauFramework\App\Debug;

use PoireauFramework\App\Controller\ControllerFactoryInterface;
use PoireauFramework\Debug\ErrorHandler;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;
use PoireauFramework\Kernel\BootableModuleInterface;
use PoireauFramework\Kernel\KernelInterface;

/**
 * Module for error handler
 * Registry:
 * error_handler.show_errors bool
 * error_handler.exception_handlers array
 */
class ErrorHandlerModule implements InjectorModuleInterface, BootableModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $errorHandler = ErrorHandler::instance();

        if ($injector->reg("error_handler")->has("show_errors")) {
            $errorHandler->showErrors($injector->reg("error_handler")->get("show_errors"));
        }

        $injector->impl(ErrorHandler::class, get_class($errorHandler));

        $injector->instance($errorHandler);

        $injector->factory(ExceptionListener::class, [$this, "createExceptionListener"]);
        $injector
            ->factory(ControllerExceptionHandler::class, [$this, "createControllerExceptionHandler"])
            ->persistent(false)
        ;
    }

    public function createExceptionListener(InjectorInterface $injector): ExceptionListener
    {
        $handlers = [];

        if ($injector->reg("error_handler")->has("exception_handlers")) {
            foreach ($injector->reg("error_handler")->get("exception_handlers") as $handler) {
                if (is_string($handler)) {
                    $handlers[] = $injector->get($handler);
                } elseif (is_array($handler)) {
                    $handlers[] = $injector->create($handler[0], $handler[1]);
                } elseif (is_object($handler)) {
                    $handlers[] = $handler;
                }
            }
        }

        return new ExceptionListener($handlers);
    }

    public function createControllerExceptionHandler(InjectorInterface $injector, string $exception, string $controller): ControllerExceptionHandler
    {
        return new ControllerExceptionHandler(
            $injector->get(ControllerFactoryInterface::class),
            $exception,
            $controller
        );
    }

    /**
     * {@inheritdoc}
     */
    public function boot(KernelInterface $kernel): void
    {
        $kernel->injector()->get("PoireauFramework\\Debug\\ErrorHandler")->register();

        $kernel->register(
            $kernel->injector()->get(ExceptionListener::class)
        );
    }
}
