<?php

namespace PoireauFramework\App\Debug;

use Exception;
use PoireauFramework\App\Controller\ControllerFactoryInterface;
use PoireauFramework\App\Exception\AppException;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Exception handler using a controller class
 */
class ControllerExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * @var ControllerFactoryInterface
     */
    private $factory;

    /**
     * @var string
     */
    private $exception;

    /**
     * @var string
     */
    private $controller;


    /**
     * Create the exception handler
     * @param ControllerFactoryInterface $factory The controller factory instance
     * @param string $exception The exception class
     * @param string $controller The controller class
     */
    public function __construct(ControllerFactoryInterface $factory, string $exception, string $controller)
    {
        $this->factory = $factory;
        $this->exception = $exception;
        $this->controller = $controller;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Exception $exception): bool
    {
        return $exception instanceof $this->exception;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Exception $exception, RequestInterface $request, ResponseInterface $response)
    {
        $controller = $this->factory->create($this->controller);

        $controller->setRequest($request);
        $controller->setResponse($response);

        $method = $this->computeMethodName($exception);

        return $controller->$method($exception);
    }

    /**
     * Compute the method name from the exception class
     * @param Exception $exception
     *
     * @return string The method name
     * @throws AppException When controller do not have corresponding method for the exception
     */
    protected function computeMethodName(Exception $exception): string
    {
        $classes = class_parents($exception);
        array_unshift($classes, get_class($exception));

        foreach ($classes as $curClass) {
            $baseName = strpos($curClass, '\\') === false
                ? $curClass
                : substr(strrchr($curClass, '\\'), 1)
            ;

            if (method_exists($this->controller, $baseName)) {
                return $baseName;
            }
        }

        throw new AppException("Cannot found any valid method name on error controller '" . $this->controller . "'");
    }
}
