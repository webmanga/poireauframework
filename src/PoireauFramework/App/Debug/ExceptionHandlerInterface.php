<?php

namespace PoireauFramework\App\Debug;

use Exception;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * Interface for handle exceptions
 */
interface ExceptionHandlerInterface
{
    /**
     * Check if the handler supports the exception
     * @param Exception $exception The exception object
     * @return bool
     */
    public function supports(Exception $exception): bool;

    /**
     * Handle the exception
     */
    public function handle(Exception $exception, RequestInterface $request, ResponseInterface $response);
}
