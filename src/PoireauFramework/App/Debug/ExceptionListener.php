<?php

namespace PoireauFramework\App\Debug;

use Exception;
use PoireauFramework\App\Http\Event\RunError;
use PoireauFramework\App\Http\Event\RunErrorArgument;
use PoireauFramework\App\Http\Event\RunErrorListenerInterface;
use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseStack;
use PoireauFramework\Kernel\Event\Contract\KernelErrorListenerInterface;
use PoireauFramework\Kernel\Event\Type\KernelError;

/**
 * Listen kernel exceptions
 */
class ExceptionListener implements EventListenerInterface, KernelErrorListenerInterface, RunErrorListenerInterface
{
    /**
     * @var ExceptionHandlerInterface[]
     */
    protected $handlers = [];


    /**
     * Construct the exception listener
     * @param ExceptionHandlerInterface[] handlers Exception handlers to register
     */
    public function __construct(array $handlers = [])
    {
        $this->handlers = $handlers;
    }

    /**
     * {@inheritdoc}
     */
    public function eventTypes(): array
    {
        return [
            KernelError::NAME,
            RunError::NAME
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function onKernelError(Exception $exception): void
    {
        //By default, rethrow exception
        throw $exception;
    }

    /**
     * {@inheritdoc}
     */
    public function onRunError(RunErrorArgument $argument): void
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($argument->exception())) {
                $argument->setResult($handler->handle(
                    $argument->exception(),
                    $argument->request(),
                    $argument->response()
                ));
            }
        }
    }
}
