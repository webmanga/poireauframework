<?php

namespace PoireauFramework\App\Router;

use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Http\Request\RequestInterface;

/**
 * Route implementation extracting controller name, and action from path info
 */
class SimpleRouter implements RouterInterface
{
    /**
     * @var ConfigItem
     */
    protected $config;


    /**
     * @param ConfigItem $config
     */
    public function __construct(ConfigItem $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveRoute(RequestInterface $request): Route
    {
        $path = array_filter(explode("/", $request->path()));

        $controller = array_shift($path);
        $action     = array_shift($path);

        return new Route(
            [$request->method()],
            $controller ?: $this->config->get("default_controller"),
            $action     ?: $this->config->get("default_action"),
            $path
        );
    }

    /**
     * {@inheritdoc}
     */
    public function route(array $arguments): Route
    {
        if (isset($arguments["_method"])) {
            $method = $arguments["_method"];
            unset($arguments["_method"]);
        } else {
            $method = "GET";
        }

        $controller = array_shift($arguments);
        $action     = array_shift($arguments);

        return new Route(
            [$method],
            $controller ?: $this->config->get("default_controller"),
            $action ?: $this->config->get("default_action"),
            $arguments
        );
    }

    /**
     * {@inheritdoc}
     */
    public function makeUrl(Route $route, array $get = []): string
    {
        $url = "/" . urlencode($route->controller()) . "/" . urlencode($route->action());

        if ($route->arguments()) {
            foreach ($route->arguments() as $arg) {
                $url .= "/" . urlencode($arg);
            }
        }

        if (!empty($get)) {
            $url .= "?" . http_build_query($get);
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function match(array $arguments, Route $route): bool
    {
        if (isset($arguments["_method"])) {
            if (!in_array($arguments["_method"], $route->methods())) {
                return false;
            }

            unset($arguments["_method"]);
        }

        $controller = array_shift($arguments);
        $action     = array_shift($arguments);

        if ($controller !== $route->controller()) {
            return false;
        }

        if ($action === null) {
            return true;
        }

        if ($action !== $route->action()) {
            return false;
        }

        return empty($arguments) || $arguments == $route->arguments();
    }
}
