<?php

namespace PoireauFramework\App\Router;

use PoireauFramework\Http\Request\RequestInterface;

/**
 * Interface for router
 */
interface RouterInterface
{
    /**
     * Resolve the route from request
     * @param RequestInterface $request The request
     * @return Route The route
     */
    public function resolveRoute(RequestInterface $request): Route;

    /**
     * Get a route object from arguments
     * @param array $arguments The route arguments
     * @return Route
     */
    public function route(array $arguments): Route;

    /**
     * Get URL from a route and GET arguments
     * @return string
     */
    public function makeUrl(Route $route, array $get = []): string;

    /**
     * Check if route arguments match with route
     */
    public function match(array $arguments, Route $route): bool;
}
