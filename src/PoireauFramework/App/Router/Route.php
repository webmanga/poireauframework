<?php

namespace PoireauFramework\App\Router;

/**
 * Class Route
 */
class Route
{
    /**
     * @var string[]
     */
    private $methods;

    /**
     * @var string|callable
     */
    private $controller;

    /**
     * @var string
     */
    private $action;

    /**
     * @var array
     */
    private $arguments = [];


    /**
     *
     */
    public function __construct(array $methods, ?string $controller, ?string $action, array $arguments = [])
    {
        $this->methods    = $methods;
        $this->controller = $controller;
        $this->action     = $action;
        $this->arguments  = $arguments;
    }

    /**
     * Get supported methods
     */
    public function methods(): array
    {
        return $this->methods;
    }

    /**
     * Get the controller name
     */
    public function controller(): ?string
    {
        return $this->controller;
    }

    /**
     * Get the action name
     */
    public function action(): ?string
    {
        return $this->action;
    }

    /**
     * Get the route extra arguments
     */
    public function arguments(): array
    {
        return $this->arguments;
    }
}
