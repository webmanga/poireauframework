<?php

namespace PoireauFramework\App\Security;

use PoireauFramework\App\Router\Route;

/**
 * Interface for access map
 * Define rules of controller accesses
 */
interface AccessMapInterface
{
    const ANONYMOUS_ACCESS = "__anonymous";

    /**
     * Get list of needed permissions for the given route
     */
    public function permissions(Route $route): array;

    /**
     * Check if the route is registered into the access map
     */
    public function has(Route $route): bool;

    /**
     * Check if the route is anonymous
     */
    public function isAnonymous(Route $route): bool;
}
