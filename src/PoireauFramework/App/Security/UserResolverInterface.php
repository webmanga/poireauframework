<?php

namespace PoireauFramework\App\Security;

use PoireauFramework\Http\Request\RequestInterface;

/**
 * Resolver for security user
 */
interface UserResolverInterface
{
    /**
     * Resolve the user
     */
    public function resolve(RequestInterface $request): ?UserInterface;
}
