<?php

namespace PoireauFramework\App\Security;

/**
 * Standard user for use security features
 */
interface UserInterface
{
    /**
     * The unique identifier of the user
     * @return int
     */
    public function id(): int;

    /**
     * Set of roles
     * @return string[]
     */
    public function roles(): array;
}
