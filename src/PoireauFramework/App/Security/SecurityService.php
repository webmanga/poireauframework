<?php

namespace PoireauFramework\App\Security;

use PoireauFramework\App\Router\Route;
use PoireauFramework\Http\Exception\ForbiddenHttpException;
use PoireauFramework\Http\Exception\UnauthorizedHttpException;
use PoireauFramework\Http\Request\RequestInterface;

/**
 * Handle access to web application
 */
class SecurityService
{
    /** Allow all access (anonymous, logged, and others) */
    const STRATEGY_ALLOW    = 1;

    /** Block all access expect master */
    const STRATEGY_BLOCK    = 2;

    /** Allow only logged users. This is the default strategy */
    const STRATEGY_RESTRICT = 3;

    /**
     * @var AccessMapInterface
     */
    protected $access;

    /**
     * @var GrantMapInterface
     */
    protected $grant;

    /**
     * @var UserResolverInterface
     */
    protected $userResolver;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var int
     */
    private $defaultStrategy;


    /**
     *
     */
    public function __construct(AccessMapInterface $access, GrantMapInterface $grant, UserResolverInterface $userResolver)
    {
        $this->access          = $access;
        $this->grant           = $grant;
        $this->defaultStrategy = self::STRATEGY_RESTRICT;
        $this->userResolver    = $userResolver;
    }

    /**
     * Set the default strategy (i.e. when no permission is given)
     * @param int $strategy One of the SecurityService::STRATEGY_* constant
     */
    public function setDefaultStrategy(int $strategy): void
    {
        $this->defaultStrategy = $strategy;
    }

    /**
     * Get the default strategy
     */
    public function defaultStrategy(): int
    {
        return $this->defaultStrategy;
    }

    /**
     * Check if the client is logged
     */
    public function isLogged(): bool
    {
        return $this->user !== null && $this->user->id() !== 0;
    }

    /**
     * Check if the current user is master
     */
    public function isMaster(): bool
    {
        return $this->isLogged() && $this->grant->isMaster($this->user);
    }

    /**
     * Get the logged user
     */
    public function user(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * Check if the current user has access to route
     */
    public function hasAccess(Route $route): bool
    {
        if (!$this->access->has($route)) {
            switch ($this->defaultStrategy) {
                case self::STRATEGY_ALLOW:
                    return true;
                case self::STRATEGY_RESTRICT:
                    return $this->isLogged();
                case self::STRATEGY_BLOCK:
                    return $this->isMaster();
            }
        }

        if ($this->access->isAnonymous($route)) {
            return true;
        }

        if (!$this->isLogged()) {
            return false;
        }

        return $this->grant->grantAll(
            $this->user,
            $this->access->permissions($route)
        );
    }

    /**
     * Start the security service
     */
    public function start(RequestInterface $request, Route $route): void
    {
        $this->user = $this->userResolver->resolve($request);

        if (!$this->hasAccess($route)) {
            if ($this->isLogged()) {
                throw new ForbiddenHttpException();
            } else {
                throw new UnauthorizedHttpException();
            }
        }
    }
}
