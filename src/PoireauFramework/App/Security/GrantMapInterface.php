<?php

namespace PoireauFramework\App\Security;

/**
 * Interface for map of granted permissions
 */
interface GrantMapInterface
{
    /**
     * Add grant permissions for a role
     * @param string $role The user role @see UserInterface::roles()
     * @param string[] $permissions Set the permissions
     * @return $this
     */
    public function grant(string $role, array $permissions): GrantMapInterface;

    /**
     * Grant permission for a user id
     * @param int $id The user id @see UserInterface::id()
     * @param string[] $permissions Set of permissions
     * @return $this
     */
    public function grantById(int $id, array $permissions): GrantMapInterface;

    /**
     * Set a user id as master (i.e. it will be all permissions)
     * @param int $id The user id @see UserInterface::id()
     * @return $this
     */
    public function grantMasterId(int $id): GrantMapInterface;

    /**
     * Set a user role as master (i.e. it will be all permissions)
     * @param string $role The user role @see UserInterface::roles()
     * @return $this
     */
    public function grantMasterRole(string $role): GrantMapInterface;

    /**
     * Check if the user grant the requested permission
     */
    public function isGranted(UserInterface $user, string $permission): bool;

    /**
     * Check if the user is a master user
     * @see GrantMapInterface::grantMasterId()
     * @see GrantMapInterface::grantMasterRole()
     */
    public function isMaster(UserInterface $user): bool;

    /**
     * Check if the user grant all of requested permissions
     * If one permission is not granted, returns false
     */
    public function grantAll(UserInterface $user, array $permissions): bool;

    /**
     * Check if the user grant at least one of the requested permissions
     */
    public function grantOne(UserInterface $user, array $permissions): bool;
}
