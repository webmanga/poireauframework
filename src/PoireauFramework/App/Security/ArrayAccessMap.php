<?php

namespace PoireauFramework\App\Security;

use PoireauFramework\App\Router\Route;

/**
 * Access map implementation using simple array representation
 */
final class ArrayAccessMap implements AccessMapInterface
{
    /**
     * @var array
     */
    private $map;


    /**
     *
     */
    public function __construct(array $map)
    {
        $this->map = $map;
    }

    /**
     * {@inheritdoc}
     */
    public function permissions(Route $route): array
    {
        $permissions = [];

        if (isset($this->map["*"])) {
            $permissions = $this->extractFromControllerRule($route, $this->map["*"]);
        }

        if (isset($this->map[$route->controller()])) {
            $permissions = array_merge(
                $permissions,
                $this->extractFromControllerRule($route, $this->map[$route->controller()])
            );
        }

        return $permissions;
    }

    /**
     * {@inheritdoc}
     */
    public function has(Route $route): bool
    {
        return
            (isset($this->map[$route->controller()], $this->map[$route->controller()][$route->action()]))
         || (isset($this->map[$route->controller()], $this->map[$route->controller()]["*"]))
         || (isset($this->map["*"], $this->map["*"][$route->action()]))
         || (isset($this->map["*"], $this->map["*"]["*"]))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function isAnonymous(Route $route): bool
    {
        if (!isset($this->map[$route->controller()])) {
            return false;
        }

        $rules = $this->map[$route->controller()];

        if (isset($rules[$route->action()]) && $rules[$route->action()] === self::ANONYMOUS_ACCESS) {
            return true;
        }

        if (isset($rules["*"]) && $rules["*"] === self::ANONYMOUS_ACCESS) {
            return true;
        }

        return false;
    }

    /**
     * Extract permission from a controller rule
     */
    private function extractFromControllerRule(Route $route, array $rule): array
    {
        $permissions = [];

        if (isset($rule["*"])) {
            if (is_array($rule["*"])) {
                $permissions = $rule["*"];
            } else {
                $permissions = [$rule["*"]];
            }
        }

        if (isset($rule[$route->action()])) {
            if (!is_array($rule[$route->action()])) {
                $permissions[] = $rule[$route->action()];
            } else {
                $permissions = array_merge($permissions, $rule[$route->action()]);
            }
        }

        return $permissions;
    }
}
