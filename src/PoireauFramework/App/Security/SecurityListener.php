<?php

namespace PoireauFramework\App\Security;

use PoireauFramework\App\Http\Event\Routing;
use PoireauFramework\App\Http\Event\RoutingArgument;
use PoireauFramework\App\Http\Event\RoutingListenerInterface;

use PoireauFramework\Event\EventListenerInterface;

/**
 * Listener for SecurityService
 */
final class SecurityListener implements EventListenerInterface, RoutingListenerInterface
{
    /**
     * @var SecurityService
     */
    private $service;


    /**
     *
     */
    public function __construct(SecurityService $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function eventTypes(): array
    {
        return [Routing::class];
    }

    /**
     * {@inheritdoc}
     */
    public function onRouting(RoutingArgument $argument): void
    {
        $this->service->start(
            $argument->request(),
            $argument->route()
        );
    }
}
