<?php

namespace PoireauFramework\App\Security;

/**
 * Simple implementation of GrantMapInterface
 */
final class GrantMap implements GrantMapInterface
{
    const MASTER = "__MASTER__";

    private $roles = [];
    private $ids   = [];

    /**
     * {@inheritdoc}
     */
    public function grant(string $role, array $permissions): GrantMapInterface
    {
        $this->roles[$role] = array_flip($permissions);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function grantById(int $id, array $permissions): GrantMapInterface
    {
        $this->ids[$id] = array_flip($permissions);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function grantMasterId(int $id): GrantMapInterface
    {
        $this->ids[$id] = self::MASTER;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function grantMasterRole(string $role): GrantMapInterface
    {
        $this->roles[$role] = self::MASTER;

        return $this;
    }

    public function isMaster(UserInterface $user): bool
    {
        if (isset($this->ids[$user->id()]) && $this->ids[$user->id()] === self::MASTER) {
            return true;
        }

        foreach ($user->roles() as $role) {
            if (isset($this->roles[$role]) && $this->roles[$role] === self::MASTER) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isGranted(UserInterface $user, string $permission): bool
    {
        if ($this->isMaster($user)) {
            return true;
        }

        if (isset($this->ids[$user->id()], $this->ids[$user->id()][$permission])) {
            return true;
        }

        foreach ($user->roles() as $role) {
            if (isset($this->roles[$role], $this->roles[$role][$permission])) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function grantAll(UserInterface $user, array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if (!$this->isGranted($user, $permission)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function grantOne(UserInterface $user, array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if ($this->isGranted($user, $permission)) {
                return true;
            }
        }

        return false;
    }
}
