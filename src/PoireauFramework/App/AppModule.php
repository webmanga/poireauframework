<?php

namespace PoireauFramework\App;

use PoireauFramework\App\Controller\BaseControllerFactory;
use PoireauFramework\App\Controller\ControllerFactoryInterface;
use PoireauFramework\App\Controller\ControllerResolverInterface;
use PoireauFramework\App\Controller\RouterControllerResolver;
use PoireauFramework\App\Http\RequestRunner;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\RouterInterface;
use PoireauFramework\App\Router\SimpleRouter;

use PoireauFramework\Config\Config;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseStack;
use PoireauFramework\Http\Response\ResponseTransformerInterface;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Module for app
 */
class AppModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(ControllerFactoryInterface::class, BaseControllerFactory::class);
        $injector->impl(ControllerResolverInterface::class, RouterControllerResolver::class);
        $injector->impl(RouterInterface::class, SimpleRouter::class);

        $injector->factory(BaseControllerFactory::class, [$this, "createControllerFactory"]);
        $injector->factory(RouterControllerResolver::class, [$this, "createControllerResolver"]);
        $injector->factory(SimpleRouter::class, [$this, "createRouter"]);
        $injector->factory(RequestRunner::class, [$this, "createRequestRunner"]);
    }

    public function createControllerFactory(InjectorInterface $injector): BaseControllerFactory
    {
        return new BaseControllerFactory($injector->get(Application::class));
    }

    public function createControllerResolver(InjectorInterface $injector): RouterControllerResolver
    {
        return new RouterControllerResolver(
            $injector->get(Config::class)->item("controller_resolver"),
            $injector->get(ControllerFactoryInterface::class),
            $injector->get(RequestStack::class),
            $injector->get(ResponseStack::class)
        );
    }

    public function createRouter(InjectorInterface $injector): SimpleRouter
    {
        return new SimpleRouter($injector->get(Config::class)->item("router"));
    }

    public function createRequestRunner(InjectorInterface $injector): RequestRunner
    {
        return new RequestRunner(
            $injector->get(EventDispatcherInterface::class),
            $injector->get(RequestStack::class),
            $injector->get(ResponseStack::class),
            $injector->get(ControllerResolverInterface::class),
            $injector->get(ResponseTransformerInterface::class),
            $injector->get(RouterInterface::class)
        );
    }
}
