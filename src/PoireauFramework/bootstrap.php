<?php
/**
 * Boot file for PoireauFramework
 * 
 * @return PoireauFramework The framework instance
 * @author Vincent QUATREVIEUX <quatrevieux.vincent@gmail.com>
 */

use PoireauFramework\Debug\ErrorHandler;
use PoireauFramework\Kernel\PoireauFramework;

//Constants define
if(!defined('PW_START_TIME'))
    define('PW_START_TIME', microtime(true));

if(!defined('PW_ROOT'))
    define('PW_ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);

ini_set('display_errors', true);

//Include loader (and init autoloader)
require_once __DIR__ . DIRECTORY_SEPARATOR . 'Loader' . DIRECTORY_SEPARATOR . 'Loader.php';

//Init error handler
ErrorHandler::getInstance();