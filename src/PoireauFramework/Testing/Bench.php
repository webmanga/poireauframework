<?php

namespace PoireauFramework\Testing;

/**
 * Benchmark class utils
 */
class Bench
{
    /**
     * @var int
     */
    private $iterations = 1000;

    /**
     * @var callable[]
     */
    private $tests = [];

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $results = [];

    /**
     * Set iterations number
     * @param int $num
     *
     * @return $this
     */
    public function iterations($num)
    {
        $this->iterations = $num;

        return $this;
    }

    /**
     * Add a new test
     *
     * @param string $label
     * @param callable $test
     *
     * @return $this
     */
    public function add($label, callable $test)
    {
        $this->tests[$label] = $test;

        return $this;
    }

    /**
     * Add new arguments
     *
     * @param string|array $label
     * @param array|null $arguments
     *
     * @return $this
     */
    public function data($label, $arguments = null)
    {
        if (is_array($label)) {
            $this->data[] = $label;
        } else {
            $this->data[$label] = (array) $arguments;
        }

        return $this;
    }

    /**
     * Run all tests
     *
     * @return $this
     */
    public function run()
    {
        foreach ($this->tests as $label => $fn) {
            $results = [];
            $data = $this->data ?: ['global' => []];

            foreach ($data as $name => $arguments) {
                $time = microtime(true);

                for ($i = 0; $i < $this->iterations; ++$i) {
                    $fn(...$arguments);
                }

                $results[$name] = [
                    'time' => microtime(true) - $time
                ];
            }

            if (!isset($results['global'])) {
                $global = [
                    'time' => 0
                ];

                foreach ($results as $result) {
                    foreach ($result as $key => $value) {
                        $global[$key] += $value;
                    }
                }

                $results['global'] = $global;
            }

            foreach ($results as $name => $result) {
                if (!isset($this->results[$name])) {
                    $this->results[$name] = [];
                }

                $this->results[$name][$label] = $result;
            }
        }

        return $this;
    }

    /**
     * Show all results
     *
     * @return $this
     */
    public function show()
    {
        foreach ($this->results as $name => $results) {
            echo '====> ', $name, ' <====', PHP_EOL;

            $baseLine = PHP_INT_MAX;

            foreach ($results as $result) {
                if ($result['time'] < $baseLine) {
                    $baseLine = $result['time'];
                }
            }

            foreach ($results as $label => $result) {
                $percent = ($result['time'] - $baseLine) / $baseLine;
                $percent *= 100;

                $sign = $percent > 0 ? '+' : '';

                echo $label, ' ', $result['time'], 's (', $sign, round($percent, 2), '%) - ', round($this->iterations / $result['time']) . ' op/sec ', PHP_EOL;
            }

            echo PHP_EOL;
        }

        return $this;
    }
}