<?php

namespace PoireauFramework\Testing;

use PHPUnit\Framework\TestCase;

/**
 * TestCase for bench
 *
 * Iterate over methods called bench_* to add tests methods
 */
class BenchTestCase extends TestCase
{
    /**
     * @var Bench
     */
    protected $bench;


    /**
     *
     */
    protected function setUp()
    {
        $this->bench = new Bench();

        $this->configure($this->bench);
        $this->locateTests($this->bench);
    }

    /**
     * Configure the benchmark
     *
     * @param Bench $bench
     */
    protected function configure(Bench $bench)
    {
    }

    /**
     * @param Bench $bench
     */
    protected function locateTests(Bench $bench)
    {
        $reflection = new \ReflectionClass($this);

        foreach ($reflection->getMethods() as $method) {
            if (substr($method->getName(), 0, 6) === 'bench_') {
                $bench->add(substr($method->getName(), 6), [$this, $method->getName()]);
            } elseif (substr($method->getName(), 0, 5) === 'data_') {
                $bench->data(substr($method->getName(), 5), $method->invoke($this));
            }
        }
    }

    /**
     *
     */
    public function test_bench()
    {
        echo PHP_EOL;

        $this->bench
            ->run()
            ->show()
        ;
    }
}
