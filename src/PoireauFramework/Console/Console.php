<?php

namespace PoireauFramework\Console;

use PoireauFramework\Console\Command\CommandInterface;
use PoireauFramework\Console\Command\HelpCommand;
use PoireauFramework\Console\Exception\CommandNotFoundException;
use PoireauFramework\Console\Output\ConsoleOutput;
use PoireauFramework\Console\Output\ConsoleOutputInterface;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Kernel\KernelInterface;
use PoireauFramework\Kernel\RunnerInterface;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Console kernel, decorate a base kernel
 * to provide CLI options
 * <pre><code>
 * $application = new Application(...);
 * $application->register();
 * // ...
 * $console = new Console($application);
 * $console->register(new MyCommand());
 * $console->run();
 * </code></pre>
 */
class Console implements KernelInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var RunnerInterface
     */
    private $runner;

    /**
     * @var ConsoleInput
     */
    private $input;

    /**
     * @var ConsoleOutputInterface
     */
    private $output;

    /**
     * @var CommandInterface[]
     */
    private $commands = [];


    /**
     * @param KernelInterface $kernel
     * @param ConsoleInput|null $input
     * @param ConsoleOutputInterface|null $output
     * @param RunnerInterface|null $runner
     *
     * @throws \PoireauFramework\Injector\Exception\BadImplementationException
     */
    public function __construct(KernelInterface $kernel, ConsoleInput $input = null, ConsoleOutputInterface $output = null, RunnerInterface $runner = null)
    {
        $this->kernel = $kernel;

        if ($kernel instanceof Console) {
            $this->commands = $kernel->commands;
        } else {
            $this->register(new HelpCommand());
        }

        $this->input  = $input ?: $this->injector()->get(ConsoleInput::class);
        $this->output = $output ?: new ConsoleOutput();
        $this->runner = $runner ?: new ConsoleRunner();
    }

    /**
     * {@inheritdoc}
     */
    public function isLoaded(): bool
    {
        return $this->kernel->isLoaded();
    }

    /**
     * {@inheritdoc}
     */
    public function load(): void
    {
        $this->kernel->load();
    }

    /**
     * {@inheritdoc}
     */
    public function injector(): InjectorInterface
    {
        return $this->kernel->injector();
    }

    /**
     * {@inheritdoc}
     */
    public function registry(): RegistryInterface
    {
        return $this->kernel->registry();
    }

    /**
     * {@inheritdoc}
     */
    public function events(): EventDispatcherInterface
    {
        return $this->kernel->events();
    }

    /**
     * {@inheritdoc}
     */
    public function register($module): void
    {
        if ($module instanceof CommandInterface) {
            $this->commands[$module->name()] = $module;
        } else {
            $this->kernel->register($module);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run(): void
    {
        $this->runner()->run($this);
    }

    /**
     * Get the runner instance
     * @return ConsoleRunner|RunnerInterface
     */
    public function runner(): RunnerInterface
    {
        return $this->runner;
    }

    /**
     * Get the input
     */
    public function input(): ConsoleInput
    {
        return $this->input;
    }

    /**
     * Get the output
     */
    public function output(): ConsoleOutputInterface
    {
        return $this->output;
    }

    /**
     * Get a command
     */
    public function command(string $name): CommandInterface
    {
        if (!isset($this->commands[$name])) {
            throw new CommandNotFoundException($name);
        }

        return $this->commands[$name];
    }

    /**
     * Get all commands
     *
     * @return CommandInterface[]
     */
    public function commands(): array
    {
        return $this->commands;
    }
}
