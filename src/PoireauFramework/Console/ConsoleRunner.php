<?php

namespace PoireauFramework\Console;

use PoireauFramework\Console\Command\CommandOptions;
use PoireauFramework\Kernel\KernelInterface;
use PoireauFramework\Kernel\RunnerInterface;

/**
 * Runner for console applications
 */
class ConsoleRunner implements RunnerInterface
{
    /**
     * @var CommandOptions
     */
    protected $runnerOptions;


    /**
     * Construct the runner
     */
    public function __construct() {
        $this->runnerOptions = CommandOptions::createDefaults("help");
    }

    /**
     * {@inheritdoc}
     */
    public function run(KernelInterface $kernel): void
    {
        $input = $this->runnerOptions->input($kernel->input());

        $kernel->command($input->argument("command"))->run($kernel);
    }
}
