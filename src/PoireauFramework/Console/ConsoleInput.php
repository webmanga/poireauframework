<?php

namespace PoireauFramework\Console;

/**
 * Handle console inputs
 * ex:
 * "console.php command -abc test -b --opt -o output.txt hello world"
 * Will be parsed :
 * arguments :
 *  0. "command"
 *  1. "test"
 *  2. "output.txt"
 *  3. "hello"
 *  4. "world"
 *
 * options :
 *  "a"   => null
 *  "b"   => [null, null]
 *  "c"   => 1
 *  "opt" => null
 *  "o"   => 2
 */
class ConsoleInput
{
    /**
     * Arguments list, indexed by number
     * @var string[]
     */
    private $arguments = [];

    /**
     * Options list
     * Indexed by option name
     * The value can be an array on multiple options defined
     * or the related argument is exists
     * @var array
     */
    private $options = [];

    private $currentOption = null;


    public function __construct(array $tokens)
    {
        $this->parse($tokens);
    }

    /**
     * Get all arguments
     * @return string[]
     */
    public function arguments(): array
    {
        return $this->arguments;
    }

    /**
     * Get one argument
     *
     * @param int $pos The argument position
     *
     * @return string
     */
    public function argument(int $pos): string
    {
        return $this->arguments[$pos];
    }

    /**
     * Count the option occurrences
     * If the option is not found, will returns 0
     * If there is only one element, will returns 1
     * In other cases, will returns the number of occurrences
     *
     * @param string $option The option name
     *
     * @return int The number of occurrences
     */
    public function count(string $option): int
    {
        if (!array_key_exists($option, $this->options)) {
            return 0;
        }

        $value = $this->options[$option];

        return is_array($value) ? count($value) : 1;
    }

    /**
     * Get option value
     * @param string option The option name
     * @return array | string | bool Return list of related arguments, or the related argument, true if there is no related, or false if option is not set
     */
    public function option(string $option)
    {
        if (!array_key_exists($option, $this->options)) {
            return false;
        }

        $value = $this->options[$option];

        if ($value === null) {
            return true;
        }

        if (is_array($value)) {
            $values = [];

            foreach ($value as $arg) {
                if ($arg !== null) {
                    $values[] = $this->argument($arg);
                } else {
                    $values[] = null;
                }
            }

            return $values;
        }

        return $this->argument($value);
    }

    /**
     * Get all options (without parsing)
     * @return array
     */
    public function options(): array
    {
        return $this->options;
    }

    /**
     * Parse all tokens
     *
     * @param string[] $tokens
     */
    private function parse(array $tokens): void
    {
        foreach ($tokens as $token) {
            if ($token[0] === "-") {
                $this->parseOption($token);
            } else {
                $this->parseArgument($token);
            }
        }
    }

    private function parseOption(string $arg): void
    {
        if ($arg === "-") {
            throw new \Exception("Expecting option after a dash");
        }

        if ($arg[1] === "-") {
            $this->addOption(substr($arg, 2));
        } else {
            for ($i = 1, $len = strlen($arg); $i < $len; ++$i) {
                $this->addOption($arg[$i]);
            }
        }
    }

    private function addOption(string $option): void
    {
        if (!array_key_exists($option, $this->options)) {
            $this->options[$option] = null;
        } else {
            $last = $this->options[$option];

            if (is_array($last)) {
                $this->options[$option][] = null;
            } else {
                $this->options[$option] = [$last, null];
            }
        }

        $this->currentOption = $option;
    }

    private function parseArgument(string $arg): void
    {
        $curArg = count($this->arguments);

        $this->arguments[] = $arg;

        if ($this->currentOption !== null) {
            $opt = $this->options[$this->currentOption];

            if (is_array($opt)) {
                $this->options[$this->currentOption][count($opt) - 1] = $curArg;
            } else {
                $this->options[$this->currentOption] = $curArg;
            }

            $this->currentOption = null;
        }
    }
}
