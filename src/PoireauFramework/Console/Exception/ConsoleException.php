<?php

namespace PoireauFramework\Console\Exception;

use RuntimeException;

/**
 * Exception thrown when an error occurs during execution
 */
class ConsoleException extends RuntimeException
{

}
