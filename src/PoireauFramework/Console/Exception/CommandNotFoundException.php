<?php

namespace PoireauFramework\Console\Exception;

/**
 * Exception when command cannot be found on Console
 */
class CommandNotFoundException extends ConsoleException
{
    /**
     * @var string
     */
    private $command;


    /**
     *
     */
    public function __construct(string $command)
    {
        parent::__construct("Command '$command' is not found");

        $this->command = $command;
    }

    /**
     * Get the command name
     */
    public function command(): string
    {
        return $this->command;
    }
}
