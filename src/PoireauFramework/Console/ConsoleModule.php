<?php

namespace PoireauFramework\Console;

use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Module for console
 */
class ConsoleModule implements InjectorModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->factory(ConsoleInput::class, [$this, "createConsoleInput"]);
    }

    public function createConsoleInput(): ConsoleInput
    {
        return new ConsoleInput($_SERVER["argv"]);
    }
}
