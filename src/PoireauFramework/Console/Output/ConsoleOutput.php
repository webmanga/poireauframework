<?php

namespace PoireauFramework\Console\Output;

use PoireauFramework\Util\IO\OutputWriter;
use PoireauFramework\Util\IO\Writable;

/**
 * Output for console
 */
class ConsoleOutput implements ConsoleOutputInterface
{
    /**
     * @var Writable
     */
    private $writer;


    /**
     * @param Writable|null $writer
     */
    public function __construct(Writable $writer = null)
    {
        $this->writer = $writer ?: new OutputWriter();
    }

    /**
     * {@inheritdoc}
     */
    public function write(string $what): Writable
    {
        $this->writer->write($what);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function writeln(string $line): Writable
    {
        $this->writer->writeln($line);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function newLine(): Writable
    {
        $this->writer->newLine();

        return $this;
    }
}
