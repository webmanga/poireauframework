<?php

namespace PoireauFramework\Console\Output;

use PoireauFramework\Util\IO\Writable;

/**
 * Interface for console output
 */
interface ConsoleOutputInterface extends Writable
{
    
}