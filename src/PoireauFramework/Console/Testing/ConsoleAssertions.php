<?php

namespace PoireauFramework\Console\Testing;

use PoireauFramework\Console\Command\CommandInterface;
use PoireauFramework\Console\Console;
use PoireauFramework\Console\ConsoleInput;
use PoireauFramework\Console\Output\ConsoleOutput;
use PoireauFramework\Util\IO\ArrayWriter;

/**
 * Class ConsoleAssertions
 */
trait ConsoleAssertions
{
    /**
     * @var Console
     */
    protected $console;

    /**
     * @param string|CommandInterface $command
     * @param array $arguments
     * @return string[]
     */
    public function executeCommand($command, array $arguments = [])
    {
        $writer = new ArrayWriter();

        $commandName = $command instanceof CommandInterface ? $command->name() : $command;

        $input = new ConsoleInput(array_merge(["console.php", $commandName], $arguments));
        $output = new ConsoleOutput($writer);
        $console = new Console($this->console, $input, $output);

        if ($command instanceof CommandInterface) {
            $command->run($console);
        } else {
            $console->run();
        }

        return $writer->lines();
    }

    /**
     * @param string|CommandInterface $command
     * @param array $arguments
     * @return string[]
     */
    public function assertCommandResult($expect, $command, array $arguments = [])
    {
        $result = $this->executeCommand($command, $arguments);

        if (!is_array($expect)) {
            $expect = [$expect];
        }

        $command = $command instanceof CommandInterface ? $command->name() : $command;

        $this->assertEquals($expect, $result, "Invalid result for command {$command}");
    }
}