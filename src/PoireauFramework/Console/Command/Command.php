<?php

namespace PoireauFramework\Console\Command;

use PoireauFramework\Console\Console;

/**
 * Controller for console command
 */
abstract class Command implements CommandInterface
{
    /**
     * @var CommandOptions
     */
    private $options;

    /**
     * @var \PoireauFramework\Console\Console
     */
    protected $console;

    /**
     * @var \PoireauFramework\Console\Output\ConsoleOutputInterface
     */
    protected $output;


    /**
     * Construct the command
     * @todo CommandOption should corresponds with ConsoleRunner options
     */
    public function __construct() {
        $this->options = CommandOptions::createDefaults($this->name());

        $this->configure($this->options);
    }

    /**
     * {@inheritdoc}
     */
    public function shortDescription(): string
    {
        return "No description";
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return $this->shortDescription();
    }

    /**
     * Configure the command
     */
    abstract protected function configure(CommandOptions $options): void;

    /**
     * {@inheritdoc}
     */
    public function options(): CommandOptions
    {
        return $this->options;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Console $console): void
    {
        $this->console = $console;
        $this->output = $console->output();

        $this->execute($this->options->input($console->input()));
    }

    /**
     * Do the execution of the command
     */
    abstract protected function execute(CommandInput $input): void;
}
