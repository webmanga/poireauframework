<?php

namespace PoireauFramework\Console\Command;

use PoireauFramework\Console\Console;

/**
 * Interface for commands
 */
interface CommandInterface
{
    /**
     * Get the command name
     *
     * @return string
     */
    public function name(): string;

    public function shortDescription(): string;

    public function description(): string;

    /**
     * Get the command options
     *
     * @return CommandOptions
     */
    public function options(): CommandOptions;

    /**
     * Run the command
     *
     * @param Console $console
     */
    public function run(Console $console): void;
}