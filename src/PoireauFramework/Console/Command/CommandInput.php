<?php

namespace PoireauFramework\Console\Command;

use PoireauFramework\Console\ConsoleInput;
use PoireauFramework\Console\Exception\ConsoleException;

/**
 * Input for a command
 * <pre><code>
 * $options = new CommandOptions();
 * // Configure options and arguments
 * $input = $options->input($consoleInput);
 *
 * //Example for copy text file
 * $data = file_get_contents($input->argument("file"));
 * file_put_contents($input->output, $data);
 * </pre></code>
 */
class CommandInput
{
    /**
     * @var ConsoleInput
     */
    private $input;

    /**
     * @var CommandOptions
     */
    private $commandOptions;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var array
     */
    private $arguments = [];


    /**
     * Construct the CommandInput
     *
     * @param CommandOptions commandOptions
     * @param ConsoleInput input
     *
     * @see CommandOptions::input() For instantiate this class
     */
    public function __construct(CommandOptions $commandOptions, ConsoleInput $input)
    {
        $this->commandOptions = $commandOptions;
        $this->input = $input;

        $this->load();
    }

    /**
     * Get the console input
     */
    public function input(): ConsoleInput
    {
        return $this->input;
    }

    /**
     * Get an option value
     */
    public function __get(string $name)
    {
        return $this->option($name);
    }

    /**
     * Get the option value
     */
    public function option(string $option)
    {
        if (!array_key_exists($option, $this->options)) {
            throw new \LogicException("Undefined option '$option'");
        }

        return $this->options[$option];
    }

    /**
     * Get the option value
     */
    public function argument(string $option)
    {
        if (!array_key_exists($option, $this->arguments)) {
            throw new \LogicException("Undefined argument '$option'");
        }

        return $this->arguments[$option];
    }

    /**
     * Load options and arguments from input
     */
    protected function load(): void
    {
        $inArguments = $this->input->arguments();

        foreach ($this->commandOptions->options() as $option) {
            switch ($option->type()) {
                case CommandOption::TYPE_BOOL:
                    $value = false;
                    break;
                case CommandOption::TYPE_STRING:
                    $value = null;
                    break;
                case CommandOption::TYPE_ARRAY:
                    $value = [];
                    break;
                case CommandOption::TYPE_COUNT:
                    $value = 0;
                    break;
            }

            foreach ([$option->name(), $option->shortcut()] as $name) {
                if ($name === null) {
                    continue;
                }

                switch ($option->type()) {
                    case CommandOption::TYPE_BOOL:
                        $value = (bool) $value | $this->input->count($name) > 0;
                        break;
                    case CommandOption::TYPE_STRING:
                        if ($this->input->count($name) > 0) {
                            if ($this->input->count($name) > 1 || !empty($value)) {
                                throw new ConsoleException("Option '{$option->name()}' cannot be set more than once");
                            }

                            $value = $this->input->option($name);
                            unset($inArguments[$this->input->options()[$name]]);
                        }
                        break;
                    case CommandOption::TYPE_ARRAY:
                        $tmp = $this->input->option($name);

                        if (empty($tmp)) {
                            break;
                        }

                        if ($tmp === true) {
                            $value[] = null;
                        } elseif (!is_array($tmp)) {
                            $value[] = $tmp;
                            unset($inArguments[$this->input->options()[$name]]);
                        } else {
                            foreach ($tmp as $v) {
                                $value[] = $v;
                            }

                            foreach ($this->input->options()[$name] as $v) {
                                if (is_int($v)) {
                                    unset($inArguments[$v]);
                                }
                            }
                        }
                        break;
                    case CommandOption::TYPE_COUNT:
                        $value += $this->input->count($name);
                        break;
                }
            }

            if ($option->type() !== CommandOption::TYPE_BOOL) {
                if (empty($value) && $option->defaultValue() !== null) {
                    $value = $option->defaultValue();
                }

                if (empty($value) && $option->isRequired()) {
                    throw new ConsoleException("Option '{$option->name()}' is required");
                }
            }

            $this->options[$option->name()] = $value;
        }

        $inArguments = array_values($inArguments);
        $length = count($inArguments);
        $pos = 0;

        foreach($this->commandOptions->arguments() as $argument) {
            if ($pos >= $length) {
                break;
            }

            if ($argument->isArray()) {
                $this->arguments[$argument->name()] = array_slice($inArguments, $pos);
                break;
            } else {
                $this->arguments[$argument->name()] = $inArguments[$pos];
                ++$pos;
            }
        }

        foreach ($this->commandOptions->arguments() as $argument) {
            if (!isset($this->arguments[$argument->name()])) {
                if ($argument->defaultValue() !== null) {
                    $this->arguments[$argument->name()] = $argument->defaultValue();
                } elseif ($argument->isRequired()) {
                    throw new ConsoleException("Argument '{$argument->name()}' is required");
                } elseif ($argument->isArray()) {
                    $this->arguments[$argument->name()] = [];
                } else {
                    $this->arguments[$argument->name()] = null;
                }
            }
        }
    }
}
