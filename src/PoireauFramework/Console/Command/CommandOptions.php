<?php

namespace PoireauFramework\Console\Command;

use PoireauFramework\Console\ConsoleInput;

/**
 * Arguments and options of a Command
 */
class CommandOptions
{
    /**
     * @var CommandOption[]
     */
    private $options = [];

    /**
     * @var CommandArgument[]
     */
    private $arguments = [];


    /**
     * Get command options
     *
     * @return CommandOption[]
     */
    public function options(): array
    {
        return $this->options;
    }

    /**
     * Add a new option (i.e. --opt, or -o)
     *
     * @param string name The full option name. For shorter name, @see CommandOption::setShortcut()
     * @param int type <p>One of the CommandOption::TYPE_* constant.
     * Available values are :
     *   - TYPE_STRING : The option is a simple string. Cannot be set more than once (ex: --output my_out.txt)
     *   - TYPE_BOOL : Check the existence of the option. (ex: --case-insensitive). Cannot set a default value, or as required
     *   - TYPE_COUNT : Same as bool, but count the occurrences of the option (ex: --verbose -vvv). A default value can be set, and also can be required
     *   - TYPE_ARRAY : The "multiple value" version of TYPE_STRING (ex: --header "Key: value" --header "Other: azerty"). Values are an array of strings. If a value is not given to the option, it will be stored as NULL
     * </p>
     *
     * @return CommandOption
     */
    public function addOption(string $name, int $type = CommandOption::TYPE_STRING): CommandOption
    {
        return $this->options[$name] = new CommandOption($name, $type);
    }

    /**
     * @return CommandArgument[]
     */
    public function arguments(): array
    {
        return $this->arguments;
    }

    /**
     * Add a new argument
     *
     * @param string $name The argument name
     *
     * @return CommandArgument
     */
    public function addArgument(string $name): CommandArgument
    {
        return $this->arguments[$name] = new CommandArgument($name);
    }

    /**
     * Create the related input
     *
     * @param ConsoleInput $input The console input parameters
     *
     * @return CommandInput
     */
    public function input(ConsoleInput $input): CommandInput
    {
        return new CommandInput($this, $input);
    }

    /**
     * Create with defaults values
     *
     * @param string $defaultCommand
     *
     * @return CommandOptions
     */
    static public function createDefaults(string $defaultCommand): CommandOptions
    {
        $options = new CommandOptions();

        $options
            ->addArgument("script")
            ->setRequired()
        ;

        $options
            ->addArgument("command")
            ->setDefaultValue($defaultCommand)
        ;

        $options
            ->addOption("help", CommandOption::TYPE_BOOL)
            ->setShortcut("h")
        ;

        return $options;
    }
}
