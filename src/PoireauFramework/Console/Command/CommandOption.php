<?php

namespace PoireauFramework\Console\Command;

/**
 *
 */
class CommandOption
{
    const TYPE_BOOL   = 1;
    const TYPE_STRING = 2;
    const TYPE_ARRAY  = 3;
    const TYPE_COUNT  = 4;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $type = self::TYPE_STRING;

    /**
     * @var bool
     */
    private $required = false;

    /**
     * @var string|null
     */
    private $shortcut;

    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * @var string
     */
    private $description;


    /**
     * @param string $name
     * @param int $type
     */
    public function __construct(string $name, int $type = self::TYPE_STRING)
    {
        $this->name = $name;
        $this->type = $type;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function type(): int
    {
        return $this->type;
    }

    public function setType(int $type): CommandOption
    {
        $this->type = $type;

        return $this;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setRequired(bool $required = true): CommandOption
    {
        $this->required = $required;

        return $this;
    }

    public function shortcut(): ?string
    {
        return $this->shortcut;
    }

    public function setShortcut(string $shortcut): CommandOption
    {
        $this->shortcut = $shortcut;

        return $this;
    }

    public function defaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(string $defaultValue): CommandOption
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): CommandOption
    {
        $this->description = $description;

        return $this;
    }
}
