<?php

namespace PoireauFramework\Console\Command;

/**
 * Argument for a command
 */
class CommandArgument
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * @var bool
     */
    private $required = false;

    /**
     * @var bool
     */
    private $isArray = false;

    /**
     * @var string
     */
    private $description;


    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function isArray(): bool
    {
        return $this->isArray;
    }

    public function setArray(bool $isArray = true): CommandArgument
    {
        $this->isArray = $isArray;

        return $this;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setRequired(bool $required = true): CommandArgument
    {
        $this->required = $required;

        return $this;
    }

    public function defaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(string $defaultValue): CommandArgument
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): CommandArgument
    {
        $this->description = $description;

        return $this;
    }
}
