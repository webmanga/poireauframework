<?php

namespace PoireauFramework\Console\Command;

/**
 * help command
 */
class HelpCommand extends Command
{
    /**
     * {@inhertidoc}
     */
    public function name(): string
    {
        return "help";
    }

    /**
     * {@inheritdoc}
     */
    public function shortDescription(): string
    {
        return "Get information on commands";
    }

    /**
     * {@inheridoc}
     */
    protected function configure(CommandOptions $options): void
    {
        $options->addArgument("cmd");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(CommandInput $input): void
    {
        if (!$input->argument("cmd")) {
            $this->executeCommandList($input);
        } else {
            $this->executeCmdHelp($input, $input->argument("cmd"));
        }
    }

    protected function executeCmdHelp(CommandInput $input, string $commandName): void
    {

    }

    protected function executeCommandList(CommandInput $input): void
    {
        $this->output->writeln("Available commands :");

        foreach ($this->console->commands() as $cmd) {
            $this->output->writeln("\t{$cmd->name()}\t{$cmd->shortDescription()}");
        }
    }
}
