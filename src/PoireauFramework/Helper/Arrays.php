<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper;

/**
 * Arrays helper
 * @author Vincent QUATREVIEUX <quatrevieux.vincent@gmail.com>
 */
final class Arrays{
    private function __construct(){}
    
    /**
     * Convert to array
     * If the parameter is an object witch implements JsonSerializable, 
     * get the value of jsonSerialize() before casting to array
     * @param mixed $object Object to convert
     * @return array
     */
    static public function toArray($object){
        if(is_array($object))
            return $object;
        
        if(is_object($object) && $object instanceof \JsonSerializable)
            $object = $object->jsonSerialize();
        
        return (array)$object;
    }
}
