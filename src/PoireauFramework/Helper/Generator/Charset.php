<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Generator;

/**
 * Charset constants pool
 *
 * @author vincent
 */
final class Charset {
    const LOWER     = 'abcdefghijklmnopqrstuvwxyz';
    const UPPER     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const LETTERS   = self::LOWER . self::UPPER;
    const DIGITS    = '0123456789';
    const ALPHA_NUM = self::DIGITS . self::LETTERS;
    const HEXA      = '0123456789ABCDEF';
    
    /**
     * Get the charset
     * @return string
     */
    static public function get($name) {
        return constant(Charset::class . '::' . strtoupper($name));
    }
}
