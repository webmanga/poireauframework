<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Generator;

/**
 * @author vincent
 */
abstract class AbstractStringGenerator implements IStringGenerator{
    public function generateString($len, $charset = null, callable $acceptor = null) {
        if ($len < 1) {
            throw new GeneratorException('The length cannot be negative or null');
        }
        
        if (!empty($charset)) {
            $charsetLen = strlen($charset);
        }
        
        for($retry = 1;; ++$retry) {
            $str = $this->generate($len);
            
            if (!empty($charset)) {
                for ($i = 0; $i < $len; ++$i) {
                    $c = (float)ord($str{$i});
                    $c /= 255.0;
                    $c *= $charsetLen;
                    $c = $charset{$c};
                    $str{$i} = $c;
                }
            }
            
            if ($acceptor === null || $acceptor($str)) {
                return $str;
            }
            
            if ($retry > $this->maxRetry()) {
                throw new GeneratorException('Cannot generate string. Max retry reached');
            }
        }
    }
    
    /**
     * Generate the random string
     * @return string
     */
    abstract protected function generate($len);
    
    /**
     * Max umber of retry
     * @return int
     */
    protected function maxRetry() {
        return 10;
    }
}
