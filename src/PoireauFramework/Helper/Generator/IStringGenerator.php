<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Generator;

/**
 * Random string generator
 * 
 * @author vincent
 */
interface IStringGenerator {
    /**
     * Generator a random string
     * @param int $len The generated string length
     * @param string|null $charset The charset. set to null if you want to use all characters
     * @param callable|null $acceptor <p>
     * The string acceptor. Check the if generated string corresponds to some constraints.
     * This callable should take the generated string as parameter, and returns boolean.
     * <pre><code>
     * // Acceptor for unique string
     * $acceptor = function($str) use($model) {
     *      return !$model->hasKey($str);
     * };
     * </pre></code>
     * 
     * If this parameter is set to NULL, the generated string is not check
     * </p>
     * @return string The generated string
     * @throws GeneratorException If no generated strings can be accepted
     */
    public function generateString($len, $charset = null, callable $acceptor = null);
}
