<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Generator;

use PoireauFramework\Exception\PoireauException;

/**
 * Exception for generators
 * @author vincent
 */
class GeneratorException extends PoireauException{
    
}
