<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Generator;

/**
 * StringGenerator using openssl_random_pseudo_bytes()
 * @see openssl_random_pseudo_bytes()
 * @author vincent
 */
class OpenSslPseudoRandomBytesGenerator extends AbstractStringGenerator{
    protected function generate($len) {
        return openssl_random_pseudo_bytes($len);
    }
}
