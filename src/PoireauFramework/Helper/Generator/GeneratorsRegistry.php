<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Generator;

/**
 * Registry for String generators
 *
 * @author vincent
 */
class GeneratorsRegistry {
    /**
     * @var IStringGenerator[]
     */
    private $generators = [];
    
    public function __construct() {
        $this->generators = $this->defaultGenerators();
    }

    /**
     * @return IStringGenerator[]
     */
    protected function defaultGenerators() {
        return [
            'openssl' => new OpenSslPseudoRandomBytesGenerator()
        ];
    }

    /**
     * Add a new generator
     * @param string $name
     * @param IStringGenerator $generator
     */
    public function add($name, IStringGenerator $generator) {
        $this->generators[$name] = $generator;
    }
    
    /**
     * Get a generator
     * @param string $name
     * @return IStringGenerator
     * @throws GeneratorException When the generator is not found
     */
    public function get($name) {
        if (!isset($this->generators[$name])) {
            throw new GeneratorException('Generator ' . $name . ' is not found');
        }
        
        return $this->generators[$name];
    }
}
