<?php

namespace PoireauFramework\Helper;

/**
 * Objet immutable (modification impossible)
 *
 * @author Gaëtan, Vincent QUATREVIEUX
 * 
 * @version 1.1 use ArrayAccess
 */
class ReadOnlyObject implements \ArrayAccess, \IteratorAggregate{
    protected $data;
    
    public function __construct(array $data) {
        $this->data = $data;
    }
    
    public function __get($name) {
        return $this->data[$name];
    }
    
    public function __isset($name) {
        return isset($this->data[$name]);
    }
    
    public function __set($name, $value) {
        throw new \BadMethodCallException('Cannot set a value into a read only object');
    }
    
    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value) {
        throw new \BadMethodCallException('Cannot set a value into a read only object');
    }

    public function offsetUnset($offset) {
        throw new \BadMethodCallException('Cannot unset a value into a read only object');
    }
    
    /**
     * @return \ArrayIterator
     */
    public function getIterator() {
        return new \ArrayIterator($this->data);
    }
}