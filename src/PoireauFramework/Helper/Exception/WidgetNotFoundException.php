<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\Exception;

use PoireauFramework\Exception\PoireauException;

/**
 * Exception on widget not found
 *
 * @author vincent
 */
class WidgetNotFoundException extends PoireauException{
    public function __construct($widgetName) {
        parent::__construct('Widget "' . $widgetName . '" is not found. Did you configure the correct namespace on helpers.widgets_ns ?');
    }
}
