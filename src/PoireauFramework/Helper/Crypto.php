<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper;

use PoireauFramework\Config;
use PoireauFramework\Loader\Creatable;
use PoireauFramework\Loader\Loader;

/**
 * Helper for cryptography (use mcrypt)
 * @author vincent
 */
class Crypto implements Creatable{
    const HMAC_SIZE = 32;
    const HMAC_ALGO = 'sha256';
    
    private $config;
    
    public function __construct($config) {
        $this->config = $config;
    }
    
    /**
     * Prepare the key, To set the key to correct length,
     * using XOR on each chars with prepare_key config
     * @param string $key The key
     * @return string The prepared key
     */
    protected function prepareKey($key){
        $out = '';
        $len = strlen($key);
        
        for($i = 0; $i < $this->config->key_length; ++$i){
            $c1 = ord($this->config->prepare_key{$i});
            $c2 = $len > $i ? ord($key{$i}) : 0;
            
            $out .= chr($c1 ^ $c2);
        }
        
        return $out;
    }
    
    protected function getIvSize(){
        return mcrypt_get_iv_size($this->config->algo, MCRYPT_MODE_CBC);
    }
    
    /**
     * Create the HMAC checksum
     * Use by encrypt and decrypt
     * @param string $data
     * @param string $key
     * @return string The binary data
     */
    public function hmac($data, $key){
        return hash_hmac(self::HMAC_ALGO, $data, $key, true);
    }

    /**
     * Crypt data
     * @param string $data The data to crypt
     * @param string $key The key
     * @return string The binary crypted data
     */
    public function encrypt($data, $key){
        $key = $this->prepareKey($key);
        $chk = $this->hmac($data, $key);
        $iv = mcrypt_create_iv($this->getIvSize(), MCRYPT_DEV_URANDOM);
        
        $len = strlen($data);
        $data = $chk . pack('N', $len) . $data;
        
        $crypt = mcrypt_encrypt($this->config->algo, $key, $data, MCRYPT_MODE_CBC, $iv);
        
        return $iv . $crypt;
    }
    
    /**
     * Decrypt the data and check if key is valid
     * @param string $data The crypted data
     * @param string $key The decrypt key
     * @return string|bool The decrypted data, or false if the data, or the key are invalid (hmac)
     */
    public function decrypt($data, $key){
        $key = $this->prepareKey($key);
        $iv = substr($data, 0, $this->getIvSize());
        $data = substr($data, $this->getIvSize());
        
        $decrypt = mcrypt_decrypt($this->config->algo, $key, $data, MCRYPT_MODE_CBC, $iv);
        
        $chk = substr($decrypt, 0, self::HMAC_SIZE);
        $len = unpack('N', substr($decrypt, self::HMAC_SIZE, 4))['1'];
        $decrypt = substr($decrypt, self::HMAC_SIZE + 4, $len);
        
        if($chk !== $this->hmac($decrypt, $key))
            return false;
        
        return $decrypt;
    }
    
    public static function createInstance(Loader $loader) {
        return new static(
            $loader->load(Config::class)->crypto
        );
    }
}
