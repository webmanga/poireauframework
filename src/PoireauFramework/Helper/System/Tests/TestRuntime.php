<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\System\Tests;

use PoireauFramework\Helper\System\Runtime;

/**
 * Runtime for tests
 *
 * @author vincent
 */
class TestRuntime extends Runtime {
    /**
     * @var TestInput 
     */
    private $input;
    
    /**
     * @var TestOutput
     */
    private $output;

    /**
     * @var bool
     */
    private $isCLI = true;

    public function __construct() {
        $this->input = new TestInput();
        $this->output = new TestOutput();
    }

    /**
     * {@inheritdoc}
     */
    public function input() {
        return $this->input;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isCLI() {
        return $this->isCLI;
    }
    
    /**
     * @param bool $isCLI
     * 
     * @return $this
     */
    public function setIsCLI($isCLI) {
        $this->isCLI = $isCLI;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function output() {
        return $this->output;
    }
    
    /**
     * Configure the Runtime instance to use TestRuntime
     */
    static public function configure() {
        self::reset();
        self::getInstance();
    }
}
