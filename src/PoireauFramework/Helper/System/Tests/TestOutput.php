<?php

namespace PoireauFramework\Helper\System\Tests;

use PoireauFramework\Helper\System\OutputInterface;

/**
 * Description of TestOutput
 *
 * @author vincent
 */
class TestOutput implements OutputInterface {
    public $cookies = [];
    public $headers = [];
    public $responseCode;
    
    public function cookie($name, $value = null, $expire = 0, $path = null, $domain = null, $secure = true, $httponly = true) {
        $this->cookies[] = func_get_args();
        
        return $this;
    }

    public function header($header, $replace = true, $httpCode = null) {
        $this->headers[] = func_get_args();
        
        return $this;
    }

    public function responseCode($code = 200) {
        $this->responseCode = $code;
        
        return $this;
    }
}
