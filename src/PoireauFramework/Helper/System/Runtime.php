<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\System;

/**
 * Runtime helper.
 * Contains all methods related to runtime and system
 *
 * @author vquatrevieux
 */
class Runtime {
    /**
     * @var Runtime
     */
    static private $instance;
    
    /**
     * @var Input
     */
    private $input;

    /**
     * @var Output
     */
    private $output;

    private function __construct() {
        $this->input = new Input($this);
        $this->output = new Output($this);
    }

    /**
     * Check if we are in CLI runtime
     * 
     * @return bool
     */
    public function isCLI() {
        return PHP_SAPI === 'cli';
    }
    
    /**
     * @return InputInterface
     */
    public function input() {
        return $this->input;
    }
    
    /**
     * @return OutputInterface
     */
    public function output() {
        return $this->output;
    }

    /**
     * Get the runtime instance
     * 
     * @return static
     */
    static public function getInstance() {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        
        return self::$instance;
    }
    
    /**
     * Reset the instance
     */
    static protected function reset() {
        self::$instance = null;
    }
}
