<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\System;

/**
 * Output object
 *
 * @author vincent
 */
class Output implements OutputInterface {
    /**
     * @var Runtime
     */
    private $runtime;

    public function __construct(Runtime $runtime) {
        $this->runtime = $runtime;
    }

    /**
     * Send the HTTP response code
     * 
     * @param int $code
     * 
     * @return $this
     */
    public function responseCode($code = 200) {
        if (!$this->runtime->isCLI()) {
            http_response_code($code);
        }
        
        return $this;
    }
    
    /**
     * Send an HTTP header
     * 
     * @param string $header
     * @param bool $replace
     * @param int $httpCode
     * 
     * @return $this
     */
    public function header($header, $replace = true, $httpCode = null) {
        if (!$this->runtime->isCLI()) {
            header($header, $replace, $httpCode);
        }
        
        return $this;
    }
    
    /**
     * Send a cookie
     * 
     * @param string $name
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * 
     * @return $this
     */
    public function cookie($name, $value = null, $expire = 0, $path = null, $domain = null, $secure = true, $httponly = true) {
        if (!$this->runtime->isCLI()) {
            setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
        }
        
        return $this;
    }
}
