<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper;

use \Exception;
use \PoireauFramework\Loader\Creatable;
use \PoireauFramework\Loader\Loader;

/**
 * Helper for system functions (like fork())
 * @author Vincent QUATREVIEUX <quatrevieux.vincent@gmail.com>
 */
class System implements Creatable{
    /**
     * @var Loader
     */
    private $loader;
    
    public function __construct(Loader $loader){
        $this->loader = $loader;
    }

    /**
     * fork() the current process, and reload all classes
     * @see Loader::reloadAll()
     * @see pcntl_fork()
     * @return int The child pid, or 0 in child process
     * @throws Exception When fork() failed (i.e. pcntl_fork() returns -1)
     */
    public function fork(){
        $pid = pcntl_fork();
        
        if($pid === -1)
            throw new Exception('fork() failed : ' . $this->getLastError());
        
        $this->loader->reloadAll();
        
        return $pid;
    }
    
    public function getLastError(){
        return pcntl_strerror(pcntl_get_last_error());
    }
    
    public static function createInstance(Loader $loader){
        return new static($loader);
    }
}
