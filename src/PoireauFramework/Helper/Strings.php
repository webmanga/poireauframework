<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper;

/**
 * String utilities
 * @author Vincent QUATREVIEUX <quatrevieux.vincent@gmail.com>
 */
final class Strings{
    private function __construct(){}
    
    static public function isAlpha($c){
        return ($c >= 'a' && $c <= 'z') || ($c >= 'A' && $c <= 'A');
    }
    
    static public function isNumber($c){
        return $c >= '0' && $c <= '9';
    }
    
    static public function isAlphaNum($c){
        return self::isAlpha($c) || self::isNumber($c);
    }
    
    static public function removeAccents($string){
        $replacements = [
            'a' => ['á', 'à', 'â', 'ä', 'ã', 'å'],
            'e' => ['é', 'è', 'ê', 'ë'],
            'i' => ['í', 'ì', 'î', 'ï'],
            'o' => ['ó', 'ò', 'ô', 'ö', 'õ'],
            'u' => ['ú', 'ù', 'û', 'ü'],
            'y' => ['ý', 'ÿ', 'ŷ'],
            'c' => ['ç'],
            
            'A' => ['Á', 'À', 'Â', 'Ä', 'Ã', 'Å'],
            'E' => ['É', 'È', 'Ê', 'Ë'],
            'I' => ['Í', 'Ï', 'Î', 'Ì'],
            'O' => ['Ó', 'Ò', 'Ô', 'Ö', 'Õ'],
            'U' => ['Ú', 'Ù', 'Û', 'Ü'],
            'Y' => ['Ŷ', 'Ỳ', 'Ÿ']
        ];
        
        foreach($replacements as $replace => $accents){
            $string = str_replace($accents, $replace, $string);
        }
        
        return $string;
    }
    
    static public function startWith($prefix, $str){
        return substr($str, 0, strlen($prefix)) === $prefix;
    }
}
