<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\GoogleApi;

use PoireauFramework\Config;
use PoireauFramework\Helper\GoogleApi\Request\ReCaptchaVerifyRequest;
use PoireauFramework\Helper\GoogleApi\Response\ReCaptchaVerifyResponse;
use PoireauFramework\Http\Form\FieldError;
use PoireauFramework\Http\Form\Form;
use PoireauFramework\Http\Form\FormField;
use PoireauFramework\Http\Form\InputAttributes;
use PoireauFramework\Http\Input;
use PoireauFramework\HttpRequest\Helper\Requests;
use PoireauFramework\HttpRequest\Helper\ResponseException;
use PoireauFramework\Loader\Creatable;
use PoireauFramework\Loader\Loader;

/**
 * Handle recaptcha
 * @author vincent
 */
class ReCaptcha implements Creatable{
    const FIELD_NAME = 'g-recaptcha-response';
    
    /**
     * @var Requests
     */
    private $requests;
    private $config;
    
    /**
     * @var Input
     */
    private $input;
    
    public function __construct(Requests $requests, $config, Input $input) {
        $this->requests = $requests;
        $this->config = $config;
        $this->input = $input;
    }

    public function widget(){
        return <<<HTML
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="g-recaptcha" data-sitekey="{$this->config->public_key}"></div>
HTML;
    }
    
    public function isValid($response = null){
        if($response === null)
            $response = $this->input->{self::FIELD_NAME};
            
        $request = new ReCaptchaVerifyRequest(
            $this->config->secret_key, 
            $response, 
            $this->input->remoteIpAddress()
        );
        
        try{
            $response = $this->requests->send($request);
        }catch(ResponseException $exception){
            throw new GoogleApiException(
                'HTTP error', 
                $exception->getCode(),
                $exception
            );
        }
        
        if(!$response->isSuccess()){
            $codes = $response->getErrorCodes();
            
            if(in_array(ReCaptchaVerifyResponse::ERROR_MISSING_SECRET, $codes)
                || in_array(ReCaptchaVerifyResponse::ERROR_INVALID_SECRET, $codes)){
                throw new GoogleApiException('ReCaptcha : invalid recaptcha.secret_key');
            }
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Add new ReCapcha field to Form
     * @param Form $form
     */
    public function addToForm(Form $form){
        $form->addField(
            $form->createField(self::FIELD_NAME)
                 ->setAttribute(InputAttributes::ATTR_REQUIRED, true)
                 ->addRule(function(FormField $field, Form $form){
                     if(!$this->isValid($field->getValue())){
                         $field->setError(FieldError::ERROR_RULE, 'ReCaptcha : invalid code');
                         return false;
                     }
                     
                     return true;
                 })
        );
    }

    public static function createInstance(Loader $loader) {
        return new static(
            $loader->load(Requests::class),
            $loader->load(Config::class)->recaptcha,
            $loader->load(Input::class)
        );
    }
}
