<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\GoogleApi\Request;

use PoireauFramework\Helper\GoogleApi\Response\ReCaptchaVerifyResponse;
use PoireauFramework\Http\Input;
use PoireauFramework\HttpRequest\Helper\RequestTemplateAdapter;

/**
 * Request for re captcha verify
 * @author vincent
 * @link https://developers.google.com/recaptcha/docs/verify
 */
class ReCaptchaVerifyRequest extends RequestTemplateAdapter{
    private $secret;
    private $response;
    private $remoteIp;
    
    public function __construct($secret, $response, $remoteIp = null) {
        $this->secret = $secret;
        $this->response = $response;
        $this->remoteIp = $remoteIp;
    }
    
    public function body() {
        $body = [
            'secret' => $this->secret,
            'response' => $this->response
        ];
        
        if(!empty($this->remoteIp))
            $body['remoteip'] = $this->remoteIp;
        
        return $body;
    }

    public function createResponseEntity() {
        return new ReCaptchaVerifyResponse();
    }

    public function method() {
        return Input::METHOD_POST;
    }

    public function url() {
        return 'https://www.google.com/recaptcha/api/siteverify';
    }
    
    /**
     * Set the secret key of captcha
     * @param string $secret The secret key
     * @return ReCaptchaVerifyRequest
     */
    public function setSecret($secret) {
        $this->secret = $secret;
        return $this;
    }

    /**
     * Set the use response (g-recaptcha-response form parameter)
     * @param string $response
     * @return ReCaptchaVerifyRequest
     */
    public function setResponse($response) {
        $this->response = $response;
        return $this;
    }

    /**
     * Set the use IP
     * @see Input::removeIpAddress()
     * @param string $remoteIp
     * @return ReCaptchaVerifyRequest
     */
    public function setRemoteIp($remoteIp) {
        $this->remoteIp = $remoteIp;
        return $this;
    }
}
