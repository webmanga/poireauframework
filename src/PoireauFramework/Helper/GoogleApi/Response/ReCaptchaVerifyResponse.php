<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper\GoogleApi\Response;

use PoireauFramework\Helper\GoogleApi\Request\ReCaptchaVerifyRequest;
use PoireauFramework\HttpRequest\Helper\Hydratable;

/**
 * Response for recaptcha verification
 * @see ReCaptchaVerifyRequest
 * @author vincent
 * @link https://developers.google.com/recaptcha/docs/verify
 */
class ReCaptchaVerifyResponse implements Hydratable{
    const ERROR_MISSING_SECRET = 'missing-input-secret';
    const ERROR_INVALID_SECRET = 'invalid-input-secret';
    const ERROR_MISSING_RESPONSE = 'missing-input-response';
    const ERROR_INVALID_RESPONSE = 'invalid-input-response';
    
    private $success;
    private $challengeTimestamp;
    private $hostname;
    private $errorCodes;
    
    public function __construct() {}

    public function hydrate($data) {
        $this->success = $data['success'];
        $this->challengeTimestamp = \DateTime::createFromFormat(\DateTime::ISO8601, $data['challenge_timestamp']);
        $this->hostname = $data['hostname'];
        
        if(!empty($data['error-codes']))
            $this->errorCodes = $data['error-codes'];
    }
    
    /**
     * The captcha code is valid ?
     * @return bool
     */
    public function isSuccess() {
        return $this->success;
    }

    /**
     * The time of challenge
     * @return \DateTime
     */
    public function getChallengeTimestamp() {
        return $this->challengeTimestamp;
    }

    /**
     * Get the registered website host
     * @return string
     */
    public function getHostname() {
        return $this->hostname;
    }

    /**
     * Get the error codes.
     * Array filled by ERROR_* constants
     * You can use in_array() to check the error
     * @return array
     */
    public function getErrorCodes() {
        return $this->errorCodes;
    }
}
