<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Helper;

use PoireauFramework\Config;
use PoireauFramework\Helper\Exception\WidgetNotFoundException;
use PoireauFramework\Loader\Creatable;
use PoireauFramework\Loader\Loader;

/**
 * widgets handler
 * @author Vincent QUATREVIEUX <quatrevieux.vincent@gmail.com>
 */
class Widgets implements Creatable{
    /**
     * @var Loader
     */
    private $loader;
    
    /**
     * @var array
     */
    private $ns;
    
    public function __construct(Loader $loader, Config $config){
        $this->loader = $loader;
        $this->ns = (array) $config->helpers->widgets_ns;
        $this->addNamespace('PoireauFramework\\Helper\\Widget\\');
    }
    
    /**
     * Add a new namespace for widgets
     * @param string $ns
     */
    public function addNamespace($ns){
        $this->ns[] = $ns;
    }
    
    public function __get($name){
        $name = ucfirst($name);
        
        foreach($this->ns as $ns){
            if($ns{strlen($ns) - 1} !== '\\')
                $ns .= '\\';
            
            $class = $ns . $name;
            
            if(class_exists($class))
                return $this->loader->load($class);
        }
        
        throw new WidgetNotFoundException($name);
    }
    
    public function __call($name, $arguments){
        return call_user_func_array($this->{$name}, $arguments);
    }

    public static function createInstance(Loader $loader){
        return new static(
            $loader,
            $loader->load(Config::class)
        );
    }
}
