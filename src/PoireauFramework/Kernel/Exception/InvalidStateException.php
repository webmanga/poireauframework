<?php

namespace PoireauFramework\Kernel\Exception;

/**
 * Exception when kernel is on invalid state
 */
class InvalidStateException extends KernelException {

}