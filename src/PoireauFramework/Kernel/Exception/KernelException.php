<?php

namespace PoireauFramework\Kernel\Exception;

/**
 * Base exception for kernel errors
 */
class KernelException extends \Exception {

}