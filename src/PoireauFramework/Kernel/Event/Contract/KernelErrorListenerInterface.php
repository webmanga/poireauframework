<?php

namespace PoireauFramework\Kernel\Event\Contract;

/**
 * Listener for KernelError event
 * @see \PoireauFramework\Kernel\Event\Type\KernelError
 */
interface KernelErrorListenerInterface
{
    public function onKernelError(\Exception $exception): void;
}