<?php

namespace PoireauFramework\Kernel\Event\Contract;

use PoireauFramework\Kernel\KernelInterface;

/**
 * Listener for BeforeRun event
 * @see \PoireauFramework\Kernel\Event\Type\BeforeRun
 */
interface BeforeRunListenerInterface
{
    public function onBeforeRun(KernelInterface $kernel): void;
}