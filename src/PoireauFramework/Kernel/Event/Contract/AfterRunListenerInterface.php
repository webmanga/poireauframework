<?php

namespace PoireauFramework\Kernel\Event\Contract;

use PoireauFramework\Kernel\KernelInterface;

/**
 * Listener for AfterRun event
 * @see \PoireauFramework\Kernel\Event\Type\AfterRun
 */
interface AfterRunListenerInterface
{
    public function onAfterRun(KernelInterface $kernel): void;
}
