<?php

namespace PoireauFramework\Kernel\Event\Contract;

use PoireauFramework\Kernel\KernelInterface;

/**
 * Listener for KernelLoaded event
 * @see \PoireauFramework\Kernel\Event\Type\KernelLoaded
 */
interface KernelLoadedListenerInterface
{
    public function onKernelLoaded(KernelInterface $kernel): void;
}
