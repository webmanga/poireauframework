<?php

namespace PoireauFramework\Kernel\Event\Type;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\Kernel\Event\Contract\BeforeRunListenerInterface;

/**
 * Event dispatched before run the kernel
 * {@link PoireauFramework\Kernel\Event\Contract\BeforeRunListenerInterface}
 */
class BeforeRun implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\Kernel\\Event\\Type\\BeforeRun";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof BeforeRunListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onBeforeRun($arguments[0]);
    }
}
