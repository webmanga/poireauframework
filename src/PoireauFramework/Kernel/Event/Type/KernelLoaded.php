<?php

namespace PoireauFramework\Kernel\Event\Type;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\Kernel\Event\Contract\KernelLoadedListenerInterface;

/**
 * Event dispatched after the kernel loading
 * {@link PoireauFramework\Kernel\Event\Contract\KernelLoadedListenerInterface}
 */
class KernelLoaded implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\Kernel\\Event\\Type\\KernelLoaded";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof KernelLoadedListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onKernelLoaded($arguments[0]);
    }
}