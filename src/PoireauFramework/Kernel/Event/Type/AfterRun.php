<?php

namespace PoireauFramework\Kernel\Event\Type;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\Kernel\Event\Contract\AfterRunListenerInterface;

/**
 * Event dispatched after run the kernel
 * {@link AfterRunListenerInterface}
 */
class AfterRun implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\Kernel\\Event\\Type\\AfterRun";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof AfterRunListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onAfterRun($arguments[0]);
    }
}
