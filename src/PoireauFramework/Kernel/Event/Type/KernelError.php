<?php

namespace PoireauFramework\Kernel\Event\Type;

use PoireauFramework\Event\Type\EventTypeInterface;
use PoireauFramework\Kernel\Event\Contract\KernelErrorListenerInterface;

/**
 * Event dispatched when an exception is thrown
 * {@link PoireauFramework\Kernel\Event\Contract\KernelErrorListenerInterface}
 */
class KernelError implements EventTypeInterface
{
    /**
     * The event type name
     */
    const NAME = "PoireauFramework\\Kernel\\Event\\Type\\KernelError";

    /**
     * {@inheritdoc}
     */
    public function supports($listener): bool
    {
        return $listener instanceof KernelErrorListenerInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onKernelError($arguments[0]);
    }
}
