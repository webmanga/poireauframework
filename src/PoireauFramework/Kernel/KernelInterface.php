<?php

namespace PoireauFramework\Kernel;

use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Registry\RegistryInterface;
use PoireauFramework\Util\Runnable;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;

/**
 * Interface for kernel
 */
interface KernelInterface extends Runnable
{
    /**
     * Check if the Kernel is loaded
     * @see KernelInterface::load()
     * @return bool
     */
    public function isLoaded(): bool;

    /**
     * Load components
     */
    public function load(): void;

    /**
     * Get the Kernel base injector
     * @return InjectorInterface
     */
    public function injector(): InjectorInterface;

    /**
     * Get the registry
     * @return RegistryInterface
     */
    public function registry(): RegistryInterface;

    /**
     * Get the event dispatcher
     * @return EventDispatcherInterface
     */
    public function events(): EventDispatcherInterface;

    /**
     * Register a module into the Kernel
     * @param object $module Module to register. Can be an InjectorModule, or an EventListenerModule
     */
    public function register($module): void;

    /**
     * Run the Kernel
     * The kernel should be loaded before call this method
     * @see KernelInterface::load()
     * @see KernelInterface::isLoaded()
     */
    public function run(): void;
}
