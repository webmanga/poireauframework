<?php

namespace PoireauFramework\Kernel;

use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Registry\RegistryInterface;
use PoireauFramework\Util\Runnable;
use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Kernel\Event\Type\KernelError;
use PoireauFramework\Kernel\Event\Type\KernelLoaded;
use PoireauFramework\Kernel\Event\Type\BeforeRun;
use PoireauFramework\Kernel\Event\Type\AfterRun;
use PoireauFramework\Kernel\Exception\InvalidStateException;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Abstract kernel class
 * You should implements runner()
 */
abstract class AbstractKernel implements KernelInterface
{
    /**
     * @var InjectorInterface
     */
    private $injector;

    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var EventDispatcherInterface
     */
    private $events;

    /**
     * Does the kernel is loaded ?
     *
     * @var bool
     */
    private $loaded = false;

    /**
     * All bootable modules
     * @var BootableModuleInterface[]
     */
    protected $bootable = [];


    /**
     * Construct the abstract kernel
     *
     * @param InjectorInterface $injector The global injector (should be configurable)
     * @param RegistryInterface $registry The global registry (should not be immutable)
     * @param EventDispatcherInterface $events The global events disptacher
     */
    public function __construct(InjectorInterface $injector, RegistryInterface $registry, EventDispatcherInterface $events)
    {
        $this->injector = $injector;
        $this->registry = $registry;
        $this->events   = $events;

        $this->loadDefaults();
    }

    /**
     * {@inheritdoc}
     */
    public function injector(): InjectorInterface
    {
        return $this->injector;
    }

    /**
     * {@inheritdoc}
     */
    public function registry(): RegistryInterface
    {
        return $this->registry;
    }

    /**
     * {@inheritdoc}
     */
    public function events(): EventDispatcherInterface
    {
        return $this->events;
    }

    /**
     * {@inheritdoc}
     */
    public function isLoaded(): bool
    {
        return $this->loaded;
    }

    /**
     * {@inheritdoc}
     */
    final public function load(): void
    {
        if ($this->loaded) {
            return;
        }

        $this->internalLoad();

        $this->loaded = true;

        $this->events()->dispatch(KernelLoaded::NAME, [$this]);
    }

    /**
     * {@inheritdoc}
     */
    public function register($module): void
    {
        if ($module instanceof InjectorModuleInterface) {
            $this->injector()->register($module);
        }

        if ($module instanceof EventListenerInterface) {
            $this->events()->register($module);
        }

        if ($module instanceof BootableModuleInterface) {
            $this->bootable[] = $module;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run(): void
    {
        if (!$this->isLoaded()) {
            throw new InvalidStateException("You should load() the kernel before run() it");
        }

        $this->events()->dispatch(BeforeRun::NAME, [$this]);

        try {
            $this->runner()->run($this);
        } catch (\Exception $e) {
            $this->events()->dispatch(KernelError::NAME, [$e]);
        }

        $this->events()->dispatch(AfterRun::NAME, [$this]);
    }

    /**
     * Load Kernel default modules
     */
    protected function loadDefaults(): void
    {
        foreach ($this->defaultModules() as $module) {
            $this->register($module);
        }
    }

    /**
     * Get default modules
     *
     * @return object[]
     */
    protected function defaultModules(): array
    {
        return [];
    }

    /**
     * Do internal loading. Called be Kernel::load()
     */
    protected function internalLoad(): void
    {
        foreach ($this->bootable as $module) {
            $module->boot($this);
        }
    }

    /**
     * Get the Runner instance
     * @see KernelInterface::run()
     * @return RunnerInterface The runner
     */
    abstract protected function runner(): RunnerInterface;
}
