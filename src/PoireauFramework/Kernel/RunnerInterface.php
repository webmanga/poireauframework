<?php

namespace PoireauFramework\Kernel;

/**
 * Run a kernel
 */
interface RunnerInterface
{
    /**
     * Run the kernel
     *
     * @param KernelInterface $kernel The kernel instance
     */
    public function run(KernelInterface $kernel): void;
}
