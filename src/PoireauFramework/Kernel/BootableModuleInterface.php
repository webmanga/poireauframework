<?php

namespace PoireauFramework\Kernel;

/**
 * Module for booting kernel (called in Kernel::load() method)
 */
interface BootableModuleInterface
{
    /**
     * Boot the kernel
     * @param KernelInterface $kernel
     */
    public function boot(KernelInterface $kernel): void;
}
