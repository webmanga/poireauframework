<?php

namespace PoireauFramework\Config;

use PoireauFramwork\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Interface for config parsers
 */
interface ConfigParserInterface
{
    /**
     * The supported file extension
     * @return string
     */
    public function extension(): string;

    /**
     * Parse the config file
     *
     * @param RegistryInterface $registry The target
     * @param string $file The configuration file
     *
     * @throws ConfigParseException When an error occurs on parsing
     */
    public function parse(RegistryInterface $registry, string $file): void;
}
