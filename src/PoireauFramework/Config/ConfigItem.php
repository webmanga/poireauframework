<?php

namespace PoireauFramework\Config;

use ArrayAccess;
use IteratorAggregate;
use PoireauFramework\Config\Exception\ConfigException;
use PoireauFramework\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Configuration item
 * Decorate registry
 */
class ConfigItem implements ArrayAccess, IteratorAggregate
{
    /**
     * @var RegistryInterface
     */
    protected $registry;

    /**
     * @var ConfigItem[]
     */
    protected $children = [];


    /**
     * Construct config
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Check if config item has a value
     * @param string $key The config sub key
     * @return bool
     */
    public function has(string $key): bool
    {
        return $this->registry->has($key);
    }

    /**
     * Get a value in the configuration
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key)
    {
        if (!$this->has($key)) {
            return null;
        }

        if (isset($this->children[$key])) {
            return $this->children[$key];
        }

        $value = $this->registry->get($key);

        if ($value instanceof RegistryInterface) {
            $this->children[$key] = new ConfigItem($value);
            return $this->children[$key];
        }

        return $value;
    }

    /**
     * Get a sub-config item
     * @param string $key
     * @return ConfigItem
     */
    public function item(string $key): ConfigItem
    {
        if (!$this->has($key)) {
            return $this->children[$key] = new ConfigItem($this->registry->sub($key));
        }

        if (isset($this->children[$key])) {
            return $this->children[$key];
        }

        $value = $this->registry->get($key);

        if ($value instanceof RegistryInterface) {
            return $this->children[$key] = new ConfigItem($value);
        }

        throw new ConfigException("The config key " . $key . " is already set and it's not a ConfigItem");
    }

    ///===============///
    /// Magic methods ///
    ///===============///

    /**
     * {@inheritdoc}
     */
    public function offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($key): bool
    {
        return $this->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($key): void
    {
        throw new ConfigException("Configuration cannot be changed");
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($key, $value): void
    {
        throw new ConfigException("Configuration cannot be changed");
    }

    /**
     * @see ConfigItem::get()
     */
    public function __get(string $key)
    {
        return $this->get($key);
    }

    /**
     * @see ConfigItem::has()
     */
    public function __isset(string $key): bool
    {
        return $this->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): ConfigItemIterator
    {
        return new ConfigItemIterator($this, $this->registry->keys());
    }

    /**
     * Hide configuration
     */
    public function __debugInfo(): array
    {
        return [];
    }
}
