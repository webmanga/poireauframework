<?php

namespace PoireauFramework\Config;

use Iterator;

/**
 * Iterator over config items
 */
class ConfigItemIterator implements Iterator
{
    /**
     * @var ConfigItem
     */
    private $config;

    /**
     * @var array
     */
    private $keys;

    /**
     * @var int
     */
    private $index = 0;


    /**
     * @param ConfigItem $config
     * @param array $keys
     */
    public function __construct(ConfigItem $config, array $keys)
    {
        $this->config = $config;
        $this->keys = $keys;
    }

    /**
     * {@inheritdoc}
     *
     * @return ConfigItem|mixed
     */
    public function current()
    {
        return $this->config->get($this->key());
    }

    /**
     * {@inheritdoc}
     */
    public function key(): string
    {
        return $this->keys[$this->index];
    }

    /**
     * {@inheritdoc}
     */
    public function next(): void
    {
        $this->index++;
    }

    /**
     * {@inheritdoc}
     */
    public function rewind(): void
    {
        $this->index = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function valid(): bool
    {
        return isset($this->keys[$this->index]);
    }
}
