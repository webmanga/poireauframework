<?php

namespace PoireauFramework\Config;

use Exception;
use PoireauFramework\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\RegistryInterface;
use PoireauFramework\Util\Arrays;

/**
 * Config parser using PHP files
 */
class PhpConfigParser implements ConfigParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function extension(): string
    {
        return "php";
    }

    /**
     * {@inheritdoc}
     */
    public function parse(RegistryInterface $registry, string $file): void
    {
        if (!is_file($file)) {
            throw new ConfigParseException($file, "File not found", __CLASS__);
        }

        try {
            $this->recursSetRegistry($registry, require $file);
        } catch (Exception $e) {
            throw new ConfigParseException($file, "PHP error", __CLASS__, $e);
        }
    }

    protected function recursSetRegistry(RegistryInterface $registry, array $values): void
    {
        foreach ($values as $key => $value) {
            if (is_array($value) && !Arrays::isSequential($value)) {
                $this->recursSetRegistry($registry->sub($key), $value);
            } else {
                $registry->set($key, $value);
            }
        }
    }
}
