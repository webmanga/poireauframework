<?php

namespace PoireauFramework\Config;

use ArrayAccess;
use PoireauFramework\Config\Exception\ConfigException;
use PoireauFramework\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryInterface;
use PoireauFramework\Util\Arrays;

/**
 * Configuration system
 * @todo Handle import other configs
 */
class Config extends ConfigItem
{
    /**
     * Config parsers indexed by file extension
     *
     * @var ConfigParserInterface[]
     */
    protected $parsers;


    /**
     * Construct config
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry);

        $this->initConfig();
    }

    /**
     * Register a new parser into config
     * @param ConfigParserInterface $parser
     */
    public function setParser(ConfigParserInterface $parser): void
    {
        $this->parsers[$parser->extension()] = $parser;
    }

    /**
     * Load a configuration file
     * @throws ConfigParseException When an error occurs on parsing
     */
    public function load(string $file): void
    {
        $ext = substr(strrchr($file, '.'), 1);

        if (!isset($this->parsers[$ext])) {
            throw new ConfigParseException($file, "Parser not found");
        }

        $this->parsers[$ext]->parse($this->registry, $file);
    }

    /**
     * Initialize the configuration
     */
    protected function initConfig(): void
    {
        $config = $this->registry->sub("config");

        if ($config->has("parsers")) {
            foreach ((array) $config->get("parsers") as $parser) {
                $this->setParser($parser);
            }
        }

        if ($config->has("files")) {
            foreach ($config->get("files") as $file) {
                $this->load($file);
            }
        }
    }
}
