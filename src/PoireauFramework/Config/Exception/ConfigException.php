<?php

namespace PoireauFramework\Config\Exception;

use LogicException;

/**
 * Base exception for config
 */
class ConfigException extends LogicException {
    
}