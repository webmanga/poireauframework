<?php

namespace PoireauFramework\Config\Exception;

use Exception;

/**
 * Exception for parsing errors
 */
class ConfigParseException extends ConfigException
{
    /**
     * @var string
     */
    private $configFile;

    /**
     * @var string|null
     */
    private $parser;


    /**
     * @param string $configFile The parsed config file
     * @param string $message The error message
     * @param string $parser The parser class
     * @param Exception $previous Cause exception
     */
    public function __construct(string $configFile, string $message, string $parser = null, Exception $previous = null)
    {
        parent::__construct("Cannot parse file : '" . $configFile . "' : " . $message, 0, $previous);

        $this->configFile = $configFile;
        $this->parser     = $parser;
    }

    /**
     * Get the config file
     * @return string
     */
    public function configFile(): string
    {
        return $this->configFile;
    }

    /**
     * Get the parser class
     * @return string
     */
    public function parser(): ?string
    {
        return $this->parser;
    }
}
