<?php

namespace PoireauFramework\Config;

use PoireauFramework\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Parser for ini configs
 */
class IniConfigParser implements ConfigParserInterface
{
    /**
     * {@inheritdoc}
     */
    public function extension(): string
    {
        return "ini";
    }

    /**
     * {@inheritdoc}
     */
    public function parse(RegistryInterface $registry, string $file): void
    {
        if (!is_file($file)) {
            throw new ConfigParseException($file, "File not found", __CLASS__);
        }

        try {
            $data = parse_ini_file($file, false, INI_SCANNER_TYPED);
        } catch (\Exception $e) {
            throw new ConfigParseException($file, "Invalid ini file", __CLASS__, $e);
        }

        if ($data === false) {
            throw new ConfigParseException($file, "Invalid ini file", __CLASS__);
        }

        foreach ($data as $key => $value) {
            $keys = explode(".", $key);

            $this->setIntoRegistry($registry, $keys, $value);
        }
    }

    protected function setIntoRegistry(RegistryInterface $registry, array $keys, $value): void
    {
        $len = count($keys);

        for ($i = 0; $i < $len - 1; ++$i) {
            $registry = $registry->sub($keys[$i]);
        }

        $registry->set($keys[$len - 1], $value);
    }
}