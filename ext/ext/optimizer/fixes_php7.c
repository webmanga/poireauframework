#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ext.h"

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/object.h"

int fix_php7_instanceof_string(zval* obj, zval* str) {
    return zephir_is_instance_of(obj, Z_STRVAL_P(str), Z_STRLEN_P(str) TSRMLS_CC);
}