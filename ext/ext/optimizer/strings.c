#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ext.h"

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/object.h"

void pf_xor_string(zval* dest, zval* other) {
    unsigned len = Z_STRLEN_P(dest);

    if (len > Z_STRLEN_P(other)) {
        len = Z_STRLEN_P(other);
    }

    char* dest_cstr = Z_STRVAL_P(dest);
    char* other_cstr = Z_STRVAL_P(other);

    for (unsigned i = 0; i < len; ++i) {
        dest_cstr[i] ^= other_cstr[i];
    }
}