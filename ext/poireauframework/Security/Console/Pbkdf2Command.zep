namespace PoireauFramework\Security\Console;

use PoireauFramework\Console\Command\Command;
use PoireauFramework\Console\Command\CommandArgument;
use PoireauFramework\Console\Command\CommandInput;
use PoireauFramework\Console\Command\CommandOptions;

use PoireauFramework\Security\Crypto\Pbkdf2;

/**
 * Command for check PBKDF2
 */
class Pbkdf2Command extends Command {
    /**
     * {@inheritdoc}
     */
    public function name() -> string {
        return "pbkdf2";
    }

    /**
     * {@inheritdoc}
     */
    public function shortDescription() -> string {
        return "Perform PBKDF2 operations";
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(<CommandOptions> options) -> void {
        options
            ->addArgument("operation")
            ->setRequired()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(<CommandInput> input) -> void {
        switch (input->argument("operation")) {
            case "cost":
                this->checkCost(input);
                break;
            case "length":
                this->computeLength(input);
                break;
        }
    }

    protected function checkCost(<CommandInput> input) -> void {
        int cost;
        double start, time;
        var config, hash;

        let config = new \StdClass();
        let hash = new Pbkdf2(config);

        for cost in range(4, 31) {
            let config->cost = cost;

            let start = (double) microtime(true);

            hash->hash("Test String passphrase");

            let time = ((double) microtime(true) - start) * 1000;

            if (time > 250) {
                break;
            }

            if (time > 100) {
                this->output->writeln("cost : " . cost . " - " . round(time) . "ms");
            }
        }
    }

    protected function computeLength(<CommandInput> input) -> void {
        var config, hash;

        let config = new \StdClass();
        let hash = new Pbkdf2(config);

        string result = (string) hash->hash("Test String passphrase");

        this->output->writeln("Length : " . result->length());
    }
}
