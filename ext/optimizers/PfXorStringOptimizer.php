<?php

namespace Zephir\Optimizers\FunctionCall;

use Zephir\Call;
use Zephir\CompilationContext;
use Zephir\CompiledExpression;
use Zephir\CompilerException;
use Zephir\Optimizers\OptimizerAbstract;

/**
 * Implements XOR on strings
 * File : ext/optimizer/strings.c
 */
class PfXorStringOptimizer extends OptimizerAbstract
{
    /**
     * {@inheritdoc}
     */
    public function optimize(array $expression, Call $call, CompilationContext $context)
    {
        if (!isset($expression["parameters"]) || count($expression["parameters"]) !== 2) {
            throw new CompilerException("'pf_xor_string' needs two parameters", $expression);
        }

        $resolvedParams = $call->getResolvedParams($expression['parameters'], $context, $expression);

        return new CompiledExpression("void", "pf_xor_string({$resolvedParams[0]}, {$resolvedParams[1]})", $expression);
    }
}
