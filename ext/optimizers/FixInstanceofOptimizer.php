<?php

namespace Zephir\Optimizers\FunctionCall;

use Zephir\Call;
use Zephir\CompilationContext;
use Zephir\CompiledExpression;
use Zephir\CompilerException;
use Zephir\Optimizers\OptimizerAbstract;

/**
 * FIX for instanceof string in php 7
 * File : ext/optimizer/fixes_php7.c
 */
class FixInstanceofOptimizer extends OptimizerAbstract
{
    /**
     * {@inheritdoc}
     */
    public function optimize(array $expression, Call $call, CompilationContext $context)
    {
        if (!isset($expression["parameters"]) || count($expression["parameters"]) !== 2) {
            throw new CompilerException("'fix_instanceof' needs two parameters", $expression);
        }

        $call->processExpectedReturn($context);

        $symbolVariable = $call->getSymbolVariable();

        if (!$symbolVariable->isBoolean()) {
            throw new CompilerException("'fix_instanceof' returns a boolean", $expression);
        }

        $resolvedParams = $call->getReadOnlyResolvedParams($expression['parameters'], $context, $expression);

        return new CompiledExpression("bool", "fix_php7_instanceof_string({$resolvedParams[0]}, {$resolvedParams[1]})", $expression);
    }
}
