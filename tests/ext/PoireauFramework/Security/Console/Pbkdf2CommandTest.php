<?php

namespace PoireauFramework\Security\Console;


use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Console\Console;
use PoireauFramework\Console\ConsoleModule;
use PoireauFramework\Console\Testing\ConsoleAssertions;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Security
 * @group ext_PoireauFramework_Security_Console
 * @group ext_PoireauFramework_Security_Console_Pbkdf2Command
 */
class Pbkdf2CommandTest extends TestCase
{
    use ConsoleAssertions;

    protected function setUp()
    {
        $app = new Application([]);
        $app->register(new ConsoleModule());

        $this->console = new Console($app);
    }

    /**
     *
     */
    public function test_cost()
    {
        $result = $this->executeCommand(new Pbkdf2Command(), ["cost"]);

        $this->assertGreaterThan(1, count($result));

        foreach ($result as $line) {
            if (empty($line)) {
                continue;
            }

            $this->assertEquals(true, preg_match("#cost : (\\d+) - (\\d+)ms#", $line, $matches));

            $this->assertGreaterThanOrEqual(10, $matches[1]);
            $this->assertLessThanOrEqual(20, $matches[1]);

            $this->assertGreaterThanOrEqual(100, $matches[2]);
            $this->assertLessThanOrEqual(250, $matches[2]);
        }
    }
}
