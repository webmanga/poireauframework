<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Extension;

use PoireauFramework\App;
use PoireauFramework\Kernel\PoireauFramework;
use PoireauFramework\Loader\Loader;

/**
 * Description of AppTestCase
 *
 * @author vquatrevieux
 */
trait AppTestCase {
    /**
     * @var App
     */
    private $app;
    
    /**
     * @return App
     */
    public function getApp()
    {
        if ($this->app) {
            return $this->app;
        }
        
        return $this->app = new App(__DIR__, [
            'config_files' => [
                __DIR__ . '/../PoireauFramework/_files/config_app.ini'
            ]
        ]);
    }
    
    /**
     * @return App
     */
    public function getAndLoad()
    {
        $app = $this->getApp();
        
        if ($app->getFramework() === null) {
            $app->load();
        }
        
        return $app;
    }

    /**
     * @return Loader
     */
    public function getLoader()
    {
        return $this->getAndLoad()->getLoader();
    }
    
    /**
     * @return PoireauFramework
     */
    public function getFramework()
    {
        return $this->getAndLoad()->getFramework();
    }
}
