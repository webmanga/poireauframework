<?php

namespace PoireauFramework\App;

require_once __DIR__."/_files/Controller/TestController.php";

use PoireauFramework\App\Debug\ErrorHandlerModule;
use PoireauFramework\App\Template\AppTemplateModule;
use PoireauFramework\App\Testing\Extension\ApplicationConfiguration;
use PoireauFramework\App\Testing\Extension\ResponseAssertion;
use PoireauFramework\App\Testing\TestingAppModule;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Testing\Bench;
use PoireauFramework\Testing\BenchTestCase;

/**
 * @group bench
 * @group bench_PoireauFramework
 * @group bench_PoireauFramework_App
 * @group bench_PoireauFramework_App_Application
 */
class ApplicationBenchTest extends BenchTestCase
{
    use ApplicationConfiguration;
    use ResponseAssertion;

    protected function configure(Bench $bench)
    {
        $bench->iterations(5000);
    }

    /**
     * @return array
     */
    public function data_hello_world()
    {
        return ['GET', '/test/hello'];
    }

    /**
     * @return array
     */
    public function data_hello_with_param()
    {
        return ['GET', '/test/hello/John'];
    }

    public function bench_Application($method, $path)
    {
        $this->app = $this->createApplication([
            "config" => [
                "config"  => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/_files/functional_config.ini"]
                ]
            ],

            "error_handler" => [
                "show_errors" => true
            ],

            "template" => [
                "paths" => [__DIR__ . "/_files/templates/"]
            ]
        ]);

        $this->configureApplication($this->app, [
            new ErrorHandlerModule(),
            new TestingAppModule(),
            new AppTemplateModule(),
        ]);

        $this->call($method, $path);
    }
}