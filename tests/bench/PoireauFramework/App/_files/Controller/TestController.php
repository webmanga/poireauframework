<?php

namespace PoireauFramework\App\Controller\Bench;

use PoireauFramework\App\Controller\Controller;

/**
 * Class TestController
 */
class TestController extends Controller
{
    static public $method;
    static public $args;

    public function indexAction(...$args)
    {
        self::$method = __METHOD__;
        self::$args   = $args;

        return [
            "foo" => "bar"
        ];
    }

    public function errorAction()
    {
        throw new \Exception(__METHOD__);
    }

    public function helloAction($name = 'World')
    {
        return "Hello $name !";
    }
}