<?php

namespace PoireauFramework\Security\Crypto;

use PoireauFramework\Testing\Bench;
use PoireauFramework\Testing\BenchTestCase;

/**
 * @group bench
 * @group bench_PoireauFramework
 * @group bench_PoireauFramework_Security
 * @group bench_PoireauFramework_Security_Crypto
 * @group bench_PoireauFramework_Security_Crypto_SignerBench
 */
class SignerBenchTest extends BenchTestCase
{
    private $signed = [];

    protected function configure(Bench $bench)
    {
        $bench->iterations(1000);
    }

    public function data_sign()
    {
        return ["sign", "Hello World !"];
    }

    public function data_unsign()
    {
        return ["unsign"];
    }

    /**
     *
     */
    public function bench_OpenSslSigner($type, $input = null)
    {
        $config = new \stdClass();
        $config->private_key_file = __DIR__."/_files/private.pem";
        $config->public_key_file = __DIR__."/_files/public.pem";

        $signer = new OpenSslSigner($config);

        if ($type === "sign") {
            return $this->signed["openssl"] = $signer->sign($input);
        } else {
            return $signer->unsign($this->signed["openssl"]);
        }
    }

    /**
     *
     */
    public function bench_CypherSigner($type, $input = null)
    {
        $signer = new CypherSigner(new Cypher(Cypher::AES256_CTR, "my very secret key"));

        if ($type === "sign") {
            return $this->signed["cypher"] = $signer->sign($input);
        } else {
            return $signer->unsign($this->signed["cypher"]);
        }
    }
}
