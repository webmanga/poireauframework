<?php

namespace PoireauFramework\Database\Mapping;

require_once __DIR__."/_files/User.php";
require_once __DIR__."/_files/UserMapper.php";

use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Bench\User;
use PoireauFramework\Database\Mapping\Bench\UserMapper;
use PoireauFramework\Database\Mapping\Helper\CriteriaTable;
use PoireauFramework\Database\Mapping\Helper\SqlCriteriaCompiler;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;
use PoireauFramework\Testing\Bench;
use PoireauFramework\Testing\BenchTestCase;

/**
 * @group bench
 * @group bench_PoireauFramework
 * @group bench_PoireauFramework_Database
 * @group bench_PoireauFramework_Database_Mapping_Query
 */
class MapperQueryBenchTest extends BenchTestCase
{
    /**
     * @var SqlConnectionInterface
     */
    private $connection;

    /**
     * @var UserMapper
     */
    private $mapper;

    protected function setUp()
    {
        parent::setUp();

        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "type"   => "sqlite",
                    "memory" => true
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        /** @var DatabaseHandler $handler */
        $handler = $injector->get(DatabaseHandler::class);

        $this->connection = $handler->getConnection("test");
        $this->mapper = new UserMapper(
            $this->connection,
            $metadata = new Metadata(),
            new MetadataHydrator($metadata, $this->connection->platform()->types())
        );

        $this->mapper->schema()->create();

        $user = new User();

        $user
            ->setId(123)
            ->setName("John")
            ->setEmail("john.doe@webmanga.fr")
            ->setPassword("MyPassword")
            ->setUsername("john_doe")
        ;

        $this->mapper->insert($user);

        //precompile queries
        $this->mapper->get_query(0);
        $this->mapper->findForLogin_query("", "");
        $this->mapper->complex_query("", "");
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->mapper->schema()->drop();
    }

    protected function configure(Bench $bench)
    {
        $bench->iterations(10000);
    }

    public function data_find_for_login()
    {
        return ["findForLogin", ["john_doe", "MyPassword"]];
    }

    public function data_complex()
    {
        return ["complex", ["john", "abcd"]];
    }

    public function data_get()
    {
        return ["get", [123]];
    }

    /**
     *
     */
    public function bench_criteria($method, $arguments)
    {
        return $this->mapper->{$method."_criteria"}(...$arguments);
    }

    /**
     *
     */
    public function bench_plain_sql($method, $arguments)
    {
        return $this->mapper->{$method."_sql"}(...$arguments);
    }

    /**
     *
     */
    public function bench_query_cache($method, $arguments)
    {
        return $this->mapper->{$method."_query"}(...$arguments);
    }

    /**
     *
     */
    public function bench_query_rebuild($method, $arguments)
    {
        return $this->mapper->{$method."_query_rebuild"}(...$arguments);
    }
}
