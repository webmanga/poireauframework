<?php

namespace PoireauFramework\Database\Mapping\Bench;

use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\SqlMapper;

class UserMapper extends SqlMapper
{
    /**
     * {@inheritdoc}
     */
    protected function configure(Metadata $metadata): void
    {
        $metadata->entity = User::class;
        $metadata->schema = "USER";
    }

    /**
     * {@inheritdoc}
     */
    protected function buildFields(FieldsBuilder $builder): void
    {
        $builder
            ->integer("id")->attribute("USER_ID")->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
        ;
    }

    public function insert(User $user)
    {
        $this->db->query(
            $this->helper->insert(),
            $this->hydrator->toDatabase($user)
        );
    }

    /**
     * @return User[]
     */
    public function all()
    {
        $entities = [];

        foreach ($this->db->query("SELECT * FROM {$this->helper->table()}")->all() as $data) {
            $entities[] = $this->hydrator->fromDatabase($data);
        }

        return $entities;
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function get_sql($id)
    {
        $result = $this->db->query("SELECT * FROM {$this->helper->table()} WHERE {$this->helper->attr("id")} = ?", [$id])->fetch();

        if ($result === false) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function get_criteria($id)
    {
        $criteria = $this->criteria(["id" => $id]);

        $result = $this->db->query("SELECT * FROM {$this->helper->table()} WHERE {$criteria->query()}", $criteria->bindings($this->db->platform()))->fetch();

        if ($result === false) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function get_query($id)
    {
        $query = $this->query()->select("get", ["id" => new Binding()]);

        return $this->connection
            ->prepare($query)
            ->bind(["id" => $id])
            ->execute()
            ->fetch()
        ;
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function get_query_rebuild($id)
    {
        $query = $this->query()->select(["id" => $id]);

        return $this->connection->execute($query)->fetch();
    }

    public function findForLogin_query($username, $password)
    {
        $query = $this->query()->select("login", [
            "username" => new Binding(),
            "password" => new Binding()
        ]);

        return $this->connection
            ->prepare($query)
            ->bind(["username" => $username, "password" => $password])
            ->execute()
            ->fetch()
        ;
    }

    public function findForLogin_query_rebuild($username, $password)
    {
        $query = $this->query()->select([
            "username" => $username,
            "password" => $password
        ]);

        return $this->connection->execute($query)->fetch();
    }

    public function findForLogin_criteria($username, $password)
    {
        $criteria = $this->criteria(["username" => $username, "password" => $password]);

        $query = "SELECT * FROM {$this->helper->table()} WHERE {$criteria->query()}";

        $stmt = $this->db->prepare($query);
        $stmt->execute($criteria->bindings($this->db->platform()));

        $result = $stmt->fetch();

        if (!$result) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    public function findForLogin_sql($username, $password)
    {
        $query = "SELECT * FROM {$this->helper->table()} WHERE {$this->attr("username")} = ? AND {$this->attr("password")} = ?";

        $stmt = $this->db->prepare($query);
        $stmt->execute([$username, $password]);

        $result = $stmt->fetch();

        if (!$result) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    public function complex_query($username, $password)
    {
        $query = $this->query()->select("complex", [
            ":or" => [
                "username" => new Binding(),
                ["name",  "LIKE", new Binding("query")],
                ["email", "LIKE", new Binding("query")],
            ],
            ["password", "!=", new Binding()]
        ]);

        return $this->connection->prepare($query)->bind([
            "username" => $username,
            "password" => $password,
            "query"    => "%$username%"
        ])->execute()->fetch();
    }

    public function complex_query_rebuild($username, $password)
    {
        $query = $this->query()->select([
            ":or" => [
                "username" => $username,
                ["name",  "LIKE", "%$username%"],
                ["email", "LIKE", "%$username%"],
            ],
            ["password", "!=", $password]
        ]);

        return $this->connection->execute($query)->fetch();
    }

    public function complex_criteria($username, $password)
    {
        $criteria = $this->criteria([
            ":or" => [
                "username" => $username,
                ["name",  "LIKE", "%$username%"],
                ["email", "LIKE", "%$username%"],
            ],
            ["password", "!=", $password]
        ]);

        $query = "SELECT * FROM {$this->helper->table()} WHERE {$criteria->query()}";

        $stmt = $this->db->prepare($query);
        $stmt->execute($criteria->bindings($this->db->platform()));

        $result = $stmt->fetch();

        if (!$result) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    public function complex_sql($username, $password)
    {
        $query = "SELECT * FROM {$this->helper->table()} WHERE ({$this->attr("username")} = ? OR {$this->attr("name")} LIKE ? OR {$this->attr("email")} LIKE ?) AND {$this->attr("password")} != ?";

        $stmt = $this->db->prepare($query);
        $stmt->execute([$username, "%$username%", "%$username%", $password]);

        $result = $stmt->fetch();

        if (!$result) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    private function criteria($array)
    {
        $criteria = new Criteria($this->metadata, $array);
        return $criteria->compile($this->db->platform()->compilers()->criteria());
    }
}
