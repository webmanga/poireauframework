<?php

namespace PoireauFramework\Database\Mapping;

require_once __DIR__."/_files/User.php";
require_once __DIR__."/_files/UserMapper.php";

use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Bench\UserMapper;
use PoireauFramework\Database\Mapping\Helper\CriteriaTable;
use PoireauFramework\Database\Mapping\Helper\SqlCriteriaCompiler;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;
use PoireauFramework\Testing\Bench;
use PoireauFramework\Testing\BenchTestCase;

/**
 * @group bench
 * @group bench_PoireauFramework
 * @group bench_PoireauFramework_Database
 * @group bench_PoireauFramework_Database_Mapping_Schema
 */
class MapperSchemaBenchTest extends BenchTestCase
{
    /**
     * @var SqlConnectionInterface
     */
    private $connection;

    /**
     * @var UserMapper
     */
    private $mapper;

    protected function setUp()
    {
        parent::setUp();

        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "type"   => "sqlite",
                    "memory" => true
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        /** @var DatabaseHandler $handler */
        $handler = $injector->get(DatabaseHandler::class);

        $this->connection = $handler->getConnection("test");
        $this->mapper = new UserMapper(
            $this->connection,
            $metadata = new Metadata(),
            new MetadataHydrator($metadata, $this->connection->platform()->types())
        );
    }

    protected function configure(Bench $bench)
    {
        $bench->iterations(5000);
    }

    /**
     *
     */
    public function bench_schema_with_mapper()
    {
        $schema = $this->mapper->schema();

        $schema->create();
        $schema->drop();
    }

    /**
     *
     */
    public function bench_schema_with_mapper_rebuild_metadata()
    {
        $mapper = new UserMapper(
            $this->connection,
            $metadata = new Metadata(),
            new MetadataHydrator($metadata, $this->connection->platform()->types())
        );

        $schema = $mapper->schema();

        $schema->create();
        $schema->drop();
    }

    /**
     *
     */
    public function bench_plain_sql()
    {
        $this->connection->query(<<<SQL
CREATE TABLE `USER` (
  `USER_ID` INTEGER PRIMARY KEY,
  `USER_LOGIN` VARCHAR,
  `USER_NAME` VARCHAR,
  `USER_PASSWORD` VARCHAR,
  `USER_MAIL` VARCHAR
)
SQL
        );

        $this->connection->query("DROP TABLE `USER`");
    }
}
