<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Compiler\CriteriaCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlCriteriaCompiler;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;
use PoireauFramework\Testing\Bench;
use PoireauFramework\Testing\BenchTestCase;

/**
 * @group bench
 * @group bench_PoireauFramework
 * @group bench_PoireauFramework_Database
 * @group bench_PoireauFramework_Database_Mapping
 * @group bench_PoireauFramework_Database_Mapping_Query
 * @group bench_PoireauFramework_Database_Mapping_Query_Bag
 * @group bench_PoireauFramework_Database_Mapping_Query_Bag_Criteria
 */
class CriteriaBenchTest extends BenchTestCase
{
    /**
     * @var CriteriaCompilerInterface
     */
    private $compiler;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var string[]
     */
    private $cache = [];


    /**
     *
     */
    protected function setUp()
    {
        parent::setUp();

        $this->platform = new SQLitePlatform(new TypeRegistry());
        $this->compiler = new SqlCriteriaCompiler($this->platform);

        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;
    }

    protected function configure(Bench $bench)
    {
        $bench->iterations(50000);
    }

    /**
     *
     */
    public function data_simple()
    {
        return [
            "simple",
            ["name" => "John"]
        ];
    }

    /**
     *
     */
    public function data_multiple()
    {
        return [
            "multiple",
            [
                "username" => "john_doe",
                "password" => "MyPassword"
            ]
        ];
    }

    /**
     * @return array
     */
    public function data_complex()
    {
        return [
            "complex",
            [
                'name' => 'value',
                ['username', '!=', 'value'],
                ':or' => [
                    'email' => 'john.doe@webmanga.fr',
                    ['email', "like", '%john_doe'],
                ],
                ':query' => [
                    'EXISTS(SELECT * FROM USERS)',
                    '1 IN (SELECT USER_ID FROM USER WHERE USER_NAME = ?)' => ['name']
                ]
            ]
        ];
    }

    /**
     *
     */
    public function data_bindings()
    {
        return [
            "multiple",
            [
                "username" => new Binding(),
                "password" => new Binding()
            ],
            [
                "username" => "john_doe",
                "password" => "MyPassword"
            ]
        ];
    }

    /**
     *
     */
    public function bench_criteria_rebuild($id, $param, $bindings = null)
    {
        $criteria = new Criteria($this->metadata, $param);
        $criteria->compile($this->compiler);

        if ($bindings) {
            $criteria->bind($bindings);
        }

        return "SELECT * FROM USER WHERE {$criteria->query()}";
    }

    /**
     *
     */
    public function bench_criteria_serialized($id, $param, $bindings = null)
    {
        if (isset($this->cache[$id])) {
            $criteria = unserialize($this->cache[$id]);
        } else {
            $criteria = new Criteria($this->metadata, $param);
            $criteria->compile($this->compiler);
            $this->cache[$id] = serialize($criteria);
        }

        if ($bindings) {
            $criteria->bind($bindings);
        }

        return "SELECT * FROM USER WHERE {$criteria->query()}";
    }

    /**
     *
     */
    public function bench_criteria_memory($id, $param, $bindings = null)
    {
        if (isset($this->cache[$id."mem"])) {
            $criteria = $this->cache[$id."mem"];
        } else {
            $criteria = new Criteria($this->metadata, $param);
            $criteria->compile($this->compiler);
            $this->cache[$id."mem"] = $criteria;
        }

        if ($bindings) {
            $criteria->bind($bindings);
        }

        return "SELECT * FROM USER WHERE {$criteria->query()}";
    }

    /**
     * @param string $id
     * @param string $param
     */
    public function bench_manual_sql($id, $param)
    {
        switch ($id) {
            case "simple":
                $query = "SELECT * FROM USER WHERE `{$this->metadata->fields["name"]->attribute}` = ?";
                $params = array_values($param);
                return $query;
            case "multiple":
                $query = "SELECT * FROM USER WHERE `{$this->metadata->fields["name"]->attribute}` = ? AND `{$this->metadata->fields["password"]->attribute}` = ?";
                $params = array_values($param);
                return $query;
            case "complex":
                $query = "SELECT * FROM USER WHERE `{$this->metadata->fields["name"]->attribute}` = ? AND `{$this->metadata->fields["username"]->attribute}` != ? AND (`{$this->metadata->fields["email"]->attribute}` = ? OR `{$this->metadata->fields["email"]->attribute}` like ?) AND EXISTS(SELECT * FROM USERS) AND 1 IN (SELECT USER_ID FROM USER WHERE USER_NAME = ?)";
                $params = [
                    $param["name"],
                    $param[0][2],
                    $param[':or']['email'],
                    $param[':or'][0][2],
                    $param[':query']['1 IN (SELECT USER_ID FROM USER WHERE USER_NAME = ?)'][0]];
                return $query;
        }
    }
}
