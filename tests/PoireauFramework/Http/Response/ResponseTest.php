<?php

namespace PoireauFramework\Http\Response;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ResponseHeadersBag;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Response
 * @group ext_PoireauFramework_Http_Response_Response
 */
class ResponseTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $response = new Response();

        $this->assertEquals(200, $response->getCode());
        $this->assertInstanceOf(ResponseHeadersBag::class, $response->headers());
        $this->assertNull($response->getBody());
    }
}
