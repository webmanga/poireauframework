<?php

namespace PoireauFramework\Http\Response;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Bag\TextBag;

/**
 * Class ResponseTransformerTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Response
 * @group ext_PoireauFramework_Http_Response_ResponseTransformer
 */
class ResponseTransformerTest extends TestCase
{
    /**
     * @var ResponseTransformer
     */
    private $transformer;

    public function setUp()
    {
        $this->transformer = new ResponseTransformer();
    }

    /**
     *
     */
    public function test_transform_null()
    {
        $response = new Response();

        $this->assertSame($response, $this->transformer->transform($response, null));
    }

    /**
     *
     */
    public function test_transform_response()
    {
        $response = new Response();
        $value    = new Response();

        $this->assertSame($value, $this->transformer->transform($response, $value));
    }

    /**
     *
     */
    public function test_transform_bag()
    {
        $response = new Response();
        $value    = new ArrayBag();

        $current = $this->transformer->transform($response, $value);
        $this->assertSame($response, $current);

        $this->assertSame($value, $current->getBody());
    }

    /**
     *
     */
    public function test_transform_array()
    {
        $response = new Response();
        $value    = ["foo" => "bar"];

        $current = $this->transformer->transform($response, $value);
        $this->assertSame($response, $current);

        $this->assertInstanceOf(ArrayBag::class, $current->getBody());
        $this->assertSame($value, $current->getBody()->raw());
    }

    /**
     *
     */
    public function test_transform_string()
    {
        $response = new Response();
        $value    = "Hello World !";

        $current = $this->transformer->transform($response, $value);
        $this->assertSame($response, $current);

        $this->assertInstanceOf(TextBag::class, $current->getBody());
        $this->assertSame($value, $current->getBody()->raw());
    }
}