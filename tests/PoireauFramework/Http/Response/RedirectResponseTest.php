<?php

namespace PoireauFramework\Http\Response;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Response
 * @group ext_PoireauFramework_Http_Response_RedirectResponse
 */
class RedirectResponseTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $response = new RedirectResponse("http://example.com");

        $this->assertEquals("http://example.com", $response->headers()->get("location"));
        $this->assertEquals(Response::CODE_MOVED_TMP, $response->getCode());
    }

    /**
     *
     */
    public function test_code()
    {
        $response = new RedirectResponse("http://example.com", Response::CODE_MOVED_PERM);

        $this->assertEquals("http://example.com", $response->headers()->get("location"));
        $this->assertEquals(Response::CODE_MOVED_PERM, $response->getCode());
    }
}