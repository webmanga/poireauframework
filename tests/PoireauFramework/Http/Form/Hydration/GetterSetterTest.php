<?php

namespace PoireauFramework\Http\Form\Hydration;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Hydration
 * @group ext_PoireauFramework_Http_Form_Hydration_GetterSetter
 */
class GetterSetterTest extends TestCase
{
    /**
     * @var Field
     */
    private $field;

    /**
     * @var MyTargetClassTest
     */
    private $target;


    /**
     *
     */
    protected function setUp()
    {
        $this->field = new Field("foo");
        $this->target = new MyTargetClassTest();
    }

    /**
     *
     */
    public function test_hydrate_default()
    {
        $this->field->setValue("BAR");

        $hydrator = new GetterSetter();
        $hydrator->hydrate($this->target, $this->field);

        $this->assertEquals("BAR", $this->target->foo());
    }

    /**
     *
     */
    public function test_hydrate_custom_prefix()
    {
        $this->field->setValue("BAR");

        $hydrator = new GetterSetter(null, null, "_");
        $hydrator->hydrate($this->target, $this->field);

        $this->assertEquals("BAR", $this->target->otherFoo);
    }

    /**
     *
     */
    public function test_hydrate_custom_property()
    {
        $this->field->setValue("BAR");

        $hydrator = new GetterSetter("other");
        $hydrator->hydrate($this->target, $this->field);

        $this->assertEquals("BAR", $this->target->other());
    }

    /**
     *
     */
    public function test_extract_default()
    {
        $this->target->setFoo("__bar__");

        $hydrator = new GetterSetter();
        $hydrator->extract($this->target, $this->field);

        $this->assertEquals("__bar__", $this->field->value());
    }

    /**
     *
     */
    public function test_extract_custom_prefix()
    {
        $this->target->setFoo("__bar__");

        $hydrator = new GetterSetter(null, "get");
        $hydrator->extract($this->target, $this->field);

        $this->assertEquals("__bar__", $this->field->value());
    }

    /**
     *
     */
    public function test_extract_custom_property()
    {
        $this->target->setOther("__bar__");

        $hydrator = new GetterSetter("other");
        $hydrator->extract($this->target, $this->field);

        $this->assertEquals("__bar__", $this->field->value());
    }
}

class MyTargetClassTest
{
    private $foo;
    private $other;

    public $otherFoo;

    /**
     * @return mixed
     */
    public function foo()
    {
        return $this->foo;
    }

    /**
     * @return mixed
     */
    public function getFoo()
    {
        return $this->foo;
    }

    /**
     * @param mixed $foo
     *
     * @return $this
     */
    public function setFoo($foo)
    {
        $this->foo = $foo;

        return $this;
    }

    /**
     * @param mixed $foo
     *
     * @return $this
     */
    public function _Foo($foo)
    {
        $this->otherFoo = $foo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function other()
    {
        return $this->other;
    }

    /**
     * @param mixed $other
     *
     * @return $this
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }
}
