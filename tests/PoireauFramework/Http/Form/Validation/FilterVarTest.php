<?php

namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\FormInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_FilterVar
 */
class FilterVarTest extends TestCase
{
    /**
     * @var ValidationContext
     */
    private $context;


    /**
     *
     */
    protected function setUp()
    {
        $this->context = new ValidationContext($this->createMock(FormInterface::class));
        $this->context->setCurrent(new Field("foo"));
    }

    /**
     *
     */
    public function test_parameters()
    {
        $filterVar = new FilterVar(FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED);

        $this->assertEquals(FILTER_VALIDATE_URL, $filterVar->getFilter());
        $this->assertEquals(FILTER_FLAG_HOST_REQUIRED, $filterVar->getOptions());
        $this->assertEquals("The value is not valid", $filterVar->getMessage());
        $this->assertEquals([
            "filter"  => FILTER_VALIDATE_URL,
            "options" => FILTER_FLAG_HOST_REQUIRED
        ], $filterVar->getParameters());
    }

    /**
     *
     */
    public function test_success()
    {
        $filterVar = new FilterVar(FILTER_VALIDATE_EMAIL);

        $this->assertTrue($filterVar->validate("test@webmanga.fr", $this->context));
        $this->assertFalse($this->context->hasErrors());
    }

    /**
     *
     */
    public function test_error_simple()
    {
        $filterVar = new FilterVar(FILTER_VALIDATE_EMAIL);

        $this->assertFalse($filterVar->validate("no an email", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(FilterVar::class, "The value is not valid", ["filter" => FILTER_VALIDATE_EMAIL, "options" => null]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_error_custom()
    {
        $filterVar = new FilterVar(FILTER_VALIDATE_EMAIL, null, "custom message");

        $this->assertFalse($filterVar->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(FilterVar::class, "custom message", ["filter" => FILTER_VALIDATE_EMAIL, "options" => null]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_error_not_string()
    {
        $filterVar = new FilterVar(FILTER_VALIDATE_EMAIL);

        $this->assertFalse($filterVar->validate([], $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(FilterVar::class, "The value is not valid", ["filter" => FILTER_VALIDATE_EMAIL, "options" => null]),
            $this->context->getError($this->context->current())
        );
    }
}
