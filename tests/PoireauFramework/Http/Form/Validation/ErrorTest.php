<?php

namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_Error
 */
class ErrorTest extends TestCase
{
    /**
     *
     */
    public function test_rule_default_message()
    {
        $rule = new Regex("pattern", "my rule message");

        $error = Error::rule($rule);

        $this->assertInstanceOf(Error::class, $error);
        $this->assertEquals(Regex::class, $error->type());
        $this->assertEquals("my rule message", $error->message());
        $this->assertEquals($rule->getParameters(), $error->parameters());
    }

    /**
     *
     */
    public function test_rule_with_message()
    {
        $rule = new Length(1, 2);

        $error = Error::rule($rule, "My custom message");

        $this->assertInstanceOf(Error::class, $error);
        $this->assertEquals(Length::class, $error->type());
        $this->assertEquals("My custom message", $error->message());
        $this->assertEquals($rule->getParameters(), $error->parameters());
    }

    /**
     *
     */
    public function test_required_default_message()
    {
        $error = Error::required();

        $this->assertInstanceOf(Error::class, $error);
        $this->assertEquals(Error::TYPE_REQUIRED, $error->type());
        $this->assertEquals("The value is required", $error->message());
        $this->assertEquals([], $error->parameters());
    }

    /**
     *
     */
    public function test_required_with_message()
    {
        $error = Error::required("My custom message");

        $this->assertInstanceOf(Error::class, $error);
        $this->assertEquals(Error::TYPE_REQUIRED, $error->type());
        $this->assertEquals("My custom message", $error->message());
        $this->assertEquals([], $error->parameters());
    }

    /**
     *
     */
    public function test_undefined_default_message()
    {
        $error = Error::undefined();

        $this->assertInstanceOf(Error::class, $error);
        $this->assertEquals(Error::TYPE_UNDEFINED, $error->type());
        $this->assertEquals("Invalid value", $error->message());
        $this->assertEquals([], $error->parameters());
    }

    /**
     *
     */
    public function test_undefined_with_message()
    {
        $error = Error::undefined("My custom message");

        $this->assertInstanceOf(Error::class, $error);
        $this->assertEquals(Error::TYPE_UNDEFINED, $error->type());
        $this->assertEquals("My custom message", $error->message());
        $this->assertEquals([], $error->parameters());
    }

    /**
     *
     */
    public function test___toString()
    {
        $error = new Error("", "My {p1} message with some {p2}", [
            "p1" => "super",
            "p2" => "parameters"
        ]);

        $this->assertEquals("My super message with some parameters", (string) $error);
    }
}
