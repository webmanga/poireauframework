<?php


namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\FormElementInterface;
use PoireauFramework\Http\Form\FormInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_ValidationContext
 */
class ValidationContextTest extends TestCase
{
    /**
     *
     */
    public function test_form()
    {
        $form = $this->createMock(FormInterface::class);
        $context = new ValidationContext($form);

        $this->assertSame($form, $context->form());
    }

    /**
     *
     */
    public function test_get_set_current()
    {
        $context = new ValidationContext($this->createMock(FormInterface::class));

        $e = $this->createMock(FormElementInterface::class);

        $context->setCurrent($e);

        $this->assertSame($e, $context->current());
    }

    /**
     *
     */
    public function test_hasErrors()
    {
        $context = new ValidationContext($this->createMock(FormInterface::class));
        $e = new Field('foo');
        $context->setCurrent($e);

        $this->assertFalse($context->hasErrors());

        $context->setError('error');

        $this->assertTrue($context->hasErrors());
    }

    /**
     *
     */
    public function test_getError()
    {
        $context = new ValidationContext($this->createMock(FormInterface::class));
        $e = new Field('foo');
        $context->setCurrent($e);

        $this->assertNull($context->getError($e));

        $context->setError('error');

        $this->assertEquals('error', $context->getError($e));
    }

    /**
     *
     */
    public function test_hasError()
    {
        $context = new ValidationContext($this->createMock(FormInterface::class));
        $e = new Field('foo');
        $context->setCurrent($e);
        $context->setError('error');

        $this->assertTrue($context->hasError($e));
        $this->assertFalse($context->hasError(new Field('other')));
    }
}
