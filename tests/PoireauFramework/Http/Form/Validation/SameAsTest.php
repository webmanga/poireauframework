<?php

namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Form;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Operation\FormOperationRegistry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_SameAs
 */
class SameAsTest extends TestCase
{
    /**
     * @var ValidationContext
     */
    private $context;

    /**
     * @var Field
     */
    private $field;

    /**
     * @var Field
     */
    private $other;


    /**
     *
     */
    protected function setUp()
    {
        $this->field = new Field("foo");
        $this->other = new Field("other");
        $this->other->setValue("bar");

        $form = new Form(new FormOperationRegistry(), [
            "foo" => $this->field,
            "other" => $this->other
        ]);

        $this->context = new ValidationContext($form);
        $this->context->setCurrent($this->field);
    }

    /**
     *
     */
    public function test_parameters()
    {
        $sameAs = new SameAs("other", "custom message");

        $this->assertEquals("custom message", $sameAs->getMessage());
        $this->assertEquals("other", $sameAs->getField());
        $this->assertEquals(["field" => "other"], $sameAs->getParameters());
    }

    /**
     *
     */
    public function test_success()
    {
        $sameAs = new SameAs("other");

        $this->assertTrue($sameAs->validate("bar", $this->context));
        $this->assertFalse($this->context->hasErrors());
    }

    /**
     *
     */
    public function test_error_default()
    {
        $sameAs = new SameAs("other");

        $this->assertFalse($sameAs->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(SameAs::class, "The value should be same as '{field}'", ["field" => "other"]),
            $this->context->getError($this->field)
        );
    }

    /**
     *
     */
    public function test_error_custom()
    {
        $sameAs = new SameAs("other", "custom message");

        $this->assertFalse($sameAs->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(SameAs::class, "custom message", ["field" => "other"]),
            $this->context->getError($this->field)
        );
    }
}
