<?php

namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\FormInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_Email
 */
class EmailTest extends TestCase
{
    /**
     * @var ValidationContext
     */
    private $context;


    /**
     *
     */
    protected function setUp()
    {
        $this->context = new ValidationContext($this->createMock(FormInterface::class));
        $this->context->setCurrent(new Field("foo"));
    }

    /**
     *
     */
    public function test_parameters()
    {
        $email = new Email();

        $this->assertEquals("The value should be a valid email address", $email->getMessage());
    }

    /**
     *
     */
    public function test_success()
    {
        $email = new Email();

        $this->assertTrue($email->validate("test@webmanga.fr", $this->context));
        $this->assertFalse($this->context->hasErrors());
    }

    /**
     *
     */
    public function test_error_simple()
    {
        $email = new Email();

        $this->assertFalse($email->validate("no an email", $this->context));
        $this->assertTrue($this->context->hasErrors());

        $error = $this->context->getError($this->context->current());

        $this->assertEquals("The value should be a valid email address", $error->message());
        $this->assertEquals(Email::class, $error->type());
    }

    /**
     *
     */
    public function test_error_custom()
    {
        $email = new Email("custom message");

        $this->assertFalse($email->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());

        $error = $this->context->getError($this->context->current());

        $this->assertEquals("custom message", $error->message());
        $this->assertEquals(Email::class, $error->type());
    }
}
