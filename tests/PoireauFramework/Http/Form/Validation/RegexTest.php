<?php

namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\FormInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_Regex
 */
class RegexTest extends TestCase
{
    /**
     * @var ValidationContext
     */
    private $context;


    /**
     *
     */
    protected function setUp()
    {
        $this->context = new ValidationContext($this->createMock(FormInterface::class));
        $this->context->setCurrent(new Field("foo"));
    }

    /**
     *
     */
    public function test_parameters()
    {
        $regex = new Regex("#\\w+#i", "custom message");

        $this->assertEquals("custom message", $regex->getMessage());
        $this->assertEquals("#\\w+#i", $regex->getPattern());
        $this->assertEquals(["pattern" => "#\\w+#i"], $regex->getParameters());
    }

    /**
     *
     */
    public function test_regex_success()
    {
        $regex = new Regex("#\\w+#i");

        $this->assertTrue($regex->validate("Hello World", $this->context));
        $this->assertFalse($this->context->hasErrors());
    }

    /**
     *
     */
    public function test_regex_error_default()
    {
        $regex = new Regex("#\\w+#i");

        $this->assertFalse($regex->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Regex::class, "The value is not valid", ["pattern" => "#\\w+#i"]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_regex_error_not_string()
    {
        $regex = new Regex("#\\w+#i");

        $this->assertFalse($regex->validate([], $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Regex::class, "The value is not valid", ["pattern" => "#\\w+#i"]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_regex_error_custom()
    {
        $regex = new Regex("#\\w+#i", "custom message");

        $this->assertFalse($regex->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Regex::class, "custom message", ["pattern" => "#\\w+#i"]),
            $this->context->getError($this->context->current())
        );
    }
}
