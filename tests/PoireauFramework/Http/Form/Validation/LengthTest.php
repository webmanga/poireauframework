<?php

namespace PoireauFramework\Http\Form\Validation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\FormInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Validation
 * @group ext_PoireauFramework_Http_Form_Validation_Length
 */
class LengthTest extends TestCase
{
    /**
     * @var ValidationContext
     */
    private $context;


    /**
     *
     */
    protected function setUp()
    {
        $this->context = new ValidationContext($this->createMock(FormInterface::class));
        $this->context->setCurrent(new Field("foo"));
    }

    /**
     *
     */
    public function test_parameters_int()
    {
        $length = new Length(12, 78);

        $this->assertEquals(12, $length->getMin());
        $this->assertEquals(78, $length->getMax());
        $this->assertEquals(["min" => 12, "max" => 78], $length->getParameters());
    }

    /**
     *
     */
    public function test_parameters_array()
    {
        $length = new Length([
            "min" => 12,
            "max" => 78
        ]);

        $this->assertEquals(12, $length->getMin());
        $this->assertEquals(78, $length->getMax());
        $this->assertEquals(["min" => 12, "max" => 78], $length->getParameters());
    }

    /**
     *
     */
    public function test_success()
    {
        $length = new Length(4, 12);

        $this->assertTrue($length->validate("Hello World", $this->context));
        $this->assertFalse($this->context->hasErrors());
    }

    /**
     *
     */
    public function test_error_min()
    {
        $length = new Length(4, 12);

        $this->assertFalse($length->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Length::class, "The value should have at least {min} characters", ["min" => 4, "max" => 12]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_error_max()
    {
        $length = new Length(4, 12);

        $this->assertFalse($length->validate("########################", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Length::class, "The value should have at most {max} characters", ["min" => 4, "max" => 12]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_error_custom()
    {
        $length = new Length([
            "min" => 5,
            "minMessage" => "custom message"
        ]);

        $this->assertFalse($length->validate("##", $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Length::class, "custom message", ["min" => 5, "max" => null]),
            $this->context->getError($this->context->current())
        );
    }

    /**
     *
     */
    public function test_error_not_string()
    {
        $length = new Length(5);

        $this->assertFalse($length->validate([], $this->context));
        $this->assertTrue($this->context->hasErrors());
        $this->assertEquals(
            new Error(Length::class, "The value should be a string", ["min" => 5, "max" => null]),
            $this->context->getError($this->context->current())
        );
    }
}
