<?php

namespace PoireauFramework\Http\Form;

require_once __DIR__."/_files/RuleAlwaysFalse.php";
require_once __DIR__."/_files/entities.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Hydration\GetterSetter;
use PoireauFramework\Http\Form\Operation\FormOperationInterface;
use PoireauFramework\Http\Form\Operation\FormOperationRegistry;
use PoireauFramework\Http\Form\Test\User;
use PoireauFramework\Http\Form\Validation\RuleAlwaysFalse;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Form
 */
class FormTest extends TestCase
{
    /**
     *
     */
    public function test_get()
    {
        $element = new Field('foo');

        $form = new Form(new FormOperationRegistry(), ['foo' => $element]);

        $this->assertSame($element, $form->get('foo'));
    }

    /**
     *
     */
    public function test_apply()
    {
        $operation = $this->createMock(FormOperationInterface::class);

        $e1 = $this->createMock(FormElementInterface::class);
        $e2 = $this->createMock(FormElementInterface::class);

        $form = new Form(new FormOperationRegistry(), [$e1, $e2]);

        $e1->expects($this->once())
            ->method('apply')
            ->with($operation)
        ;

        $e2->expects($this->once())
            ->method('apply')
            ->with($operation)
        ;

        $this->assertSame($operation, $form->apply($operation));
    }

    /**
     *
     */
    public function test_bind_functional()
    {
        $form = new Form(new FormOperationRegistry(), [
            'foo'  => new Field('foo'),
            'name' => new Field('name')
        ]);

        $this->assertSame($form, $form->bind(new ArrayBag(['foo' => 'bar', 'name' => 'John', 'other' => '???'])));

        $this->assertEquals('bar', $form->get('foo')->value());
        $this->assertEquals('John', $form->get('name')->value());
    }

    /**
     *
     */
    public function test_validate_functional_error()
    {
        $form = new Form(new FormOperationRegistry(), [
            'foo'  => $foo  = new Field('foo', [], true),
            'name' => $name = new Field('name', [], true, [new RuleAlwaysFalse()])
        ]);

        $form->bind(new ArrayBag(["foo" => "bar", "name" => "John"]));

        $this->assertFalse($form->validate());
        $this->assertTrue($form->hasErrors());
        $this->assertEquals("cannot be valid", $form->error("name"));
        $this->assertTrue($form->hasError("name"));
    }

    /**
     *
     */
    public function test_validate_functional_success()
    {
        $form = new Form(new FormOperationRegistry(), [
            'foo'  => new Field('foo', [], true),
            'name' => new Field('name', [], true)
        ]);

        $form->bind(new ArrayBag(["foo" => "bar", "name" => "John"]));

        $this->assertTrue($form->validate());
        $this->assertFalse($form->hasErrors());
        $this->assertEmpty($form->errors());
    }

    /**
     *
     */
    public function test_toArray_functional()
    {
        $form = new Form(new FormOperationRegistry(), [
            'foo'  => new Field('foo'),
            'name' => new Field('name')
        ]);

        $form->bind(new ArrayBag(["foo" => "bar", "name" => "John"]));

        $this->assertEquals(["foo" => "bar", "name" => "John"], $form->toArray());
    }

    /**
     *
     */
    public function test_export_with_object()
    {
        $form = new Form(new FormOperationRegistry(), [
            "username" => new Field("username", [], false, [], new GetterSetter()),
            "password" => new Field("password", [], false, [], new GetterSetter())
        ]);

        $form->bind(new ArrayBag([
            "username" => "john_doe",
            "password" => "BestPassword"
        ]));

        $user = new User();
        $this->assertSame($user, $form->export($user));

        $this->assertEquals("john_doe", $user->username());
        $this->assertEquals("BestPassword", $user->password());
    }

    /**
     *
     */
    public function test_export_with_class_name()
    {
        $form = new Form(new FormOperationRegistry(), [
            "username" => new Field("username", [], false, [], new GetterSetter()),
            "password" => new Field("password", [], false, [], new GetterSetter())
        ]);

        $form->bind(new ArrayBag([
            "username" => "john_doe",
            "password" => "BestPassword"
        ]));

        $user = $form->export(User::class);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals("john_doe", $user->username());
        $this->assertEquals("BestPassword", $user->password());
    }

    /**
     *
     */
    public function test_export_with_attach_string()
    {
        $form = new Form(new FormOperationRegistry(), [
            "username" => new Field("username", [], false, [], new GetterSetter()),
            "password" => new Field("password", [], false, [], new GetterSetter())
        ]);

        $form->bind(new ArrayBag([
            "username" => "john_doe",
            "password" => "BestPassword"
        ]));

        $form->attach(User::class);

        $user = $form->export();

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals("john_doe", $user->username());
        $this->assertEquals("BestPassword", $user->password());
    }

    /**
     *
     */
    public function test_export_with_attach_object()
    {
        $form = new Form(new FormOperationRegistry(), [
            "username" => new Field("username", [], false, [], new GetterSetter()),
            "password" => new Field("password", [], false, [], new GetterSetter())
        ]);

        $form->bind(new ArrayBag([
            "username" => "john_doe",
            "password" => "BestPassword"
        ]));

        $user = new User();
        $form->attach($user);

        $this->assertSame($user, $form->export($user));

        $this->assertEquals("john_doe", $user->username());
        $this->assertEquals("BestPassword", $user->password());
    }
}
