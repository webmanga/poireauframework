<?php

namespace PoireauFramework\Http\Form\Element;

require_once __DIR__."/../_files/entities.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Form\Filter\Closure;
use PoireauFramework\Http\Form\Filter\Csv;
use PoireauFramework\Http\Form\Filter\FilterInterface;
use PoireauFramework\Http\Form\Filter\Secret;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Hydration\GetterSetter;
use PoireauFramework\Http\Form\Operation\Bind;
use PoireauFramework\Http\Form\Operation\FormOperationInterface;
use PoireauFramework\Http\Form\Test\User;
use PoireauFramework\Http\Form\Validation\Error;
use PoireauFramework\Http\Form\Validation\RuleInterface;
use PoireauFramework\Http\Form\Validation\ValidationContext;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Element
 * @group ext_PoireauFramework_Http_Form_Element_Field
 */
class FieldTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $field = new Field('foo');

        $this->assertEquals('foo', $field->name());
        $this->assertNull($field->value());
    }

    /**
     *
     */
    public function test_get_set_value()
    {
        $field = new Field('foo');

        $field->setValue('bar');
        $this->assertEquals('bar', $field->value());
    }

    /**
     *
     */
    public function test_httpValue_csv()
    {
        $field = new Field("foo", [new Csv()]);
        $field->setValue("hello,world");

        $this->assertEquals(["hello", "world"], $field->value());
        $this->assertEquals("hello,world", $field->httpValue());
    }

    /**
     *
     */
    public function test_httpValue_secret()
    {
        $field = new Field("foo", [new Secret()]);
        $field->setValue("Hello World !");

        $this->assertEmpty($field->httpValue());
    }

    /**
     *
     */
    public function test_apply()
    {
        $field = new Field('foo');

        $operation = $this->createMock(FormOperationInterface::class);

        $operation->expects($this->once())
            ->method('applyOnField')
            ->with($field)
        ;

        $this->assertSame($operation, $field->apply($operation));
    }

    /**
     *
     */
    public function test_apply_bind()
    {
        $field = new Field('foo');

        $field->apply(new Bind(new ArrayBag(['foo' => 'bar'])));

        $this->assertEquals('bar', $field->value());
    }

    /**
     *
     */
    public function test_setValue_with_filter_unit()
    {
        $filter = $this->createMock(FilterInterface::class);

        $field = new Field('foo', [$filter]);

        $filter->expects($this->once())
            ->method('fromHttp')
            ->with('bar')
            ->willReturn('babar')
        ;

        $field->setValue('bar');

        $this->assertEquals('babar', $field->value());
    }

    /**
     *
     */
    public function test_setValue_with_filter_closure()
    {
        $filter = new Closure(function ($value) {
            return $value.$value;
        });

        $field = new Field('foo', [$filter]);

        $field->setValue('bar');

        $this->assertEquals('barbar', $field->value());
    }

    /**
     *
     */
    public function test_validate_required_with_message()
    {
        $field = new Field('foo', [], 'required message');

        $context = new ValidationContext($this->createMock(FormInterface::class));
        $context->setCurrent($field);

        $this->assertFalse($field->validate($context));
        $this->assertTrue($context->hasError($field));
        $this->assertEquals(Error::required("required message"), $context->getError($field));
    }

    /**
     *
     */
    public function test_validate_required_default()
    {
        $field = new Field('foo', [], true);

        $context = new ValidationContext($this->createMock(FormInterface::class));
        $context->setCurrent($field);

        $this->assertFalse($field->validate($context));
        $this->assertTrue($context->hasError($field));
        $this->assertEquals(Error::required(), $context->getError($field));
    }

    /**
     *
     */
    public function test_validate_required_success()
    {
        $field = new Field('foo', [], true);
        $field->setValue('bar');

        $context = new ValidationContext($this->createMock(FormInterface::class));
        $context->setCurrent($field);

        $this->assertTrue($field->validate($context));
        $this->assertFalse($context->hasError($field));
    }

    /**
     *
     */
    public function test_validate_rule_error()
    {
        $rule = $this->createMock(RuleInterface::class);

        $field = new Field('foo', [], false, [$rule]);
        $field->setValue('bar');

        $context = new ValidationContext($this->createMock(FormInterface::class));
        $context->setCurrent($field);

        $rule->expects($this->once())
            ->method('validate')
            ->with('bar', $context)
            ->willReturn(false)
        ;

        $this->assertFalse($field->validate($context));
    }

    /**
     *
     */
    public function test_extract_with_extractor()
    {
        $field = new Field("username", [], false, [], null, new GetterSetter());

        $target = new User();
        $target->setUsername("JohnDoe");

        $field->extract($target);

        $this->assertEquals("JohnDoe", $field->value());
    }

    /**
     *
     */
    public function test_extract_no_extractor()
    {
        $field = new Field("username");

        $target = new User();
        $target->setUsername("JohnDoe");

        $field->extract($target);

        $this->assertEmpty($field->value());
    }

    /**
     *
     */
    public function test_hydrate_with_hydrator()
    {
        $field = new Field("username", [], false, [], new GetterSetter());
        $field->setValue("JohnDoe");

        $target = new User();
        $field->hydrate($target);

        $this->assertEquals("JohnDoe", $target->username());
    }

    /**
     *
     */
    public function test_hydrate_no_hydrator()
    {
        $field = new Field("username");
        $field->setValue("JohnDoe");

        $target = new User();
        $field->hydrate($target);

        $this->assertEmpty($target->username());
    }
}
