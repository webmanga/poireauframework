<?php

namespace PoireauFramework\Http\Form\Filter;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Filter
 * @group ext_PoireauFramework_Http_Form_Filter_Csv
 */
class CsvTest extends TestCase
{
    /**
     *
     */
    public function test_fromHttp_simple()
    {
        $csv = new Csv();

        $this->assertEquals(["Hello", "World", "!"], $csv->fromHttp("Hello,World,!"));
    }

    /**
     *
     */
    public function test_fromHttp_enclosed()
    {
        $csv = new Csv();

        $this->assertEquals(["Hello", "World", "!"], $csv->fromHttp('"Hello","World","!"'));
    }

    /**
     *
     */
    public function test_fromHttp_escaped()
    {
        $csv = new Csv();

        $this->assertEquals(['"az', 'er,ty', 'ui\\'], $csv->fromHttp('"\\"az","er,ty","ui\\\\"'));
    }

    /**
     *
     */
    public function test_toHttp_simple()
    {
        $csv = new Csv();

        $this->assertEquals("Hello,World,!", $csv->toHttp(["Hello", "World", "!"]));
    }

    /**
     *
     */
    public function test_toHttp_enclosed()
    {
        $csv = new Csv();

        $this->assertEquals('\\"az,"er,ty",ui\\\\', $csv->toHttp(['"az', 'er,ty', 'ui\\']));
    }

    /**
     *
     */
    public function test_custom_enclosure()
    {
        $csv = new Csv();

        $csv->enclosure("@");

        $this->assertEquals(["Hello", "World", "!"], $csv->fromHttp('@Hello@,@World@,@!@'));
        $this->assertEquals('"",\\@', $csv->toHttp(['""', "@"]));
    }

    /**
     *
     */
    public function test_custom_escape()
    {
        $csv = new Csv();

        $csv->escape('[');

        $this->assertEquals(['"', '\\"'], $csv->fromHttp('[",\\"'));
        $this->assertEquals('["[",\\', $csv->toHttp(['""', "\\"]));
    }

    /**
     *
     */
    public function test_custom_separator()
    {
        $csv = new Csv();

        $csv->delimiter('#');

        $this->assertEquals(["Hello,World", "Foo,bar"], $csv->fromHttp('Hello,World#Foo,bar'));
        $this->assertEquals('Hello,World#"my#name#is#john"', $csv->toHttp(["Hello,World", "my#name#is#john"]));
    }
}
