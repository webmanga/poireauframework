<?php

namespace PoireauFramework\Http\Form\Builder;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Form;
use PoireauFramework\Http\Form\Operation\FormOperationRegistry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Builder
 * @group ext_PoireauFramework_Http_Form_Builder_FormBuilder
 */
class FormBuilderTest extends TestCase
{
    /**
     *
     */
    public function test_build_defaults()
    {
        $builder = new FormBuilder();

        $form = $builder->build();

        $this->assertInstanceOf(Form::class, $form);
        $this->assertEquals(new Form(new FormOperationRegistry(), []), $form);
    }

    /**
     *
     */
    public function test_field()
    {
        $builder = new FormBuilder();

        $this->assertInstanceOf(FieldBuilder::class, $builder->field("foo"));

        $form = $builder->build();

        $this->assertInstanceOf(Field::class, $form->get("foo"));
    }
    /**
     *
     */
    public function test_operations()
    {
        $operations = $this->createMock(FormOperationRegistry::class);

        $builder = new FormBuilder();

        $this->assertSame($builder, $builder->operations($operations));

        $form = $builder->build();

        $this->assertInstanceOf(Form::class, $form);
        $this->assertEquals(new Form($operations, []), $form);
    }

    /**
     *
     */
    public function test_build_with_fields()
    {
        $builder = new FormBuilder();

        $builder->field("foo");
        $builder->field("name");

        $form = $builder->build();

        $this->assertEquals(new Form(new FormOperationRegistry(), [
            "foo"  => new Field("foo"),
            "name" => new Field("name")
        ]), $form);
    }

    /**
     *
     */
    public function test_attach()
    {
        $attachment = new \stdClass();

        $builder = new FormBuilder();
        $this->assertSame($builder, $builder->attach($attachment));

        $form = $builder->build();

        $this->assertSame($attachment, $form->export());
    }
}
