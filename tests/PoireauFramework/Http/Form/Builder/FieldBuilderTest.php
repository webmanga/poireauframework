<?php

namespace PoireauFramework\Http\Form\Builder;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Filter\Closure as ClosureFilter;
use PoireauFramework\Http\Form\Filter\FilterInterface;
use PoireauFramework\Http\Form\Hydration\ExtractorInterface;
use PoireauFramework\Http\Form\Hydration\GetterSetter;
use PoireauFramework\Http\Form\Hydration\HydratorInterface;
use PoireauFramework\Http\Form\Validation\Closure as ClosureRule;
use PoireauFramework\Http\Form\Validation\RuleInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Builder
 * @group ext_PoireauFramework_Http_Form_Builder_FieldBuilder
 */
class FieldBuilderTest extends TestCase
{
    /**
     *
     */
    public function test_build_defaults()
    {
        $builder = new FieldBuilder("foo");

        $field = $builder->build();

        $this->assertInstanceOf(Field::class, $field);
        $this->assertEquals(new Field("foo"), $field);
    }

    /**
     *
     */
    public function test_filter_simple()
    {
        $builder = new FieldBuilder("foo");
        $filter = $this->createMock(FilterInterface::class);

        $this->assertSame($builder, $builder->filter($filter));

        $this->assertEquals(new Field("foo", [$filter]), $builder->build());
    }

    /**
     *
     */
    public function test_filter_closure()
    {
        $builder = new FieldBuilder("foo");
        $filter = function () {};

        $this->assertSame($builder, $builder->filter($filter));

        $this->assertEquals(new Field("foo", [new ClosureFilter($filter)]), $builder->build());
    }

    /**
     *
     */
    public function test_rule_simple()
    {
        $builder = new FieldBuilder("foo");
        $rule = $this->createMock(RuleInterface::class);

        $this->assertSame($builder, $builder->rule($rule));

        $this->assertEquals(new Field("foo", [], false, [$rule]), $builder->build());
    }

    /**
     *
     */
    public function test_rule_closure()
    {
        $builder = new FieldBuilder("foo");
        $rule = function () {};

        $this->assertSame($builder, $builder->rule($rule));

        $this->assertEquals(new Field("foo", [], false, [new ClosureRule($rule)]), $builder->build());
    }

    /**
     *
     */
    public function test_required()
    {
        $builder = new FieldBuilder("foo");

        $this->assertSame($builder, $builder->required("my required message"));

        $this->assertEquals(new Field("foo", [], "my required message"), $builder->build());
    }

    /**
     *
     */
    public function test_name()
    {
        $builder = new FieldBuilder("foo");

        $this->assertSame($builder, $builder->name("bar"));

        $this->assertEquals(new Field("bar"), $builder->build());
    }

    /**
     *
     */
    public function test_set_hydrator()
    {
        $hydrator = $this->createMock(HydratorInterface::class);
        $builder = new FieldBuilder("foo");
        $this->assertSame($builder, $builder->set($hydrator));

        $this->assertEquals(new Field("foo", [], false, [], $hydrator), $builder->build());
    }

    /**
     *
     */
    public function test_set_extractor()
    {
        $extractor = $this->createMock(ExtractorInterface::class);
        $builder = new FieldBuilder("foo");
        $this->assertSame($builder, $builder->set($extractor));

        $this->assertEquals(new Field("foo", [], false, [], null, $extractor), $builder->build());
    }

    /**
     *
     */
    public function test_set_hydrator_extractor()
    {
        $he = new GetterSetter();
        $builder = new FieldBuilder("foo");
        $this->assertSame($builder, $builder->set($he));

        $this->assertEquals(new Field("foo", [], false, [], $he, $he), $builder->build());
    }

    /**
     *
     */
    public function test_getSet()
    {
        $gs = new GetterSetter();
        $builder = new FieldBuilder("foo");
        $this->assertSame($builder, $builder->getSet());

        $this->assertEquals(new Field("foo", [], false, [], $gs, $gs), $builder->build());
    }
}
