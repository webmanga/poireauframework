<?php

namespace PoireauFramework\Http\Form;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Form\Builder\FormBuilder;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Filter\Secret;
use PoireauFramework\Http\Form\Test\User;
use PoireauFramework\Http\Form\Validation\Email;
use PoireauFramework\Http\Form\Validation\Error;
use PoireauFramework\Http\Form\Validation\Length;
use PoireauFramework\Http\Form\Validation\Regex;
use PoireauFramework\Http\Form\Validation\SameAs;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Functional
 */
class FunctionalTest extends TestCase
{
    /**
     * @return FormInterface
     */
    private function registrationForm()
    {
        $builder = new FormBuilder();

        $builder->attach(User::class);

        $builder->field("username")
            ->required()
            ->rule(new Length(4, 12))
            ->rule(new Regex("#^\\w+$#"))
            ->getSet()
        ;

        $builder->field("password")
            ->required()
            ->rule(new Length(6))
            ->filter(new Secret())
            ->getSet()
        ;

        $builder->field("confirm")
            ->required()
            ->filter(new Secret())
            ->rule(new SameAs("password"))
        ;

        $builder->field("mail")
            ->required()
            ->rule(new Email())
            ->getSet()
        ;

        return $builder->build();
    }

    /**
     *
     */
    public function test_reg_fields()
    {
        $form = $this->registrationForm();

        $this->assertInstanceOf(Field::class, $form->get("username"));
        $this->assertInstanceOf(Field::class, $form->get("password"));
        $this->assertInstanceOf(Field::class, $form->get("confirm"));
        $this->assertInstanceOf(Field::class, $form->get("mail"));
    }

    /**
     *
     */
    public function test_reg_error_required()
    {
        $form = $this->registrationForm();

        $this->assertFalse($form->bind(new ArrayBag())->validate());

        $this->assertEquals([
            "username" => Error::required(),
            "password" => Error::required(),
            "confirm"  => Error::required(),
            "mail"     => Error::required(),
        ], $form->errors());
    }

    /**
     *
     */
    public function test_reg_error_username()
    {
        $form = $this->registrationForm();

        $this->assertFalse($form->bind(new ArrayBag([
            "username" => "##INVALID##"
        ]))->validate());

        $this->assertTrue($form->hasError("username"));
        $this->assertEquals("The value is not valid", $form->error("username"));
    }

    /**
     *
     */
    public function test_reg_error_password()
    {
        $form = $this->registrationForm();

        $this->assertFalse($form->bind(new ArrayBag([
            "password" => "1234"
        ]))->validate());

        $this->assertTrue($form->hasError("password"));
        $this->assertEquals("The value should have at least 6 characters", $form->error("password"));
    }

    /**
     *
     */
    public function test_reg_error_confirm()
    {
        $form = $this->registrationForm();

        $this->assertFalse($form->bind(new ArrayBag([
            "password" => "azertyuiop",
            "confirm" => "1234"
        ]))->validate());

        $this->assertTrue($form->hasError("confirm"));
        $this->assertEquals("The value should be same as 'password'", $form->error("confirm"));
    }

    /**
     *
     */
    public function test_reg_error_mail()
    {
        $form = $this->registrationForm();

        $this->assertFalse($form->bind(new ArrayBag([
            "mail" => "no_an_email"
        ]))->validate());

        $this->assertTrue($form->hasError("mail"));
        $this->assertEquals("The value should be a valid email address", $form->error("mail"));
    }

    /**
     *
     */
    public function test_reg_success()
    {
        $form = $this->registrationForm();

        $this->assertTrue($form->bind(new ArrayBag([
            "username" => "JohnDoe",
            "password" => "MyVeryStrongPassword",
            "confirm"  => "MyVeryStrongPassword",
            "mail"     => "john.doe@webmanga.fr",
        ]))->validate());

        $this->assertFalse($form->hasErrors());
        $this->assertEmpty($form->errors());

        $this->assertEquals("JohnDoe",              $form->get("username")->value());
        $this->assertEquals("MyVeryStrongPassword", $form->get("password")->value());
        $this->assertEquals("MyVeryStrongPassword", $form->get("confirm")->value());
        $this->assertEquals("john.doe@webmanga.fr", $form->get("mail")->value());
    }

    /**
     *
     */
    public function test_reg_export()
    {
        $form = $this->registrationForm();

        $user = $form->bind(new ArrayBag([
            "username" => "JohnDoe",
            "password" => "MyVeryStrongPassword",
            "confirm"  => "MyVeryStrongPassword",
            "mail"     => "john.doe@webmanga.fr",
        ]))->export();

        $this->assertInstanceOf(User::class, $user);

        $this->assertEquals("JohnDoe",              $user->username());
        $this->assertEquals("MyVeryStrongPassword", $user->password());
        $this->assertEquals("john.doe@webmanga.fr", $user->mail());
    }
}
