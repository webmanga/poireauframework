<?php

namespace PoireauFramework\Http\Form\Validation;

class RuleAlwaysFalse implements RuleInterface
{
    private $message;

    /**
     * RuleAlwaysFalse constructor.
     *
     * @param $message
     */
    public function __construct($message = "cannot be valid")
    {
        $this->message = $message;
    }


    public function validate($value, ValidationContext $context): bool
    {
        $context->setError($this->message);

        return false;
    }

    public function getParameters(): array
    {
        return [];
    }
}
