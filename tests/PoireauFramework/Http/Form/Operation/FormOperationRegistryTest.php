<?php

namespace PoireauFramework\Http\Form\Operation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Validation\ValidationContext;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Operation
 * @group ext_PoireauFramework_Http_Form_Operation_FormOperationRegistry
 */
class FormOperationRegistryTest extends TestCase
{
    /**
     * @var FormOperationRegistry
     */
    private $registry;


    /**
     *
     */
    protected function setUp()
    {
        $this->registry = new FormOperationRegistry();
    }

    /**
     *
     */
    public function test_bind()
    {
        $this->assertInstanceOf(Bind::class, $this->registry->bind(new ArrayBag()));
    }

    /**
     *
     */
    public function test_toArray()
    {
        $this->assertInstanceOf(ToArray::class, $this->registry->toArray());
    }

    /**
     *
     */
    public function test_export()
    {
        $target = new \stdClass();
        $op = $this->registry->export($target);

        $this->assertInstanceOf(Export::class, $op);
        $this->assertSame($target, $op->target());
    }

    /**
     *
     */
    public function test_validate()
    {
        $form = $this->createMock(FormInterface::class);
        $validate = $this->registry->validate($form);

        $this->assertInstanceOf(Validate::class, $validate);
        $this->assertInstanceOf(ValidationContext::class, $validate->context());
        $this->assertSame($form, $validate->context()->form());
    }
}
