<?php

namespace PoireauFramework\Http\Form\Operation;

require_once __DIR__."/../_files/entities.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Hydration\GetterSetter;
use PoireauFramework\Http\Form\Test\User;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Operation
 * @group ext_PoireauFramework_Http_Form_Operation_Export
 */
class ExportTest extends TestCase
{
    /**
     *
     */
    public function test_applyOnField_set_value()
    {
        $target = new User();

        $export = new Export($target);

        $field = new Field("username", [], false, [], new GetterSetter());
        $field->setValue("JohnDoe");
        $export->applyOnField($field);

        $this->assertEquals("JohnDoe", $target->username());
    }
}
