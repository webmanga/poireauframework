<?php

namespace PoireauFramework\Http\Form\Operation;

require_once __DIR__."/../_files/RuleAlwaysFalse.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\FormInterface;
use PoireauFramework\Http\Form\Validation\RuleAlwaysFalse;
use PoireauFramework\Http\Form\Validation\ValidationContext;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Operation
 * @group ext_PoireauFramework_Http_Form_Operation_Validate
 */
class ValidateTest extends TestCase
{
    /**
     *
     */
    public function test_applyOnField_success()
    {
        $validate = new Validate(new ValidationContext($this->createMock(FormInterface::class)));
        $field = new Field('foo');
        $field->setValue('bar');

        $validate->applyOnField($field);

        $this->assertFalse($validate->context()->hasErrors());
    }

    /**
     *
     */
    public function test_applyOnField_error()
    {
        $validate = new Validate(new ValidationContext($this->createMock(FormInterface::class)));
        $field = new Field('foo', [], false, [new RuleAlwaysFalse()]);

        $field->setValue('bar');

        $validate->applyOnField($field);

        $this->assertTrue($validate->context()->hasErrors());
        $this->assertEquals("cannot be valid", $validate->context()->getError($field));
    }
}
