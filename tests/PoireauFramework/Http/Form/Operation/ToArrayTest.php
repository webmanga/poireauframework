<?php


namespace PoireauFramework\Http\Form\Operation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Form\Element\Field;


/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Operation
 * @group ext_PoireauFramework_Http_Form_Operation_ToArray
 */
class ToArrayTest extends TestCase
{
    /**
     *
     */
    public function test_applyOnField()
    {
        $toArray = new ToArray();

        $field = new Field("foo");
        $field->setValue("bar");

        $toArray->applyOnField($field);

        $this->assertEquals(["foo" => "bar"], $toArray->data());
    }
}
