<?php


namespace PoireauFramework\Http\Form\Operation;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Form\Element\Field;


/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Form
 * @group ext_PoireauFramework_Http_Form_Operation
 * @group ext_PoireauFramework_Http_Form_Operation_Bind
 */
class BindTest extends TestCase
{
    /**
     *
     */
    public function test_applyOnField_no_data()
    {
        $bind = new Bind(new ArrayBag());

        $field = $this->getMockBuilder(Field::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs(['foo'])
            ->setMethods(['setValue'])
            ->getMock()
        ;

        $field->expects($this->never())->method('setValue');

        $bind->applyOnField($field);
    }

    /**
     *
     */
    public function test_applyOnField_set_value()
    {
        $bind = new Bind(new ArrayBag(['foo' => 'bar']));

        $field = new Field('foo');
        $bind->applyOnField($field);

        $this->assertEquals('bar', $field->value());
    }
}