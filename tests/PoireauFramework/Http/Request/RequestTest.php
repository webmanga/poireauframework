<?php

namespace PoireauFramework\Http\Request;

use PHPUnit\Framework\TestCase;

use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Bag\CookieBag;
use PoireauFramework\Http\Bag\HeadersBag;
use PoireauFramework\Http\Bag\QueryBag;
use PoireauFramework\Http\Request\Request;

/**
 * Class RequestTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Request
 * @group ext_PoireauFramework_Http_Request_Request
 */
class RequestTest extends TestCase
{
    /**
     * @var Request
     */
    private $request;

    public function setUp()
    {
        $this->request = new Request(
            "GET", "/", "index.php", new QueryBag(["foo" => "bar"]), new HeadersBag(["Content-Type" => "application/json", "Host" => "127.0.0.1"]), new ArrayBag([
                "first_name" => "John",
                "last_name"  => "Doe"
            ]), new CookieBag(["__Secure-danger" => "very dangerous", "my_cookie" => "foo"])
        );
    }

    /**
     *
     */
    public function test_defaults()
    {
        $this->assertInstanceOf(ArrayBag::class, $this->request->query());
        $this->assertInstanceOf(ArrayBag::class, $this->request->body());
        $this->assertInstanceOf(HeadersBag::class, $this->request->headers());
        $this->assertInstanceOf(CookieBag::class, $this->request->cookies());

        $this->assertEquals("GET", $this->request->method());
        $this->assertEquals("/", $this->request->path());
        $this->assertEquals("index.php", $this->request->script());
    }

    /**
     *
     */
    public function test_get()
    {
        $this->assertEquals("bar", $this->request->query()->get("foo"));
    }

    /**
     *
     */
    public function test_body()
    {
        $this->assertEquals([
            "first_name" => "John",
            "last_name"  => "Doe"
        ], $this->request->body()->raw());
    }

    /**
     *
     */
    public function test_headers()
    {
        $this->assertEquals("application/json", $this->request->headers()->get("Content-Type"));
        $this->assertEquals("127.0.0.1", $this->request->headers()->get("Host"));
    }

    /**
     *
     */
    public function test_cookies()
    {
        $this->assertEquals("very dangerous", $this->request->cookies()->secure("danger"));
        $this->assertEquals("foo", $this->request->cookies()->get("my_cookie"));
    }

    /**
     *
     */
    public function test_attachment()
    {
        $this->assertFalse($this->request->has("foo"));

        $this->request->attach("foo", "bar");

        $this->assertTrue($this->request->has("foo"));
        $this->assertEquals("bar", $this->request->attachment("foo"));
    }
}