<?php

namespace PoireauFramework\Http\Request;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\ArrayBag;

/**
 * Class RequestBuilderTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Request
 * @group ext_PoireauFramework_Http_Request_RequestBuilder
 */
class RequestBuilderTest extends TestCase
{
    /**
     *
     */
    public function test_build_defaults()
    {
        $builder = new RequestBuilder();

        $request = $builder->build();

        $this->assertInstanceOf(Request::class, $request);

        $this->assertEquals("GET", $request->method());
        $this->assertEquals("/", $request->path());
        $this->assertEmpty($request->script());
        $this->assertEmpty($request->query()->raw());
        $this->assertEmpty($request->headers()->raw());
        $this->assertEmpty($request->cookies()->raw());
        $this->assertNull($request->body());
    }

    /**
     *
     */
    public function test_method()
    {
        $builder = new RequestBuilder();

        $this->assertSame($builder, $builder->method(Request::METHOD_POST));

        $this->assertEquals("POST", $builder->build()->method());
    }

    /**
     *
     */
    public function test_path()
    {
        $builder = new RequestBuilder();

        $this->assertSame($builder, $builder->path("/home"));

        $this->assertEquals("/home", $builder->build()->path());
    }

    /**
     *
     */
    public function test_script()
    {
        $builder = new RequestBuilder();

        $this->assertSame($builder, $builder->script("index.php"));

        $this->assertEquals("index.php", $builder->build()->script());
    }

    /**
     *
     */
    public function test_body()
    {
        $builder = new RequestBuilder();

        $body = new ArrayBag([
            "name" => "John Doe"
        ]);

        $this->assertSame($builder, $builder->body($body));

        $this->assertSame($body, $builder->build()->body());
    }

    /**
     *
     */
    public function test_query()
    {
        $builder = new RequestBuilder();

        $builder->query()->set("foo", "bar");

        $this->assertEquals("bar", $builder->build()->query()->get("foo"));
    }

    /**
     *
     */
    public function test_headers()
    {
        $builder = new RequestBuilder();

        $builder->headers()->set("Content-Type", "application/json");

        $this->assertEquals("application/json", $builder->build()->headers()->get("Content-Type"));
    }

    /**
     *
     */
    public function test_cookies()
    {
        $builder = new RequestBuilder();

        $builder->cookies()->setHost("foo", "bar");

        $this->assertEquals("bar", $builder->build()->cookies()->host("foo"));
    }

    /**
     *
     */
    public function test_createFromGlobals()
    {
        $_GET = [
            "foo" => "bar"
        ];

        $_POST = [
            "first_name" => "John",
            "last_name"  => "Doe"
        ];

        $_SERVER = [
            "REQUEST_METHOD" => "GET",
            "HTTP_HOST"      => "127.0.0.1",
            "HTTP_CONTENT_TYPE" => "application/json",
            "SCRIPT_NAME"          => "/home/index.php"
        ];

        $_COOKIE = [
            "__Secure-foo" => "secure bar",
            "id" => "123"
        ];

        $builder = RequestBuilder::fromGlobals();

        $this->assertInstanceOf(RequestBuilder::class, $builder);

        $request = $builder->build();

        $this->assertEquals($_GET, $request->query()->raw());
        $this->assertEquals($_POST, $request->body()->raw());
        $this->assertEquals($_COOKIE, $request->cookies()->raw());
        $this->assertEquals([
            "Content-Type" => "application/json",
            "Host"         => "127.0.0.1"
        ], $request->headers()->raw());

        $this->assertEquals("/", $request->path());
        $this->assertEquals("/home/index.php", $request->script());
        $this->assertEquals("GET", $request->method());
    }
}