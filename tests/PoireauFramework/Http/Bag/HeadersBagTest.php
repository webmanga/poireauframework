<?php

namespace PoireauFramework\Http\Bag;

use PHPUnit\Framework\TestCase;

/**
 * Class HeadersBagTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Bag
 * @group ext_PoireauFramework_Http_Bag_HeadersBag
 */
class HeadersBagTest extends TestCase
{
    /**
     *
     */
    public function test_normalizeName()
    {
        $this->assertSame('Content-Type', HeadersBag::normalizeName('CONTENT_TYPE'));
        $this->assertSame('Location', HeadersBag::normalizeName('location'));
        $this->assertSame('Set-Cookie', HeadersBag::normalizeName('Set-cookie'));
    }

    /**
     *
     */
    public function test_construct()
    {
        $bag = new HeadersBag([
            'content-type: text/html',
            'set-cookie: foo=bar',
            'set-cookie: a=b',
            'Location' => 'index.php'
        ]);

        $this->assertEquals([
            'Content-Type' => 'text/html',
            'Set-Cookie' => ['foo=bar', 'a=b'],
            'Location' => 'index.php'
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_set()
    {
        $bag = new HeadersBag();

        $bag->set('content_type', 'text/html');

        $this->assertEquals([
            'Content-Type' => 'text/html'
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_add()
    {
        $bag = new HeadersBag([
            'Set-Cookie' => [
                'foo=bar'
            ]
        ]);

        $bag->add('set-cookie', 'a=b');

        $this->assertEquals([
            'Set-Cookie' => [
                'foo=bar',
                'a=b'
            ]
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_get()
    {
        $bag = new HeadersBag([
            'Content-Type' => 'text/html'
        ]);

        $this->assertEquals('text/html', $bag->get('CONTENT_TYPE'));
    }

    /**
     *
     */
    public function test_has()
    {
        $bag = new HeadersBag([
            'Content-Type' => 'text/html'
        ]);

        $this->assertTrue($bag->has('CONTENT_TYPE'));
    }

    /**
     *
     */
    public function test_normalizeArray()
    {
        $this->assertEquals([
            'Content-Type' => 'text/html',
            'Cookie' => 'foo=bar',
            'Location' => 'index.php'
        ], HeadersBag::normalizeArray([
            'content-type: text/html',
            'cookie' => 'foo=bar',
            'Location' => 'index.php'
        ]));

        $this->assertEquals([
            'Set-Cookie' => [
                'foo=bar',
                'a=b',
                'test='
            ]
        ], HeadersBag::normalizeArray([
            'set-cookie: foo=bar',
            'SET_COOKIE:a=b',
            'Set-cookie : test='
        ]));

        $this->assertEquals([
            'Set-Cookie' => [
                'foo=bar',
                'a=b',
                'test='
            ],
            'Content-Type' => 'text/html'
        ], HeadersBag::normalizeArray([
            'Set-Cookie' => [
                'foo=bar',
                'a=b',
                'test='
            ],
            'Content-Type' => 'text/html'
        ]));
    }

    /**
     *
     */
    public function test_contentType()
    {
        $bag = new HeadersBag([
            'content-type: text/html; charset=UTF-8',
            'set-cookie: foo=bar',
            'set-cookie: a=b',
            'Location' => 'index.php'
        ]);

        $this->assertEquals("text/html", $bag->contentType());
    }

    /**
     *
     */
    public function test_setLocation()
    {
        $bag = new HeadersBag();

        $bag->setLocation("http://example.com");

        $this->assertEquals("http://example.com", $bag->get("location"));
    }
}
