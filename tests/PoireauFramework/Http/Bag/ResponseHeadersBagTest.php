<?php

namespace PoireauFramework\Http\Bag;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Cookie\CookieBuilder;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Bag
 * @group ext_PoireauFramework_Http_Bag_ResponseHeadersBag
 */
class ResponseHeadersBagTest extends TestCase
{
    /**
     *
     */
    public function test_getCookies()
    {
        $bag = new ResponseHeadersBag();

        $this->assertEquals([], $bag->getCookies());

        $cookie = new CookieBuilder("foo", "bar");
        $bag = new ResponseHeadersBag([
            "Set-Cookie" => $cookie
        ]);

        $this->assertSame([$cookie], $bag->getCookies());


        $cookie2 = new CookieBuilder("a", "b");
        $bag = new ResponseHeadersBag([
            "Set-Cookie" => [
                $cookie,
                $cookie2
            ]
        ]);

        $this->assertSame([$cookie, $cookie2], $bag->getCookies());
    }

    /**
     *
     */
    public function test_addCookie()
    {
        $bag = new ResponseHeadersBag();
        $cookie = new CookieBuilder("foo", "bar");

        $bag->addCookie($cookie);

        $this->assertSame([
            "Set-Cookie" => $cookie
        ], $bag->raw());

        $cookie2 = new CookieBuilder("a", "b");

        $bag->addCookie($cookie2);
        $this->assertSame([
            "Set-Cookie" => [
                $cookie,
                $cookie2
            ]
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_add_get_cookies()
    {
        $bag = new ResponseHeadersBag();
        $cookie = new CookieBuilder("foo", "bar");

        $bag->addCookie($cookie);

        $this->assertSame([$cookie], $bag->getCookies());
    }
}
