<?php

namespace PoireauFramework\Http\Bag;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Bag
 * @group ext_PoireauFramework_Http_Bag_QueryBag
 */
class QueryBagTest extends TestCase
{
    /**
     *
     */
    public function test_scalar_found()
    {
       $bag = new QueryBag(["foo" => "bar"]);

       $this->assertSame("bar", $bag->scalar("foo"));
    }

    /**
     *
     */
    public function test_scalar_with_array()
    {
        $bag = new QueryBag(["foo" => ["bar"]]);

        $this->assertNull($bag->scalar("foo"));
    }

    /**
     *
     */
    public function test_scalar_default_value()
    {
        $bag = new QueryBag([]);

        $this->assertSame("default", $bag->scalar("foo", "default"));
    }

    /**
     *
     */
    public function test_string_found()
    {
       $bag = new QueryBag(["foo" => "bar"]);

       $this->assertSame("bar", $bag->string("foo"));
    }

    /**
     *
     */
    public function test_string_with_array()
    {
        $bag = new QueryBag(["foo" => ["bar"]]);

        $this->assertSame("", $bag->string("foo"));
    }

    /**
     *
     */
    public function test_string_default_value()
    {
        $bag = new QueryBag([]);

        $this->assertSame("default", $bag->string("foo", "default"));
    }

    /**
     *
     */
    public function test_integer_found()
    {
       $bag = new QueryBag(["foo" => "123"]);

       $this->assertSame(123, $bag->integer("foo"));
    }

    /**
     *
     */
    public function test_integer_with_array()
    {
        $bag = new QueryBag(["foo" => ["bar"]]);

        $this->assertSame(0, $bag->integer("foo"));
    }

    /**
     *
     */
    public function test_integer_default_value()
    {
        $bag = new QueryBag([]);

        $this->assertSame(321, $bag->integer("foo", 321));
    }

    /**
     *
     */
    public function test_float_found()
    {
       $bag = new QueryBag(["foo" => "12.3"]);

       $this->assertSame(12.3, $bag->float("foo"));
    }

    /**
     *
     */
    public function test_float_with_array()
    {
        $bag = new QueryBag(["foo" => ["bar"]]);

        $this->assertSame(0.0, $bag->float("foo"));
    }

    /**
     *
     */
    public function test_float_default_value()
    {
        $bag = new QueryBag([]);

        $this->assertSame(32.1, $bag->float("foo", 32.1));
    }

    /**
     *
     */
    public function test_array_found()
    {
        $bag = new QueryBag(["foo" => ["bar"]]);

        $this->assertSame(["bar"], $bag->array("foo"));
    }

    /**
     *
     */
    public function test_array_not_found()
    {
        $bag = new QueryBag([]);

        $this->assertSame([], $bag->array("foo"));
    }

    /**
     *
     */
    public function test_array_with_scalar()
    {
        $bag = new QueryBag(["foo" => "bar"]);

        $this->assertSame([], $bag->array("foo"));
    }
}
