<?php

namespace PoireauFramework\Http\Bag;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Bag
 * @group ext_PoireauFramework_Http_Bag_CookieBag
 */
class CookieBagTest extends TestCase
{
    /**
     *
     */
    public function test_secure()
    {
        $bag = new CookieBag([
            "not_secure" => "my value",
            "__Secure-safe" => "my secure value"
        ]);

        $this->assertNull($bag->secure("not_secure"));
        $this->assertEquals("my secure value", $bag->secure("safe"));
    }

    /**
     *
     */
    public function test_host()
    {
        $bag = new CookieBag([
            "not_host" => "my value",
            "__Host-safe" => "my host value"
        ]);

        $this->assertNull($bag->host("not_host"));
        $this->assertEquals("my host value", $bag->host("safe"));
    }

    /**
     *
     */
    public function test_get_set_secure()
    {
        $bag = new CookieBag();

        $bag->setSecure("foo", "bar");
        $this->assertEquals("bar", $bag->secure("foo"));
    }

    /**
     *
     */
    public function test_get_set_host()
    {
        $bag = new CookieBag();

        $bag->setHost("foo", "bar");
        $this->assertEquals("bar", $bag->host("foo"));
    }
}
