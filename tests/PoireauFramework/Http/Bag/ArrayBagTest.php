<?php

namespace PoireauFramework\Http\Bag;

use PHPUnit\Framework\TestCase;

/**
 * Class ArrayBagTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Bag
 * @group ext_PoireauFramework_Http_Bag_ArrayBag
 */
class ArrayBagTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $bag = new ArrayBag();

        $this->assertEquals([], $bag->raw());
    }

    /**
     *
     */
    public function test_constructor()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $this->assertEquals(['foo' => 'bar'], $bag->raw());
    }

    /**
     *
     */
    public function test_get_null()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $this->assertNull($bag->get('aaa'));
    }

    /**
     *
     */
    public function test_get()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $this->assertEquals('bar', $bag->get('foo'));
    }


    /**
     *
     */
    public function test_has_true()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $this->assertTrue($bag->has('foo'));
    }


    /**
     *
     */
    public function test_has_false()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $this->assertFalse($bag->has('aaa'));
    }


    /**
     *
     */
    public function test_set_override()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $bag->set('foo', 'test');

        $this->assertEquals([
            'foo' => 'test'
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_set_add()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $bag->set('a', 'b');

        $this->assertEquals([
            'foo' => 'bar',
            'a'   => 'b'
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_add_new()
    {
        $bag = new ArrayBag([]);

        $bag->add('foo', 'bar');

        $this->assertEquals([
            'foo' => 'bar'
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_add_on_string()
    {
        $bag = new ArrayBag([
            'foo' => 'bar'
        ]);

        $bag->add('foo', 'test');

        $this->assertEquals([
            'foo' => ['bar', 'test']
        ], $bag->raw());
    }

    /**
     *
     */
    public function test_add_on_array()
    {
        $bag = new ArrayBag([
            'foo' => ['bar', 'test']
        ]);

        $bag->add('foo', 'test2');

        $this->assertEquals([
            'foo' => ['bar', 'test', 'test2']
        ], $bag->raw());
    }
}