<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Security\Crypto\Cypher;
use PoireauFramework\Security\Crypto\CypherSigner;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_Parser
 * @group ext_PoireauFramework_Http_Cookie_Parser_SignedParser
 */
class SignedParserTest extends TestCase
{
    /**
     * @var SignedParser
     */
    private $parser;


    protected function setUp()
    {
        $this->parser = new SignedParser(new CypherSigner(new Cypher(Cypher::AES256_CTR, "my key")));
    }

    /**
     *
     */
    public function test_stringify()
    {
        $input = "Hello World !";
        $this->assertNotEquals($input, $this->parser->stringify($input));
    }

    /**
     *
     */
    public function test_parse()
    {
        $input = "Hello World !";

        $signed = $this->parser->stringify($input);
        $this->assertEquals("Hello World !", $this->parser->parse($signed));
    }

    /**
     *
     */
    public function test_parse_bad_signature()
    {
        $input = "Hello World !";
        $parser = new SignedParser(new CypherSigner(new Cypher(Cypher::AES256_CTR, "other key")));

        $signed = $parser->stringify($input);
        $this->assertNull($this->parser->parse($signed));
    }
}
