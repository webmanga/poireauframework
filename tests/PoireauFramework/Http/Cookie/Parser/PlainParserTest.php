<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_Parser
 * @group ext_PoireauFramework_Http_Cookie_Parser_PlainParser
 */
class PlainParserTest extends TestCase
{
    /**
     *
     */
    public function test_instance()
    {
        $this->assertInstanceOf(PlainParser::class, PlainParser::instance());
        $this->assertSame(PlainParser::instance(), PlainParser::instance());
    }

    /**
     *
     */
    public function test_stringify()
    {
        $parser = new PlainParser();

        $this->assertEquals("Hello World !", $parser->stringify("Hello World !"));
    }

    /**
     *
     */
    public function test_parse()
    {
        $parser = new PlainParser();

        $this->assertEquals("Hello World !", $parser->parse("Hello World !"));
    }
}