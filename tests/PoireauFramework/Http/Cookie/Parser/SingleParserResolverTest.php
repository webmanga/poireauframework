<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_SingleParserResolver
 */
class SingleParserResolverTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $resolver = new SingleParserResolver();

        $this->assertSame(PlainParser::instance(), $resolver->parser(""));
        $this->assertSame(PlainParser::instance(), $resolver->defaultParser());
    }

    /**
     *
     */
    public function test_unit()
    {
        $parser = $this->createMock(ParserInterface::class);
        $resolver = new SingleParserResolver($parser);

        $this->assertSame($parser, $resolver->parser(""));
        $this->assertSame($parser, $resolver->defaultParser());
    }
}
