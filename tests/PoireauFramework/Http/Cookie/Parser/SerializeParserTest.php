<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_Parser
 * @group ext_PoireauFramework_Http_Cookie_Parser_SerializeParser
 */
class SerializeParserTest extends TestCase
{
    /**
     *
     */
    public function test_stringify()
    {
        $parser = new SerializeParser();

        $this->assertEquals('a:1:{s:3:"foo";s:3:"bar";}', $parser->stringify(["foo" => "bar"]));
    }

    /**
     *
     */
    public function test_parse()
    {
        $parser = new SerializeParser();

        $this->assertEquals(["foo" => "bar"], $parser->parse('a:1:{s:3:"foo";s:3:"bar";}'));
    }
}