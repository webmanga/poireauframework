<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_MapParserResolver
 */
class MapParserResolverTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $resolver = new MapParserResolver();

        $this->assertSame(PlainParser::instance(), $resolver->parser(""));
        $this->assertSame(PlainParser::instance(), $resolver->defaultParser());
    }

    /**
     *
     */
    public function test_defaultParser()
    {
        $parser = $this->createMock(ParserInterface::class);
        $resolver = new MapParserResolver($parser);

        $this->assertSame($parser, $resolver->parser(""));
        $this->assertSame($parser, $resolver->defaultParser());
    }

    /**
     *
     */
    public function test_parser_custom()
    {
        $parser = $this->createMock(ParserInterface::class);
        $resolver = new MapParserResolver(null, ["custom" => $parser]);

        $this->assertSame($parser, $resolver->parser("custom"));
    }
}
