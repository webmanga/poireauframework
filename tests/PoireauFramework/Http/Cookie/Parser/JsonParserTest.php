<?php

namespace PoireauFramework\Http\Cookie\Parser;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_Parser
 * @group ext_PoireauFramework_Http_Cookie_Parser_JsonParser
 */
class JsonParserTest extends TestCase
{

    /**
     *
     */
    public function test_stringify()
    {
        $parser = new JsonParser();

        $this->assertEquals('{"foo":"bar"}', $parser->stringify(["foo" => "bar"]));
    }

    /**
     *
     */
    public function test_parse()
    {
        $parser = new JsonParser();

        $this->assertEquals((object) ["foo" => "bar"], $parser->parse('{"foo":"bar"}'));
    }

    /**
     *
     */
    public function test_parse_useAssoc()
    {
        $parser = new JsonParser();
        $parser->useAssoc();

        $this->assertEquals(["foo" => "bar"], $parser->parse('{"foo":"bar"}'));
    }
}