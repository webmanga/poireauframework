<?php

namespace PoireauFramework\Http\Cookie;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Cookie\Parser\ParserInterface;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseStack;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_CookieBuilder
 */
class CookieServiceTest extends TestCase
{
    /**
     * @var CookieService
     */
    private $service;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var ResponseStack
     */
    private $response;

    /**
     *
     */
    protected function setUp()
    {
        $this->response = new ResponseStack();
        $this->response->push(new Response());

        $this->request = new RequestStack(RequestBuilder::fromGlobals()->build());

        $this->service = new CookieService($this->request, $this->response);
    }

    /**
     *
     */
    public function test_create()
    {
        $cookie = $this->service->create("foo", "bar");

        $this->assertInstanceOf(CookieBuilder::class, $cookie);
        $this->assertEquals("foo", $cookie->getName());
        $this->assertEquals("bar", $cookie->getValue());

        $this->assertSame([$cookie], $this->response->headers()->getCookies());
    }

    /**
     *
     */
    public function test_createSecure()
    {
        $cookie = $this->service->createSecure("foo", "bar");

        $this->assertInstanceOf(CookieBuilder::class, $cookie);
        $this->assertEquals("__Secure-foo", $cookie->getName());
        $this->assertEquals("bar", $cookie->getValue());
        $this->assertTrue($cookie->isSecure());

        $this->assertSame([$cookie], $this->response->headers()->getCookies());
    }

    /**
     *
     */
    public function test_createHost()
    {
        $cookie = $this->service->createHost("foo", "bar");

        $this->assertInstanceOf(CookieBuilder::class, $cookie);
        $this->assertEquals("__Host-foo", $cookie->getName());
        $this->assertEquals("bar", $cookie->getValue());
        $this->assertEquals("/", $cookie->getPath());
        $this->assertTrue($cookie->isSecure());
        $this->assertEmpty($cookie->getDomain());

        $this->assertSame([$cookie], $this->response->headers()->getCookies());
    }
}