<?php

namespace PoireauFramework\Http\Cookie;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_Cookie
 * @group ext_PoireauFramework_Http_Cookie_CookieBuilder
 */
class CookieBuilderTest extends TestCase
{
    /**
     * @var CookieBuilder
     */
    private $cookie;


    /**
     *
     */
    protected function setUp()
    {
        $this->cookie = new CookieBuilder("my_cookie", "my_value");
    }

    /**
     *
     */
    public function test_defaults()
    {
        $this->assertEquals("my_cookie", $this->cookie->getName());
        $this->assertEquals("my_value", $this->cookie->getValue());
        $this->assertTrue($this->cookie->isHttpOnly());
        $this->assertTrue($this->cookie->isSecure());
        $this->assertNull($this->cookie->getDomain());
        $this->assertNull($this->cookie->getPath());
        $this->assertNull($this->cookie->getExpires());
        $this->assertNull($this->cookie->getMaxAge());
    }

    /**
     *
     */
    public function test_get_set_value()
    {
        $this->assertSame($this->cookie, $this->cookie->value("new_value"));
        $this->assertEquals("new_value", $this->cookie->getValue());
    }

    /**
     *
     */
    public function test_get_set_domain()
    {
        $this->assertSame($this->cookie, $this->cookie->domain("new_value"));
        $this->assertEquals("new_value", $this->cookie->getDomain());
    }

    /**
     *
     */
    public function test_get_set_path()
    {
        $this->assertSame($this->cookie, $this->cookie->path("new_value"));
        $this->assertEquals("new_value", $this->cookie->getPath());
    }

    /**
     *
     */
    public function test_get_set_secure()
    {
        $this->assertSame($this->cookie, $this->cookie->secure(false));
        $this->assertFalse($this->cookie->isSecure());
    }

    /**
     *
     */
    public function test_get_set_httpOnly()
    {
        $this->assertSame($this->cookie, $this->cookie->httpOnly(false));
        $this->assertFalse($this->cookie->isHttpOnly());
    }

    /**
     *
     */
    public function test_get_set_expires()
    {
        $date = new \DateTime();
        $this->assertSame($this->cookie, $this->cookie->expires($date));
        $this->assertEquals($date, $this->cookie->getExpires());
    }

    /**
     *
     */
    public function test_get_set_maxAge()
    {
        $this->assertSame($this->cookie, $this->cookie->maxAge(123));
        $this->assertEquals(123, $this->cookie->getMaxAge());
    }

    /**
     *
     */
    public function test_get_set_sameSite()
    {
        $this->assertSame($this->cookie, $this->cookie->sameSite(CookieBuilder::SAME_SITE_STRICT));
        $this->assertEquals(CookieBuilder::SAME_SITE_STRICT, $this->cookie->getSameSite());
    }

    /**
     *
     */
    public function test___toString_default()
    {
        $this->assertEquals("my_cookie=my_value; HttpOnly; Secure", (string) $this->cookie);
    }

    /**
     *
     */
    public function test___toString_custom()
    {
        $this->cookie
            ->secure()
            ->httpOnly()
            ->domain("webmanga.fr")
            ->path("/home")
            ->maxAge(7200)
        ;

        $this->assertEquals("my_cookie=my_value; Max-Age=7200; Domain=webmanga.fr; Path=/home; HttpOnly; Secure", (string) $this->cookie);
    }
}