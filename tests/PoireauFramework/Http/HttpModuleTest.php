<?php

namespace PoireauFramework\Http;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Cookie\CookieService;
use PoireauFramework\Http\Cookie\Parser\JsonParser;
use PoireauFramework\Http\Cookie\Parser\ParserInterface;
use PoireauFramework\Http\Cookie\Parser\PlainParser;
use PoireauFramework\Http\Cookie\Parser\SignedParser;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\HttpResponseSender;
use PoireauFramework\Http\Response\ResponseSenderInterface;
use PoireauFramework\Http\Response\ResponseTransformer;
use PoireauFramework\Http\Response\ResponseTransformerInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Security\Crypto\CryptoModule;
use PoireauFramework\Security\Crypto\Cypher;
use PoireauFramework\Security\Crypto\CypherSigner;

/**
 * Class HttpModuleTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Http
 * @group ext_PoireauFramework_Http_HttpModule
 */
class HttpModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;

    /**
     * @var HttpModule
     */
    private $module;

    public function setUp()
    {
        $this->injector = new BaseInjector();
        $this->injector->register($this->module = new HttpModule());
    }

    /**
     *
     */
    public function test_instances()
    {
        $this->assertInstanceOf(RequestStack::class, $this->injector->get(RequestStack::class));
        $this->assertInstanceOf(Request::class, $this->injector->get(RequestInterface::class));
        $this->assertInstanceOf(ResponseTransformer::class, $this->injector->get(ResponseTransformerInterface::class));
        $this->assertInstanceOf(HttpResponseSender::class, $this->injector->get(ResponseSenderInterface::class));
        $this->assertInstanceOf(CookieService::class, $this->injector->get(CookieService::class));
        $this->assertInstanceOf(Request::class, $this->module->createBaseRequest());

        $this->assertSame(PlainParser::instance(), $this->injector->get(PlainParser::class));
    }

    /**
     *
     */
    public function test_create_signed_parser()
    {
        $this->injector->register(new CryptoModule());

        $inner = $this->createMock(ParserInterface::class);

        $parser = $this->injector->create(SignedParser::class, [$inner, (object) ["key" => "abcd"]]);

        $this->assertInstanceOf(SignedParser::class, $parser);
        $this->assertEquals(new SignedParser(new CypherSigner(new Cypher(Cypher::AES256_CTR, "abcd")), $inner), $parser);
    }

    /**
     *
     */
    public function test_create_json_parser()
    {
        $parser = $this->injector->create(JsonParser::class);
        $this->assertInstanceOf(JsonParser::class, $parser);

        $parser = $this->injector->create(JsonParser::class, [(object) ['useAssoc' => true]]);
        $expected = new JsonParser();
        $expected->useAssoc();

        $this->assertEquals($expected, $parser);
    }

    /**
     *
     */
    public function test_create_serialize_parser()
    {
        $parser = $this->injector->create(JsonParser::class);
        $this->assertInstanceOf(JsonParser::class, $parser);
    }
}
