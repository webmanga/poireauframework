<?php
namespace PoireauFramework\Event\Dispatcher;

require_once dirname(__DIR__) . '/TestClasses.php';

use PoireauFramework\Event\AEventListener;
use PoireauFramework\Event\AEventType;
use PoireauFramework\Event\BEventType;
use PoireauFramework\Event\BListener;
use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Event\EventModule;
use PoireauFramework\Event\Exception\InvalidListenerException;
use PoireauFramework\Event\Resolver\EventTypeResolverInterface;
use PoireauFramework\Injector\BaseInjector;
use PHPUnit\Framework\TestCase;

/**
 * Class BaseEventDispatcherTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Event
 * @group ext_PoireauFramework_Event_Dispatcher
 * @group ext_PoireauFramework_Event_Dispatcher_BaseEventDisptacher
 */
class BaseEventDispatcherTest extends TestCase
{
    /**
     * @var TestBaseEventDispatcher
     */
    private $dispatcher;

    protected function setUp()
    {
        parent::setUp();

        $injector = new BaseInjector();
        $injector->register(new EventModule());

        $this->dispatcher = new TestBaseEventDispatcher(
            $injector->get(BaseEventDispatcher::class)
        );
    }

    public function test_addListener_success()
    {
        $listener = new AEventListener();

        $this->dispatcher->addListener(AEventType::class, $listener);

        $this->assertArrayHasKey(AEventType::class, $this->dispatcher->getListeners());
        $this->assertEquals([$listener], $this->dispatcher->getListeners()[AEventType::class]);
    }

    public function test_register()
    {
        $listener = new AEventListener();

        $this->dispatcher->register($listener);

        $this->assertArrayHasKey(AEventType::class, $this->dispatcher->getListeners());
        $this->assertEquals([$listener], $this->dispatcher->getListeners()[AEventType::class]);
    }

    public function test_dispatch_listener_called()
    {
        $listener = new AEventListener();

        $this->dispatcher->dispatch(AEventType::class);

        $this->assertFalse($listener->isCalled);

        $this->dispatcher->addListener(AEventType::class, $listener);
        $this->dispatcher->dispatch(AEventType::class);

        $this->assertTrue($listener->isCalled);
    }

    public function test_dispatch_other_event()
    {
        $listener = new AEventListener();

        $this->dispatcher->addListener(AEventType::class, $listener);
        $this->dispatcher->dispatch(BEventType::class);

        $this->assertFalse($listener->isCalled);
    }

    public function test_dispatch_with_arguments()
    {
        $listener = new AEventListener();
        $arguments = [
            'foo' => 'bar'
        ];

        $this->dispatcher->addListener(AEventType::class, $listener);
        $this->dispatcher->dispatch(AEventType::class, $arguments);

        $this->assertTrue($listener->isCalled);
        $this->assertSame($arguments, $listener->arguments);
    }
}

class TestBaseEventDispatcher extends BaseEventDispatcher {
    /**
     * @var BaseEventDispatcher
     */
    private $delegate;

    /**
     * TestBaseEventDispatcher constructor.
     * @param BaseEventDispatcher $delegate
     */
    public function __construct(BaseEventDispatcher $delegate)
    {
        $this->delegate = $delegate;
    }

    public function addListener(string $event, $listener): void
    {
        $this->delegate->addListener($event, $listener);
    }

    public function register(EventListenerInterface $listener): void
    {
        $this->delegate->register($listener);
    }

    public function dispatch(string $event, array $arguments = array()): void
    {
        $this->delegate->dispatch($event, $arguments);
    }

    protected function resolver(): EventTypeResolverInterface
    {
        return $this->delegate->resolver();
    }

    public function getListeners()
    {
        return $this->delegate->listeners;
    }
}