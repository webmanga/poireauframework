<?php
namespace PoireauFramework\Event;

use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Event\Type\EventTypeInterface;

class AEventType implements EventTypeInterface {

    /**
     * Get the listener interface name
     *
     * @return string interface name
     */
    public function supports($listener): bool
    {
        return $listener instanceof AEventListenerInterface;
    }

    /**
     * Execute the listener
     *
     * @param object $listener The event listener. Should implements {@link EventTypeInterface::listenerInterface()}
     * @param array $arguments The listener arguments
     */
    public function execute($listener, array $arguments): void
    {
        $listener->onA($arguments);
    }
}

interface AEventListenerInterface {
    public function onA(array $arguments);
}

class AEventListener implements AEventListenerInterface, EventListenerInterface {
    public $arguments;
    public $isCalled = false;

    public function eventTypes(): array
    {
        return [AEventType::class];
    }

    public function onA(array $arguments)
    {
        $this->isCalled = true;
        $this->arguments = $arguments;
    }
}

class BEventType implements EventTypeInterface {
    public function supports($listener): bool
    {
        return $listener instanceof BEventListenerInterface;
    }

    public function execute($listener, array $arguments): void
    {
        $listener->onB();
    }
}

interface BEventListenerInterface {
    public function onB();
}

class BListener implements BEventListenerInterface {
    public function onB() {
    }
}