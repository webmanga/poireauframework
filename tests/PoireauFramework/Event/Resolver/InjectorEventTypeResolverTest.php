<?php
namespace PoireauFramework\Event\Resolver;

require_once dirname(__DIR__) . '/TestClasses.php';

use PoireauFramework\Event\AEventType;
use PoireauFramework\Event\BEventType;
use PoireauFramework\Event\EventModule;
use PoireauFramework\Event\Exception\ResolverException;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Injector\Exception\InjectorException;
use PHPUnit\Framework\TestCase;

/**
 * Class InjectorEventTypeResolverTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Event
 * @group ext_PoireauFramework_Event_Resolver
 * @group ext_PoireauFramework_Event_Resolver_InjectorEventTypeResolver
 */
class InjectorEventTypeResolverTest extends TestCase
{
    public function test_resolve_injector_event_not_found()
    {
        $injector = $this->createMock(BaseInjector::class);
        $injector->expects($this->once())
            ->method('get')
            ->with('NotFoundEventType')
            ->willThrowException(new InjectorException())
        ;

        $resolver = new InjectorEventTypeResolver($injector);

        $this->expectException(ResolverException::class);

        $resolver->resolve('NotFoundEventType');
    }

    public function test_resolve_injector_not_event_type()
    {
        $injector = $this->createMock(BaseInjector::class);
        $injector->expects($this->once())
            ->method('get')
            ->with(NotAnEventType::class)
            ->willReturn(new NotAnEventType())
        ;

        $resolver = new InjectorEventTypeResolver($injector);

        $this->expectException(ResolverException::class);
        $this->expectExceptionMessage('Cannot resolve event type \'' . NotAnEventType::class . '\'');

        $resolver->resolve(NotAnEventType::class);
    }

    public function test_resolve_injector_not_event_success()
    {
        $injector = $this->createMock(BaseInjector::class);
        $injector->expects($this->once())
            ->method('get')
            ->with(AEventType::class)
            ->willReturn(new AEventType())
        ;

        $resolver = new InjectorEventTypeResolver($injector);

        $this->assertInstanceOf(AEventType::class, $resolver->resolve(AEventType::class));
    }

    public function test_resolver_functional()
    {
        $injector = new BaseInjector();
        $injector->register(new EventModule());

        $resolver = $injector->get(EventTypeResolverInterface::class);

        $this->assertInstanceOf(AEventType::class, $resolver->resolve(AEventType::class));
        $this->assertInstanceOf(BEventType::class, $resolver->resolve(BEventType::class));
    }
}

class NotAnEventType {

}