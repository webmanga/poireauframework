<?php

namespace PoireauFramework\Util;

use PHPUnit\Framework\TestCase;

/**
 * Class ArraysTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Util
 * @group ext_PoireauFramework_Util_Arrays
 */
class ArraysTest extends TestCase
{
    /**
     *
     */
    public function test_isSequential()
    {
        $this->assertTrue(Arrays::isSequential([]));
        $this->assertTrue(Arrays::isSequential([1, "hello", true]));
        $this->assertFalse(Arrays::isSequential(["name" => "John"]));
        $this->assertFalse(Arrays::isSequential([
            5 => 1,
            8 => 2
        ]));
    }
}