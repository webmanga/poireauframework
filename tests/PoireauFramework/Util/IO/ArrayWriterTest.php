<?php

namespace PoireauFramework\Util\IO;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Util
 * @group ext_PoireauFramework_Util_IO
 * @group ext_PoireauFramework_Util_IO_ArrayWriter
 */
class ArrayWriterTest extends TestCase
{
    /**
     * @var ArrayWriter
     */
    private $writer;

    /**
     *
     */
    protected function setUp()
    {
        $this->writer = new ArrayWriter();
    }

    /**
     *
     */
    public function test_write()
    {
        $this->writer->write("Hello World !");

        $this->assertEquals(["Hello World !"], $this->writer->lines());
    }

    /**
     *
     */
    public function test_newLine()
    {
        $this->writer->newLine();

        $this->assertEquals(["", ""], $this->writer->lines());
    }

    /**
     *
     */
    public function test_writeln()
    {
        $this->writer->writeln("Hello World !");

        $this->assertEquals(["Hello World !", ""], $this->writer->lines());
    }
}
