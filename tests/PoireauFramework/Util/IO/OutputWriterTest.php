<?php

namespace PoireauFramework\Util\IO;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Util
 * @group ext_PoireauFramework_Util_IO
 * @group ext_PoireauFramework_Util_IO_OutputWriter
 */
class OutputWriterTest extends TestCase
{
    /**
     * @var OutputWriter
     */
    private $writer;

    /**
     *
     */
    protected function setUp()
    {
        $this->writer = new OutputWriter();
    }

    /**
     *
     */
    public function test_write()
    {
        $this->expectOutputString("Hello World !");

        $this->writer->write("Hello World !");
    }

    /**
     *
     */
    public function test_newLine()
    {
        $this->expectOutputString(PHP_EOL);

        $this->writer->newLine();
    }

    /**
     *
     */
    public function test_writeln()
    {
        $this->expectOutputString("Hello World !" . PHP_EOL);

        $this->writer->writeln("Hello World !");
    }
}
