<?php

namespace PoireauFramework\Util\File;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Util\File\Exception\ParseFileException;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Util
 * @group ext_PoireauFramework_Util_File
 * @group ext_PoireauFramework_Util_File_ClassFile
 */
class ClassFileTest extends TestCase
{
    /**
     * @dataProvider provideParseNamespace
     */
    public function test_parse_namespace($filename, array $namespace)
    {
        $file = new ClassFile($filename);

        $this->assertEquals($namespace, $file->getNamespace());
    }

    public function provideParseNamespace()
    {
        return [
            "simple"           => [__DIR__."/_files/MyClassWithNamespace.php", ["Test", "My", "ClassNs"]],
            "no_namespace"     => [__DIR__."/_files/NoNamespace.php", []],
            "already_included" => [__FILE__, ["PoireauFramework", "Util", "File"]],
        ];
    }

    /**
     * @dataProvider provideParseClass
     */
    public function test_parse_class($filename, $class, $type)
    {
        $file = new ClassFile($filename);

        $this->assertEquals($class, $file->getClassName());
        $this->assertEquals($type, $file->getClassInfo(ClassFile::CLASS_INFO_TYPE));
    }

    public function provideParseClass()
    {
        return [
            "simple"           => [__DIR__."/_files/MyClassWithNamespace.php", "MyClassWithNamespace", ClassFile::CLASS_TYPE_CLASS],
            "interface"        => [__DIR__."/_files/MyInterface.php", "MyInterface", ClassFile::CLASS_TYPE_INTERFACE],
            "abstract"         => [__DIR__."/_files/MyAbstractClass.php", "MyAbstractClass", ClassFile::CLASS_TYPE_ABSTRACT],
            "trait"            => [__DIR__."/_files/MyTrait.php", "MyTrait", ClassFile::CLASS_TYPE_TRAIT],
            "already_included" => [__FILE__, "ClassFileTest", ClassFile::CLASS_TYPE_CLASS],
            "no_namespace"     => [__DIR__."/_files/NoNamespace.php", "NoNamespace", ClassFile::CLASS_TYPE_CLASS],
        ];
    }

    /**
     *
     */
    public function test_error_on_multiple_namespaces()
    {
        $this->expectException(ParseFileException::class);
        $this->expectExceptionMessage("Cannot handle more than one namespace declaration");

        new ClassFile(__DIR__ . "/_files/multiple_namespace.php");
    }

    /**
     *
     */
    public function test_error_on_multiple_classes()
    {
        $this->expectException(ParseFileException::class);
        $this->expectExceptionMessage("Cannot handle more than one class definition per file");

        new ClassFile(__DIR__ . "/_files/multiple_classes.php");
    }

    /**
     *
     */
    public function test_getFullClassName()
    {
        $file = new ClassFile(__DIR__ . "/_files/MyClassWithNamespace.php");

        $this->assertEquals("Test\\My\\ClassNs\\MyClassWithNamespace", $file->getFullClassName());
    }

    /**
     *
     */
    public function test_no_class_def()
    {
        $file = new ClassFile(__DIR__ . "/_files/no_class.php");

        $this->assertFalse($file->hasClassDefinition());
    }

    /**
     *
     */
    public function test_isAbstract()
    {
        $file = new ClassFile(__DIR__ . "/_files/MyAbstractClass.php");

        $this->assertTrue($file->isAbstract());
    }

    /**
     *
     */
    public function test_isInterface()
    {
        $file = new ClassFile(__DIR__ . "/_files/MyInterface.php");

        $this->assertTrue($file->isInterface());
    }

    /**
     *
     */
    public function test_isTrait()
    {
        $file = new ClassFile(__DIR__ . "/_files/MyTrait.php");

        $this->assertTrue($file->isTrait());
    }

    /**
     *
     */
    public function test_isLoaded()
    {
        $file = new ClassFile(__DIR__ . "/_files/MyTrait.php");
        $this->assertFalse($file->isLoaded());

        $file = new ClassFile(__FILE__);
        $this->assertTrue($file->isLoaded());
    }
}
