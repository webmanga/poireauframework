<?php

namespace PoireauFramework\Util\File;

use C2;
use C311;
use C321;
use Ns\C1\C1;
use Ns\C1\f2\C3;
use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\C;
use Test\My\ClassNs\MyAbstractClass;
use Test\My\ClassNs\MyClassWithNamespace;
use Test\My\ClassNs\MyInterface;
use Test\My\ClassNs\MyTrait;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Util
 * @group ext_PoireauFramework_Util_File
 * @group ext_PoireauFramework_Util_File_ClassFileIterator
 */
class ClassFileIteratorTest extends TestCase
{
    /**
     *
     */
    public function test_with_custom_iterator()
    {
        $iterator = new ClassFileIterator(new \DirectoryIterator(__DIR__ . "/_files/"));

        $classes = [];

        foreach ($iterator as $class) {
            $this->assertInstanceOf(ClassFile::class, $class);
            $this->assertTrue($class->hasClassDefinition());

            $classes[$class->getFullClassName()] = $class;
        }

        $this->assertCount(5, $classes);

        $this->assertArrayHasKey(MyAbstractClass::class, $classes);
        $this->assertArrayHasKey(MyClassWithNamespace::class, $classes);
        $this->assertArrayHasKey(MyInterface::class, $classes);
        $this->assertArrayHasKey(MyTrait::class, $classes);
        $this->assertArrayHasKey(\NoNamespace::class, $classes);
    }

    /**
     *
     */
    public function test_directory()
    {
        $iterator = ClassFileIterator::directory(__DIR__ . "/_files");
        $this->assertInstanceOf(ClassFileIterator::class, $iterator);

        $classes = [];

        foreach ($iterator as $class) {
            $this->assertInstanceOf(ClassFile::class, $class);
            $this->assertTrue($class->hasClassDefinition());

            $classes[$class->getFullClassName()] = $class;
        }

        $this->assertCount(5, $classes);

        $this->assertArrayHasKey(MyAbstractClass::class, $classes);
        $this->assertArrayHasKey(MyClassWithNamespace::class, $classes);
        $this->assertArrayHasKey(MyInterface::class, $classes);
        $this->assertArrayHasKey(MyTrait::class, $classes);
        $this->assertArrayHasKey(\NoNamespace::class, $classes);
    }

    /**
     *
     */
    public function test_recursive()
    {
        $iterator = ClassFileIterator::recursive(__DIR__ . "/_files/f1");
        $this->assertInstanceOf(ClassFileIterator::class, $iterator);

        $classes = [];

        foreach ($iterator as $class) {
            $this->assertInstanceOf(ClassFile::class, $class);
            $this->assertTrue($class->hasClassDefinition());

            $classes[$class->getFullClassName()] = $class;
        }

        $this->assertCount(5, $classes);

        $this->assertArrayHasKey(C1::class, $classes);
        $this->assertArrayHasKey(C2::class, $classes);
        $this->assertArrayHasKey(C311::class, $classes);
        $this->assertArrayHasKey(C3::class, $classes);
        $this->assertArrayHasKey(C321::class, $classes);
    }
}
