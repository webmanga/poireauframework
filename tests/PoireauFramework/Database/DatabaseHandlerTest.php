<?php

namespace PoireauFramework\Database;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_DatabaseHandler
 */
class DatabaseHandlerTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $factory = $this->createMock(ConnectionFactoryInterface::class);
        $handler = new DatabaseHandler($factory);

        $this->assertSame($factory, $handler->factory());
    }

    /**
     *
     */
    public function test_getConnection_unit()
    {
        $factory = $this->createMock(ConnectionFactoryInterface::class);
        $handler = new DatabaseHandler($factory);

        $connection = $this->createMock(ConnectionInterface::class);

        $factory->expects($this->once())
            ->method("createConnection")
            ->with("my_connection")
            ->willReturn($connection)
        ;

        $this->assertSame($connection, $handler->getConnection("my_connection"));
        $this->assertSame($handler->getConnection("my_connection"), $handler->getConnection("my_connection"));
    }
}