<?php

namespace PoireauFramework\Database\Testing;

require_once __DIR__."/../Mapping/_files/User.php";
require_once __DIR__."/../Mapping/_files/UserMapper.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Resolver\MapperResolverInterface;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Test\UserMapper;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group PoireauFramework
 * @group PoireauFramework_Database
 * @group PoireauFramework_Database_Testing
 * @group PoireauFramework_Database_Testing_DataSet
 */
class DataSetTest extends TestCase
{
    /**
     * @var DataSet
     */
    private $dataSet;

    /**
     * @var SqlConnectionInterface
     */
    private $connection;

    /**
     * @var BaseInjector
     */
    private $injector;


    /**
     *
     */
    protected function setUp()
    {
        $this->injector = new BaseInjector(RegistryFactory::createFromArray([
            "database" => [
                "default_connection" => "test"
            ]
        ]));

        $this->injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "type"   => "sqlite",
                    "memory" => true
                ]
            ]
        ])));

        $this->injector->register(new DatabaseModule());

        $this->dataSet = new DataSet(
            $this->injector->get(MapperResolverInterface::class)
        );

        $this->connection = $this->injector->get(DatabaseHandler::class)->getConnection("test");
    }

    protected function tearDown()
    {
        $this->dataSet->destroy();
    }

    /**
     *
     */
    public function test_declare_string()
    {
        $this->dataSet->declare(UserMapper::class);

        $this->assertTableExists("USER");
    }

    /**
     *
     */
    public function test_declare_mapper_object()
    {
        $mapper = new UserMapper(
            $this->connection,
            $metadata = new Metadata(),
            new MetadataHydrator($metadata, $this->connection->platform()->types())
        );

        $this->dataSet->declare($mapper);

        $this->assertTableExists("USER");
    }

    /**
     *
     */
    public function test_declare_array()
    {
        $this->dataSet->declare([UserMapper::class]);

        $this->assertTableExists("USER");
    }

    /**
     *
     */
    public function test_declare_twice()
    {
        $this->dataSet
            ->declare(UserMapper::class)
            ->declare(UserMapper::class)
        ;

        $this->assertTableExists("USER");
    }

    /**
     *
     */
    public function test_destroy_will_drop_tables()
    {
        $this->dataSet->declare(UserMapper::class);

        $this->assertTableExists("USER");

        $this->dataSet->destroy();

        $this->assertTableNotExists("USER");
    }

    /**
     *
     */
    public function test_mapper_not_found()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Cannot found mapper for ".User::class);

        $this->dataSet->mapper(User::class);
    }

    /**
     *
     */
    public function test_mapper_declared()
    {
        $this->dataSet->declare(UserMapper::class);

        $this->assertInstanceOf(UserMapper::class, $this->dataSet->mapper(User::class));
    }

    /**
     *
     */
    public function test_destroy_will_remove_mapper()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Cannot found mapper for ".User::class);

        $this->dataSet
            ->declare(UserMapper::class)
            ->destroy()
        ;

        $this->dataSet->mapper(User::class);
    }

    /**
     *
     */
    public function test_set_with_name()
    {
        $user = new User();

        $user
            ->setId(123)
            ->setUsername("username")
            ->setEmail("user@webmanga.fr")
            ->setPassword("my password")
        ;

        $this->dataSet
            ->declare(UserMapper::class)
            ->set("user", $user)
        ;

        $this->assertEquals($user, $this->dataSet->get("user"));
        $this->assertEquals($user, $this->dataSet->mapper(User::class)->get(123));
    }

    /**
     *
     */
    public function test_set_no_name()
    {
        $user = new User();

        $user
            ->setId(123)
            ->setUsername("username")
            ->setEmail("user@webmanga.fr")
            ->setPassword("my password")
        ;

        $this->dataSet
            ->declare(UserMapper::class)
            ->set($user)
        ;

        $this->assertEquals($user, $this->dataSet->mapper(User::class)->get(123));
    }

    /**
     *
     */
    public function test_set_array()
    {
        $user1 = new User();

        $user1
            ->setId(123)
            ->setUsername("username")
            ->setEmail("user@webmanga.fr")
            ->setPassword("my password")
        ;

        $user2 = new User();

        $user2
            ->setId(321)
            ->setUsername("user2")
            ->setEmail("user2@webmanga.fr")
            ->setPassword("my password")
        ;

        $user3 = new User();

        $user3
            ->setId(741)
            ->setUsername("user3")
            ->setEmail("user3@webmanga.fr")
            ->setPassword("my password")
        ;

        $this->dataSet
            ->declare(UserMapper::class)
            ->set([
                "user1" => $user1,
                $user2, $user3
            ])
        ;

        $this->assertEquals($user1, $this->dataSet->get("user1"));
        $this->assertEquals([$user1, $user2, $user3], $this->dataSet->mapper(User::class)->all());
    }

    /**
     *
     */
    public function test_set_with_autoincrement()
    {
        $user = new User();

        $user
            ->setUsername("username")
            ->setEmail("user@webmanga.fr")
            ->setPassword("my password")
        ;

        $this->dataSet
            ->declare(UserMapper::class)
            ->set("user", $user)
        ;

        $this->assertEquals(1, $this->dataSet->get("user")->id());
    }

    /**
     * @param string $tableName
     */
    protected function assertTableExists($tableName)
    {
        $query = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";

        $result = $this->connection->query($query, [$tableName])->fetch();

        $this->assertTrue($result !== false && $result["name"] === $tableName);
    }

    /**
     * @param string $tableName
     */
    protected function assertTableNotExists($tableName)
    {
        $query = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";

        $result = $this->connection->query($query, [$tableName])->fetch();

        $this->assertFalse($result);
    }
}
