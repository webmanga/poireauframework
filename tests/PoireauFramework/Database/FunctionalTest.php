<?php

namespace PoireauFramework\Database;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Functional
 */
class FunctionalTest extends TestCase
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var DatabaseHandler
     */
    private $handler;

    private $data;


    protected function setUp()
    {
        $this->pdo = new \PDO("sqlite::memory:", null, null, [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_PERSISTENT => true
        ]);

        $this->pdo->query(<<<SQL
CREATE TABLE TEST_TABLE (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    KEY TEXT UNIQUE,
    VALUE BLOB
)
SQL
        );

        $stmt = $this->pdo->prepare("INSERT INTO TEST_TABLE(ID, KEY, VALUE) VALUES(:ID, :KEY, :VALUE)");

        $this->data = [
            [
                "ID" => 1,
                "KEY" => "foo",
                "VALUE" => "BAR"
            ],
            [
                "ID" => 2,
                "KEY" => "name",
                "VALUE" => "John"
            ]
        ];

        foreach ($this->data as $data) {
            $stmt->execute($data);
        }

        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "dsn" => "sqlite::memory:"
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        $this->handler = $injector->get(DatabaseHandler::class);
    }

    protected function tearDown()
    {
        $this->pdo->query("DROP TABLE TEST_TABLE");
    }

    /**
     *
     */
    public function test_select()
    {
        $data = $this->handler->getConnection("test")->query("SELECT * FROM TEST_TABLE")->all();

        $this->assertEquals($data, $this->data);
    }

    /**
     *
     */
    public function test_insert()
    {
        /** @var SqlConnectionInterface $connection */
        $connection = $this->handler->getConnection("test");

        $connection->query("INSERT INTO TEST_TABLE(KEY, VALUE) VALUES(?,?)", ["test", "value"]);

        $row = $connection->query("SELECT VALUE FROM TEST_TABLE WHERE KEY = ?", ["test"])->fetch();

        $this->assertEquals($row["VALUE"], "value");
    }

    public function test_update_prepare()
    {
        /** @var SqlConnectionInterface $connection */
        $connection = $this->handler->getConnection("test");

        $stmt = $connection->prepare("UPDATE TEST_TABLE SET VALUE = ? WHERE KEY = ?");

        $stmt->execute(["BAZ", "foo"]);
        $stmt->execute(["John Doe", "name"]);

        $this->assertEquals([
            [
                "ID" => 1,
                "KEY" => "foo",
                "VALUE" => "BAZ"
            ],
            [
                "ID" => 2,
                "KEY" => "name",
                "VALUE" => "John Doe"
            ]
        ], $connection->query("SELECT * FROM TEST_TABLE")->all());
    }
}