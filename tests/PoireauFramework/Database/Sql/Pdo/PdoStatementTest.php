<?php

namespace PoireauFramework\Database\Sql\Pdo;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\SqlStatementInterface;

/**
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Sql
 * @group ext_PoireauFramework_Database_Sql_Pdo
 * @group ext_PoireauFramework_Database_Sql_Pdo_PdoStatement
 */
class PdoStatementTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $connection = $this->connection();
        $query = "SELECT * FROM TEST_TABLE";

        $stmt = new PdoStatement($query, $connection, $this->createMock(\PDOStatement::class));

        $this->assertEquals($query, $stmt->query());
        $this->assertSame($connection, $stmt->connection());
    }

    /**
     *
     */
    public function test_execute()
    {
        $connection = $this->connection();
        $query = "SELECT * FROM TEST_TABLE";
        $pdo = $this->createMock(\PDOStatement::class);

        $stmt = new PdoStatement($query, $connection, $pdo);

        $pdo->expects($this->once())
            ->method("execute")
            ->with(["foo" => "bar"])
            ->willReturn(true)
        ;

        $this->assertTrue($stmt->execute(["foo" => "bar"]));
    }

    /**
     *
     */
    public function test_execute_error()
    {
        $this->expectException(SqlException::class);
        $this->expectExceptionMessage("[test_connect] Error on query : 'SELECT 1 + 1' : SQLSTATE[HY000]: General error: 25");

        $connection = $this->connection();
        $query = "SELECT 1 + 1";

        $stmt = new PdoStatement($query, $connection, $connection->native()->prepare($query));

        $stmt->execute(['foo' => 'bar']);
    }

    /**
     *
     */
    public function test_fetch()
    {
        $connection = $this->connection();
        $query = "SELECT * FROM TEST_TABLE";
        $pdo = $this->createMock(\PDOStatement::class);

        $stmt = new PdoStatement($query, $connection, $pdo);

        $pdo->expects($this->once())
            ->method('fetch')
            ->willReturn(['foo' => 'bar'])
        ;

        $this->assertEquals(['foo' => 'bar'], $stmt->fetch());
    }

    /**
     *
     */
    public function test_all()
    {
        $connection = $this->connection();
        $query = "SELECT * FROM TEST_TABLE";
        $pdo = $this->createMock(\PDOStatement::class);

        $stmt = new PdoStatement($query, $connection, $pdo);

        $pdo->expects($this->once())
            ->method('fetchAll')
            ->willReturn([['foo' => 'bar']])
        ;

        $this->assertEquals([['foo' => 'bar']], $stmt->all());
    }

    /**
     * @dataProvider bindProvider
     */
    public function test_bind($query, $bindKey, $bindValue, $bindType, $result)
    {
        $connection = $this->connection();
        $connection->connect();

        $connection->query(<<<SQL
CREATE TABLE PERSON (
    FIRST_NAME TEXT,
    LAST_NAME  TEXT,
    AGE        INTEGER
);
SQL
);
        $connection->query("INSERT INTO PERSON VALUES ('John', 'Doe', 45), ('Albert', 'Retuin', 85), ('Paul', 'Asduio', 25), (NULL, NULL, NULL)");

        $stmt = new PdoStatement($query, $connection, $connection->native()->prepare($query));
        $stmt->bind($bindKey, $bindValue, $bindType);
        $stmt->execute();

        $all = $stmt->all();

        $this->assertEquals($result, $all);
    }

    /**
     *
     */
    public function bindProvider()
    {
        return [
            'int'  => ["SELECT * FROM PERSON LIMIT ?", 1, 1, SqlStatementInterface::TYPE_INT,                              [["FIRST_NAME" => "John", "LAST_NAME" => "Doe", "AGE" => 45]]],
            'str'  => ["SELECT * FROM PERSON WHERE FIRST_NAME = :name", "name", "Albert", SqlStatementInterface::TYPE_STR, [["FIRST_NAME" => "Albert", "LAST_NAME" => "Retuin", "AGE" => 85]]],
            'null' => ["SELECT * FROM PERSON WHERE AGE IS ?", 1, "???", SqlStatementInterface::TYPE_NULL,                  [["FIRST_NAME" => null, "LAST_NAME" => null, "AGE" => null]]],
            'auto' => ["SELECT * FROM PERSON LIMIT ?", 1, 1, SqlStatementInterface::TYPE_AUTO,                             [["FIRST_NAME" => "John", "LAST_NAME" => "Doe", "AGE" => 45]]],
        ];
    }

    /**
     * @return PdoConnection
     */
    protected function connection()
    {
        $config = new PdoConfigStruct();

        $config->name = 'test_connect';
        $config->dsn = "sqlite::memory:";

        $connection = new PdoConnection($config, new SQLitePlatform(new TypeRegistry()));

        $connection->connect();

        return $connection;
    }
}