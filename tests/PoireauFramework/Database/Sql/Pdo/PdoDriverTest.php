<?php

namespace PoireauFramework\Database\Sql\Pdo;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Database\Exception\DatabaseConfigException;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformRegistry;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Sql
 * @group ext_PoireauFramework_Database_Sql_Pdo
 * @group ext_PoireauFramework_Database_Sql_Pdo_PdoDriver
 */
class PdoDriverTest extends TestCase
{
    /**
     * @var PdoDriver
     */
    private $driver;

    /**
     * @var \PDO
     */
    private $pdo;


    /**
     *
     */
    protected function setUp()
    {
        $platforms = new SqlPlatformRegistry(new TypeRegistry());
        $platforms->register(new SQLitePlatform(new TypeRegistry()));

        $this->driver = new PdoDriver($platforms);


        $this->pdo = new \PDO("sqlite::memory:", null, null, [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_PERSISTENT => true
        ]);

        $this->pdo->query(<<<SQL
CREATE TABLE TEST_TABLE (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    KEY TEXT UNIQUE,
    VALUE BLOB
)
SQL
        );
    }

    protected function tearDown()
    {
        $this->pdo->query("DROP TABLE TEST_TABLE");
    }

    /**
     *
     */
    public function test_name()
    {
        $this->assertEquals("pdo", $this->driver->name());
    }

    /**
     *
     */
    public function test_configure()
    {
        $connection = $this->driver->configure(new ConfigItem(new Registry([
            "dsn" => "sqlite::memory:"
        ])));

        $this->assertInstanceOf(PdoConnection::class, $connection);

        $connection->connect();

        $this->assertCount(1, $connection->query("SELECT * FROM sqlite_master WHERE type='table' AND name='TEST_TABLE'")->all());
    }

    /**
     *
     */
    public function test_resolveDsn_explicit()
    {
        $config = new ConfigItem(new Registry([
            "dsn" => "mysql:host=145.45.78.78;dbname=MY_DB"
        ]));

        $this->assertEquals("mysql:host=145.45.78.78;dbname=MY_DB", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_sqlite_memory()
    {
        $config = new ConfigItem(new Registry([
            "type" => "sqlite",
            "memory" => true
        ]));

        $this->assertEquals("sqlite::memory:", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_sqlite_path()
    {
        $config = new ConfigItem(new Registry([
            "type" => "sqlite",
            "path" => "my/path/to/db.sqlite"
        ]));

        $this->assertEquals("sqlite:my/path/to/db.sqlite", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_mysql_no_host()
    {
        $config = new ConfigItem(new Registry([
            "type"   => "mysql",
            "dbname" => "MY_DB"
        ]));

        $this->assertEquals("mysql:host=127.0.0.1;dbname=MY_DB", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_mysql_classic()
    {
        $config = new ConfigItem(new Registry([
            "type"   => "mysql",
            "host"   => "145.45.78.78",
            "dbname" => "MY_DB"
        ]));

        $this->assertEquals("mysql:host=145.45.78.78;dbname=MY_DB", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_mysql_extended()
    {
        $config = new ConfigItem(new Registry([
            "type"   => "mysql",
            "host"   => "145.45.78.78",
            "port"   => 1452,
            "charset" => "UTF8",
            "dbname" => "MY_DB"
        ]));

        $this->assertEquals("mysql:host=145.45.78.78;port=1452;charset=UTF8;dbname=MY_DB", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_mysql_socket()
    {
        $config = new ConfigItem(new Registry([
            "type"   => "mysql",
            "unix_socket" => "my/database.sock",
            "dbname" => "MY_DB"
        ]));

        $this->assertEquals("mysql:unix_socket=my/database.sock;dbname=MY_DB", $this->driver->resolveDsn($config));
    }

    /**
     *
     */
    public function test_resolveDsn_mysql_error_no_dbname()
    {
        $this->expectException(DatabaseConfigException::class);
        $this->expectExceptionMessage("You should specify a dbname for MySQL connection");

        $config = new ConfigItem(new Registry([
            "type"   => "mysql",
        ]));

        $this->driver->resolveDsn($config);
    }

    /**
     *
     */
    public function test_resolveDsn_sqlite_error_no_db()
    {
        $this->expectException(DatabaseConfigException::class);
        $this->expectExceptionMessage("You should specify a path (or memory) for SQLite connection");

        $config = new ConfigItem(new Registry([
            "type"   => "sqlite",
        ]));

        $this->driver->resolveDsn($config);
    }

    /**
     *
     */
    public function test_resolveDsn_error_unknown_type()
    {
        $this->expectException(DatabaseConfigException::class);
        $this->expectExceptionMessage("Unknown database type 'unknown'");

        $config = new ConfigItem(new Registry([
            "type"   => "unknown",
        ]));

        $this->driver->resolveDsn($config);
    }

    /**
     *
     */
    public function test_resolveOptions_persistent()
    {
        $config = new ConfigItem(RegistryFactory::createFromArray([
            "type"   => "unknown",
            "options" => [
                "persistent" => true
            ]
        ]));

        $struct = new PdoConfigStruct();

        $this->assertEquals([
            \PDO::ATTR_PERSISTENT         => true,
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ], $this->driver->resolveOptions($config, $struct));
    }

    /**
     *
     */
    public function test_resolveOptions_sqlite_memory()
    {
        $config = new ConfigItem(new Registry([
            "type"   => "sqlite",
        ]));

        $struct = new PdoConfigStruct();

        $struct->type = PdoConfigStruct::TYPE_SQLITE;
        $struct->dsn  = "sqlite::memory:";

        $this->assertEquals([
            \PDO::ATTR_PERSISTENT         => true,
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ], $this->driver->resolveOptions($config, $struct));
    }
}
