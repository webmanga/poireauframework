<?php

namespace PoireauFramework\Database\Sql\Pdo;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Exception\DatabaseConnectionException;
use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Sql
 * @group ext_PoireauFramework_Database_Sql_Pdo
 * @group ext_PoireauFramework_Database_Sql_Pdo_PdoConnection
 */
class PdoConnectionTest extends TestCase
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var array
     */
    private $data;


    protected function setUp()
    {
        $this->pdo = new \PDO("sqlite::memory:", null, null, [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_PERSISTENT => true
        ]);

        $this->pdo->query(<<<SQL
CREATE TABLE TEST_TABLE (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    KEY TEXT UNIQUE,
    VALUE BLOB
)
SQL
);
        $stmt = $this->pdo->prepare("INSERT INTO TEST_TABLE(ID, KEY, VALUE) VALUES(:ID, :KEY, :VALUE)");

        $this->data = [
            [
                "ID" => 1,
                "KEY" => "foo",
                "VALUE" => "BAR"
            ],
            [
                "ID" => 2,
                "KEY" => "name",
                "VALUE" => "John"
            ]
        ];

        foreach ($this->data as $data) {
            $stmt->execute($data);
        }
    }

    protected function tearDown()
    {
        $this->pdo->query("DROP TABLE TEST_TABLE");
    }

    /**
     *
     */
    public function test_platform()
    {
        $this->assertInstanceOf(SQLitePlatform::class, $this->connection()->platform());
    }

    /**
     *
     */
    public function test_connect_success()
    {
        $config = new PdoConfigStruct();

        $config->name = 'test_connect';
        $config->dsn = "sqlite::memory:";

        $connection = new PdoConnection($config, new SQLitePlatform(new TypeRegistry()));

        $connection->connect();

        $this->assertInstanceOf(\PDO::class, $connection->native());
    }

    /**
     *
     */
    public function test_connect_error()
    {
        $this->expectException(DatabaseConnectionException::class);
        $this->expectExceptionMessage("Could not connect to database 'test_connect'");

        $config = new PdoConfigStruct();

        $config->name = 'test_connect';
        $config->dsn = "qdqdd:bad_dsn";

        $connection = new PdoConnection($config, new SQLitePlatform(new TypeRegistry()));

        $connection->connect();
    }

    /**
     *
     */
    public function test_query_select_simple()
    {
        $stmt = $this->connection()->query("SELECT * FROM TEST_TABLE");

        $this->assertInstanceOf(PdoStatement::class, $stmt);

        $this->assertEquals($this->data, $stmt->all());
    }

    /**
     *
     */
    public function test_query_select_prepared()
    {
        $stmt = $this->connection()->query("SELECT * FROM TEST_TABLE WHERE KEY = ?", ["foo"]);

        $this->assertInstanceOf(PdoStatement::class, $stmt);

        $this->assertEquals([$this->data[0]], $stmt->all());
    }

    /**
     *
     */
    public function test_lastInsertedId()
    {
        $this->connection()->query("INSERT INTO TEST_TABLE(KEY, VALUE) VALUES('hello', 'world')");

        $this->assertEquals(3, $this->connection()->lastInsertedId());
    }

    /**
     *
     */
    public function test_query_select_error()
    {
        $this->expectException(SqlException::class);
        $this->expectExceptionMessage("[test_connect] Error on query : 'INVALID SQL' : SQLSTATE[HY000]: General error: 1 near \"INVALID\": syntax error");

        $this->connection()->query("INVALID SQL");
    }

    /**
     *
     */
    public function test_prepare()
    {
        $stmt = $this->connection()->prepare("SELECT * FROM TEST_TABLE WHERE KEY = ?");

        $this->assertInstanceOf(PdoStatement::class, $stmt);

        $this->assertEquals("SELECT * FROM TEST_TABLE WHERE KEY = ?", $stmt->query());
    }

    /**
     *
     */
    public function test_prepare_error()
    {
        $this->expectException(SqlException::class);
        $this->expectExceptionMessage("[test_connect] Error on query : 'INVALID SQL' : SQLSTATE[HY000]: General error: 1 near \"INVALID\": syntax error");

        $this->connection()->prepare("INVALID SQL");
    }

    /**
     * @return PdoConnection
     */
    protected function connection()
    {
        $config = new PdoConfigStruct();

        $config->name = 'test_connect';
        $config->dsn = "sqlite::memory:";
        $config->options += [\PDO::ATTR_PERSISTENT => true];

        $connection = new PdoConnection($config, new SQLitePlatform(new TypeRegistry()));

        $connection->connect();

        return $connection;
    }
}