<?php


namespace PoireauFramework\Database\Sql\Platform;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Sql
 * @group ext_PoireauFramework_Database_Sql_Platform
 * @group ext_PoireauFramework_Database_Sql_Platform_SqlPlatformRegistry
 */
class SqlPlatformRegistryTest extends TestCase
{
    /**
     * @var SqlPlatformRegistry
     */
    private $platforms;


    protected function setUp()
    {
        $this->platforms = new SqlPlatformRegistry(new TypeRegistry());
    }

    /**
     *
     */
    public function test_register_get()
    {
        $platform = new SQLitePlatform(new TypeRegistry());

        $this->platforms->register($platform);

        $this->assertSame($platform, $this->platforms->get("sqlite"));
    }

    /**
     *
     */
    public function test_set_get()
    {
        $this->platforms->set("sqlite", SQLitePlatform::class);

        $this->assertInstanceOf(SQLitePlatform::class, $this->platforms->get("sqlite"));
        $this->assertSame($this->platforms->get("sqlite"), $this->platforms->get("sqlite"));
    }
}
