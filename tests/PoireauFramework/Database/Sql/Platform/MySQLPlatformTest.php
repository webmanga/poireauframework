<?php

namespace PoireauFramework\Database\Sql\Platform;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Sql
 * @group ext_PoireauFramework_Database_Sql_Platform
 * @group ext_PoireauFramework_Database_Sql_Platform_MySQLPlatform
 */
class MySQLPlatformTest extends TestCase
{
    /**
     * @var MySQLPlatform
     */
    private $platform;


    protected function setUp()
    {
        $this->platform = new MySQLPlatform(new TypeRegistry());
    }

    public function test_name()
    {
        $this->assertEquals("mysql", $this->platform->name());
    }

    /**
     *
     */
    public function test_autoIncrementDeclaration()
    {
        $this->assertEquals("`id` integer primary key AUTO_INCREMENT", $this->platform->autoIncrementDeclaration("id", "integer", "primary key"));
    }
}
