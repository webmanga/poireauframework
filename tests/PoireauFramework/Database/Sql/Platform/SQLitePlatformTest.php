<?php


namespace PoireauFramework\Database\Sql\Platform;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlCompilerRegistry;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Sql
 * @group ext_PoireauFramework_Database_Sql_Platform
 * @group ext_PoireauFramework_Database_Sql_Platform_SQLitePlatform
 */
class SQLitePlatformTest extends TestCase
{
    /**
     * @var SQLitePlatform
     */
    private $platform;


    protected function setUp()
    {
        $this->platform = new SQLitePlatform(new TypeRegistry());
    }

    public function test_name()
    {
        $this->assertEquals("sqlite", $this->platform->name());
    }

    /**
     *
     */
    public function test_autoIncrementDeclaration()
    {
        $this->assertEquals("`id` integer primary key AUTOINCREMENT", $this->platform->autoIncrementDeclaration("id", "integer", "primary key"));
    }

    /**
     *
     */
    public function test_quote()
    {
        $this->assertSame(123, $this->platform->quote(123));
        $this->assertSame(12.3, $this->platform->quote(12.3));
        $this->assertSame("12.3", $this->platform->quote("12.3"));
        $this->assertSame(1, $this->platform->quote(true));
        $this->assertSame(0, $this->platform->quote(false));
        $this->assertSame("'hello world !'", $this->platform->quote("hello world !"));
        $this->assertSame("'str value'", $this->platform->quote(new StrObj()));
        $this->assertSame("'uns''af''e obj'''", $this->platform->quote("uns'af'e obj'"));
    }

    /**
     *
     */
    public function test_identifier()
    {
        $this->assertEquals("`hello`", $this->platform->identifier(["hello"]));
        $this->assertEquals("`hell``o`", $this->platform->identifier(["hell`o"]));
        $this->assertEquals("`hello`.`world`", $this->platform->identifier(["hello", "world"]));
    }

    /**
     *
     */
    public function test_compilers()
    {
        $this->assertInstanceOf(SqlCompilerRegistry::class, $this->platform->compilers());
    }
}

class StrObj
{
    function __toString()
    {
        return "str value";
    }
}