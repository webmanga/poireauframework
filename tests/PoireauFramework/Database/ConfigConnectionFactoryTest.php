<?php

namespace PoireauFramework\Database;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Database\Exception\DatabaseConfigException;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Pdo\PdoConnection;
use PoireauFramework\Database\Sql\Pdo\PdoDriver;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformRegistry;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_ConfigConnectionFactory
 */
class ConfigConnectionFactoryTest extends TestCase
{
    /**
     *
     */
    public function test_createConnection_config_not_found()
    {
        $this->expectException(DatabaseConfigException::class);
        $this->expectExceptionMessage("Cannot find config item");

        $factory = new ConfigConnectionFactory(new ConfigItem(new Registry()));

        $factory->createConnection("not_found");
    }

    /**
     *
     */
    public function test_createConnection_driver_not_given()
    {
        $this->expectException(DatabaseConfigException::class);
        $this->expectExceptionMessage("No driver given");

        $factory = new ConfigConnectionFactory(new ConfigItem(RegistryFactory::createFromArray([
            "my_conn" => [
                "dbname" => "test"
            ]
        ])));

        $factory->createConnection("my_conn");
    }

    /**
     *
     */
    public function test_createConnection_driver_not_found()
    {
        $this->expectException(DatabaseConfigException::class);
        $this->expectExceptionMessage("Driver 'not_found' not found");

        $factory = new ConfigConnectionFactory(new ConfigItem(RegistryFactory::createFromArray([
            "my_conn" => [
                "driver" => "not_found"
            ]
        ])));

        $factory->createConnection("my_conn");
    }

    /**
     *
     */
    public function test_createConnection_will_call_driver()
    {
        $factory = new ConfigConnectionFactory(new ConfigItem(RegistryFactory::createFromArray([
            "my_conn" => [
                "driver" => "test",
                "username" => "john",
                "password" => "doe1234"
            ]
        ])));

        $factory->register(new TestConnectionDriver());

        TestConnectionDriver::$connection = $this->createMock(ConnectionInterface::class);

        $this->assertSame(TestConnectionDriver::$connection, $factory->createConnection("my_conn"));

        $this->assertEquals("test", TestConnectionDriver::$config->driver);
        $this->assertEquals("john", TestConnectionDriver::$config->username);
        $this->assertEquals("doe1234", TestConnectionDriver::$config->password);
    }

    /**
     *
     */
    public function test_createConnection_functional()
    {
        $factory = new ConfigConnectionFactory(new ConfigItem(RegistryFactory::createFromArray([
            "my_conn" => [
                "driver" => "pdo",
                "dsn" => "sqlite::memory:"
            ]
        ])));

        $platforms = new SqlPlatformRegistry(new TypeRegistry());
        $platforms->register(new SQLitePlatform(new TypeRegistry()));

        $factory->register(new PdoDriver($platforms));

        $connection = $factory->createConnection("my_conn");

        $this->assertInstanceOf(PdoConnection::class, $connection);
    }
}

class TestConnectionDriver implements ConnectionDriverInterface {
    static public $connection;
    static public $config;

    public function name(): string
    {
        return "test";
    }

    public function configure(\PoireauFramework\Config\ConfigItem $config): ConnectionInterface
    {
        self::$config = $config;

        return self::$connection;
    }
}
