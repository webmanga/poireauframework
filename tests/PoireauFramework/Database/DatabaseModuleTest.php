<?php

namespace PoireauFramework\Database;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\Mapping\Resolver\MapperResolver;
use PoireauFramework\Database\Mapping\Resolver\MapperResolverInterface;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\SimpleListType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Pdo\PdoDriver;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformRegistry;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\Registry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_DatabaseModule
 */
class DatabaseModuleTest extends TestCase
{
    /**
     *
     */
    public function test_instances()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(new Registry()));
        $injector->register(new DatabaseModule());

        $this->assertInstanceOf(DatabaseHandler::class, $injector->get(DatabaseHandler::class));
        $this->assertInstanceOf(ConfigConnectionFactory::class, $injector->get(ConnectionFactoryInterface::class));
        $this->assertInstanceOf(MapperResolver::class, $injector->get(MapperResolverInterface::class));
    }

    /**
     *
     */
    public function test_sql_platform_registry()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(new Registry()));
        $injector->register(new DatabaseModule());

        $registry = $injector->get(SqlPlatformRegistry::class);

        $this->assertInstanceOf(SqlPlatformRegistry::class, $registry);
        $this->assertInstanceOf(SQLitePlatform::class, $registry->get("sqlite"));
        $this->assertInstanceOf(MySQLPlatform::class, $registry->get("mysql"));
    }

    /**
     *
     */
    public function test_type_registry()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(new Registry()));
        $injector->register(new DatabaseModule());

        $types = $injector->get(TypeRegistry::class);

        $this->assertInstanceOf(TypeRegistry::class, $types);
        $this->assertInstanceOf(StringType::class, $types->get("string"));
        $this->assertInstanceOf(IntegerType::class, $types->get("integer"));
        $this->assertInstanceOf(SimpleListType::class, $types->get("list"));
    }

    /**
     *
     */
    public function test_getConnectionDrivers()
    {
        $injector = new BaseInjector();
        $module = new DatabaseModule();
        $injector->register($module);

        $drivers = $module->getConnectionDrivers($injector);

        $this->assertCount(1, $drivers);
        $this->assertInstanceOf(PdoDriver::class, $drivers[0]);
    }
}
