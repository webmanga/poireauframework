<?php

namespace PoireauFramework\Database\Mapping;

require_once __DIR__."/_files/User.php";
require_once __DIR__."/_files/UserMapper.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Helper\CriteriaTable;
use PoireauFramework\Database\Mapping\Helper\SqlCriteriaCompiler;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Test\UserMapper;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_SqlMapper
 */
class SqlMapperTest extends TestCase
{
    /**
     * @var UserMapper
     */
    private $mapper;


    /**
     *
     */
    protected function setUp()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "type"   => "sqlite",
                    "memory" => true
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        /** @var DatabaseHandler $handler */
        $handler = $injector->get(DatabaseHandler::class);

        $this->mapper = new UserMapper(
            $connection = $handler->getConnection("test"),
            $metadata = new Metadata(),
            new MetadataHydrator($metadata, $connection->platform()->types())
        );
        $this->mapper->schema()->create();
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->mapper->schema()->drop();
    }

    /**
     *
     */
    public function test_insert_manual_id()
    {
        $user = new User();

        $user->setId("123");
        $user->setUsername("john_doe");
        $user->setName("John");
        $user->setEmail("j.doe@webmanga.fr");
        $user->setPassword("MyPassword");

        $this->mapper->insert($user);

        $this->assertEquals([$user], $this->mapper->all());
    }

    /**
     *
     */
    public function test_insert_auto_increment_id()
    {
        $user = new User();

        $user->setUsername("john_doe");
        $user->setName("John");
        $user->setEmail("j.doe@webmanga.fr");
        $user->setPassword("MyPassword");

        $this->mapper->insert($user);

        $this->assertEquals(1, $user->id());

        $this->assertEquals([$user], $this->mapper->all());
    }

    /**
     *
     */
    public function test_create_manual_id()
    {
        $user = new User();

        $user->setId("123");
        $user->setUsername("john_doe");
        $user->setName("John");
        $user->setEmail("j.doe@webmanga.fr");
        $user->setPassword("MyPassword");

        $newUser = $this->mapper->create($user);

        $this->assertNotSame($user, $newUser);
        $this->assertEquals($user, $newUser);
        $this->assertEquals([$newUser], $this->mapper->all());
    }

    /**
     *
     */
    public function test_create_auto_increment_id()
    {
        $user = new User();

        $user->setUsername("john_doe");
        $user->setName("John");
        $user->setEmail("j.doe@webmanga.fr");
        $user->setPassword("MyPassword");

        $newUser = $this->mapper->create($user);

        $this->assertNotSame($user, $newUser);
        $this->assertInstanceOf(User::class, $newUser);
        $this->assertEquals(1, $newUser->id());

        $this->assertEquals([$newUser], $this->mapper->all());
    }

    /**
     *
     */
    public function test_get()
    {
        $user = new User();

        $user->setId("123");
        $user->setUsername("john_doe");
        $user->setName("John");
        $user->setEmail("j.doe@webmanga.fr");
        $user->setPassword("MyPassword");

        $this->mapper->insert($user);

        $this->assertEquals($user, $this->mapper->get(123));
        $this->assertNull($this->mapper->get(456));
    }

    /**
     *
     */
    public function test_criteria()
    {
        $user = new User();

        $user->setId("123");
        $user->setUsername("john_doe");
        $user->setName("John");
        $user->setEmail("j.doe@webmanga.fr");
        $user->setPassword("MyPassword");

        $this->mapper->insert($user);

        $this->assertEquals($user, $this->mapper->findForLogin("john_doe", "MyPassword"));
        $this->assertNull($this->mapper->findForLogin("john_doe", "bad password"));
    }
}
