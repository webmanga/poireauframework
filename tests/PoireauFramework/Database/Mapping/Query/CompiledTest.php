<?php

namespace PoireauFramework\Database\Mapping\Query;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainer;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiled
 */
class CompiledTest extends TestCase
{
    /**
     *
     */
    public function test_query()
    {
        $compiled = new Compiled("my query", new BindingContainer([]));

        $this->assertSame("my query", $compiled->query());
    }

    /**
     *
     */
    public function test_bindings()
    {
        $compiled = new Compiled("my query", new BindingContainer([
            new Binding("foo")
        ]));

        $this->assertEquals([null], $compiled->bindings(new MySQLPlatform(new TypeRegistry())));

        $compiled->bind(["foo" => "bar"]);

        $this->assertEquals(["bar"], $compiled->bindings(new MySQLPlatform(new TypeRegistry())));
    }

    /**
     *
     */
    public function test_convert()
    {
        $hydrator = $this->createMock(HydratorInterface::class);

        $compiled = new Compiled("", new BindingContainer([]), $hydrator);

        $dbValue = ["FOO" => "bar"];

        $hydrator->expects($this->once())
            ->method("fromDatabase")
            ->with($dbValue)
            ->willReturn((object) ["foo" => "bar"])
        ;

        $this->assertEquals((object) ["foo" => "bar"], $compiled->convert($dbValue));
    }
}
