<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;

require_once __DIR__."/../../../_files/User.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainer;
use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Exception\QueryException;
use PoireauFramework\Database\Mapping\Query\Insert;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql_SqlInsertCompiler
 */
class SqlInsertCompilerTest extends TestCase
{
    /**
     * @var SqlInsertCompiler
     */
    private $compiler;

    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var Insert
     */
    private $query;


    protected function setUp()
    {
        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->platform = new MySQLPlatform($types);
        $this->compiler = new SqlInsertCompiler($this->platform);

        $metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;

        $metadata->schema = 'USER';
        $metadata->entity = User::class;

        $this->query = new Insert($metadata, new MetadataHydrator($metadata, $types));
    }

    /**
     *
     */
    public function test_compileInsert_default()
    {
        $compiled = $this->compiler->compileInsert($this->query);

        $this->assertInstanceOf(Compiled::class, $compiled);
        $this->assertEquals("INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES (?, ?, ?, ?, ?)", $compiled->query());
        $this->assertAttributeEquals(new BindingContainer([
            new Binding("id", "integer"),
            new Binding("username", "string"),
            new Binding("name", "string"),
            new Binding("password", "string"),
            new Binding("email", "string"),
        ]), "bindings", $compiled);
    }

    /**
     *
     */
    public function test_compileInsert_attributes_subset()
    {
        $this->query->attributes(['username', 'password']);

        $compiled = $this->compiler->compileInsert($this->query);

        $this->assertEquals("INSERT INTO `USER`(`USER_LOGIN`, `USER_PASSWORD`) VALUES (?, ?)", $compiled->query());
        $this->assertAttributeEquals(new BindingContainer([
            new Binding("username", "string"),
            new Binding("password", "string"),
        ]), "bindings", $compiled);
    }

    /**
     *
     */
    public function test_compileInsert_with_custom_attributes()
    {
        $this->query->attributes(['MY_CUSTOM_ATTR', 'MY_CUSTOM_ATTR']);

        $compiled = $this->compiler->compileInsert($this->query);

        $this->assertEquals("INSERT INTO `USER`(`MY_CUSTOM_ATTR`, `MY_CUSTOM_ATTR`) VALUES (?, ?)", $compiled->query());
        $this->assertAttributeEquals(new BindingContainer([
            new Binding("MY_CUSTOM_ATTR"),
            new Binding("MY_CUSTOM_ATTR"),
        ]), "bindings", $compiled);
    }

    /**
     *
     */
    public function test_compileInsert_bulk()
    {
        $this->query
            ->bulk()
            ->values(
                (new User())
                    ->setName("John Doe")
                    ->setUsername("john_doe")
                    ->setEmail("john.doe@webmanga.fr")
                    ->setPassword("MyVerySecurePassword")
            )
            ->values(
                (new User())
                    ->setId(456)
                    ->setName("Alan Smith")
                    ->setUsername("alan_smith")
                    ->setEmail("a.smith@webmanga.fr")
                    ->setPassword("P4S2W0R9")
            )
        ;

        $compiled = $this->compiler->compileInsert($this->query);

        $this->assertEquals(
            "INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES " .
            "(NULL, 'john_doe', 'John Doe', 'MyVerySecurePassword', 'john.doe@webmanga.fr') " .
            "(456, 'alan_smith', 'Alan Smith', 'P4S2W0R9', 'a.smith@webmanga.fr')"
            ,
            $compiled->query()
        );
        $this->assertAttributeEquals(new BindingContainer([]), "bindings", $compiled);
    }

    /**
     *
     */
    public function test_compileInsert_bulk_sql_injection()
    {
        $this->query
            ->bulk()
            ->attributes(["name"])
            ->values(
                (new User())->setName("with'fail\"")
            )
        ;

        $compiled = $this->compiler->compileInsert($this->query);

        $this->assertEquals(
            "INSERT INTO `USER`(`USER_NAME`) VALUES ('with''fail\"')",
            $compiled->query()
        );
    }

    /**
     *
     */
    public function test_compileInsert_with_bindings()
    {
        $this->query->values(
            (new User())
                ->setName("John Doe")
                ->setUsername("john_doe")
                ->setEmail("john.doe@webmanga.fr")
                ->setPassword("MyVerySecurePassword")
        );

        $compiled = $this->compiler->compileInsert($this->query);

        $this->assertInstanceOf(Compiled::class, $compiled);
        $this->assertEquals("INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES (?, ?, ?, ?, ?)", $compiled->query());
        $this->assertEquals([null, "john_doe", "John Doe", "MyVerySecurePassword", "john.doe@webmanga.fr"], $compiled->bindings($this->platform));
    }

    /**
     *
     */
    public function test_compileInsert_replace()
    {
        $compiled = $this->compiler->compileInsert($this->query->replace());

        $this->assertStringStartsWith("REPLACE INTO `USER`", $compiled->query());
    }

    /**
     *
     */
    public function test_compileInsert_ignore()
    {
        $compiled = $this->compiler->compileInsert($this->query->ignore());

        $this->assertStringStartsWith("INSERT IGNORE INTO `USER`", $compiled->query());
    }

    /**
     *
     */
    public function test_compileInsert_error_bulk_no_values()
    {
        $this->expectException(QueryException::class);
        $this->expectExceptionMessage("No values given on bulk insert query");

        $this->compiler->compileInsert($this->query->bulk());
    }

    /**
     *
     */
    public function test_compileInsert_error_invalid_attributes()
    {
        $this->expectException(QueryException::class);
        $this->expectExceptionMessage("Cannot extract non-empty list of attributes");

        $this->compiler->compileInsert(new Insert(new Metadata(), $this->createMock(HydratorInterface::class)));
    }
}
