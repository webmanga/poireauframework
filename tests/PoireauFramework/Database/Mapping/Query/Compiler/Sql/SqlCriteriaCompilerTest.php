<?php

namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\SimpleListType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;

/**
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql_SqlCriteriaCompiler
 */
class SqlCriteriaCompilerTest extends TestCase
{
    /**
     *
     * @var SqlCriteriaCompiler
     */
    private $compiler;

    /**
     * @var Metadata
     */
    private $metadata;


    /**
     *
     */
    protected function setUp() {
        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("foo")
            ->string("subentity.name")->attribute("sub_name")
            ->list("subentity.value")->attribute("sub_value")
            ->build()
        ;

        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());
        $types->add(new SimpleListType());

        $this->compiler = new SqlCriteriaCompiler(new SQLitePlatform($types));
    }

    /**
     * 
     */
    public function test_build_criteria_explicit_operator()
    {
        $criteria = [['foo', '!=', 123]];
        
        $this->assertEquals([
            'criteria' => '`foo` != ?',
            'parameters' => [123]
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_invalid_value()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [['foo', '!=', new \stdClass()]];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_implicit_operator()
    {
        $criteria = ['foo' => 123];
        
        $this->assertEquals([
            'criteria' => '`foo` = ?',
            'parameters' => [123]
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_multiple()
    {
        $criteria = [
            ['foo', '!=', 123],
            'subentity.name' => 'toto'
        ];
        
        $this->assertEquals([
            'criteria' => '`foo` != ? OR `sub_name` = ?',
            'parameters' => [123, 'toto']
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria, 'OR')));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_null()
    {
        $criteria = [
            'subentity.value' => null
        ];
        
        $this->assertEquals([
            'criteria' => '`sub_value` IS NULL',
            'parameters' => []
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_not_null()
    {
        $criteria = [
            ['subentity.value', '!=', null]
        ];
        
        $this->assertEquals([
            'criteria' => '`sub_value` IS NOT NULL',
            'parameters' => []
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_null_invalid_operator()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [
            ['subentity.value', '>', null]
        ];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_array_invalid_operator()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [
            'foo' => [1,2,3]
        ];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_array_in()
    {
        $criteria = [
            ['subentity.value', 'IN', [1,2,"hello"]]
        ];
        
        $this->assertEquals([
            'criteria' => "`sub_value` IN (1, 2, 'hello')",
            'parameters' => []
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_array_between()
    {
        $criteria = [
            ['subentity.value', 'BETWEEN', [1,3]]
        ];
        
        $this->assertEquals([
            'criteria' => "`sub_value` BETWEEN ? AND ?",
            'parameters' => [1,3]
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_array_between_invalid_range()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [
            ['subentity.value', 'BETWEEN', [1]]
        ];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_subquery()
    {
        $criteria = [
            ['foo', ':query', 'IN (SELECT id FROM other_table)']
        ];
        
        $this->assertEquals([
            'criteria' => "`foo` IN (SELECT id FROM other_table)",
            'parameters' => []
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_with_subquery_invalid()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [
            ['foo', ':query', 123]
        ];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_query()
    {
        $criteria = [
            ':query' => 'EXISTS(SELECT * FROM other_table)'
        ];
        
        $this->assertEquals([
            'criteria' => "EXISTS(SELECT * FROM other_table)",
            'parameters' => []
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_query_array()
    {
        $criteria = [
            ':query' => [
                'EXISTS(SELECT * FROM other_table)',
                'IN (SELECT id FROM my_table)'
            ]
        ];
        
        $this->assertEquals([
            'criteria' => "EXISTS(SELECT * FROM other_table) OR IN (SELECT id FROM my_table)",
            'parameters' => []
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria, 'OR')));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_query_parameters()
    {
        $criteria = [
            ':query' => [
                'EXISTS(SELECT * FROM other_table WHERE name LIKE ?)' => ['param'],
            ]
        ];
        
        $this->assertEquals([
            'criteria' => "EXISTS(SELECT * FROM other_table WHERE name LIKE ?)",
            'parameters' => ['param']
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria, 'OR')));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_query_invalid()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [
            ':query' => [
                'EXISTS(SELECT * FROM other_table WHERE name LIKE ?)' => '???',
            ]
        ];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_sub_condition()
    {
        $criteria = [
            'foo' => 'bar',
            ':or' => [
                'subentity.name' => 'foo',
                'subentity.value' => 'bar'
            ]
        ];
        
        $this->assertEquals([
            'criteria' => "`foo` = ? AND (`sub_name` = ? OR `sub_value` = ?)",
            'parameters' => ['bar', 'foo', 'bar']
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_sub_condition_not_add_brackets()
    {
        $criteria = [
            'foo' => 'bar',
            ':and' => [
                'subentity.name' => 'foo',
                'subentity.value' => 'bar'
            ]
        ];
        
        $this->assertEquals([
            'criteria' => "`foo` = ? AND `sub_name` = ? AND `sub_value` = ?",
            'parameters' => ['bar', 'foo', 'bar']
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
    
    /**
     * 
     */
    public function test_build_criteria_pseudo_fn_invalid()
    {
        $this->expectException(SqlException::class);
        
        $criteria = [
            ':invalid' => []
        ];
        
        $this->compiler->compile(new Criteria($this->metadata, $criteria));
    }
    
    /**
     * 
     */
    public function test_build_criteria_complex()
    {
        $criteria = [
            'foo' => 'value',
            ['subentity.name', '!=', 'value'],
            ':or' => [
               'subentity.value' => 5
            ],
            ':query' => [
                'EXISTS(SELECT * FROM USERS)',
                '1 IN (SELECT USER_ID FROM USER WHERE USER_NAME = ?)' => ['name']
            ]
        ];
        
        $this->assertEquals([
            'criteria' => "`foo` = ? AND `sub_name` != ? AND (`sub_value` = ?) AND EXISTS(SELECT * FROM USERS) AND 1 IN (SELECT USER_ID FROM USER WHERE USER_NAME = ?)",
            'parameters' => ['value', 'value', 5, 'name']
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }

    /**
     *
     */
    public function test_build_criteria_binding()
    {
        $criteria = [
            'foo' => new Binding(),
            ['subentity.name', '!=', new Binding()],
           'subentity.value' => new Binding('baz')
        ];

        $this->assertEquals([
            'criteria' => "`foo` = ? AND `sub_name` != ? AND `sub_value` = ?",
            'parameters' => [new Binding('foo', 'integer'), new Binding('subentity.name', 'string'), new Binding('baz')]
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }

    /**
     *
     */
    public function test_build_criteria_or_with_sub_and()
    {
        $criteria = [
            "foo" => "bar",
            ":or" => [
                [
                    "subentity.name"  => "foo",
                    "subentity.value" => "bar"
                ],
                [
                    "subentity.name"  => "bar",
                    "subentity.value" => "foo"
                ]
            ]
        ];

        $this->assertEquals([
            'criteria' => "`foo` = ? AND ((`sub_name` = ? AND `sub_value` = ?) OR (`sub_name` = ? AND `sub_value` = ?))",
            'parameters' => ['bar', 'foo', 'bar', 'bar', 'foo']
        ], $this->compiler->compile(new Criteria($this->metadata, $criteria)));
    }
}
