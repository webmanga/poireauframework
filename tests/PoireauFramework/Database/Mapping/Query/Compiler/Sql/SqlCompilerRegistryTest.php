<?php


namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;


use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql_SqlCompilerRegistry
 */
class SqlCompilerRegistryTest extends TestCase
{
    /**
     * @var SqlCompilerRegistry
     */
    private $registry;


    /**
     *
     */
    protected function setUp()
    {
        $this->registry = new SqlCompilerRegistry(new SQLitePlatform(new TypeRegistry()));
    }

    /**
     *
     */
    public function test_instances()
    {
        $this->assertInstanceOf(SqlCriteriaCompiler::class, $this->registry->criteria());
        $this->assertInstanceOf(SqlSelectCompiler::class, $this->registry->select());
        $this->assertInstanceOf(SqlInsertCompiler::class, $this->registry->insert());
    }

    /**
     *
     */
    public function test_persitant()
    {
        $this->assertSame($this->registry->select(), $this->registry->select());
        $this->assertSame($this->registry->insert(), $this->registry->insert());
        $this->assertSame($this->registry->criteria(), $this->registry->criteria());
    }
}
