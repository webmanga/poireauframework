<?php


namespace PoireauFramework\Database\Mapping\Query\Compiler\Sql;


use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\HydratorInterface;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainerAggregate;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Query\Select;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql
 * @group ext_PoireauFramework_Database_Mapping_Query_Compiler_Sql_SqlSelectCompiler
 */
class SqlSelectCompilerTest extends TestCase
{
    /**
     * @var SqlSelectCompiler
     */
    private $compiler;

    /**
     * @var MySQLPlatform
     */
    private $platform;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * @var Select
     */
    private $query;


    /**
     *
     */
    protected function setUp()
    {
        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->platform = new MySQLPlatform($types);
        $this->compiler = new SqlInsertCompiler($this->platform);

        $this->compiler = new SqlSelectCompiler(
            $this->platform,
            new SqlCriteriaCompiler($this->platform)
        );

        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;

        $this->metadata->schema = 'USER';
        $this->metadata->entity = User::class;

        $this->hydrator = new MetadataHydrator($this->metadata, $types);
        $this->query = new Select($this->metadata, $this->hydrator);
    }

    /**
     *
     */
    public function test_compileSelect_default()
    {
        $compiled = $this->compiler->compileSelect($this->query);

        $this->assertInstanceOf(Compiled::class, $compiled);

        $this->assertEquals("SELECT * FROM `USER`", $compiled->query());
        $this->assertEmpty($compiled->bindings($this->platform));
        $this->assertAttributeSame($this->hydrator, "hydrator", $compiled);
    }

    /**
     *
     */
    public function test_compileSelect_with_fields()
    {
        $compiled = $this->compiler->compileSelect($this->query->attributes(["id", "password"]));

        $this->assertInstanceOf(Compiled::class, $compiled);

        $this->assertEquals("SELECT `USER_ID`, `USER_PASSWORD` FROM `USER`", $compiled->query());
        $this->assertEmpty($compiled->bindings($this->platform));
    }

    /**
     *
     */
    public function test_compileSelect_with_custom_attributes()
    {
        $compiled = $this->compiler->compileSelect($this->query->attributes(["MY_CUSTOM"]));

        $this->assertInstanceOf(Compiled::class, $compiled);

        $this->assertEquals("SELECT `MY_CUSTOM` FROM `USER`", $compiled->query());
        $this->assertEmpty($compiled->bindings($this->platform));
    }

    /**
     *
     */
    public function test_compileSelect_with_simple_where()
    {
        $compiled = $this->compiler->compileSelect($this->query->where("username", "john_doe"));

        $this->assertInstanceOf(Compiled::class, $compiled);

        $this->assertEquals("SELECT * FROM `USER` WHERE `USER_LOGIN` = ?", $compiled->query());
        $this->assertEquals(["john_doe"], $compiled->bindings($this->platform));
    }

    /**
     *
     */
    public function test_compileSelect_with_simple_where_bindings()
    {
        $compiled = $this->compiler->compileSelect($this->query->where("username", new Binding()));

        $this->assertInstanceOf(Compiled::class, $compiled);

        $this->assertEquals("SELECT * FROM `USER` WHERE `USER_LOGIN` = ?", $compiled->query());

        $compiled->bind(["username" => "john_doe"]);
        $this->assertEquals(["john_doe"], $compiled->bindings($this->platform));
    }
}
