<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Bag
 * @group ext_PoireauFramework_Database_Mapping_Query_Bag_BindingContainer
 */
class BindingContainerTest extends TestCase
{
    /**
     * @var MySQLPlatform
     */
    private $platform;


    protected function setUp()
    {
        $this->platform = new MySQLPlatform(new TypeRegistry());
    }

    /**
     *
     */
    public function test_bindings_with_field_array()
    {
        $container = new BindingContainer([
                new Binding("name"),
                new Binding("email")
            ])
        ;

        $container->bind(["name" => "John", "email" => "%john%"]);

        $this->assertEquals(["John", "%john%"], $container->bindings($this->platform));
    }

    /**
     *
     */
    public function test_bindings_with_field_entity()
    {
        $user = new User();

        $user->setName("John");
        $user->setEmail("%john_doe%");

        $container = new BindingContainer([
                new Binding("name"),
                new Binding("email")
            ])
        ;

        $container->bind($user);

        $this->assertEquals(["John", "%john_doe%"], $container->bindings($this->platform));
    }

    /**
     *
     */
    public function test_multiple_bind()
    {
        $container = new BindingContainer([
            new Binding("name"),
            new Binding("email")
        ]);

        $container
            ->bind(["name" => "John"])
            ->bind(["email" => "john.doe@webmanga.fr"])
        ;

        $this->assertEquals(["John", "john.doe@webmanga.fr"], $container->bindings($this->platform));
    }
}
