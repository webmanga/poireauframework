<?php

namespace PoireauFramework\Database\Mapping\Query\Bag;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\SimpleListType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Bag
 * @group ext_PoireauFramework_Database_Mapping_Query_Bag_Bindings
 */
class BindingTest extends TestCase
{
    /**
     * @var MySQLPlatform
     */
    private $platform;

    protected function setUp()
    {
        $registry = new TypeRegistry();
        $registry->add(new IntegerType());
        $registry->add(new StringType());
        $registry->add(new SimpleListType());

        $this->platform = new MySQLPlatform($registry);
    }

    /**
     *
     */
    public function test_bind_array()
    {
        $binding = new Binding("foo");

        $binding->bind(["foo" => "bar"]);

        $this->assertEquals("bar", $binding->value($this->platform));
    }

    /**
     *
     */
    public function test_bind_object()
    {
        $binding = new Binding("foo");

        $binding->bind((object) ["foo" => "bar"]);

        $this->assertEquals("bar", $binding->value($this->platform));
    }

    /**
     *
     */
    public function test_bind_not_corresponding()
    {
        $binding = new Binding("foo");

        $binding->bind(["oof" => "bar"]);

        $this->assertNull($binding->value($this->platform));
    }

    /**
     *
     */
    public function test_bind_twice()
    {
        $binding = new Binding("foo");

        $binding->bind(["foo" => "bar"]);
        $binding->bind(["oof" => "bar"]);

        $this->assertEquals("bar", $binding->value($this->platform));
    }

    /**
     *
     */
    public function test_with_type()
    {
        $binding = new Binding("foo", "list");

        $binding->bind(["foo" => ["bar", "baz"]]);

        $this->assertEquals("bar,baz", $binding->value($this->platform));
    }
}
