<?php


namespace PoireauFramework\Database\Mapping\Query\Bag;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Compiler\CriteriaCompilerInterface;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlCriteriaCompiler;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\SimpleListType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Bag
 * @group ext_PoireauFramework_Database_Mapping_Query_Bag_Criteria
 */
class CriteriaTest extends TestCase
{
    /**
     * @var CriteriaCompilerInterface
     */
    private $compiler;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var SqlPlatformInterface
     */
    private $platform;


    /**
     *
     */
    protected function setUp()
    {
        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());
        $types->add(new SimpleListType());

        $this->platform = new SQLitePlatform($types);
        $this->compiler = new SqlCriteriaCompiler($this->platform);

        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;
    }

    /**
     *
     */
    public function test_getters()
    {
        $criteria = $this->criteria(["foo" => "bar"]);

        $this->assertSame($this->metadata, $criteria->metadata());
        $this->assertEquals("AND", $criteria->separator());
        $this->assertEquals(["foo" => "bar"], $criteria->criteria());
    }

    /**
     *
     */
    public function test_simple_with_attribute_resolve()
    {
        $criteria = $this->criteria(["name" => "John"])->compile($this->compiler);

        $this->assertEquals("`USER_NAME` = ?", $criteria->query());
        $this->assertEquals(["John"], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_simple_no_attribute_resolve()
    {
        $criteria = $this->criteria(["foo" => "bar"])->compile($this->compiler);

        $this->assertEquals("foo = ?", $criteria->query());
        $this->assertEquals(["bar"], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_multiple()
    {
        $criteria = $this->criteria([
            "name" => "John",
            ["email", "LIKE", "%john%"]
        ])->compile($this->compiler);

        $this->assertEquals("`USER_NAME` = ? AND `USER_MAIL` LIKE ?", $criteria->query());
        $this->assertEquals(["John", "%john%"], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_complex_embedded()
    {
        $criteria = $this->criteria([
            "name" => "John",
            ":or"  => [
                "username" => "john_doe",
                ["email", "LIKE", "%john_doe%"],
                ["email", "LIKE", "%john.doe%"],
            ],
            ["email", "!=", null]
        ])->compile($this->compiler);

        $this->assertEquals("`USER_NAME` = ? AND (`USER_LOGIN` = ? OR `USER_MAIL` LIKE ? OR `USER_MAIL` LIKE ?) AND `USER_MAIL` IS NOT NULL", $criteria->query());
        $this->assertEquals(["John", "john_doe", "%john_doe%", "%john.doe%"], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_bindings_with_field_array()
    {
        $criteria = $this->criteria([
                "name" => new Binding(),
                ["email", "LIKE", new Binding()]
            ])
            ->compile($this->compiler)
            ->bind(["name" => "John", "email" => "%john%"])
        ;

        $this->assertEquals("`USER_NAME` = ? AND `USER_MAIL` LIKE ?", $criteria->query());
        $this->assertEquals(["John", "%john%"], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_bindings_with_field_entity()
    {
        $user = new User();

        $user->setName("John");
        $user->setEmail("%john_doe%");

        $criteria = $this->criteria([
                "name" => new Binding(),
                ["email", "LIKE", new Binding()]
            ])
            ->compile($this->compiler)
            ->bind($user)
        ;

        $this->assertEquals(["John", "%john_doe%"], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_bindings_named()
    {
        $criteria = $this->criteria([
                "id" => new Binding("custom"),
            ])
            ->compile($this->compiler)
            ->bind(["custom" => 123])
        ;

        $this->assertEquals([123], $criteria->bindings($this->platform));
    }

    /**
     *
     */
    public function test_multiple_bind()
    {
        $criteria = $this->criteria([
            "name" => new Binding(),
            ["email", "LIKE", new Binding()]
        ])
            ->compile($this->compiler)
            ->bind(["name" => "John"])
            ->bind(["email" => "john.doe@webmanga.fr"])
        ;

        $this->assertEquals(["John", "john.doe@webmanga.fr"], $criteria->bindings($this->platform));
    }

    /**
     * @param array $criteria
     *
     * @return Criteria
     */
    protected function criteria(array $criteria)
    {
        return new Criteria($this->metadata, $criteria);
    }
}
