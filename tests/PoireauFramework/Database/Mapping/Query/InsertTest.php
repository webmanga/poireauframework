<?php

namespace PoireauFramework\Database\Mapping\Query;

require_once __DIR__."/../_files/User.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlInsertCompiler;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Insert
 */
class InsertTest extends TestCase
{
    /**
     * @var SqlInsertCompiler
     */
    private $compiler;

    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var MetadataHydrator
     */
    private $hydrator;


    protected function setUp()
    {
        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->platform = new MySQLPlatform($types);
        $this->compiler = new SqlInsertCompiler($this->platform);

        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;

        $this->metadata->schema = 'USER';
        $this->metadata->entity = User::class;

        $this->hydrator = new MetadataHydrator($this->metadata, $types);
    }

    /**
     *
     */
    public function test_defaults()
    {
        $query = new Insert($this->metadata, $this->hydrator);

        $this->assertSame($this->metadata, $query->getMetadata());
        $this->assertSame($this->hydrator, $query->getHydrator());
        $this->assertEmpty($query->getValues());
        $this->assertEmpty($query->getAttributes());
        $this->assertFalse($query->isBulk());
        $this->assertSame(Insert::TYPE_DEFAULT, $query->getType());
    }

    /**
     *
     */
    public function test_bulk()
    {
        $query = new Insert($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->bulk());
        $this->assertTrue($query->isBulk());
        $this->assertFalse($query->bulk(false)->isBulk());
    }

    /**
     *
     */
    public function test_values()
    {
        $query = new Insert($this->metadata, $this->hydrator);

        $this->assertSame($query, $query
            ->values(
                $user1 = (new User())
                    ->setName("John Doe")
                    ->setUsername("john_doe")
                    ->setEmail("john.doe@webmanga.fr")
                    ->setPassword("MyVerySecurePassword")
            )
            ->values(
                $user2 = (new User())
                    ->setId(456)
                    ->setName("Alan Smith")
                    ->setUsername("alan_smith")
                    ->setEmail("a.smith@webmanga.fr")
                    ->setPassword("P4S2W0R9")
            )
        );

        $this->assertSame([$user1, $user2], $query->getValues());
    }

    /**
     *
     */
    public function test_ignore()
    {
        $query = new Insert($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->ignore());
        $this->assertSame(Insert::TYPE_IGNORE, $query->getType());
    }

    /**
     *
     */
    public function test_replace()
    {
        $query = new Insert($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->replace());
        $this->assertSame(Insert::TYPE_REPLACE, $query->getType());
    }

    /**
     *
     */
    public function test_attributes()
    {
        $query = new Insert($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->attributes(["foo", "bar"]));
        $this->assertSame(["foo", "bar"], $query->getAttributes());
    }

    /**
     *
     */
    public function test_compile()
    {
        $query = new Insert($this->metadata, $this->hydrator);


        $compiled = $query->compile($this->compiler);

        $this->assertInstanceOf(Compiled::class, $compiled);
        $this->assertEquals("INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES (?, ?, ?, ?, ?)", $compiled->query());
    }
}
