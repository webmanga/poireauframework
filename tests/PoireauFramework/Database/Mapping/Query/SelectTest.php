<?php

namespace PoireauFramework\Database\Mapping\Query;

require_once __DIR__."/../_files/User.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\Criteria;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlCriteriaCompiler;
use PoireauFramework\Database\Mapping\Query\Compiler\Sql\SqlSelectCompiler;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\Platform\SqlPlatformInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Select
 */
class SelectTest extends TestCase
{
    /**
     * @var SqlSelectCompiler
     */
    private $compiler;

    /**
     * @var SqlPlatformInterface
     */
    private $platform;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var MetadataHydrator
     */
    private $hydrator;


    protected function setUp()
    {
        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->platform = new MySQLPlatform($types);
        //$this->compiler = new SqlSelectCompiler($this->platform);

        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;

        $this->metadata->schema = 'USER';
        $this->metadata->entity = User::class;

        $this->hydrator = new MetadataHydrator($this->metadata, $types);
    }

    /**
     *
     */
    public function test_defaults()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($this->metadata, $query->getMetadata());
        $this->assertSame($this->hydrator, $query->getHydrator());
        $this->assertNull($query->getWhere());
        $this->assertEmpty($query->getAttributes());
    }

    /**
     *
     */
    public function test_attributes()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->attributes(["foo", "bar"]));
        $this->assertSame(["foo", "bar"], $query->getAttributes());
    }

    /**
     *
     */
    public function test_where_single_condition_with_operator()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->where("name", "!=", "John"));

        $this->assertEquals(new Criteria($this->metadata, [
            ["name", "!=", "John"]
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_single_condition_implicit_operator()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->where("name", "John"));

        $this->assertEquals(new Criteria($this->metadata, [
            "name" => "John"
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_single_condition_array()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->where(["name", "!=", "John"]));

        $this->assertEquals(new Criteria($this->metadata, [
            ["name", "!=", "John"]
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_complex_condition_array()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->where([
            ["name", "!=", "John"],
            ["username", "like", "%smith%"]
        ]));

        $this->assertEquals(new Criteria($this->metadata, [
            ["name", "!=", "John"],
            ["username", "like", "%smith%"]
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_double_condition()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $query
            ->where("name", "John")
            ->where("age", ">", 25)
        ;

        $this->assertEquals(new Criteria($this->metadata, [
            "name" => "John",
            ["age", ">", 25]
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_with_or_not_normalized()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $query
            ->where([
                ":or" => [
                    "name"     => "John",
                    "username" => "john_doe",
                ],
                ["id", ">", 0]
            ])
            ->where([
                ["email", "like", "%john%doe%"],
                "age" => 25
            ])
        ;

        $this->assertEquals(new Criteria($this->metadata, [
            ":or" => [
                [
                    "username" => "john_doe",
                    ["email", "like", "%john%doe%"],
                    "age" => 25
                ],
                "name" => "John",
            ],
            ["id", ">", 0]
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_with_or_normalized()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $query
            ->where([
                ":or" => [
                    [
                        "name" => "John"
                    ],
                    [
                        ["username", "like", "%john_doe%"]
                    ],
                ],
                ["id", ">", 0]
            ])
            ->where([
                ["email", "like", "%john%doe%"],
                "age" => 25
            ])
        ;

        $this->assertEquals(new Criteria($this->metadata, [
            ":or" => [
                [
                    "name" => "John"
                ],
                [
                    ["username", "like", "%john_doe%"],
                    ["email", "like", "%john%doe%"],
                    "age" => 25
                ],
            ],
            ["id", ">", 0]
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_with_or_on_root()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $query
            ->where([
                ":or" => [
                    [
                        "name" => "John"
                    ],
                    [
                        ["username", "like", "%john_doe%"]
                    ],
                ],
            ])
        ;

        $this->assertEquals(new Criteria($this->metadata, [
            [
                "name" => "John"
            ],
            [
                ["username", "like", "%john_doe%"],
            ],
        ], "OR"), $query->getWhere());
    }

    /**
     *
     */
    public function test_where_with_binding()
    {
        $query = new Select($this->metadata, $this->hydrator);

        $this->assertSame($query, $query->where("name", new Binding()));

        $this->assertEquals(new Criteria($this->metadata, [
            "name" => new Binding()
        ]), $query->getWhere());
    }

    /**
     *
     */
    public function test_compile()
    {
        $query = new Select($this->metadata, $this->hydrator);
        $compiled = $query->compile(new SqlSelectCompiler(new SQLitePlatform(new TypeRegistry()), new SqlCriteriaCompiler(new SQLitePlatform(new TypeRegistry()))));

        $this->assertInstanceOf(Compiled::class, $compiled);
        $this->assertEquals("SELECT * FROM `USER`", $compiled->query());
    }
}
