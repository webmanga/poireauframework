<?php

namespace PoireauFramework\Database\Mapping\Query\Connection\Sql;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Query\Bag\BindingContainer;
use PoireauFramework\Database\Mapping\Query\Compiled;
use PoireauFramework\Database\Mapping\Schema\SchemaInterface;
use PoireauFramework\Database\Mapping\Schema\SqlSchema;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_Connection
 * @group ext_PoireauFramework_Database_Mapping_Query_Connection_Sql
 */
class SqlQueryConnectionTest extends TestCase
{
    /**
     * @var SqlConnectionInterface
     */
    private $connection;

    /**
     * @var SchemaInterface
     */
    private $schema;

    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var SqlQueryConnection
     */
    private $queryConnection;

    /**
     * @var MetadataHydrator
     */
    private $hydrator;


    /**
     *
     */
    protected function setUp()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "dsn" => "sqlite::memory:"
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        $this->connection = $injector->get(DatabaseHandler::class)->getConnection("test");

        $this->metadata = new Metadata();

        $this->metadata->schema = "my_schema";
        $this->metadata->entity = \stdClass::class;

        (new FieldsBuilder($this->metadata))
            ->integer("id")->autoincrement()->primary()
            ->string("firstName")->attribute("first_name")->notNull()
            ->string("lastName")->attribute("last_name")
            ->integer("age")
            ->build()
        ;

        $this->schema = new SqlSchema($this->metadata, $this->connection);

        $this->schema->create();

        $this->connection->query("INSERT INTO my_schema VALUES(1, 'John', 'Doe', 35)");
        $this->connection->query("INSERT INTO my_schema VALUES(2, 'Alan', 'Smith', 65)");
        $this->connection->query("INSERT INTO my_schema VALUES(3, 'Robert', 'Hossein', 89)");

        $this->queryConnection = new SqlQueryConnection($this->connection);
        $this->hydrator = new MetadataHydrator($this->metadata, $injector->get(TypeRegistry::class));
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->schema->drop();
    }

    /**
     *
     */
    public function test_execute_simple()
    {
        $compiled = new Compiled("SELECT * FROM my_schema", new BindingContainer([]), $this->hydrator);

        $result = $this->queryConnection->execute($compiled);

        $this->assertInstanceOf(SqlQueryResult::class, $result);

        $all = $result->all();

        $this->assertCount(3, $all);

        $this->assertEquals([
            (object) ["id" => 1, "firstName" => "John", "lastName" => "Doe", "age" => 35],
            (object) ["id" => 2, "firstName" => "Alan", "lastName" => "Smith", "age" => 65],
            (object) ["id" => 3, "firstName" => "Robert", "lastName" => "Hossein", "age" => 89],
        ], $all);
    }

    /**
     *
     */
    public function test_prepare_bind()
    {
        $compiled = new Compiled("SELECT * FROM my_schema WHERE first_name = ?", new BindingContainer([new Binding("name")]), $this->hydrator);

        $result = $this->queryConnection->prepare($compiled);

        $this->assertInstanceOf(SqlQueryResult::class, $result);

        $this->assertSame($result, $result->bind(["name" => "Not found"]));
        $this->assertSame($result, $result->execute());

        $this->assertEmpty($result->all());

        $all = $result->bind(["name" => "John"])->execute()->all();
        $this->assertCount(1, $all);

        $this->assertEquals((object) ["id" => 1, "firstName" => "John", "lastName" => "Doe", "age" => 35], $all[0]);
    }

    /**
     *
     */
    public function test_fetch_no_data()
    {
        $compiled = new Compiled("SELECT * FROM my_schema WHERE first_name = ?", new BindingContainer([new Binding("name")]), $this->hydrator);

        $result = $this->queryConnection->prepare($compiled);

        $this->assertInstanceOf(SqlQueryResult::class, $result);

        $this->assertSame($result, $result->bind(["name" => "Not found"]));
        $this->assertSame($result, $result->execute());

        $this->assertNull($result->fetch());
    }

    /**
     *
     */
    public function test_fetch_with_data()
    {
        $compiled = new Compiled("SELECT * FROM my_schema WHERE first_name = ?", new BindingContainer([new Binding("name")]), $this->hydrator);

        $result = $this->queryConnection->prepare($compiled);

        $this->assertInstanceOf(SqlQueryResult::class, $result);

        $data = $result->bind(["name" => "John"])->execute()->fetch();

        $this->assertEquals((object) ["id" => 1, "firstName" => "John", "lastName" => "Doe", "age" => 35], $data);
    }
}
