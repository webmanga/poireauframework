<?php

namespace PoireauFramework\Database\Mapping\Query;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\MySQLPlatform;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Query
 * @group ext_PoireauFramework_Database_Mapping_Query_QueryFactory
 */
class QueryFactoryTest extends TestCase
{
    /**
     * @var QueryFactory
     */
    private $factory;
    private $metadata;
    private $platform;
    private $hydrator;

    protected function setUp()
    {
        $types = new TypeRegistry();

        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->platform = new MySQLPlatform($types);

        $this->metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;

        $this->metadata->schema = 'USER';
        $this->metadata->entity = User::class;

        $this->hydrator = new MetadataHydrator($this->metadata, $types);
        $this->factory = new QueryFactory($this->metadata, $this->hydrator, $this->platform);
    }

    /**
     *
     */
    public function test_select_defaults()
    {
        $query = $this->factory->select();

        $this->assertInstanceOf(Compiled::class, $query);
        $this->assertEquals("SELECT * FROM `USER`", $query->query());

        $this->assertSame($query, $this->factory->select());
        $this->assertSame($query, $this->factory->select("select_all"));
    }

    /**
     *
     */
    public function test_select_named_default()
    {
        $query = $this->factory->select("my_lbl");

        $this->assertInstanceOf(Compiled::class, $query);
        $this->assertSame($query, $this->factory->select("my_lbl"));
    }

    /**
     *
     */
    public function test_select_named_with_builder()
    {
        $query = $this->factory->select("my_query", function (Select $select) {
            $select->where([
                [":or" => [
                    "username" => new Binding(),
                    "email"    => new Binding()
                ]],
                "password" => new Binding()
            ]);
        });

        $this->assertInstanceOf(Compiled::class, $query);
        $this->assertEquals("SELECT * FROM `USER` WHERE ((`USER_LOGIN` = ? OR `USER_MAIL` = ?)) AND `USER_PASSWORD` = ?", $query->query());

        $this->assertSame($query, $this->factory->select("my_query"));
    }

    /**
     *
     */
    public function test_select_with_criteria_array()
    {
        $query = $this->factory->select([
            "username" => new Binding(),
            "password" => new Binding()
        ]);

        $this->assertInstanceOf(Compiled::class, $query);
        $this->assertEquals("SELECT * FROM `USER` WHERE `USER_LOGIN` = ? AND `USER_PASSWORD` = ?", $query->query());
    }

    /**
     *
     */
    public function test_select_with_named_criteria_array()
    {
        $query = $this->factory->select("my_query", [
            "username" => new Binding(),
            "password" => new Binding()
        ]);

        $this->assertInstanceOf(Compiled::class, $query);
        $this->assertEquals("SELECT * FROM `USER` WHERE `USER_LOGIN` = ? AND `USER_PASSWORD` = ?", $query->query());
        $this->assertSame($query, $this->factory->select("my_query"));
    }

    /**
     *
     */
    public function test_select_anonymous()
    {
        $builder = function (Select $select) {
            $select->where([
                ["username", "LIKE", "%john%doe%"],
                "name" => "John"
            ]);
        };
        $query = $this->factory->select($builder);

        $this->assertInstanceOf(Compiled::class, $query);
        $this->assertEquals("SELECT * FROM `USER` WHERE `USER_LOGIN` LIKE ? AND `USER_NAME` = ?", $query->query());

        $this->assertNotSame($query, $this->factory->select($builder));
        $this->assertEquals($query, $this->factory->select($builder));
    }

    /**
     *
     */
    public function test_insert_anonymous()
    {
        $insert = $this->factory->insert();

        $this->assertInstanceOf(Compiled::class, $insert);
        $this->assertEquals("INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES (?, ?, ?, ?, ?)", $insert->query());

        $this->assertSame($insert, $this->factory->insert());
    }

    /**
     *
     */
    public function test_insert_named()
    {
        $insert = $this->factory->insert("my_insert");

        $this->assertInstanceOf(Compiled::class, $insert);
        $this->assertEquals("INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES (?, ?, ?, ?, ?)", $insert->query());
        $this->assertSame($insert, $this->factory->insert("my_insert"));
    }

    /**
     *
     */
    public function test_insert_builder()
    {
        $insert = $this->factory->insert(function (Insert $insert) {
            $insert
                ->bulk()
                ->attributes(["username", "password"])
                ->values((new User())
                    ->setUsername("John")
                    ->setPassword("password")
                )
            ;
        });

        $this->assertInstanceOf(Compiled::class, $insert);
        $this->assertEquals("INSERT INTO `USER`(`USER_LOGIN`, `USER_PASSWORD`) VALUES ('John', 'password')", $insert->query());
    }

    /**
     *
     */
    public function test_insert_builder_named()
    {
        $insert = $this->factory->insert("bulk", function (Insert $insert) {
            $insert
                ->bulk()
                ->attributes(["username", "password"])
                ->values((new User())
                    ->setUsername("John")
                    ->setPassword("password")
                )
            ;
        });

        $this->assertInstanceOf(Compiled::class, $insert);
        $this->assertEquals("INSERT INTO `USER`(`USER_LOGIN`, `USER_PASSWORD`) VALUES ('John', 'password')", $insert->query());
        $this->assertSame($insert, $this->factory->insert("bulk"));
    }
}
