<?php

namespace PoireauFramework\Database\Mapping;

require_once __DIR__."/_files/User.php";
require_once __DIR__."/_files/UserMapper.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Resolver\MapperResolverInterface;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Test\UserMapper;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Functional
 */
class FunctionalTest extends TestCase
{
    /**
     * @var UserMapper
     */
    private $userMapper;

    /**
     * @var User[]
     */
    private $users = [];

    protected function setUp()
    {
        $injector = new BaseInjector(RegistryFactory::createFromArray([
            "database" => [
                "default_connection" => "test"
            ]
        ]));

        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "type"   => "sqlite",
                    "memory" => true
                ]
            ]
        ])));

        $injector->register(new DatabaseModule());

        $this->userMapper = $injector->get(MapperResolverInterface::class)->get(UserMapper::class);
        $this->userMapper->schema()->create();

        $this->users = [
            "john_doe" => (new User())
                ->setUsername("john_doe")
                ->setName("John")
                ->setEmail("john.doe@webmanga.fr")
                ->setPassword("MySecurePasswd")
                ->setId(1),
            "robert" => (new User())
                ->setUsername("rob")
                ->setName("Robert")
                ->setEmail("rob@webmanga.fr")
                ->setPassword("robert123")
                ->setId(2),
        ];

        foreach ($this->users as $user) {
            $this->userMapper->insert($user);
        }
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->userMapper->schema()->drop();
    }

    /**
     *
     */
    public function test_get_user()
    {
        $this->assertEquals($this->users["john_doe"], $this->userMapper->get(1));
        $this->assertEquals($this->users["robert"], $this->userMapper->get(2));
    }

    /**
     *
     */
    public function test_findForLogin()
    {
        $this->assertEquals($this->users["john_doe"], $this->userMapper->findForLogin("john_doe", "MySecurePasswd"));
        $this->assertNull($this->userMapper->findForLogin("john_doe", "BadPasswd"));
    }

    /**
     *
     */
    public function test_all_users()
    {
        $this->assertEquals(array_values($this->users), $this->userMapper->all());
    }

    /**
     *
     */
    public function test_insert()
    {
        $newUser = (new User())
            ->setName("Paul")
            ->setUsername("polo")
            ->setPassword("azerty")
            ->setEmail("polo@webmanga.fr")
        ;

        $this->userMapper->insert($newUser);

        $this->assertEquals(3, $newUser->id());
        $this->assertEquals($newUser, $this->userMapper->get(3));
    }

    /**
     *
     */
    public function test_login_with_rebuild_closure()
    {
        $this->assertEquals($this->users["john_doe"], $this->userMapper->loginWithRebuild("john_doe", "MySecurePasswd"));
        $this->assertNull($this->userMapper->loginWithRebuild("john_doe", "BadPasswd"));
    }

    /**
     *
     */
    public function test_login_with_rebuild_array()
    {
        $this->assertEquals($this->users["john_doe"], $this->userMapper->loginWithArray("john_doe", "MySecurePasswd"));
        $this->assertNull($this->userMapper->loginWithArray("john_doe", "BadPasswd"));
    }
}
