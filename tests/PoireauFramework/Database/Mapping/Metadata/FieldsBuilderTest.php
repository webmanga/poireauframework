<?php

namespace PoireauFramework\Database\Mapping\Metadata;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Metadata
 * @group ext_PoireauFramework_Database_Mapping_Metadata_FieldsBuilder
 */
class FieldsBuilderTest extends TestCase
{
    /**
     * @var FieldsBuilder
     */
    private $builder;


    /**
     *
     */
    protected function setUp()
    {
        $this->builder = new FieldsBuilder(new Metadata());
    }

    /**
     *
     */
    public function test_add()
    {
        $this->assertSame($this->builder, $this->builder->add("foo", "text"));

        $metadata = $this->builder->build();

        $this->assertInstanceOf(Field::class, $metadata->fields["foo"]);
        $this->assertInstanceOf(Field::class, $metadata->attributes["foo"]);

        $field = $metadata->fields["foo"];

        $this->assertEquals("foo", $field->name);
        $this->assertEquals("foo", $field->attribute);
        $this->assertEquals("text", $field->type);
    }

    /**
     *
     */
    public function test_attribute()
    {
        $this->assertSame($this->builder, $this->builder->add("foo", "text")->attribute("db_foo"));

        $metadata = $this->builder->build();

        $field = $metadata->fields["foo"];

        $this->assertEquals("foo", $field->name);
        $this->assertEquals("db_foo", $field->attribute);
    }

    /**
     *
     */
    public function test_primary()
    {
        $this->assertSame($this->builder, $this->builder->add("foo", "text")->primary());

        $metadata = $this->builder->build();

        $field = $metadata->fields["foo"];

        $this->assertTrue($field->is(Field::PRIMARY));
        $this->assertEquals([$field], $metadata->primary);
    }

    /**
     *
     */
    public function test_unique()
    {
        $this->assertSame($this->builder, $this->builder->add("foo", "text")->unique());

        $metadata = $this->builder->build();

        $field = $metadata->fields["foo"];

        $this->assertTrue($field->is(Field::UNIQUE));
    }

    /**
     *
     */
    public function test_autoincrement()
    {
        $this->assertSame($this->builder, $this->builder->add("foo", "text")->autoincrement());

        $metadata = $this->builder->build();

        $field = $metadata->fields["foo"];

        $this->assertTrue($field->is(Field::AUTO_INCREMENT));
        $this->assertSame([$field], $metadata->generated);
    }

    /**
     *
     */
    public function test_notNull()
    {
        $this->assertSame($this->builder, $this->builder->add("foo", "text")->notNull());

        $metadata = $this->builder->build();

        $field = $metadata->fields["foo"];

        $this->assertTrue($field->is(Field::NOT_NULL));
    }

    /**
     *
     */
    public function test_string()
    {
        $this->assertSame($this->builder, $this->builder->string("foo", 123));

        $metadata = $this->builder->build();

        $this->assertInstanceOf(Field::class, $metadata->fields["foo"]);
        $this->assertInstanceOf(Field::class, $metadata->attributes["foo"]);

        $field = $metadata->fields["foo"];

        $this->assertEquals("foo", $field->name);
        $this->assertEquals("foo", $field->attribute);
        $this->assertEquals("string", $field->type);
        $this->assertEquals(123, $field->length);
    }

    /**
     *
     */
    public function test_integer()
    {
        $this->assertSame($this->builder, $this->builder->integer("foo"));

        $metadata = $this->builder->build();

        $this->assertInstanceOf(Field::class, $metadata->fields["foo"]);
        $this->assertInstanceOf(Field::class, $metadata->attributes["foo"]);

        $field = $metadata->fields["foo"];

        $this->assertEquals("foo", $field->name);
        $this->assertEquals("foo", $field->attribute);
        $this->assertEquals("integer", $field->type);
    }

    /**
     *
     */
    public function test_list()
    {
        $this->assertSame($this->builder, $this->builder->list("foo"));

        $metadata = $this->builder->build();

        $this->assertInstanceOf(Field::class, $metadata->fields["foo"]);
        $this->assertInstanceOf(Field::class, $metadata->attributes["foo"]);

        $field = $metadata->fields["foo"];

        $this->assertEquals("foo", $field->name);
        $this->assertEquals("foo", $field->attribute);
        $this->assertEquals("list", $field->type);
    }
}
