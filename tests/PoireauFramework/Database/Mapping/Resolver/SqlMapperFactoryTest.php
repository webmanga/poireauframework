<?php

namespace PoireauFramework\Database\Mapping\Resolver;

require_once __DIR__."/../_files/User.php";
require_once __DIR__."/../_files/UserMapper.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\Test\UserMapper;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Resolver
 * @group ext_PoireauFramework_Database_Mapping_Resolver_SqlMapperFactory
 */
class SqlMapperFactoryTest extends TestCase
{
    /**
     * @var SqlMapperFactory
     */
    private $factory;

    /**
     *
     */
    protected function setUp()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "dsn" => "sqlite::memory:"
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        $this->factory = new SqlMapperFactory($injector->get(DatabaseHandler::class));
    }

    /**
     *
     */
    public function test_supports()
    {
        $this->assertTrue($this->factory->supports(UserMapper::class));
        $this->assertFalse($this->factory->supports(self::class));
    }

    /**
     *
     */
    public function test_create()
    {
        $config = new ConfigStruct();

        $config->connection = "test";

        /** @var UserMapper $mapper */
        $mapper = $this->factory->create(UserMapper::class, $config);

        $this->assertInstanceOf(UserMapper::class, $mapper);

        $mapper->schema()->create();
        $mapper->schema()->drop();
    }
}
