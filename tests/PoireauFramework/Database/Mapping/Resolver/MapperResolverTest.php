<?php

namespace PoireauFramework\Database\Mapping\Resolver;

require_once __DIR__."/../_files/User.php";
require_once __DIR__."/../_files/UserMapper.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Mapping\MapperInterface;
use PoireauFramework\Database\Mapping\Test\UserMapper;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Resolver
 * @group ext_PoireauFramework_Database_Mapping_Resolver_MapperResolver
 */
class MapperResolverTest extends TestCase
{
    /**
     * @var MapperResolver
     */
    private $resolver;


    /**
     *
     */
    protected function setUp()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "dsn" => "sqlite::memory:"
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        $factory = new SqlMapperFactory($injector->get(DatabaseHandler::class));

        $this->resolver = new MapperResolver([$factory], "test");
    }

    /**
     *
     */
    public function test_get_unit()
    {
        $mapper = $this->createMock(MapperInterface::class);

        $badFactory = $this->createMock(MapperFactoryInterface::class);
        $factory = $this->createMock(MapperFactoryInterface::class);

        $factory->expects($this->once())
            ->method("supports")
            ->with(get_class($mapper))
            ->willReturn(true)
        ;

        $badFactory->expects($this->once())
            ->method("supports")
            ->with(get_class($mapper))
            ->willReturn(false)
        ;

        $config = new ConfigStruct();
        $config->connection = "default";

        $factory->expects($this->once())
            ->method("create")
            ->with(get_class($mapper), $config)
            ->willReturn($mapper)
        ;

        $resolver = new MapperResolver([$badFactory, $factory]);

        $this->assertSame($mapper, $resolver->get(get_class($mapper)));
        $this->assertSame($mapper, $resolver->get(get_class($mapper)));
    }

    /**
     *
     */
    public function test_get_functional()
    {
        $this->assertInstanceOf(UserMapper::class, $this->resolver->get(UserMapper::class));
        $this->assertSame($this->resolver->get(UserMapper::class), $this->resolver->get(UserMapper::class));
    }
}
