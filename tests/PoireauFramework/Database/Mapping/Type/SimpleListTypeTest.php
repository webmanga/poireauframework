<?php

namespace PoireauFramework\Database\Mapping\Type;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Type
 * @group ext_PoireauFramework_Database_Mapping_Type_SimpleListType
 */
class SimpleListTypeTest extends TestCase
{
    /**
     * @var SimpleListType
     */
    private $type;

    /**
     *
     */
    protected function setUp()
    {
        $this->type = new SimpleListType();
    }

    /**
     *
     */
    public function test_toDatabase()
    {
        $this->assertSame("foo,bar", $this->type->toDatabase(["foo", "bar"]));
    }

    /**
     *
     */
    public function test_fromDatabase()
    {
        $this->assertSame(["foo", "bar"], $this->type->fromDatabase("foo,,,bar,"));
    }

    /**
     *
     */
    public function test_with_null()
    {
        $this->assertNull($this->type->fromDatabase(null));
        $this->assertNull($this->type->toDatabase(null));
    }

    /**
     *
     */
    public function test_sqlDeclaration()
    {
        $field = new Field();

        $this->assertEquals("TEXT", $this->type->sqlDeclaration($field));
    }
}
