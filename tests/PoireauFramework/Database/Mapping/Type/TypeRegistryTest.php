<?php

namespace PoireauFramework\Database\Mapping\Type;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Type
 * @group ext_PoireauFramework_Database_Mapping_Type_TypeRegistry
 */
class TypeRegistryTest extends TestCase
{
    /**
     * @var TypeRegistry
     */
    private $types;


    /**
     *
     */
    protected function setUp()
    {
        $this->types = new TypeRegistry();
    }

    /**
     *
     */
    public function test_add_get()
    {
        $type = new StringType();
        $this->types->add($type);

        $this->assertSame($type, $this->types->get("string"));
    }

    /**
     *
     */
    public function test_lazy_get()
    {
        $this->types->lazy("string", StringType::class);

        $this->assertInstanceOf(StringType::class, $this->types->get("string"));
        $this->assertSame($this->types->get("string"), $this->types->get("string"));
    }
}
