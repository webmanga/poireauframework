<?php

namespace PoireauFramework\Database\Mapping\Type;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Metadata\Field;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Type
 * @group ext_PoireauFramework_Database_Mapping_Type_StringType
 */
class StringTypeTest extends TestCase
{
    /**
     * @var StringType
     */
    private $type;


    /**
     *
     */
    protected function setUp()
    {
        $this->type = new StringType();
    }

    /**
     *
     */
    public function test_toDatabase()
    {
        $this->assertSame("123", $this->type->toDatabase(123));
    }

    /**
     *
     */
    public function test_fromDatabase()
    {
        $this->assertSame("123", $this->type->fromDatabase("123"));
    }

    /**
     *
     */
    public function test_with_null()
    {
        $this->assertNull($this->type->fromDatabase(null));
        $this->assertNull($this->type->toDatabase(null));
    }

    /**
     *
     */
    public function test_sqlDeclaration()
    {
        $field = new Field();

        $this->assertEquals("VARCHAR", $this->type->sqlDeclaration($field));

        $field->length = 12;
        $this->assertEquals("VARCHAR(12)", $this->type->sqlDeclaration($field));
    }
}
