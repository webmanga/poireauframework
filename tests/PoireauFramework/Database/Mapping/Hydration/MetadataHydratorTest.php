<?php

namespace PoireauFramework\Database\Hydration;

require_once __DIR__."/../_files/User.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Hydration\MetadataHydrator;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Test\User;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Hydration
 * @group ext_PoireauFramework_Database_Mapping_Hydration_MetadataHydrator
 */
class MetadataHydratorTest extends TestCase
{
    /**
     * @var MetadataHydrator
     */
    private $hydrator;


    /**
     *
     */
    protected function setUp()
    {
        $metadata = new Metadata();

        $metadata->entity = User::class;
        $metadata->schema = "user";

        $builder = new FieldsBuilder($metadata);

        $builder
            ->integer("id")->attribute("_id")
            ->string("username")->attribute("_username")
            ->string("name")->attribute("_name")
            ->string("email")->attribute("_email")
            ->string("password")->attribute("_password")
            ->build()
        ;

        $types = new TypeRegistry();
        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->hydrator = new MetadataHydrator($metadata, $types);
    }

    /**
     *
     */
    public function test_fromDatabase()
    {
        /** @var User $entity */
        $entity = $this->hydrator->fromDatabase([
            "_id"       => "123",
            "_username" => "john_doe",
            "_name"     => "John",
            "_email"    => "j.doe@webmanga.fr",
        ]);

        $this->assertInstanceOf(User::class, $entity);

        $this->assertSame(123, $entity->id());
        $this->assertSame("john_doe", $entity->username());
        $this->assertSame("John", $entity->name());
        $this->assertSame("j.doe@webmanga.fr", $entity->email());
        $this->assertNull($entity->password());
    }

    /**
     *
     */
    public function test_fromDatabase_null()
    {
        /** @var User $entity */
        $entity = $this->hydrator->fromDatabase([
            "_id"       => null,
        ]);

        $this->assertInstanceOf(User::class, $entity);

        $this->assertNull($entity->id());
    }

    /**
     *
     */
    public function test_toDatabase()
    {
        $entity = new User();

        $entity->setId(123);
        $entity->setUsername("john_doe");
        $entity->setName("John");
        $entity->setEmail("j.doe@webmanga.fr");

        $this->assertSame([
            "_id"       => 123,
            "_username" => "john_doe",
            "_name"     => "John",
            "_email"    => "j.doe@webmanga.fr",
            "_password" => null
        ], $this->hydrator->toDatabase($entity));
    }

    /**
     *
     */
    public function test_toDatabase_null()
    {
        $entity = new User();

        $this->assertEquals([
            "_id"       => null,
            "_username" => null,
            "_name"     => null,
            "_email"    => null,
            "_password" => null
        ], $this->hydrator->toDatabase($entity));
    }
}
