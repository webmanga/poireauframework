<?php

namespace PoireauFramework\Database\Mapping\Test;

use PoireauFramework\Database\Mapping\Query\Bag\Binding;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Select;
use PoireauFramework\Database\Mapping\SqlMapper;

class UserMapper extends SqlMapper
{
    /**
     * {@inheritdoc}
     */
    protected function configure(Metadata $metadata): void
    {
        $metadata->entity = User::class;
        $metadata->schema = "USER";
    }

    /**
     * {@inheritdoc}
     */
    protected function buildFields(FieldsBuilder $builder): void
    {
        $builder
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
        ;
    }

    public function insert(User $user)
    {
        $query = $this->query()->insert();

        $this->connection->prepare($query)->bind($user)->execute();

        if ($user->id() === null) {
            return $user->setId($this->db->lastInsertedId());
        }

        return $user;
    }

    /**
     * @return User[]
     */
    public function all()
    {
        return $this->connection->execute($this->query()->select())->all();
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function get($id)
    {
        $query = $this->query()->select("get", ["id" => new Binding()]);

        return $this->connection
            ->prepare($query)
            ->bind(["id" => $id])
            ->execute()
            ->fetch()
        ;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return User|null
     */
    public function findForLogin($username, $password)
    {
        $query = $this->query()->select("login", function (Select $select) {
            $select->where([
                "username" => new Binding(),
                "password" => new Binding()
            ]);
        });

        return $this->connection
            ->prepare($query)
            ->bind(["username" => $username, "password" => $password])
            ->execute()
            ->fetch()
        ;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return User|null
     */
    public function loginWithRebuild($username, $password)
    {
        $query = $this->query()->select(function (Select $select) use($username, $password) {
            $select->where([
                "username" => $username,
                "password" => $password
            ]);
        });

        return $this->connection
            ->execute($query)
            ->fetch()
        ;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return User|null
     */
    public function loginWithArray($username, $password)
    {
        $query = $this->query()->select([
            "username" => $username,
            "password" => $password
        ]);

        return $this->connection
            ->execute($query)
            ->fetch()
        ;
    }
}
