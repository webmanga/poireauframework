<?php

namespace PoireauFramework\Database\Mapping\Schema;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Exception\SqlException;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Type\IntegerType;
use PoireauFramework\Database\Mapping\Type\StringType;
use PoireauFramework\Database\Mapping\Type\TypeRegistry;
use PoireauFramework\Database\Sql\Platform\SQLitePlatform;
use PoireauFramework\Database\Sql\SqlConnectionInterface;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Schema
 * @group ext_PoireauFramework_Database_Mapping_Schema_SqlSchema
 */
class SqlSchemaTest extends TestCase
{
    /**
     * @var SqlSchema
     */
    private $schema;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $connection;


    /**
     *
     */
    protected function setUp()
    {
        $types = new TypeRegistry();
        $types->add(new StringType());
        $types->add(new IntegerType());

        $this->connection = $this->createMock(SqlConnectionInterface::class);
        $this->connection->expects($this->any())->method("platform")->willReturn(new SQLitePlatform($types));

        $metadata = new Metadata();

        $metadata->schema = "my_schema";

        (new FieldsBuilder($metadata))
            ->integer("id")->autoincrement()->primary()
            ->string("firstName")->attribute("first_name")->notNull()
            ->string("lastName")->attribute("last_name")
            ->integer("age")
            ->build()
        ;

        $this->schema = new SqlSchema($metadata, $this->connection);
    }

    /**
     *
     */
    public function test_create()
    {
        $this->connection->expects($this->once())
            ->method("query")
            ->with("CREATE TABLE `my_schema` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `first_name` VARCHAR NOT NULL, `last_name` VARCHAR, `age` INTEGER)")
        ;

        $this->schema->create();
    }

    /**
     *
     */
    public function test_drop()
    {
        $this->connection->expects($this->once())
            ->method("query")
            ->with("DROP TABLE `my_schema`")
        ;

        $this->schema->drop();
    }

    /**
     *
     */
    public function test_functional()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "database" => [
                "test" => [
                    "driver" => "pdo",
                    "dsn" => "sqlite::memory:"
                ]
            ]
        ])));
        $injector->register(new DatabaseModule());

        /** @var SqlConnectionInterface $connection */
        $connection = $injector->get(DatabaseHandler::class)->getConnection("test");

        $metadata = new Metadata();

        $metadata->schema = "my_schema";

        (new FieldsBuilder($metadata))
            ->integer("id")->autoincrement()->primary()
            ->string("firstName")->attribute("first_name")->notNull()
            ->string("lastName")->attribute("last_name")
            ->integer("age")
            ->build()
        ;

        $schema = new SqlSchema($metadata, $connection);

        $schema->create();

        $connection->query("INSERT INTO my_schema VALUES (NULL, 'John', 'Doe', 26)");

        $this->assertEquals(["id" => 1, "first_name" => "John", "last_name" => "Doe", "age" => "26"], $connection->query("SELECT * FROM my_schema")->fetch());

        $schema->drop();

        try {
            $connection->query("SELECT * FROM my_schema");
        } catch (SqlException $exception) {
            $this->assertContains("no such table: my_schema", $exception->getMessage());
            return;
        }

        $this->fail("expect exception");
    }
}
