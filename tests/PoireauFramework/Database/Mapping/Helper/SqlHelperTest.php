<?php

namespace PoireauFramework\Database\Mapping\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Database
 * @group ext_PoireauFramework_Database_Mapping
 * @group ext_PoireauFramework_Database_Mapping_Helper
 * @group ext_PoireauFramework_Database_Mapping_Helper_SqlHelper
 */
class SqlHelperTest extends TestCase
{
    /**
     * @var SqlHelper
     */
    protected $helper;


    /**
     *
     */
    protected function setUp()
    {
        $metadata = (new FieldsBuilder(new Metadata()))
            ->integer("id")->attribute("USER_ID")
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
            ->build()
        ;

        $metadata->schema = "USER";

        $this->helper = new SqlHelper($metadata);
    }

    /**
     *
     */
    public function test_insert()
    {
        $this->assertEquals(
            "INSERT INTO `USER`(`USER_ID`, `USER_LOGIN`, `USER_NAME`, `USER_PASSWORD`, `USER_MAIL`) VALUES(:USER_ID, :USER_LOGIN, :USER_NAME, :USER_PASSWORD, :USER_MAIL)",
            $this->helper->insert()
        );
    }

    /**
     *
     */
    public function test_identifier()
    {
        $this->assertEquals("`my``identifier`", $this->helper->identifier("my`identifier"));
    }

    /**
     *
     */
    public function test_table()
    {
        $this->assertEquals("`USER`", $this->helper->table());
    }

    public function test_attr()
    {
        $this->assertEquals("`USER_ID`", $this->helper->attr("id"));
        $this->assertEquals("`u2`.`USER_ID`", $this->helper->attr("id", "u2"));
    }
}
