<?php

namespace PoireauFramework\Helper;

use PHPUnit\Framework\TestCase;

/**
 * Description of ReadOnlyObjectTest
 *
 * @author vquatrevieux
 */
class ReadOnlyObjectTest extends TestCase {
    /**
     * 
     */
    public function test_contructor() {
        $object = new ReadOnlyObject([
            'foo' => 'bar'
        ]);
        
        $this->assertAttributeEquals(['foo' => 'bar'], 'data', $object);
    }
    
    /**
     * 
     */
    public function test_array_get()
    {
        $object = new ReadOnlyObject([
            'foo' => 'bar'
        ]);
        
        $this->assertEquals('bar', $object['foo']);
    }
    
    /**
     * 
     */
    public function test_object_get()
    {
        $object = new ReadOnlyObject([
            'foo' => 'bar'
        ]);
        
        $this->assertEquals('bar', $object->foo);
    }
    
    /**
     * 
     */
    public function test_isset()
    {
        $object = new ReadOnlyObject([
            'foo' => 'bar'
        ]);
        
        $this->assertTrue(isset($object->foo));
        $this->assertFalse(isset($object->aaa));
    }
    
    /**
     * 
     */
    public function test_set_object()
    {
        $object = new ReadOnlyObject([]);
        
        $this->expectException(\BadMethodCallException::class);
        
        $object->foo = 'bar';
    }
    
    /**
     * 
     */
    public function test_array_set()
    {
        $object = new ReadOnlyObject([]);
        
        $this->expectException(\BadMethodCallException::class);
        
        $object['foo'] = 'bar';
    }

    /**
     *
     */
    public function test_array_isset()
    {
        $object = new ReadOnlyObject([
            'foo' => 'bar'
        ]);

        $this->assertTrue(isset($object["foo"]));
        $this->assertFalse(isset($object["aaa"]));
    }
    
    /**
     * 
     */
    public function test_array_unset()
    {
        $object = new ReadOnlyObject([]);
        
        $this->expectException(\BadMethodCallException::class);
        
        unset($object['foo']);
    }
}
