<?php
namespace PoireauFramework\Injector;

require_once 'TestClasses.php';

use PHPUnit\Framework\TestCase;

/**
 * Class ChildInjectorTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Injector
 * @group ext_PoireauFramework_Injector_ChildInjector
 */
class ChildInjectorTest extends TestCase
{
    /**
     * @var ChildInjector
     */
    private $injector;

    /**
     * @var BaseInjector
     */
    private $parent;


    protected function setUp()
    {
        parent::setUp();

        $this->parent = new BaseInjector();
        $this->injector = $this->parent->fork('child');
    }

    public function test_parent()
    {
        $this->assertSame($this->parent, $this->injector->parent());
    }

    public function test_isRoot()
    {
        $this->assertFalse($this->injector->isRoot());
    }

    public function test_get_from_parent()
    {
        $a = new A();
        $this->parent->instance($a);

        $this->assertSame($a, $this->injector->get(A::class));
    }

    public function test_create_from_parent()
    {
        $this->parent->factory(A::class, function(InjectorInterface $injector) {
            return new A();
        });

        $this->assertInstanceOf(A::class, $this->injector->create(A::class));
    }

    public function test_get_from_parent_impl()
    {
        $this->parent->factory(B::class, function(InjectorInterface $injector) {
            return new B();
        });

        $this->injector->impl(I::class, B::class);

        $this->assertInstanceOf(B::class, $this->injector->get(I::class));
    }
}
