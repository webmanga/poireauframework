<?php
/**
 * Created by PhpStorm.
 * User: vquatrevieux
 * Date: 03/11/16
 * Time: 09:15
 */

namespace PoireauFramework\Injector;


use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;

class A {}

interface I{}

class B implements I{}

class C {
    /**
     * @var A
     */
    public $a;

    /**
     * C constructor.
     * @param A $a
     */
    public function __construct(A $a)
    {
        $this->a = $a;
    }
}

class D implements InjectorInstantiable {
    public $a;
    public $injector;

    static public function createInstance(InjectorInterface $injector, array $parameters = array()): InjectorInstantiable
    {
        $d =  new D();

        $d->a = 123;
        $d->injector = $injector;

        return $d;
    }
}

class AModule implements InjectorModuleInterface {

    /**
     * {@inheritdoc}
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->factory(A::class, function (InjectorInterface $injector) {
            return new A();
        });
    }
}