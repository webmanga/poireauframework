<?php
namespace PoireauFramework\Injector;


use PoireauFramework\Injector\Exception\InjectorException;
use PHPUnit\Framework\TestCase;

/**
 * Class BaseInjectorTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Injector
 * @group ext_PoireauFramework_Injector_BaseInjector
 */
class BaseInjectorTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;

    protected function setUp()
    {
        parent::setUp();

        $this->injector = new BaseInjector();
    }

    public function test_parent() {
        $this->expectException(InjectorException::class);
        $this->expectExceptionMessage("BaseInjector do not have parent");

        $this->injector->parent();
    }

    public function test_isRoot() {
        $this->assertTrue($this->injector->isRoot());
    }
}