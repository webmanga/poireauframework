<?php
namespace PoireauFramework\Injector;

require_once 'TestClasses.php';

use PoireauFramework\Injector\Exception\InjectorException;
use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\Factory\FactoryContainer;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Class AbstractInjectorTest
 * @package PoireauFramework\Injector
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Injector
 * @group ext_PoireauFramework_Injector_AbstractInjector
 */
class AbstractInjectorTest extends TestCase
{
    /**
     * @var InjectorImpl
     */
    private $injector;

    protected function setUp()
    {
        parent::setUp();

        $this->injector = new InjectorImpl();
    }

    public function test_create_no_factory()
    {
        $this->expectException(InjectorException::class);
        $this->expectExceptionMessage("No factory found for A");

        $this->injector->create('A');
    }

    public function test_get_no_factory()
    {
        $this->expectException(InjectorException::class);
        $this->expectExceptionMessage("No factory found for A");

        $this->injector->get('A');
    }

    public function test_instance()
    {
        $a = new A();

        $this->injector->instance($a);

        $this->assertTrue($this->injector->registry()->has(A::class));
        $this->assertSame($a, $this->injector->registry()->get(A::class));
    }

    public function test_impl()
    {
        $this->injector->impl(I::class, B::class);

        $this->assertEquals(B::class, $this->injector->registry()->get(I::class));
    }

    public function test_factory()
    {
        $factory = $this->injector->factory(A::class, function($i) {
            return new A;
        });

        $this->assertInstanceOf(FactoryContainer::class, $factory);
        $this->assertSame($factory, $this->injector->registry()->get(A::class));
    }

    public function test_get_registered_instance()
    {
        $a = new A();

        $this->injector->instance($a);

        $this->assertSame($a, $this->injector->get(A::class));
    }

    public function test_get_registered_instance_impl()
    {
        $this->injector->impl(I::class, B::class);

        $b = new B();

        $this->injector->instance($b);

        $this->assertSame($b, $this->injector->get(I::class));
    }

    public function test_get_factory()
    {
        $isCalled = false;

        $factory = function(InjectorInterface $injector) use(&$isCalled) {
            $isCalled = true;
            $this->assertSame($this->injector, $injector);
            return new A();
        };

        $this->injector->factory(A::class, $factory);

        $a = $this->injector->get(A::class);

        $this->assertTrue($isCalled, 'The factory has not been called');
        $this->assertInstanceOf(A::class, $a);
    }

    public function test_get_factory_with_child()
    {
        $child = $this->injector->fork('child');
        $a = new A();
        $child->instance($a);

        $factory = function(InjectorInterface $injector) use($child) {
            $this->assertSame($child, $injector);
            return new C(
                $injector->get(A::class)
            );
        };

        $this->injector->factory(C::class, $factory)
            ->injector($child)
        ;

        $c = $this->injector->get(C::class);

        $this->assertInstanceOf(C::class, $c);
        $this->assertSame($a, $c->a);
    }

    public function test_register()
    {
        $module = new AModule();

        $this->injector->register($module);

        $this->assertTrue($this->injector->registry()->has(A::class));
        $this->assertInstanceOf(A::class, $this->injector->get(A::class));
    }

    public function test_addDefaultFactory()
    {
        $factory = function($className, InjectorInterface $injector, array $arguments) {
            return null;
        };

        $this->injector->addDefaultFactory($factory);

        $this->assertEquals([$factory], $this->injector->getDefaultFactories());
    }

    public function test_create_with_default_factory()
    {
        $isCalled = false;

        $factory = function($className) use(&$isCalled) {
            $isCalled = true;
            return new $className;
        };

        $this->injector->addDefaultFactory($factory);
        $this->injector->create(A::class);

        $this->assertTrue($isCalled);
        $this->assertInstanceOf(A::class, $this->injector->create(A::class));
    }

    public function test_resolveClassName_no_impl()
    {
        $className = '\Foo\Bar';

        $this->assertEquals('Foo\Bar', $this->injector->resolveClassName($className));
    }

    public function test_resolveClassName_impl()
    {
        $this->injector->impl(I::class, B::class);

        $this->assertEquals(B::class, $this->injector->resolveClassName(I::class));
    }

    public function test_createWithDefaultFactories_no_factories()
    {
        $this->assertNull($this->injector->createWithDefaultFactories(A::class));
    }

    public function test_createWithDefaultFactories_one_factory()
    {
        $isCalled = false;

        $this->injector->addDefaultFactory(function($className) use(&$isCalled) {
            $isCalled = true;
            return new $className;
        });

        $instance = $this->injector->createWithDefaultFactories(A::class);

        $this->assertTrue($isCalled);
        $this->assertInstanceOf(A::class, $instance);
    }

    public function test_createWithDefaultFactories_multiple()
    {
        $this->injector->addDefaultFactory(function($className) use(&$isCalled) {
            if (is_subclass_of($className, B::class)) {
                return new B();
            }
        });

        $this->injector->addDefaultFactory(function($className) use(&$isCalled) {
            return null;
        });

        $this->injector->addDefaultFactory(function($className) {
            return new $className;
        });

        $isCalled = false;

        $this->injector->addDefaultFactory(function($className) use(&$isCalled) {
            $isCalled = true;
            return new B();
        });

        $this->assertInstanceOf(A::class, $this->injector->createWithDefaultFactories(A::class));
        $this->assertFalse($isCalled);
    }

    /**
     *
     */
    public function test_factory_twice()
    {
        $factory = $this->injector->factory(A::class, function () {
            return new A();
        });

        $this->assertSame($factory, $this->injector->factory(A::class, function () {
            return null;
        }));

        $this->assertNull($this->injector->get(A::class));
    }

    public function test_factory_with_custom_registry()
    {
        $this->injector->registry()->sub('child')->set('my_a', 456);

        $this->injector->factory(D::class, function (InjectorInterface $injector) {
            $d = new D();

            $d->injector = $injector;
            $d->a = $injector->raw('my_a');

            return $d;
        })->fork('child');

        $d = $this->injector->get(D::class);

        $this->assertEquals(456, $d->a);
    }

    public function test_create_with_factory()
    {
        $this->injector->factory(D::class, function (InjectorInterface $injector, $a) {
            $d = new D();

            $d->injector = $injector;
            $d->a = $a;

            return $d;
        })->fork('child');

        $d = $this->injector->create(D::class, ['Hello']);

        $this->assertEquals('Hello', $d->a);
    }

    public function test_non_persistent()
    {
        $this->injector->factory(A::class, function() {
            return new A();
        })->persistent(false);

        $this->assertInstanceOf(A::class, $this->injector->get(A::class));

        $this->assertNotSame($this->injector->get(A::class), $this->injector->get(A::class));
    }

    public function test_with_InjectorInstantiable()
    {
        $d = $this->injector->create(D::class);

        $this->assertInstanceOf(D::class, $d);

        $this->assertSame($this->injector, $d->injector);
        $this->assertEquals(123, $d->a);
    }

    public function test_raw()
    {
        $this->injector->registry()->set('my_raw', 'my_raw_value');

        $this->assertEquals('my_raw_value', $this->injector->raw('my_raw'));
    }

    public function test_has()
    {
        $this->injector->registry()->set('my_raw', 'my_raw_value');

        $this->assertTrue($this->injector->has('my_raw'));
        $this->assertFalse($this->injector->has('not_found'));
    }

    public function test_reg()
    {
        $this->assertInstanceOf(RegistryInterface::class, $this->injector->reg('sub'));

        $this->injector->registry()->sub('sub')->set('a', 'b');

        $this->assertEquals('b', $this->injector->reg('sub')->get('a'));
    }
}

class InjectorImpl extends AbstractInjector {

    /**
     * Get the parent injector
     *
     * @throws InjectorException When the injecto do not have a parent (root)
     * @see InjectorInterface::isRoot()
     * @return InjectorInterface
     */
    public function parent(): InjectorInterface
    {
        return null;
    }

    /**
     * Check if the current injector is root (i.e. do not have a parent)
     *
     * @see InjectorInterface::parent()
     * @return bool if this injector do not have a parent
     */
    public function isRoot(): bool
    {
        return true;
    }

    /**
     * @return RegistryInterface
     */
    public function registry()
    {
        return $this->registry;
    }

    /**
     * @return \callable[]
     */
    public function getDefaultFactories()
    {
        return $this->defaultFactories;
    }

    public function resolveClassName(string $className): string
    {
        return parent::resolveClassName($className); // TODO: Change the autogenerated stub
    }

    public function getDefault(string $className)
    {
        return parent::getDefault($className); // TODO: Change the autogenerated stub
    }

    public function createWithDefaultFactories(string $className, array $parameters = array())
    {
        return parent::createWithDefaultFactories($className, $parameters); // TODO: Change the autogenerated stub
    }

    public function createDefault(string $className, array $parameters = array())
    {
        return parent::createDefault($className, $parameters); // TODO: Change the autogenerated stub
    }


}