<?php

namespace PoireauFramework\Injector\Factory;

require_once __DIR__ . "/../TestClasses.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\A;
use PoireauFramework\Injector\AbstractInjector;
use PoireauFramework\Injector\B;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Injector\C;
use PoireauFramework\Injector\ChildInjector;
use PoireauFramework\Injector\D;

/**
 * Class FactoryContainerTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Injector
 * @group ext_PoireauFramework_Injector_Factory
 * @group ext_PoireauFramework_Injector_Factory_FactoryContainer
 */
class FactoryContainerTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $injector = new BaseInjector();
        $factory  = function () {
            return new A();
        };

        $container = new FactoryContainer($injector, A::class, $factory);

        $this->assertSame($injector, $container->getInjector());
        $this->assertEquals(A::class, $container->getClass());
        $this->assertTrue($container->isPersistent());
    }

    /**
     *
     */
    public function test_make()
    {
        $called = false;

        $injector = new BaseInjector();
        $callerContainer = null;

        $factory  = function ($a1, $a2) use(&$called, $injector, &$callerContainer) {
            $called = true;
            $callerContainer = $a2;

            $this->assertSame($injector, $a1);

            return new A();
        };

        $container = new FactoryContainer($injector, A::class, $factory);

        $a = $container->make();

        $this->assertInstanceOf(A::class, $a);
        $this->assertTrue($called);
        $this->assertSame($container, $callerContainer);
    }

    /**
     *
     */
    public function test_makeCustom()
    {
        $injector = new BaseInjector();
        $calledArguments = null;

        $factory  = function ($a1, $a2) use(&$calledArguments) {
            $calledArguments = func_get_args();

            return new A();
        };

        $container = new FactoryContainer($injector, A::class, $factory);

        $a = $container->makeCustom(["Hello", "World"]);

        $this->assertInstanceOf(A::class, $a);
        $this->assertEquals([$injector, "Hello", "World"], $calledArguments);
    }

    /**
     *
     */
    public function test_use_methods()
    {
        $injector = $this->createMock(AbstractInjector::class);

        $container = new FactoryContainer($injector, A::class, function() { return new A(); });

        $injector->expects($this->once())
            ->method('impl')
            ->with('a', 'b')
        ;
        $this->assertSame($container, $container->useImpl('a', 'b'));


        $injector->expects($this->once())
            ->method('factory')
            ->with(B::class, function() {})
        ;
        $this->assertSame($container, $container->useFactory(B::class, function() {}));


        $injector->expects($this->once())
            ->method('instance')
            ->with(new D())
        ;
        $this->assertSame($container, $container->useInstance(new D()));


        $injector->expects($this->once())
            ->method('addDefaultFactory')
            ->with(function(){})
        ;
        $this->assertSame($container, $container->useDefaultFactory(function(){}));
    }

    /**
     *
     */
    public function test_fork()
    {
        $injector = new BaseInjector();

        $factory  = function () {
            return new A();
        };

        $container = new FactoryContainer($injector, A::class, $factory);

        $this->assertSame($container, $container->fork('child'));
        $this->assertNotSame($injector, $container->getInjector());
        $this->assertInstanceOf(ChildInjector::class, $container->getInjector());
    }

    /**
     *
     */
    public function test_persistent()
    {
        $injector = new BaseInjector();

        $factory  = function () {
            return new A();
        };

        $container = new FactoryContainer($injector, A::class, $factory);

        $this->assertSame($container, $container->persistent(false));
        $this->assertFalse($container->isPersistent());
    }
}
