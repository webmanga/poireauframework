<?php

namespace PoireauFramework\Template\Helper;

require_once __DIR__ . "/../_files/helpers.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Exception\HelperException;
use PoireauFramework\Template\Exception\HelperNotFoundException;
use Tests\PoireauFramework\Template\Helper\Foo;

/**
 * Class InjectorHelperLocatorTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Helper
 * @group ext_PoireauFramework_Helper_InjectorHelperLocator
 */
class InjectorHelperLocatorTest extends TestCase
{
    /**
     * @var InjectorHelperLocator
     */
    private $locator;

    /**
     * @var BaseInjector
     */
    private $injector;

    protected function setUp()
    {
        $this->injector = new BaseInjector();

        $this->locator = new InjectorHelperLocator(
            $this->injector
        );
    }

    /**
     *
     */
    public function test_register_get()
    {
        $helper = $this->createMock(HelperInterface::class);

        $helper->expects($this->any())
            ->method('name')
            ->willReturn('test');

        $this->locator->register($helper);

        $this->assertSame($helper, $this->locator->get($helper->name()));
    }

    /**
     *
     */
    public function test_get_not_found()
    {
        $this->expectException(HelperNotFoundException::class);

        $this->locator->get("Not found");
    }

    /**
     *
     */
    public function test_get_from_customer_ns()
    {
        $this->locator->addNamespace("Tests\\PoireauFramework\\Template\\Helper");

        $helper = $this->locator->get("foo");

        $this->assertInstanceOf(Foo::class, $helper);

        $this->assertSame($helper, $this->locator->get("foo"));
    }

    /**
     *
     */
    public function test_run()
    {
        $this->locator->addNamespace("Tests\\PoireauFramework\\Template\\Helper");

        $engine = $this->createMock(ViewEngineInterface::class);

        $return = $this->locator->run("foo", $engine, ["foo", "bar"]);

        $this->assertInstanceOf(Foo::class, $return);
        $this->assertSame($engine, $return->engine);
        $this->assertEquals(["foo", "bar"], $return->arguments);
    }

    /**
     *
     */
    public function test_run_error()
    {
        $this->expectException(HelperException::class);
        $this->expectExceptionMessage("ERROR for helper 'error'");

        $this->locator->addNamespace("Tests\\PoireauFramework\\Template\\Helper");

        $engine = $this->createMock(ViewEngineInterface::class);

        $this->locator->run("error", $engine, []);
    }
}