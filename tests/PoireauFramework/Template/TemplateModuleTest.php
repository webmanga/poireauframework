<?php

namespace PoireauFramework\Template;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryFactory;
use PoireauFramework\Template\Engine\PhpEngine;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperLocatorInterface;
use PoireauFramework\Template\Helper\InjectorHelperLocator;

/**
 * Class TemplateModuleTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Template
 * @group ext_PoireauFramework_Template_TemplateModule
 */
class TemplateModuleTest extends TestCase
{
    /**
     * @var TemplateModule
     */
    private $module;

    /**
     * @var BaseInjector
     */
    private $injector;

    protected function setUp()
    {
        $this->module = new TemplateModule();

        $this->injector = new BaseInjector(RegistryFactory::createFromArray([
            "template" => [
                "paths" => []
            ]
        ]));

        $this->injector->register($this->module);
    }

    /**
     *
     */
    public function test_instances()
    {
        $this->assertInstanceOf(InjectorHelperLocator::class, $this->injector->get(HelperLocatorInterface::class));
        $this->assertInstanceOf(PhpEngine::class, $this->injector->get(ViewEngineInterface::class));
        $this->assertInstanceOf(TemplateService::class, $this->injector->get(TemplateService::class));
    }
}
