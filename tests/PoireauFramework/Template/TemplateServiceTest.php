<?php

namespace PoireauFramework\Template;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Template\Engine\PhpEngine;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperLocatorInterface;
use PoireauFramework\Template\Helper\InjectorHelperLocator;
use PoireauFramework\Template\Resolver\FileTemplateResolver;

/**
 * Class TemplateServiceTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Template
 */
class TemplateServiceTest extends TestCase
{
    /**
     * @var TemplateService
     */
    protected $service;

    protected function setUp()
    {
        $this->service = new TemplateService(
            new PhpEngine(
                new FileTemplateResolver([__DIR__ . "/_files/"])
            ),
            new InjectorHelperLocator(
                new BaseInjector()
            )
        );
    }

    /**
     *
     */
    public function test_render()
    {
        $content = $this->service->render("test", [
            "test" => "World"
        ]);

        $this->assertEquals(<<<HTML
<!DOCTYPE html>
<html>
    <head>
        <title>Test PoireauFramework</title>
    </head>
    <body>
        Hello World !    </body>
</html>
HTML
        , $content);
    }

    /**
     *
     */
    public function test_get_set_engine()
    {
        $engine = $this->createMock(ViewEngineInterface::class);

        $this->service->setEngine($engine);

        $this->assertSame($engine, $this->service->getEngine());
    }

    /**
     *
     */
    public function test_get_set_helper_locator()
    {
        $helpers = $this->createMock(HelperLocatorInterface::class);

        $this->service->setHelperLocator($helpers);

        $this->assertSame($helpers, $this->service->getHelperLocator());
    }
}
