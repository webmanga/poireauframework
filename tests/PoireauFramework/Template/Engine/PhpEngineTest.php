<?php

namespace PoireauFramework\Template\Engine;

require_once __DIR__ . "/../_files/helpers.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Template\Exception\TemplateException;
use PoireauFramework\Template\Helper\InjectorHelperLocator;
use PoireauFramework\Template\Resolver\FileTemplateResolver;
use PoireauFramework\Template\View;
use Tests\PoireauFramework\Template\Helper\Foo;

/**
 * Class PhpEngineTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Template
 * @group ext_PoireauFramework_Template_Engine
 * @group ext_PoireauFramework_Template_Engine_PhpEngine
 */
class PhpEngineTest extends TestCase
{
    /**
     * @var PhpEngine
     */
    private $engine;

    public function setUp()
    {
        $this->engine = new PhpEngine(
            new FileTemplateResolver([__DIR__ . "/../_files/"])
        );
    }

    /**
     *
     */
    public function test_part()
    {
        $view = new View('', ['test' => 'test']);

        $this->setView($view);

        $this->expectOutputString('Hello test !');

        $this->engine->part("test");

        $this->assertEquals([
            "template" => "layout",
            "vars"     => []
        ], $view->getParent());
    }

    /**
     *
     */
    public function test_part_exception()
    {
        $view = new View('');
        $this->setView($view);

        $this->expectException(TemplateException::class);
        $this->expectExceptionMessage("Error during execution");

        $this->engine->part("error");
    }

    /**
     *
     */
    public function test_render()
    {
        $view = new View('test-render', ['test' => 'test']);

        $this->assertEquals("test-render : Hello test !", $this->engine->render($view));
    }

    /**
     *
     */
    public function test_render_deep()
    {
        $view = new View('deep-render');

        $this->assertEquals("test-render : Hello John !", $this->engine->render($view));
    }

    /**
     *
     */
    public function test___call()
    {
        $helpers = new InjectorHelperLocator(new BaseInjector());
        $helpers->addNamespace('Tests\PoireauFramework\Template\Helper');

        $this->engine->setHelperLocator($helpers);

        $ret = $this->engine->foo('hello', 'world');

        $this->assertInstanceOf(Foo::class, $ret);
        $this->assertSame($this->engine, $ret->engine);
        $this->assertEquals(['hello', 'world'], $ret->arguments);
    }

    /**
     * @param View $view
     */
    protected function setView(View $view)
    {
        $r = new \ReflectionProperty($this->engine, 'views');
        $r->setAccessible(true);
        $r->setValue($this->engine, [$view]);
    }
}