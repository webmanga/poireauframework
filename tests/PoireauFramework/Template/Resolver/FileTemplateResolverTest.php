<?php

namespace PoireauFramework\Template\Resolver;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Template\Exception\TemplateNotFoundException;

/**
 * Class FileTemplateResolverTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Template
 * @group ext_PoireauFramework_Template_Resolver
 * @group ext_PoireauFramework_Template_Resolver_FileTemplateResolver
 */
class FileTemplateResolverTest extends TestCase
{
    /**
     * @var FileTemplateResolver
     */
    private $resolver;


    protected function setUp()
    {
        $this->resolver = new FileTemplateResolver([__DIR__ . "/../_files/"]);
    }

    /**
     *
     */
    public function test_resolveTemplate_not_found()
    {
        $this->expectException(TemplateNotFoundException::class);

        $this->resolver->resolveTemplate('NotFoundTemplate');
    }

    /**
     *
     */
    public function test_resolveTemplate_success()
    {
        $this->assertEquals(__DIR__ . "/../_files/test.part.php", $this->resolver->resolveTemplate("test"));
        $this->assertEquals(__DIR__ . "/../_files/test-render.html.php", $this->resolver->resolveTemplate("test-render"));
    }

    /**
     *
     */
    public function test_setAvailableExtensions()
    {
        $this->resolver->setAvailableExtensions([".json.php"]);
        $this->assertEquals(__DIR__ . "/../_files/test.json.php", $this->resolver->resolveTemplate("test"));
    }
}