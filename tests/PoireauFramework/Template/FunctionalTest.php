<?php

namespace PoireauFramework\Template;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * Class FunctionalTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Template
 * @group ext_PoireauFramework_Template_Functional
 */
class FunctionalTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;

    /**
     * @var TemplateService
     */
    private $service;


    protected function setUp()
    {
        $this->injector = new BaseInjector(RegistryFactory::createFromArray([
            "template" => [
                "paths" => [__DIR__ . "/_files/"],
                "helpers_ns" => ["Tests\\PoireauFramework\\Template\\Helper"]
            ]
        ]));

        $this->injector->register(new TemplateModule());

        $this->service = $this->injector->get(TemplateService::class);
    }

    /**
     *
     */
    public function test_render_simple()
    {
        $this->assertEquals("Hello World !", $this->service->render("hello"));
    }

    /**
     *
     */
    public function test_render_dynamic()
    {
        $this->assertEquals("Hello World !Hello World !Hello World !Hello World !", $this->service->render("dynamic", ['count' => 4]));
    }

    /**
     *
     */
    public function test_render_layout()
    {
        $content = $this->service->render("test", [
            "test" => "World"
        ]);

        $this->assertEquals(<<<HTML
<!DOCTYPE html>
<html>
    <head>
        <title>Test PoireauFramework</title>
    </head>
    <body>
        Hello World !    </body>
</html>
HTML
            , $content);
    }

    /**
     *
     */
    public function test_render_with_helper()
    {
        $content = $this->service->render("with-helper", [
            "test" => "World"
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
<!DOCTYPE html>
<html>
    <head>
        <title>Test PoireauFramework</title>
    </head>
    <body>
        <p>Hello World !</p>
        <input type="submit" value="click"/>
    </body>
</html>
HTML
            , $content);
    }
}