<?php

namespace Tests\PoireauFramework\Template\Helper;

use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Template\Engine\ViewEngineInterface;
use PoireauFramework\Template\Helper\HelperInterface;

class Foo implements HelperInterface, InjectorInstantiable {
    public $engine;

    public $arguments;

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return "foo";
    }

    /**
     * {@inheritdoc}
     */
    public function run(ViewEngineInterface $engine, array $arguments)
    {
        $this->engine = $engine;
        $this->arguments = $arguments;

        return $this;
    }

    static public function createInstance(InjectorInterface $injector, array $parameters = array()): InjectorInstantiable
    {
        return new static();
    }
}

class Error implements HelperInterface, InjectorInstantiable
{
    public function name(): string
    {
        return "error";
    }

    public function run(ViewEngineInterface $engine, array $arguments)
    {
        throw new \Exception("ERROR");
    }

    static public function createInstance(InjectorInterface $injector, array $parameters = array()): InjectorInstantiable
    {
        return new static();
    }
}

class Button implements HelperInterface, InjectorInstantiable
{
    private $value;

    public function name(): string 
    {
        return "button";
    }

    public function run(ViewEngineInterface $engine, array $arguments)
    {
        $this->value = $arguments[0];

        return $this;
    }

    public function submit()
    {
        return <<<HTML
<input type="submit" value="{$this->value}"/>
HTML;

    }

    static public function createInstance(InjectorInterface $injector, array $parameters = array()): InjectorInstantiable
    {
        return new static();
    }
}