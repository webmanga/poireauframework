<?php

namespace PoireauFramework\Console;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_ConsoleInput
 */
class ConsoleInputTest extends TestCase
{
    /**
     * @var ConsoleInput
     */
    private $input;

    /**
     *
     */
    protected function setUp()
    {
        $this->input = new ConsoleInput(["command", "-abc", "test", "-b", "--opt", "-o", "output.txt", "hello", "world"]);
    }

    /**
     *
     */
    public function test_arguments()
    {
        $this->assertEquals(["command", "test", "output.txt", "hello", "world"], $this->input->arguments());
    }

    /**
     *
     */
    public function test_options()
    {
        $this->assertEquals([
            "a" => null,
            "b" => [null, null],
            "c" => 1,
            "opt" => null,
            "o" => 2
        ], $this->input->options());
    }

    /**
     *
     */
    public function test_count()
    {
        $this->assertEquals(1, $this->input->count("a"));
        $this->assertEquals(2, $this->input->count("b"));
        $this->assertEquals(0, $this->input->count("not_found"));
    }

    /**
     *
     */
    public function test_argument()
    {
        $this->assertEquals("test", $this->input->argument(1));
    }

    /**
     *
     */
    public function test_option()
    {
        $this->assertTrue($this->input->option("a"));
        $this->assertEquals("output.txt", $this->input->option("o"));
        $this->assertEquals([null, null], $this->input->option("b"));
        $this->assertFalse($this->input->option("not_found"));
    }
}
