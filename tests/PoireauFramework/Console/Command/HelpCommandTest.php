<?php

namespace PoireauFramework\Console\Command;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Console\Console;
use PoireauFramework\Console\ConsoleInput;
use PoireauFramework\Console\ConsoleModule;
use PoireauFramework\Console\Testing\ConsoleAssertions;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_Command
 * @group ext_PoireauFramework_Console_Command_HelpCommand
 */
class HelpCommandTest extends TestCase
{
    use ConsoleAssertions;

    /**
     * @var HelpCommand
     */
    private $command;


    protected function setUp()
    {
        $app = new Application([]);
        $app->register(new ConsoleModule());

        $this->console = new Console($app);
        $this->command = new HelpCommand();
    }

    /**
     *
     */
    public function test_list_functional()
    {
        $this->expectOutputString(<<<OUT
Available commands :
\thelp\tGet information on commands

OUT
);
        $this->command->run(new Console($this->console, new ConsoleInput(["console.php", "help"])));
    }

    /**
     *
     */
    public function test_list()
    {
        $this->assertCommandResult([
            "Available commands :",
            "\thelp\tGet information on commands",
            ""
        ], $this->command);
    }
}
