<?php

namespace PoireauFramework\Console\Command;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Console\ConsoleInput;
use PoireauFramework\Console\Exception\ConsoleException;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_Command
 * @group ext_PoireauFramework_Console_Command_CommandInput
 */
class CommandInputTest extends TestCase
{
    /**
     *
     */
    public function test_option_success()
    {
        $console = new ConsoleInput(["command", "-abc", "test", "-b", "--opt", "-o", "output.txt", "hello", "world"]);
        $options = new CommandOptions();

        $options
            ->addOption("append", CommandOption::TYPE_BOOL)
            ->setShortcut("a")
        ;

        $options
            ->addOption("blob", CommandOption::TYPE_COUNT)
            ->setShortcut("b")
        ;

        $options
            ->addOption("config", CommandOption::TYPE_STRING)
            ->setShortcut("c")
        ;

        $options
            ->addOption("opt", CommandOption::TYPE_ARRAY)
            ->setShortcut("o")
        ;

        $input = $options->input($console);

        $this->assertEquals(true, $input->option("append"));
        $this->assertEquals(2, $input->option("blob"));
        $this->assertEquals("test", $input->option("config"));
        $this->assertEquals([null, "output.txt"], $input->option("opt"));
    }

    /**
     *
     */
    public function test_option_error()
    {
        $this->expectException(ConsoleException::class);
        $this->expectExceptionMessage("Option 'output' is required");

        $console = new ConsoleInput(["command", "hello", "world"]);
        $options = new CommandOptions();

        $options
            ->addOption("output", CommandOption::TYPE_STRING)
            ->setRequired()
            ->setShortcut("o")
        ;

        $options->input($console);
    }

    /**
     *
     */
    public function test_option_defaultValue()
    {
        $console = new ConsoleInput(["command", "hello", "world"]);
        $options = new CommandOptions();

        $options
            ->addOption("output", CommandOption::TYPE_STRING)
            ->setDefaultValue("out.txt")
            ->setShortcut("o")
        ;

        $input = $options->input($console);

        $this->assertEquals("out.txt", $input->option("output"));
    }

    /**
     *
     */
    public function test___get()
    {
        $console = new ConsoleInput(["-o", "output.txt"]);
        $options = new CommandOptions();

        $options
            ->addOption("output", CommandOption::TYPE_STRING)
            ->setShortcut("o")
        ;

        $input = $options->input($console);

        $this->assertEquals("output.txt", $input->output);
    }

    /**
     *
     */
    public function test_arguments_success()
    {
        $console = new ConsoleInput(["command", "-abc", "test", "-b", "--opt", "-o", "output.txt", "hello", "world"]);
        $options = new CommandOptions();

        $options
            ->addOption("append", CommandOption::TYPE_BOOL)
            ->setShortcut("a")
        ;

        $options
            ->addOption("blob", CommandOption::TYPE_COUNT)
            ->setShortcut("b")
        ;

        $options
            ->addOption("config", CommandOption::TYPE_STRING)
            ->setShortcut("c")
        ;

        $options
            ->addOption("opt", CommandOption::TYPE_ARRAY)
            ->setShortcut("o")
        ;

        $options
            ->addArgument("command")
            ->setRequired()
        ;

        $options
            ->addArgument("words")
            ->setArray()
        ;

        $input = $options->input($console);

        $this->assertEquals("command", $input->argument("command"));
        $this->assertEquals(["hello", "world"], $input->argument("words"));
    }

    /**
     *
     */
    public function test_argument_required_missing()
    {
        $this->expectException(ConsoleException::class);
        $this->expectExceptionMessage("Argument 'args' is required");

        $console = new ConsoleInput(["-a", "test"]);
        $options = new CommandOptions();

        $options
            ->addOption("a")
        ;

        $options
            ->addArgument("args")
            ->setRequired()
            ->setArray()
        ;

        $options->input($console);
    }

    /**
     *
     */
    public function test_argument_defaultValue()
    {
        $console = new ConsoleInput([]);
        $options = new CommandOptions();

        $options
            ->addArgument("foo")
            ->setDefaultValue("bar")
        ;

        $input = $options->input($console);

        $this->assertEquals("bar", $input->argument("foo"));
    }
}
