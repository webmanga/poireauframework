<?php

namespace PoireauFramework\Console\Command;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Console\ConsoleInput;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_Command
 * @group ext_PoireauFramework_Console_Command_CommandOptions
 */
class CommandOptionsTest extends TestCase
{
    /**
     *
     */
    public function test_addOption()
    {
        $options = new CommandOptions();

        $option = $options->addOption('test', CommandOption::TYPE_STRING);

        $this->assertInstanceOf(CommandOption::class, $option);
        $this->assertEquals('test', $option->name());
        $this->assertEquals(CommandOption::TYPE_STRING, $option->type());

        $this->assertEquals(['test' => $option], $options->options());
    }

    /**
     *
     */
    public function test_addArgument()
    {
        $options = new CommandOptions();

        $arg = $options->addArgument('test');

        $this->assertInstanceOf(CommandArgument::class, $arg);
        $this->assertEquals('test', $arg->name());

        $this->assertEquals(['test' => $arg], $options->arguments());
    }

    /**
     *
     */
    public function test_input()
    {
        $options = new CommandOptions();

        $consoleInput = $this->createMock(ConsoleInput::class);
        $input = $options->input($consoleInput);

        $this->assertInstanceOf(CommandInput::class, $input);
        $this->assertSame($consoleInput, $input->input());
    }

    /**
     *
     */
    public function test_createDefaults()
    {
        $options = CommandOptions::createDefaults("default_cmd");

        $this->assertEquals([
            "script" => (new CommandArgument("script"))->setRequired(),
            "command" => (new CommandArgument("command"))->setDefaultValue("default_cmd")
        ], $options->arguments());

        $this->assertEquals([
            "help" => (new CommandOption("help", CommandOption::TYPE_BOOL))->setShortcut("h")
        ], $options->options());
    }
}
