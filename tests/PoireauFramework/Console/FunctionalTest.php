<?php

namespace PoireauFramework\Console;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Console\Command\Command;
use PoireauFramework\Console\Command\CommandInput;
use PoireauFramework\Console\Command\CommandOption;
use PoireauFramework\Console\Command\CommandOptions;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_Console
 */
class FunctionalTest extends TestCase
{
    /**
     *
     */
    public function test_echo()
    {
        $this->expectOutputString("hello world");

        $app = new Application([]);
        $app->register(new ConsoleModule());

        $console = new Console($app, new ConsoleInput(["console.php", "echo", "-n", "hello", "world"]));
        $console->register(new EchoCommand());

        $console->load();
        $console->run();
    }
}

class EchoCommand extends Command
{
    protected function configure(CommandOptions $options): void
    {
        $options
            ->addArgument("words")
            ->setArray()
        ;

        $options
            ->addOption("no-newline", CommandOption::TYPE_BOOL)
            ->setShortcut("n")
        ;
    }

    protected function execute(CommandInput $input): void
    {
        $words = implode(" ", $input->argument("words"));

        $this->output->write($words);

        if (!$input->option("no-newline")) {
            $this->output->newLine();
        }
    }

    public function name(): string
    {
        return "echo";
    }

}