<?php

namespace PoireauFramework\Console;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\BaseInjector;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_ConsoleModule
 */
class ConsoleModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;

    protected function setUp()
    {
        $this->injector = new BaseInjector();
        $this->injector->register(new ConsoleModule());
    }

    /**
     *
     */
    public function test_input()
    {
        global $argv;

        $this->assertInstanceOf(ConsoleInput::class, $this->injector->get(ConsoleInput::class));
        $this->assertEquals(new ConsoleInput($argv), $this->injector->get(ConsoleInput::class));
    }
}
