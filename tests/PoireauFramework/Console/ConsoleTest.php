<?php

namespace PoireauFramework\Console;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Console\Command\Command;
use PoireauFramework\Console\Command\CommandInput;
use PoireauFramework\Console\Command\CommandInterface;
use PoireauFramework\Console\Command\CommandOptions;
use PoireauFramework\Console\Exception\CommandNotFoundException;
use PoireauFramework\Console\Output\ConsoleOutput;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Console
 * @group ext_PoireauFramework_Console_Console
 */
class ConsoleTest extends TestCase
{
    /**
     * @var Console
     */
    private $console;


    /**
     *
     */
    protected function setUp()
    {
        $application = new Application([]);
        $application->register(new ConsoleModule());

        $this->console = new Console($application);
    }

    /**
     *
     */
    public function test_defaults()
    {
        $this->assertInstanceOf(ConsoleInput::class, $this->console->input());
        $this->assertInstanceOf(ConsoleOutput::class, $this->console->output());
        $this->assertInstanceOf(ConsoleRunner::class, $this->console->runner());
    }

    /**
     *
     */
    public function test_register_command()
    {
        $cmd = $this->createMock(CommandInterface::class);
        $cmd->expects($this->any())
            ->method("name")
            ->willReturn("cmd");

        $this->console->register($cmd);

        $this->assertSame($cmd, $this->console->command("cmd"));
    }

    /**
     *
     */
    public function test_command_not_found()
    {
        $this->expectException(CommandNotFoundException::class);
        $this->expectExceptionMessage("Command 'not_found' is not found");

        $this->console->command("not_found");
    }

    /**
     *
     */
    public function test_run_functional()
    {
        $command = new TestCommand();
        $input = new ConsoleInput(["console.php", "test"]);

        $console = new Console($this->console, $input);
        $console->register($command);

        $console->load();
        $console->run();

        $this->assertSame($console, $command->console);
        $this->assertSame($input, $command->input);
    }
}

class TestCommand extends Command
{
    public $console;

    public $input;

    protected function configure(CommandOptions $options): void
    {
        // TODO: Implement configure() method.
    }

    public function name(): string
    {
        return "test";
    }

    public function run(\PoireauFramework\Console\Console $console): void
    {
        $this->console = $console;
        $this->input = $console->input();
    }

    protected function execute(CommandInput $input): void
    {
        // TODO: Implement execute() method.
    }
}
