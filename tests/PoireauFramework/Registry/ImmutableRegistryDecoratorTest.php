<?php
namespace PoireauFramework\Registry;

use PoireauFramework\Registry\Exception\ImmutableValueException;
use PHPUnit\Framework\TestCase;

/**
 * Class ImmutableRegistryDecoratorTest
 *
 * @group ext
 */
class ImmutableRegistryDecoratorTest extends TestCase
{
    /**
     * @var Registry
     */
    private $baseRegistry;

    /**
     * @var ImmutableRegistryDecorator
     */
    private $registry;

    protected function setUp()
    {
        parent::setUp();

        $this->baseRegistry = new Registry();
        $this->registry = new ImmutableRegistryDecorator($this->baseRegistry);
    }

    public function test_get()
    {
        $this->baseRegistry->set('key', 'value');

        $this->assertSame('value', $this->registry->get('key'));
    }

    public function test_set()
    {
        $this->expectException(ImmutableValueException::class);

        $this->registry->set('key', 'value');
    }

    public function test_get_instanceof_registry()
    {
        $this->baseRegistry->set('reg', new Registry());

        $this->assertInstanceOf(ImmutableRegistryDecorator::class, $this->registry->get('reg'));
    }

    public function test_sub_not_exists()
    {
        $this->expectException(ImmutableValueException::class);

        $this->registry->sub('not_found');
    }

    public function test_sub()
    {
        $this->baseRegistry->sub('sub');

        $this->assertInstanceOf(ImmutableRegistryDecorator::class, $this->registry->sub('sub'));
    }
}