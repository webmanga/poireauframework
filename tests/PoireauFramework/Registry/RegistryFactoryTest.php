<?php

namespace PoireauFramework\Registry;

use PHPUnit\Framework\TestCase;

/**
 * Class RegistryFactoryTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Registry
 * @group ext_PoireauFramework_Registry_RegistryFactory
 */
class RegistryFactoryTest extends TestCase
{
    /**
     *
     */
    public function test_createFromArray_simple()
    {
        $registry = RegistryFactory::createFromArray([
            "first_name" => "John",
            "last_name"  => "Doe"
        ]);

        $this->assertInstanceOf(Registry::class, $registry);

        $this->assertEquals("John", $registry->get("first_name"));
        $this->assertEquals("Doe",  $registry->get("last_name"));
    }

    /**
     *
     */
    public function test_createFromArray_deep()
    {
        $registry = RegistryFactory::createFromArray([
            "id" => 15,
            "person" => [
                "first_name" => "John",
                "last_name"  => "Doe"
            ]
        ]);

        $this->assertInstanceOf(Registry::class, $registry);
        $this->assertInstanceOf(Registry::class, $registry->sub("person"));

        $this->assertEquals(15,  $registry->get("id"));
        $this->assertEquals("John", $registry->sub("person")->get("first_name"));
        $this->assertEquals("Doe",  $registry->sub("person")->get("last_name"));
    }

    /**
     *
     */
    public function test_createFromArray_with_list()
    {
        $registry = RegistryFactory::createFromArray([
            "id" => 15,
            "words" => ["hello", "world", "!"]
        ]);

        $this->assertInstanceOf(Registry::class, $registry);

        $this->assertEquals(15,  $registry->get("id"));
        $this->assertEquals(["hello", "world", "!"],  $registry->get("words"));
    }
}
