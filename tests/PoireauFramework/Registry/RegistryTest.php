<?php
namespace PoireauFramework\Registry;


use PoireauFramework\Registry\Exception\ImmutableValueException;
use PoireauFramework\Registry\Exception\RegistryException;
use PoireauFramework\Registry\Exception\ValueNotFoundException;
use PHPUnit\Framework\TestCase;

/**
 * Class RegistryTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Registry
 * @group ext_PoireauFramework_Registry_Registry
 */
class RegistryTest extends TestCase
{
    /**
     * @var RegistryImpl
     */
    private $registry;

    protected function setUp()
    {
        parent::setUp();

        $this->registry = new RegistryImpl();
    }

    public function test_set_simple()
    {
        $this->registry->set('key', 'value');

        $this->assertArrayHasKey('key', $this->registry->getData());
    }

    public function test_get_not_found()
    {
        $this->expectException(ValueNotFoundException::class);

        $this->registry->get('no_found');
    }

    public function test_get_set()
    {
        $this->registry->set('key', 'value');

        $this->assertSame('value', $this->registry->get('key'));
    }

    public function test_has_no_value()
    {
        $this->assertFalse($this->registry->has('not_found'));
    }

    public function test_has_null()
    {
        $this->registry->set('key', null);
        $this->assertTrue($this->registry->has('key'));
    }

    public function test_has_not_null()
    {
        $this->registry->set('key', 'value');
        $this->assertTrue($this->registry->has('key'));
    }

    public function test_sub_create()
    {
        $sub = $this->registry->sub('sub');

        $this->assertInstanceOf(RegistryInterface::class, $sub);
        $this->assertArrayHasKey('sub', $this->registry->getData());
    }

    public function test_sub_existent()
    {
        $sub = $this->registry->sub('sub');

        $this->assertSame($sub, $this->registry->sub('sub'));
    }

    public function test_sub_bad_value()
    {
        $this->registry->set('sub', null);

        $this->expectException(RegistryException::class);

        $this->registry->sub('sub');
    }

    public function test_get_sub()
    {
        $sub = $this->registry->sub('sub');

        $this->assertSame($sub, $this->registry->get('sub'));
    }

    /**
     *
     */
    public function test_keys()
    {
        $registry = new Registry([
            'first_name' => 'John',
            'last_name'  => 'Doe',
            'age' => 23
        ]);

        $this->assertEquals(['first_name', 'last_name', 'age'], $registry->keys());
    }
}

class RegistryImpl extends Registry {
    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}