<?php

namespace PoireauFramework\Config;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryFactory;
use PoireauFramework\Registry\RegistryInterface;

/**
 * Class IniConfigParserTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Config
 * @group ext_PoireauFramework_Config_IniConfigParser
 */
class IniConfigParserTest extends TestCase
{
    /**
     * @var IniConfigParser
     */
    private $parser;

    public function setUp()
    {
        $this->parser = new IniConfigParser();
    }

    /**
     *
     */
    public function test_parse_file_not_found()
    {
        $this->expectException(ConfigParseException::class);
        $this->expectExceptionMessage("File not found");

        $this->parser->parse($this->createMock(RegistryInterface::class), "not_exists");
    }

    /**
     *
     */
    public function test_parse_file_error()
    {
        $this->expectException(ConfigParseException::class);
        $this->expectExceptionMessage("Invalid ini file");

        $this->parser->parse($this->createMock(RegistryInterface::class), __DIR__ . "/_files/test_config_error.ini");
    }

    /**
     *
     */
    public function test_parse_file_success()
    {
        $registry = new Registry();
        $this->parser->parse($registry, __DIR__ . "/_files/test_config.ini");

        $this->assertEquals(RegistryFactory::createFromArray([
            "hello" => "World !",
            "a" => [
                "b" => "c",
                "d" => "e"
            ]
        ]), $registry);
    }

    /**
     *
     */
    public function test_parse_file_merge()
    {
        $registry = new Registry();

        $this->parser->parse($registry, __DIR__ . "/_files/test_config.ini");
        $this->parser->parse($registry, __DIR__ . "/_files/test_config2.ini");

        $this->assertEquals(RegistryFactory::createFromArray([
            "hello" => "John !",
            "a" => [
                "b" => "c",
                "c" => "d",
                "d" => "e"
            ]
        ]), $registry);
    }
}
