<?php

namespace PoireauFramework\Config;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Exception\ConfigParseException;
use PoireauFramework\Registry\RegistryFactory;

/**
 * Class ConfigTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Config
 * @group ext_PoireauFramework_Config_Config
 */
class ConfigTest extends TestCase
{
    /**
     * @var Config
     */
    private $config;

    /**
     *
     */
    public function setUp()
    {
        $this->config = new Config(RegistryFactory::createFromArray([
            "foo" => "bar",
            "person" => [
                "first_name" => "John",
                "last_name"  => "Doe",
                "age"        => 32
            ],
            "array" => ["Hello", "World"]
        ]));
    }

    /**
     *
     */
    public function test_get()
    {
        $this->assertEquals("bar", $this->config->get("foo"));
        $this->assertEquals("bar", $this->config["foo"]);
        $this->assertEquals("bar", $this->config->foo);

        $this->assertNull($this->config->get("not_found"));
        $this->assertNull($this->config["not_found"]);
        $this->assertNull($this->config->not_found);
    }

    /**
     *
     */
    public function test_get_sub_config()
    {
        $this->assertInstanceOf(ConfigItem::class, $this->config->get("person"));

        $this->assertEquals("John", $this->config->person->first_name);
        $this->assertEquals("Doe", $this->config->person->last_name);
        $this->assertEquals(32, $this->config->person->age);
    }

    /**
     *
     */
    public function test_get_array()
    {
        $this->assertEquals(["Hello", "World"], $this->config->get("array"));
    }

    /**
     *
     */
    public function test_load_ext_not_handled()
    {
        $this->expectException(ConfigParseException::class);
        $this->expectExceptionMessage("Parser not found");

        $this->config->load("invalid.ext");
    }

    /**
     *
     */
    public function test_load_functional()
    {
        $this->config->setParser(new PhpConfigParser());

        $this->config->load(__DIR__ . "/_files/test_config.php");

        $this->assertEquals("World !", $this->config->hello);
        $this->assertEquals("c", $this->config->a->b);
        $this->assertEquals("e", $this->config->a->d);
    }
}
