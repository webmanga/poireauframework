<?php

namespace PoireauFramework\Config;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Exception\ConfigException;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryFactory;

/**
 * Class ConfigItemTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Config
 * @group ext_PoireauFramework_Config_ConfigItem
 */
class ConfigItemTest extends TestCase
{
    /**
     * @var ConfigItem
     */
    private $config;

    /**
     * @var Registry
     */
    private $registry;


    protected function setUp()
    {
        $this->registry = RegistryFactory::createFromArray([
            "id" => 15,
            "person" => [
                "first_name" => "John",
                "last_name"  => "Doe"
            ],
            "words" => ["Hello", "World", "!"]
        ]);

        $this->config = new ConfigItem($this->registry);
    }

    /**
     *
     */
    public function test_has()
    {
        $this->assertTrue($this->config->has("id"));
        $this->assertTrue($this->config->has("person"));
        $this->assertTrue($this->config->has("words"));
        $this->assertFalse($this->config->has("not_found"));
    }

    /**
     *
     */
    public function test_get_scalar()
    {
        $this->assertEquals(15, $this->config->get("id"));
    }

    public function test_get_not_found()
    {
        $this->assertNull($this->config->get("not_found"));
    }

    /**
     *
     */
    public function test_get_list()
    {
        $this->assertEquals(["Hello", "World", "!"], $this->config->get("words"));
    }

    /**
     *
     */
    public function test_get_sub_config()
    {
        $item = $this->config->get("person");

        $this->assertInstanceOf(ConfigItem::class, $item);

        $this->assertEquals("John", $item->get("first_name"));
        $this->assertEquals("Doe",  $item->get("last_name"));

        $this->assertSame($item, $this->config->get("person"));
    }

    /**
     *
     */
    public function test_item_create_new()
    {
        $item = $this->config->item("not_found");

        $this->assertInstanceOf(ConfigItem::class, $item);
        $this->assertSame($item, $this->config->item("not_found"));
        $this->assertSame($item, $this->config->get("not_found"));
    }

    /**
     *
     */
    public function test_item_already_valid()
    {
        $item = $this->config->item("person");

        $this->assertInstanceOf(ConfigItem::class, $item);

        $this->assertEquals("John", $item->get("first_name"));
        $this->assertEquals("Doe",  $item->get("last_name"));

        $this->assertSame($item, $this->config->item("person"));
    }

    /**
     *
     */
    public function test_item_already_invalid()
    {
        $this->expectException(ConfigException::class);
        $this->expectExceptionMessage("The config key id is already set and it's not a ConfigItem");

        $this->config->item('id');
    }

    /**
     *
     */
    public function test_magic_methods()
    {
        $this->assertEquals(15, $this->config->id);
        $this->assertEquals(15, $this->config["id"]);

        $this->assertTrue(isset($this->config->id));
        $this->assertTrue(isset($this->config["id"]));
    }

    /**
     *
     */
    public function test_update_from_registry()
    {
        $this->registry->set("new_value", "My new value");

        $this->assertTrue($this->config->has("new_value"));
        $this->assertEquals("My new value", $this->config->get("new_value"));
    }

    /**
     *
     */
    public function test_offsetUnset_error()
    {
        $this->expectException(ConfigException::class);
        unset($this->config["id"]);
    }

    /**
     *
     */
    public function test_offsetSet_error()
    {
        $this->expectException(ConfigException::class);
        $this->config["id"] = 0;
    }

    /**
     *
     */
    public function test_iterator()
    {
        $this->assertEquals([
            "id" => 15,
            "person" => new ConfigItem(new Registry([
                "first_name" => "John",
                "last_name"  => "Doe"
            ])),
            "words" => ["Hello", "World", "!"]
        ], iterator_to_array($this->config->getIterator()));
    }
}
