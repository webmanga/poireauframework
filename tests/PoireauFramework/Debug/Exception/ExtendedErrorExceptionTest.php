<?php

namespace PoireauFramework\Debug\Exception;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Debug
 * @group ext_PoireauFramework_Debug_Exception
 * @group ext_PoireauFramework_Debug_Exception_ExtendedErrorException
 */
class ExtendedErrorExceptionTest extends TestCase
{
    /**
     * 
     */
    public function test_createFromArray()
    {
        $lastHandler = set_error_handler(null);
        @trigger_error('MY_ERROR', E_USER_WARNING);
        
        $error = error_get_last();

        $exception = ExtendedErrorException::createFromArray($error);

        $this->assertInstanceOf(ExtendedErrorException::class, $exception);
        
        $this->assertEquals('MY_ERROR', $exception->getMessage());
        $this->assertEquals(E_USER_WARNING, $exception->getSeverity());
        $this->assertEquals(22, $exception->getLine());
        $this->assertEquals(__FILE__, $exception->getFile());

        set_error_handler($lastHandler);
    }
    
    /**
     * 
     */
    public function test_setStackTrace()
    {
        $stack = debug_backtrace();
        $exception = new ExtendedErrorException('', 0, '', 0, [], $stack);
        
        $this->assertSame($stack, $exception->getTrace());
    }
}
