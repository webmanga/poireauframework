<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PoireauFramework\Debug;

use PHPUnit\Framework\TestCase;

/**
 * Description of DebugTest
 *
 * @author vquatrevieux
 */
class DebugTest extends TestCase {
    /**
     * @var Debug
     */
    private $debug;
    
    protected function setUp() {
        $this->debug = new Debug();
    }
    
    /**
     * @dataProvider provideArgs
     */
    public function test_formatArgs($value, $type, $class, $display)
    {
        $line = $this->debug->formatArgs([$value]);

        if ($type) {
            $this->assertContains($type, $line);
        }

        if ($class) {
            $this->assertRegExp('#class=".*' . $class . '.*"#', $line);
        }

        $this->assertContains($display, $line);
    }

    public function provideArgs()
    {
        return [
            ['test', 'string', 'arg-string', 'test'],
            [false, 'bool', 'arg-bool', 'false'],
            [123, 'int', 'arg-int', '123'],
            [1.23, 'float', 'arg-float', '1.23'],
            [new \stdClass(), 'object', 'arg-object', \stdClass::class],
            [null, '', 'arg-null', 'NULL'],
            [[], 'array', '', '{}']
        ];
    }

    /**
     * @dataProvider provideSeverityString
     */
    public function test_getSeverityString($str, $errors)
    {
        foreach ((array) $errors as $error) {
            $this->assertEquals($str, $this->debug->getSeverityString($error));
        }
    }

    /**
     *
     */
    public function provideSeverityString()
    {
        return [
            ['Fatal error', [E_ERROR, E_USER_ERROR, E_CORE_ERROR]],
            ['Compile error', E_COMPILE_ERROR],
            ['Warning', [E_WARNING, E_USER_WARNING, E_CORE_WARNING, E_COMPILE_WARNING]],
            ['Notice', [E_USER_NOTICE, E_NOTICE]],
        ];
    }

    /**
     *
     */
    public function test_showErrorFile()
    {
        ob_start();

        $this->debug->showErrorFile(__DIR__ . '/_files/ClassToDebug.php', 19);

        $display = ob_get_clean();

        $this->assertContains("__construct", $display);
        $this->assertContains('prop', $display);
        $this->assertContains('$this', $display);
    }

    /**
     *
     */
    public function test_showEvalCode()
    {
       ob_start();

       $this->debug->showErrorFile(__DIR__ . '/_files/ClassToDebug.php' . "(40) : eval()'d code", 1);

       $display = ob_get_clean();

        $this->assertContains('eval', $display);
        $this->assertContains('return&nbsp;(new&nbsp;ClassToDebug(123))-&gt;getProp();', $display);
    }

    /**
     *
     */
    public function test_isFatal()
    {
        $this->assertFalse($this->debug->isFatal(E_USER_NOTICE));
        $this->assertTrue($this->debug->isFatal(E_ERROR));
    }

    /**
     *
     */
    public function test_showTrace()
    {
        ob_start();

        $this->debug->showTrace(debug_backtrace());

        $display = ob_get_clean();

        $this->assertContains('<table class="debug trace">', $display);
        $this->assertContains('<td class="debug function">' . __CLASS__ . '->' . __FUNCTION__, $display);
        $this->assertContains('<td class="debug file">', $display);
    }

    /**
     *
     */
    public function test_functionals()
    {
        $ex = new \Exception();

        ob_start();

        $this->debug->showException($ex);
        $this->debug->decorateError($ex, '');
        $this->debug->showContext(['foo' => 'bar']);

        ob_end_clean();
    }
}
