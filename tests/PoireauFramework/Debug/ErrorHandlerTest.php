<?php

namespace PoireauFramework\Debug;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Debug\Exception\ExtendedErrorException;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Debug
 * @group ext_PoireauFramework_Debug_ErrorHandler
 */
class ErrorHandlerTest extends TestCase
{
    /**
     * @var ErrorHandler
     */
    private $handler;


    /**
     *
     */
    public function setUp()
    {
        $this->handler = new ErrorHandler();
    }

    /**
     *
     */
    public function test_instance()
    {
        $this->assertInstanceOf(ErrorHandler::class, ErrorHandler::instance());
        $this->assertSame(ErrorHandler::instance(), ErrorHandler::instance());
    }

    /**
     *
     */
    public function test_handleError()
    {
        $this->expectException(ExtendedErrorException::class);
        $this->expectExceptionMessage("error message");

        $this->handler->handleError(0, "error message", __FILE__, __LINE__);
    }

    /**
     *
     */
    public function test_handleException()
    {

    }

    /**
     *
     */
    public function test_register_with_error()
    {
        $this->handler->register();

        try {
            $i = $i + 5;
        } catch (ExtendedErrorException $e) {
            $this->assertEquals("Undefined variable: i", $e->getMessage());
            $this->assertEquals(E_NOTICE, $e->severity());
            $this->assertFalse($e->isFatal());

            $error = $e->getTrace()[0];

            $this->assertEquals(__FILE__, $error['file']);

            $this->handler->restore();
            return;
        }

        $this->fail("Exception not thrown");
    }

    /**
     *
     */
    public function test_handleException_with_handler()
    {
        $this->handler->showErrors();

        $exception = new \Exception("ERROR");
        $isCalled = false;

        $handler = function ($param) use (&$isCalled, $exception) {
            $this->assertSame($exception, $param);
            $isCalled = true;
            return true;
        };

        $this->handler->addExceptionHandler($handler);

        $this->handler->handleException($exception);

        $this->assertTrue($isCalled);
    }
}
