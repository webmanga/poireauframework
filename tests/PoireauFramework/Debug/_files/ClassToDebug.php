<?php

/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 15/01/17
 * Time: 12:40
 */
class ClassToDebug
{
    public $prop;

    /**
     * ClassToDebug constructor.
     * @param $prop
     */
    public function __construct($prop)
    {
        $this->prop = $prop;
    }

    /**
     * @return mixed
     */
    public function getProp()
    {
        return $this->prop;
    }

    /**
     * @param mixed $prop
     */
    public function setProp($prop)
    {
        $this->prop = $prop;
    }

    public function myEval()
    {
        return eval('return (new ClassToDebug(123))->getProp();');
    }
}