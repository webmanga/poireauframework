<?php

namespace PoireauFramework\Session\Storage;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Session\Session;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Session
 * @group ext_PoireauFramework_Session_Storage
 * @group ext_PoireauFramework_Session_Storage_ArraySessionStorage
 */
class ArraySessionStorageTest extends TestCase
{
    /**
     * @var ArraySessionStorage
     */
    private $storage;


    /**
     *
     */
    protected function setUp()
    {
        $this->storage = new ArraySessionStorage();
    }

    /**
     *
     */
    public function test_store_retrieve()
    {
        $session = new Session("123");

        $this->storage->store($session);
        $this->assertSame($session, $this->storage->retrieve("123"));
    }

    /**
     *
     */
    public function test_retrieve_not_found()
    {
        $this->assertNull($this->storage->retrieve("123"));
    }

    /**
     *
     */
    public function test_store_two_sessions()
    {
        $s1 = new Session("1");
        $s2 = new Session("2");

        $this->storage->store($s1);
        $this->storage->store($s2);

        $this->assertSame($s1, $this->storage->retrieve("1"));
        $this->assertSame($s2, $this->storage->retrieve("2"));
    }

    /**
     *
     */
    public function test_delete()
    {
        $s1 = new Session("1");
        $s2 = new Session("2");

        $this->storage->store($s1);
        $this->storage->store($s2);

        $this->storage->delete($s1);

        $this->assertNull($this->storage->retrieve("1"));
        $this->assertNotNull($this->storage->retrieve("2"));
    }
}
