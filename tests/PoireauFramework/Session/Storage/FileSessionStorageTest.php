<?php

namespace PoireauFramework\Session\Storage;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Session\Session;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Session
 * @group ext_PoireauFramework_Session_Storage
 * @group ext_PoireauFramework_Session_Storage_FileSessionStorage
 */
class FileSessionStorageTest extends TestCase
{
    /**
     * @var FileSessionStorage
     */
    private $storage;


    /**
     *
     */
    public function setUp()
    {
        $this->storage = new FileSessionStorage(__DIR__."/tmp");
    }

    protected function tearDown()
    {
        foreach (new \DirectoryIterator(__DIR__."/tmp") as $file) {
            if ($file->isFile()) {
                unlink($file->getRealPath());
            }
        }

        rmdir(__DIR__."/tmp");
    }

    /**
     *
     */
    public function test_start()
    {
        $this->storage->start();

        $this->assertTrue(is_dir(__DIR__."/tmp"));
    }

    /**
     *
     */
    public function test_retrieve()
    {
        $this->storage->start();

        $this->assertNull($this->storage->retrieve("my_sid"));

        file_put_contents(__DIR__."/tmp/session_my_sid.ser", serialize(123));

        $this->assertNull($this->storage->retrieve("my_sid"));

        $session = new Session("my_sid");
        file_put_contents(__DIR__."/tmp/session_my_sid.ser", serialize($session));

        $this->assertEquals($session, $this->storage->retrieve("my_sid"));
    }

    /**
     *
     */
    public function test_store()
    {
        $this->storage->start();

        $session = new Session("123");
        $this->storage->store($session);

        $this->assertFileExists(__DIR__."/tmp/session_123.ser");
    }

    /**
     *
     */
    public function test_store_retrieve()
    {
        $this->storage->start();

        $session = new Session("my_sid");
        $this->storage->store($session);

        $this->assertSame($session, $this->storage->retrieve("my_sid"));
    }

    /**
     *
     */
    public function test_delete()
    {
        $this->storage->start();

        $session = new Session("my_sid");
        $this->storage->store($session);
        $this->storage->delete($session);

        $this->assertNull($this->storage->retrieve("my_sid"));
    }
}
