<?php

namespace PoireauFramework\Session\Storage;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Session\Session;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Session
 * @group ext_PoireauFramework_Session_Storage
 * @group ext_PoireauFramework_Session_Storage_NativeSessionStorage
 */
class NativeSessionStorageTest extends TestCase
{
    /**
     * @var NativeSessionStorage
     */
    private $storage;


    /**
     *
     */
    public function setUp()
    {
        $_SESSION = [];
        $this->storage = new NativeSessionStorage('my_ns');
        $this->storage->start();
    }

    protected function tearDown()
    {
        $this->storage->stop();
    }

    /**
     *
     */
    public function test_retrieve()
    {
        $this->assertNull($this->storage->retrieve('my_sid'));

        $_SESSION['my_ns'] = 123;

        $this->assertNull($this->storage->retrieve('my_sid'));

        $session = new Session("123");

        $_SESSION['my_ns'] = $session;

        $this->assertSame($session, $this->storage->retrieve('my_ns'));
    }

    /**
     *
     */
    public function test_store()
    {
        $session = new Session("123");
        $this->storage->store($session);

        $this->assertSame($session, $_SESSION['my_ns']);
    }

    /**
     *
     */
    public function test_store_retrieve()
    {
        $session = new Session("123");
        $this->storage->store($session);

        $this->assertSame($session, $this->storage->retrieve('my_sid'));
    }

    /**
     *
     */
    public function test_delete()
    {
        $session = new Session("123");
        $this->storage->store($session);
        $this->storage->delete($session);

        $this->assertNull($this->storage->retrieve('my_sid'));
    }
}
