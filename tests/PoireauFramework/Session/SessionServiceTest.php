<?php

namespace PoireauFramework\Session;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Session\Behavior\NativeSessionBehavior;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\Exception\SessionStatusException;
use PoireauFramework\Session\Storage\NativeSessionStorage;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Session
 * @group ext_PoireauFramework_Session_SessionService
 */
class SessionServiceTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $service = new SessionService($this->createMock(SessionStorageInterface::class), $this->createMock(SessionBehaviorInterface::class));

        $this->assertNull($service->session());
        $this->assertFalse($service->isStarted());
    }

    /**
     *
     */
    public function test_start_empty_session()
    {
        $_SESSION = [];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        $service->start();

        $this->assertInstanceOf(Session::class, $service->session());
        $this->assertEquals(session_id(), $service->session()->sid());
        $this->assertEmpty($service->session()->data());
        $this->assertTrue($service->isStarted());
    }

    /**
     *
     */
    public function test_start_retrieve_session()
    {
        $lastSession = new Session('123');

        $_SESSION = [
            'my_ns' => $lastSession
        ];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        $service->start();

        $this->assertSame($lastSession, $service->session());
    }

    /**
     *
     */
    public function test_stop_will_end_session()
    {
        $lastSession = new Session('123');

        $_SESSION = [
            'my_ns' => $lastSession
        ];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        $service->start();

        $service->stop();

        $this->assertFalse($service->isStarted());
        $this->assertNull($service->session());
    }

    /**
     *
     */
    public function test_destroy()
    {
        $lastSession = new Session('123');
        $lastSession["foo"] = "bar";

        $_SESSION = [
            "my_ns" => $lastSession
        ];

        $service = new SessionService(
            new NativeSessionStorage("my_ns"),
            new NativeSessionBehavior()
        );

        $service->start();
        $service->destroy();

        $this->assertFalse($service->isStarted());
        $this->assertNull($service->session());

        $service->start();
        $this->assertEmpty($service->session()->data());
    }

    /**
     *
     */
    public function test_offsetGet_not_started()
    {
        $this->expectException(SessionStatusException::class);

        $_SESSION = [];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        $service["test"];
    }

    /**
     *
     */
    public function test_offsetSet_not_started()
    {
        $this->expectException(SessionStatusException::class);

        $_SESSION = [];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        $service["test"] = "abcd";
    }

    /**
     *
     */
    public function test_offsetExists_not_started()
    {
        $this->expectException(SessionStatusException::class);

        $_SESSION = [];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        isset($service["test"]);
    }

    /**
     *
     */
    public function test_offsetUnset_not_started()
    {
        $this->expectException(SessionStatusException::class);

        $_SESSION = [];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        unset($service["test"]);
    }

    /**
     *
     */
    public function test_array_operations()
    {
        $this->expectException(SessionStatusException::class);

        $_SESSION = [];

        $service = new SessionService(
            new NativeSessionStorage('my_ns'),
            new NativeSessionBehavior()
        );

        $service["test"] = "abcd";

        $this->assertEquals(["test" => "abcd"], $service->session()->data());
        $this->assertEquals("abcd", $service["test"]);
        $this->assertTrue(isset($service["test"]));

        unset($service["test"]);
        $this->assertFalse(isset($service["test"]));
        $this->assertEmpty($service->session()->data());
    }
}
