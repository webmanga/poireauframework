<?php

namespace PoireauFramework\Session;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;
use PoireauFramework\Session\Behavior\DummySessionBehavior;
use PoireauFramework\Session\Behavior\NativeSessionBehavior;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\Storage\ArraySessionStorage;
use PoireauFramework\Session\Storage\NativeSessionStorage;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Session
 * @group ext_PoireauFramework_Session_SessionModule
 */
class SessionModuleTest extends TestCase
{
    public function setUp()
    {
        $_SESSION = [];
    }

    /**
     *
     */
    public function test_configure()
    {
        $injector = new BaseInjector(RegistryFactory::createFromArray([
            "session" => [
                "namespace" => "my_ns"
            ]
        ]));

        $injector->register(new SessionModule());

        $this->assertInstanceOf(SessionService::class, $injector->get(SessionService::class));
        $this->assertInstanceOf(NativeSessionStorage::class, $injector->get(NativeSessionStorage::class));
        $this->assertInstanceOf(NativeSessionBehavior::class, $injector->get(NativeSessionBehavior::class));

        $this->assertInstanceOf(NativeSessionStorage::class, $injector->get(SessionStorageInterface::class));
        $this->assertInstanceOf(NativeSessionBehavior::class, $injector->get(SessionBehaviorInterface::class));

        $this->assertInstanceOf(ArraySessionStorage::class, $injector->get(ArraySessionStorage::class));
        $this->assertInstanceOf(DummySessionBehavior::class, $injector->get(DummySessionBehavior::class));
    }

    /**
     *
     */
    public function test_stop_if_started()
    {
        $application = new Application([]);

        $application->register(new SessionModule());
        $application->load();

        /** @var SessionService $service */
        $service = $application->injector()->get(SessionService::class);
        $service->start();

        $application->run();

        $this->assertFalse($service->isStarted());
    }
}
