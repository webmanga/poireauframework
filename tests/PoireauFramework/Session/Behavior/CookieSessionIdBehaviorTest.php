<?php

namespace PoireauFramework\Session\Behavior;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Cookie\CookieBuilder;
use PoireauFramework\Http\Cookie\CookieService;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseStack;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Session
 * @group ext_PoireauFramework_Session_Behavior
 * @group ext_PoireauFramework_Session_Behavior_CookieSessionIdBehavior
 */
class CookieSessionIdBehaviorTest extends TestCase
{
    /**
     * @var CookieSessionIdBehavior
     */
    private $behavior;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;


    protected function setUp()
    {
        $this->request = (new RequestBuilder())->build();
        $this->response = new Response();

        $this->behavior = new CookieSessionIdBehavior(
            new CookieService(
                new RequestStack($this->request),
                $responseStack = new ResponseStack()
            ),
            "my_sid"
        );

        $responseStack->push($this->response);
    }

    /**
     *
     */
    public function test_getSessionId_retrieve()
    {
        $this->request->cookies()->set("my_sid", "1234");

        $this->assertEquals("1234", $this->behavior->getSessionId());
    }

    /**
     *
     */
    public function test_getSessionId_generation()
    {
        $sid = $this->behavior->getSessionId();

        $this->assertEquals(40, strlen($sid));
        $this->assertEquals($sid, $this->behavior->getSessionId());

        /** @var CookieBuilder $cookie */
        $cookie = $this->response->headers()->getCookies()[0];
        $this->assertEquals("my_sid", $cookie->getName());
        $this->assertEquals($sid, $cookie->getValue());

        $other = new CookieSessionIdBehavior(
            new CookieService(
                new RequestStack($this->request),
                $responseStack = new ResponseStack()
            ),
            "my_sid"
        );
        $responseStack->push(new Response());

        $this->assertNotEquals($other->getSessionId(), $sid);
    }
}
