<?php


namespace PoireauFramework\Cache;


use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Cache
 * @group ext_PoireauFramework_Cache_DummyCache
 */
class DummyCacheTest extends TestCase
{
    /**
     * @var DummyCache
     */
    private $cache;


    /**
     *
     */
    protected function setUp()
    {
        $this->cache = new DummyCache();
    }

    /**
     *
     */
    public function test_bools()
    {
        $this->assertTrue($this->cache->commit());
        $this->assertTrue($this->cache->clear());
        $this->assertTrue($this->cache->deleteItem("item"));
        $this->assertTrue($this->cache->deleteItems(["i1", "i2"]));
        $this->assertTrue($this->cache->save(new CacheItem("key")));
        $this->assertTrue($this->cache->saveDeferred(new CacheItem("key")));
        $this->assertFalse($this->cache->hasItem("key"));
        $this->assertEquals(new CacheItem("key"), $this->cache->getItem("key"));
    }

    /**
     *
     */
    public function test_getItems()
    {
        $this->assertEquals([
            "k1" => new CacheItem("k1"),
            "k2" => new CacheItem("k2"),
            "k3" => new CacheItem("k3"),
        ], $this->cache->getItems(["k1", "k2", "k3"]));
    }
}