<?php

namespace PoireauFramework\Cache;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Cache
 * @group ext_PoireauFramework_Cache_CacheItem
 */
class CacheItemTest extends TestCase
{
    /**
     *
     */
    public function test_defaults()
    {
        $item = new CacheItem("my_key");

        $this->assertEquals("my_key", $item->getKey());
        $this->assertNull($item->get());
        $this->assertFalse($item->isHit());
    }

    /**
     *
     */
    public function test_set_get()
    {
        $item = new CacheItem("my_key");
        $val = new \stdClass();

        $this->assertSame($item, $item->set($val));
        $this->assertSame($val, $item->get());
    }

    /**
     *
     */
    public function test_isHit_no_expiration()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");

        $this->assertTrue($item->isHit());
    }

    /**
     *
     */
    public function test_expiresAt()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");

        $this->assertSame($item, $item->expiresAt(new \DateTime("-1days")));
        $this->assertFalse($item->isHit());

        $this->assertSame($item, $item->expiresAt(new \DateTime("+1days")));
        $this->assertTrue($item->isHit());

        $this->assertSame($item, $item->expiresAt(null));
        $this->assertTrue($item->isHit());
    }

    /**
     *
     */
    public function test_expiresAfter_with_int()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");

        $this->assertSame($item, $item->expiresAfter(1));
        $this->assertTrue($item->isHit());

        sleep(2);
        $this->assertFalse($item->isHit());
    }

    /**
     *
     */
    public function test_expiresAfter_with_interval()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");

        $this->assertSame($item, $item->expiresAfter(new \DateInterval("PT1S")));
        $this->assertTrue($item->isHit());

        sleep(2);
        $this->assertFalse($item->isHit());
    }

    /**
     *
     */
    public function test_expiresAfter_with_null()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");

        $this->assertSame($item, $item->expiresAfter(null));
        $this->assertTrue($item->isHit());
    }

    /**
     *
     */
    public function test_export_no_expiration()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");

        $this->assertEquals("\x01" . pack("q", -1) . 's:3:"foo";', $item->export());
    }

    /**
     *
     */
    public function test_export_with_expiration()
    {
        $item = new CacheItem("my_key");
        $item->set("foo");
        $item->expiresAt(new \DateTime("@123"));

        $this->assertEquals("\x01" . pack("q", 123) . 's:3:"foo";', $item->export());
    }

    /**
     *
     */
    public function test_import_bad_version()
    {
        $item = new CacheItem("my_key");

        $this->assertSame($item, $item->import("\x05" . pack("q", -1) . 's:3:"foo";'));

        $this->assertFalse($item->isHit());
        $this->assertNull($item->get());
    }

    /**
     *
     */
    public function test_import_bad_data()
    {
        $item = new CacheItem("my_key");

        $this->assertSame($item, $item->import("not_valid"));

        $this->assertFalse($item->isHit());
        $this->assertNull($item->get());
    }

    /**
     *
     */
    public function test_import_no_expiration()
    {
        $item = new CacheItem("my_key");

        $this->assertSame($item, $item->import("\x01" . pack("q", -1) . 's:3:"bar";'));

        $this->assertTrue($item->isHit());
        $this->assertEquals("bar", $item->get());

        $this->assertEquals((new CacheItem("my_key"))->set("bar"), $item);
    }

    /**
     *
     */
    public function test_import_with_expiration()
    {
        $item = new CacheItem("my_key");

        $this->assertSame($item, $item->import("\x01" . pack("q", time() + 1) . 's:3:"bar";'));

        $this->assertTrue($item->isHit());
        $this->assertEquals("bar", $item->get());

        $this->assertEquals((new CacheItem("my_key"))->set("bar")->expiresAfter(1), $item);
    }

    /**
     *
     */
    public function test_import_with_expiration2()
    {
        $item = new CacheItem("my_key");

        $this->assertSame($item, $item->import("\x01" . pack("q", time() - 1) . 's:3:"bar";'));

        $di = new \DateInterval("PT1S");
        $di->invert = 1;

        $this->assertEquals((new CacheItem("my_key"))->set("bar")->expiresAfter($di), $item);
    }

    /**
     *
     */
    public function test_functional_import_export()
    {
        $item = new CacheItem("my_key");
        $item
            ->set(["foo" => "bar"])
            ->expiresAt(new \DateTime("2017-02-25 15:12:32"))
        ;

        $this->assertEquals($item, (new CacheItem("my_key"))->import($item->export()));
    }
}
