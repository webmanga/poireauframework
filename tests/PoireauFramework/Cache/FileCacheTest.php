<?php

namespace PoireauFramework\Cache;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Cache
 * @group ext_PoireauFramework_Cache_FileCache
 */
class FileCacheTest extends TestCase
{
    /**
     * @var FileCache
     */
    private $cache;


    /**
     *
     */
    protected function setUp()
    {
        $this->cache = new FileCache(__DIR__."/_files");
    }

    /**
     *
     */
    protected function tearDown()
    {
        if (!is_dir(__DIR__."/_files")) {
            return;
        }

        $it = new \DirectoryIterator(__DIR__."/_files");

        foreach ($it as $file) {
            if ($file->isFile()) {
                unlink($file->getPathname());
            }
        }
    }

    /**
     *
     */
    public function test_getItem_not_found()
    {
        $item = $this->cache->getItem("not_found");

        $this->assertInstanceOf(CacheItem::class, $item);
        $this->assertFalse($item->isHit());
    }

    /**
     *
     */
    public function test_hasItem_not_found()
    {
        $this->assertFalse($this->cache->hasItem("not_found"));
    }

    /**
     *
     */
    public function test_save_no_hit()
    {
        $item = $this->cache->getItem("my_key");
        $item
            ->set("foo")
            ->expiresAt(new \DateTime("-1days"))
        ;

        $this->assertFalse($this->cache->save($item));
        $this->assertFileNotExists(__DIR__."/_files/pw-cache-".base64_encode("my_key").".cache");
    }

    /**
     *
     */
    public function test_save_success()
    {
        $item = $this->cache->getItem("my_key");
        $item
            ->set(["foo" => "bar"])
            ->expiresAfter(120)
        ;

        $this->assertTrue($this->cache->save($item));
        $this->assertFileExists(__DIR__."/_files/pw-cache-".base64_encode("my_key").".cache");

        $saved = $this->cache->getItem("my_key");

        $this->assertEquals($item, $saved);
        $this->assertTrue($saved->isHit());
        $this->assertTrue($this->cache->hasItem("my_key"));
    }

    /**
     *
     */
    public function test_hasItem_no_hit()
    {
        $item = new CacheItem("no_hit");
        $item
            ->set("foo")
            ->expiresAt(new \DateTime("-1days"))
        ;

        file_put_contents(__DIR__."/_files/pw-cache-".base64_encode($item->getKey()).".cache", $item->export());

        $this->assertTrue($this->cache->hasItem("no_hit"));
    }

    /**
     *
     */
    public function test_getItem_no_hit()
    {
        $item = new CacheItem("no_hit");
        $item
            ->set("foo")
            ->expiresAt(new \DateTime("-1days"))
        ;

        file_put_contents(__DIR__."/_files/pw-cache-".base64_encode($item->getKey()).".cache", $item->export());

        $saved = $this->cache->getItem("no_hit");

        $this->assertEquals($item, $saved);
        $this->assertFalse($saved->isHit());
    }

    /**
     *
     */
    public function test_getItems()
    {
        $i1 = $this->cache->getItem("k1")->set("foo");
        $i2 = $this->cache->getItem("k2")->set("bar");

        $this->cache->save($i1);
        $this->cache->save($i2);

        $this->assertEquals([
            "k1" => $i1,
            "k2" => $i2,
            "not_found" => $this->cache->getItem("not_found")
        ], $this->cache->getItems(["k1", "k2", "not_found"]));
    }

    /**
     *
     */
    public function test_getItems_empty()
    {
        $this->assertSame([], $this->cache->getItems());
    }

    /**
     *
     */
    public function test_deleteItem_not_found()
    {
        $this->assertTrue($this->cache->deleteItem("not_found"));
    }

    /**
     *
     */
    public function test_deleteItem_success()
    {
        $item = $this->cache->getItem("my_key")->set("foo");

        $this->cache->save($item);
        $this->assertTrue($this->cache->deleteItem("my_key"));

        $this->assertFileNotExists(__DIR__."/_files/pw-cache-".base64_encode("my_key").".cache");
        $this->assertFalse($this->cache->hasItem("my_key"));
    }

    /**
     *
     */
    public function test_deleteItems()
    {
        $i1 = $this->cache->getItem("k1")->set("foo");
        $i2 = $this->cache->getItem("k2")->set("bar");

        $this->cache->save($i1);
        $this->cache->save($i2);

        $this->assertTrue($this->cache->deleteItems(["k1", "k2", "not_found"]));

        $this->assertFalse($this->cache->hasItem("k1"));
        $this->assertFalse($this->cache->hasItem("k2"));
    }

    /**
     *
     */
    public function test_deleteItems_empty()
    {
        $this->assertTrue($this->cache->deleteItems([]));
    }

    ///////////////////
    // Test deferred //
    ///////////////////

    /**
     *
     */
    public function test_saveDeferred()
    {
        $item = $this->cache->getItem("my_key")->set("foo");

        $this->assertTrue($this->cache->saveDeferred($item));
        $this->assertFileNotExists(__DIR__."/_files/pw-cache-".base64_encode("my_key").".cache");
    }

    /**
     *
     */
    public function test_getItem_deferred()
    {
        $item = $this->cache->getItem("my_key")->set("foo");

        $this->cache->saveDeferred($item);

        $get = $this->cache->getItem("my_key");
        $this->assertEquals($item, $get);
        $this->assertNotSame($item, $get);
    }

    /**
     *
     */
    public function test_hasItem_deferred()
    {
        $item = $this->cache->getItem("my_key")->set("foo");

        $this->cache->saveDeferred($item);

        $this->assertTrue($this->cache->hasItem("my_key"));
    }

    /**
     *
     */
    public function test_commit_pending()
    {
        $i1 = $this->cache->getItem("k1")->set("foo");
        $i2 = $this->cache->getItem("k2")->set("bar");

        $this->cache->saveDeferred($i1);
        $this->cache->saveDeferred($i2);

        $this->assertAttributeCount(2, "deferred", $this->cache);

        $this->assertTrue($this->cache->commit());
        $this->assertAttributeEmpty("deferred", $this->cache);

        $this->assertFileExists(__DIR__."/_files/pw-cache-".base64_encode("k1").".cache");
        $this->assertFileExists(__DIR__."/_files/pw-cache-".base64_encode("k2").".cache");

        $this->assertEquals(["k1" => $i1, "k2" => $i2], $this->cache->getItems(["k1", "k2"]));
    }

    /**
     *
     */
    public function test_commit_empty()
    {
        $this->assertTrue($this->cache->commit());
    }

    /**
     *
     */
    public function test_deleteItem_deferred()
    {
        $i1 = $this->cache->getItem("k1")->set("foo");

        $this->cache->saveDeferred($i1);

        $this->assertTrue($this->cache->deleteItem("k1"));

        $this->assertAttributeEmpty("deferred", $this->cache);
        $this->assertFalse($this->cache->hasItem("k1"));
    }
}
