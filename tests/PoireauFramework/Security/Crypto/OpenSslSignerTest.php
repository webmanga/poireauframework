<?php

namespace PoireauFramework\Security\Crypto;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Security
 * @group ext_PoireauFramework_Security_Crypto
 * @group ext_PoireauFramework_Security_Crypto_OpenSslSigner
 */
class OpenSslSignerTest extends TestCase
{
    /**
     * @var OpenSslSigner
     */
    private $signer;

    /**
     * @var object
     */
    private $config;

    /**
     *
     */
    protected function setUp()
    {
        $this->config = new \stdClass();
        $this->config->private_key_file = __DIR__."/_files/private.pem";
        $this->config->public_key_file = __DIR__."/_files/public.pem";

        $this->signer = new OpenSslSigner($this->config);
    }

    /**
     *
     */
    public function test_sign()
    {
        $input = "Hello World !";
        $signed = $this->signer->sign($input);

        $decoded = base64_decode($signed);
        $this->assertEquals(512, strlen($decoded)); //key size / 8 => 4096 / 8

        $this->assertEquals($signed, $this->signer->sign($input));
    }

    /**
     *
     */
    public function test_sign_big_data()
    {
        $input = file_get_contents(__DIR__ . "/_files/big_data");
        $signed = $this->signer->sign($input);

        $decoded = base64_decode($signed);

        $chunkCount = ceil(strlen($input) / 511); // input / (keysize - 11)
        $this->assertEquals($chunkCount * 512, strlen($decoded)); //(keysize / 8) * chunkCount
    }

    /**
     *
     */
    public function test_sign_unsign()
    {
        $input = "Hello World !";
        $signed = $this->signer->sign($input);
        $unsigned = $this->signer->unsign($signed);

        $this->assertEquals($input, $unsigned);
    }

    /**
     *
     */
    public function test_sign_unsign_big_data()
    {
        $input = file_get_contents(__DIR__ . "/_files/big_data");
        $signed = $this->signer->sign($input);
        $unsigned = $this->signer->unsign($signed);

        $this->assertEquals($input, $unsigned);
    }

    /**
     *
     */
    public function test_unsign_invalid_data()
    {
        $this->assertNull($this->signer->unsign(base64_encode("Invalid data")));
    }

    /**
     *
     */
    public function test_unsign_bad_key()
    {
        $signer = new OpenSslSigner($conf = (object) [
            "private_key_bits" => 1024,
            "private_key_file" => __DIR__ . "/_files/other.priv",
            "public_key_file"  => __DIR__ . "/_files/other.pub"
        ]);
        $signer->generateKeys();

        $signed = $signer->sign("Hello World !");

        $this->assertNull($this->signer->unsign($signed));

        unlink($conf->private_key_file);
        unlink($conf->public_key_file);
    }
}
