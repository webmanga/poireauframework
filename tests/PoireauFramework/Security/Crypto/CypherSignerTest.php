<?php

namespace PoireauFramework\Security\Crypto;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Security
 * @group ext_PoireauFramework_Security_Crypto
 * @group ext_PoireauFramework_Security_Crypto_CypherSigner
 */
class CypherSignerTest extends TestCase
{
    /**
     * @var CypherSigner
     */
    private $signer;

    /**
     *
     */
    protected function setUp()
    {
        $this->signer = new CypherSigner(new Cypher(Cypher::AES256_CTR, "my key"));
    }

    /**
     *
     */
    public function test_sign()
    {
        $input = "Hello World !";
        $signed = $this->signer->sign($input);

        $this->assertNotEquals($input, $signed);
    }

    /**
     *
     */
    public function test_sign_big_data()
    {
        $input = file_get_contents(__DIR__ . "/_files/big_data");
        $signed = $this->signer->sign($input);

        $this->assertGreaterThan(strlen($input), strlen($signed));
    }

    /**
     *
     */
    public function test_sign_unsign()
    {
        $input = "Hello World !";
        $signed = $this->signer->sign($input);
        $unsigned = $this->signer->unsign($signed);

        $this->assertEquals($input, $unsigned);
    }

    /**
     *
     */
    public function test_sign_unsign_big_data()
    {
        $input = file_get_contents(__DIR__ . "/_files/big_data");
        $signed = $this->signer->sign($input);
        $unsigned = $this->signer->unsign($signed);

        $this->assertEquals($input, $unsigned);
    }

    /**
     *
     */
    public function test_unsign_invalid_data()
    {
        $this->assertNull($this->signer->unsign(base64_encode("Invalid data")));
    }

    /**
     *
     */
    public function test_unsign_bad_key()
    {
        $signer = new CypherSigner(new Cypher(Cypher::AES256_CTR, "bad key"));
        $signer->generateKeys();

        $signed = $signer->sign("Hello World !");

        $this->assertNull($this->signer->unsign($signed));
    }
}
