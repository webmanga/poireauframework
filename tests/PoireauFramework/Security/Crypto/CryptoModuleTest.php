<?php

namespace PoireauFramework\Security\Crypto;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Injector\BaseInjector;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Security
 * @group ext_PoireauFramework_Security_Crypto
 * @group ext_PoireauFramework_Security_Crypto_CryptoModule
 */
class CryptoModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;


    protected function setUp()
    {
        $this->injector = new BaseInjector();
        $this->injector->register(new CryptoModule());
    }

    /**
     *
     */
    public function test_signer_default_make()
    {
        $signer = $this->injector->create(SignerInterface::class, [(object) ["key" => "abcd"]]);

        $this->assertInstanceOf(CypherSigner::class, $signer);
        $this->assertEquals(new CypherSigner(new Cypher(Cypher::AES256_CTR, "abcd")), $signer);
    }

    /**
     *
     */
    public function test_make_openssl_signer()
    {
        $signer = $this->injector->create(OpenSslSigner::class, [(object) [
            "private_key_file" => __DIR__."/_files/private.pem",
            "public_key_file"  => __DIR__."/_files/public.pem",
        ]]);

        $this->assertInstanceOf(OpenSslSigner::class, $signer);
    }

    /**
     *
     */
    public function test_make_cypher_signer()
    {
        $signer = $this->injector->create(CypherSigner::class, [(object) [
            "algo" => "des",
            "key"  => "abcd",
        ]]);

        $this->assertInstanceOf(CypherSigner::class, $signer);
        $this->assertEquals(new CypherSigner(new Cypher("des", "abcd")), $signer);
    }
}
