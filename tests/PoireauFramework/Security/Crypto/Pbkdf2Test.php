<?php

namespace PoireauFramework\Security\Crypto;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Security
 * @group ext_PoireauFramework_Security_Crypto
 * @group ext_PoireauFramework_Security_Crypto_Pbkdf2
 */
class Pbkdf2Test extends TestCase
{
    /**
     * @var \stdClass
     */
    private $config;

    /**
     * @var Pbkdf2
     */
    private $hash;


    /**
     *
     */
    protected function setUp()
    {
        $this->config = new \stdClass();
        $this->hash = new Pbkdf2($this->config);
    }

    /**
     *
     */
    public function test_hash_defaults()
    {

        $input = "Hello World !";

        $hash = $this->hash->hash($input);

        $decoded = base64_decode($hash);
        //SHA512 + 32 salt
        $this->assertEquals(64 + 32, strlen($decoded));
    }

    /**
     *
     */
    public function test_hash_twice_defaults()
    {
        $input = "Hello World !";

        $hash1 = $this->hash->hash($input);
        $hash2 = $this->hash->hash($input);

        $this->assertNotEquals($hash1, $hash2);
        $this->assertEquals(strlen($hash1), strlen($hash2));
    }

    /**
     *
     */
    public function test_hash_no_salt()
    {
        $this->config->salt_length = 0;
        $input = "Hello World !";

        $hash1 = $this->hash->hash($input);
        $hash2 = $this->hash->hash($input);

        $this->assertEquals($hash1, $hash2);
    }

    /**
     *
     */
    public function test_hash_md5()
    {
        $this->config->salt_length = 12;
        $this->config->algo = "md5";

        $input = "Hello World !";

        $hash = $this->hash->hash($input);

        $this->assertEquals(16 + 12, strlen(base64_decode($hash)));
    }

    /**
     *
     */
    public function test_hash_cost()
    {
        $input = "Hello World !";

        $this->config->cost = 10;

        $start = microtime(true);
        $this->hash->hash($input);
        $time10 = (microtime(true) - $start) * 1000;


        $this->config->cost = 11;

        $start = microtime(true);
        $this->hash->hash($input);
        $time11 = (microtime(true) - $start) * 1000;

        $this->assertEquals($time11, $time10 * 2, "Expect that cost + 1 should be two time slower", $time10/2);
    }

    /**
     *
     */
    public function test_check()
    {
        $input = "Hello World !";

        $hash = $this->hash->hash($input);

        $this->assertFalse($this->hash->check($hash, "Invalid"));
        $this->assertTrue($this->hash->check($hash, $input));
    }
}
