<?php


namespace PoireauFramework\Security\Crypto;


use PHPUnit\Framework\TestCase;
use PoireauFramework\Security\Crypto\Exception\CypherException;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Security
 * @group ext_PoireauFramework_Security_Crypto
 * @group ext_PoireauFramework_Security_Crypto_Cypher
 */
class CypherTest extends TestCase
{
    /**
     * @var Cypher
     */
    private $cypher;


    /**
     *
     */
    protected function setUp()
    {
        $this->cypher = new Cypher(Cypher::AES256_CTR, "my key");
    }

    /**
     *
     */
    public function test_invalid_algo()
    {
        $this->expectException(CypherException::class);
        $this->expectExceptionMessage("Unknown cipher algorithm");

        new Cypher("invalid", "");
    }

    /**
     *
     */
    public function test_encrypt_randomize()
    {
        $input = "Hello World !";

        $this->assertNotEquals(
            $this->cypher->encrypt($input),
            $this->cypher->encrypt($input)
        );
    }

    /**
     *
     */
    public function test_encrypt_not_readable()
    {
        $input = "Hello World !";

        $this->assertNotEquals(
            $input,
            $this->cypher->encrypt($input)
        );
    }

    /**
     *
     */
    public function test_encrypt_decrypt()
    {
        $input = "Hello World !";

        $this->assertEquals(
            $input,
            $this->cypher->decrypt($this->cypher->encrypt($input))
        );
    }

    /**
     *
     */
    public function test_decrypt_not_base64()
    {
        $this->assertFalse($this->cypher->decrypt("invalid"));
    }

    /**
     *
     */
    public function test_decrypt_invalid_data()
    {
        $this->assertFalse($this->cypher->decrypt(base64_encode("Invalid")));
    }

    /**
     *
     */
    public function test_decrypt_invalid_password()
    {
        $other = new Cypher(Cypher::AES256_CTR, "other");

        $this->assertFalse($this->cypher->decrypt($other->encrypt("test")));
    }
}
