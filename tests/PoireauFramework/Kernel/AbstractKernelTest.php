<?php

namespace PoireauFramework\Kernel;

require_once __DIR__ . '/TestEvents.php';

use PoireauFramework\Event\Dispatcher\EventDispatcherInterface;
use PoireauFramework\Event\EventModule;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Kernel\Event\Type\AfterRun;
use PoireauFramework\Kernel\Event\Type\BeforeRun;
use PoireauFramework\Kernel\Event\Type\KernelError;
use PoireauFramework\Kernel\Event\Type\KernelLoaded;
use PoireauFramework\Kernel\Exception\InvalidStateException;
use PoireauFramework\Kernel\KernelInterface;
use PoireauFramework\Registry\RegistryInterface;
use PoireauFramework\Registry\RegistryModule;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractKernelTest
 *
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_Kernel
 * @group ext_PoireauFramework_Kernel_AbstractKernel
 */
class AbstractKernelTest extends TestCase
{
    /**
     *
     */
    public function test_construct()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);

        $this->assertFalse($kernel->isLoaded());
        $this->assertSame($injector, $kernel->injector());
        $this->assertSame($disptacher, $kernel->events());
        $this->assertSame($registry, $kernel->registry());
    }

    /**
     *
     */
    public function test_load()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);

        $disptacher->expects($this->once())
            ->method('dispatch')
            ->with(KernelLoaded::NAME, [$kernel]);

        $kernel->load();

        $this->assertTrue($kernel->isLoaded());
    }

    /**
     *
     */
    public function test_load_already_loaded_do_nothing()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);

        $kernel->load();

        $disptacher->expects($this->never())
            ->method('dispatch')
            ->with(KernelLoaded::NAME, [$kernel]);

        $kernel->load();
        $this->assertTrue($kernel->isLoaded());
    }

    /**
     *
     */
    public function test_run_not_loaded()
    {
        $this->expectException(InvalidStateException::class);

        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);

        $kernel->run();
    }

    /**
     *
     */
    public function test_run()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);
        $kernel->load();

        $disptacher
            ->expects($this->exactly(2))
            ->method('dispatch')
            ->withConsecutive(
                [BeforeRun::class, [$kernel]],
                [AfterRun::class, [$kernel]]
            )
        ;

        $kernel->run();

        $this->assertTrue($kernel->runner->called);
        $this->assertSame($kernel, $kernel->runner->kernel);
    }

    /**
     *
     */
    public function test_run_with_exception()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);
        $kernel->load();

        $exception = new \Exception('Runner Error');

        $kernel->runner = new TestKernelRunner();
        $kernel->runner->exception = $exception;

        $disptacher
            ->expects($this->exactly(3))
            ->method('dispatch')
            ->withConsecutive(
                [BeforeRun::class, [$kernel]],
                [KernelError::NAME, [$exception]],
                [AfterRun::class, [$kernel]]
            )
        ;

        $kernel->run();
    }

    /**
     *
     */
    public function test_functional_run()
    {
        $injector = new BaseInjector();
        $injector->register(new RegistryModule());
        $injector->register(new EventModule());

        $kernel = new TestAbstractKernel(
            $injector,
            $injector->get(RegistryInterface::class),
            $injector->get(EventDispatcherInterface::class)
        );

        $eventsModule = new TestKernelEventsModule();
        $kernel->register($eventsModule);
        $kernel->load();

        $kernel->run();

        $this->assertTrue($kernel->runner->called);
        $this->assertSame($kernel, $kernel->runner->kernel);

        $this->assertTrue($eventsModule->beforeRun->called);
        $this->assertTrue($eventsModule->afterRun->called);
        $this->assertTrue($eventsModule->kernelLoaded->called);
        $this->assertFalse($eventsModule->kernelError->called);
    }

    /**
     *
     */
    public function test_register()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);

        $eventsModule = new TestKernelEventsModule();

        $disptacher->expects($this->once())
            ->method('register')
            ->with($eventsModule);

        $kernel->register($eventsModule);

        $injectorModule = new RegistryModule();
        $injector->expects($this->once())
            ->method('register')
            ->with($injectorModule);

        $kernel->register($injectorModule);
    }

    /**
     *
     */
    public function test_register_bootable()
    {
        $injector = $this->createMock(InjectorInterface::class);
        $disptacher = $this->createMock(EventDispatcherInterface::class);
        $registry = $this->createMock(RegistryInterface::class);

        $kernel = new TestAbstractKernel($injector, $registry, $disptacher);

        $module = $this->createMock(BootableModuleInterface::class);

        $kernel->register($module);

        $module->expects($this->once())
            ->method('boot')
            ->with($kernel);

        $kernel->load();
    }
}

class TestAbstractKernel extends AbstractKernel {
    /**
     * @var TestKernelRunner
     */
    public $runner;

    protected function runner(): RunnerInterface
    {
        if (!$this->runner) {
            $this->runner = new TestKernelRunner();
        }

        return $this->runner;
    }
}

class TestKernelRunner implements RunnerInterface {
    public $called = false;
    public $kernel = null;

    /**
     * @var \Exception
     */
    public $exception = null;

    public function run(KernelInterface $kernel): void
    {
        $this->called = true;
        $this->kernel = $kernel;

        if ($this->exception) {
            throw $this->exception;
        }
    }
}