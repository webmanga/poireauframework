<?php

namespace PoireauFramework\Kernel;


use PoireauFramework\Event\EventListenerInterface;
use PoireauFramework\Kernel\Event\Contract\AfterRunListenerInterface;
use PoireauFramework\Kernel\Event\Contract\BeforeRunListenerInterface;
use PoireauFramework\Kernel\Event\Contract\KernelErrorListenerInterface;
use PoireauFramework\Kernel\Event\Contract\KernelLoadedListenerInterface;
use PoireauFramework\Kernel\Event\Type\AfterRun;
use PoireauFramework\Kernel\Event\Type\BeforeRun;
use PoireauFramework\Kernel\Event\Type\KernelError;
use PoireauFramework\Kernel\Event\Type\KernelLoaded;

class TestAfterRunListener implements AfterRunListenerInterface {
    public $called = false;
    public $kernel = null;

    public function onAfterRun(KernelInterface $kernel): void
    {
        $this->called = true;
        $this->kernel = $kernel;
    }
}

class TestBeforeRunListener implements BeforeRunListenerInterface {
    public $called = false;
    public $kernel = null;

    public function onBeforeRun(KernelInterface $kernel): void
    {
        $this->called = true;
        $this->kernel = $kernel;
    }
}

class TestKernelLoadedListener implements KernelLoadedListenerInterface {
    public $called = false;
    public $kernel = null;

    public function onKernelLoaded(KernelInterface $kernel): void
    {
        $this->called = true;
        $this->kernel = $kernel;
    }
}

class TestKernelErrorListener implements KernelErrorListenerInterface {
    public $called = false;
    public $exception = null;

    public function onKernelError(\Exception $kernel): void
    {
        $this->called = true;
        $this->exception = $kernel;
    }
}

class TestKernelEventsModule implements EventListenerInterface, BeforeRunListenerInterface, AfterRunListenerInterface, KernelLoadedListenerInterface, KernelErrorListenerInterface {
    /**
     * @var TestBeforeRunListener
     */
    public $beforeRun;

    /**
     * @var TestAfterRunListener
     */
    public $afterRun;

    /**
     * @var TestKernelLoadedListener
     */
    public $kernelLoaded;

    /**
     * @var TestKernelErrorListener
     */
    public $kernelError;

    /**
     * TestKernelEventsModule constructor.
     */
    public function __construct()
    {
        $this->beforeRun = new TestBeforeRunListener;
        $this->afterRun = new TestAfterRunListener();
        $this->kernelLoaded = new TestKernelLoadedListener();
        $this->kernelError = new TestKernelErrorListener();
    }


    /**
     * {@inheritdoc}
     */
    public function eventTypes(): array
    {
        return [
            BeforeRun::NAME, AfterRun::NAME, KernelLoaded::NAME, KernelError::NAME
        ];
    }

    public function onAfterRun(\PoireauFramework\Kernel\KernelInterface $kernel): void
    {
        $this->afterRun->onAfterRun($kernel);
    }

    public function onBeforeRun(\PoireauFramework\Kernel\KernelInterface $kernel): void
    {
        $this->beforeRun->onBeforeRun($kernel);
    }

    public function onKernelError(\Exception $exception): void
    {
        $this->kernelError->onKernelError($exception);
    }

    public function onKernelLoaded(\PoireauFramework\Kernel\KernelInterface $kernel): void
    {
        $this->kernelLoaded->onKernelLoaded($kernel);
    }


}