<?php

namespace PoireauFramework\App\Kernel;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Controller\TestController;
use PoireauFramework\App\Http\RequestRunner;
use PoireauFramework\App\Testing\TestingAppModule;
use PoireauFramework\App\Testing\TestingResponseSender;
use PoireauFramework\Config\Config;
use PoireauFramework\Config\PhpConfigParser;
use PoireauFramework\Event\Dispatcher\BaseEventDispatcher;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseSenderInterface;
use PoireauFramework\Http\Response\ResponseStack;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Kernel\Event\Contract\KernelErrorListenerInterface;
use PoireauFramework\Kernel\Event\Type\KernelError;
use PoireauFramework\Registry\Registry;

/**
 * Class ApplicationTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Kernel
 * @group ext_PoireauFramework_App_Kernel_Application
 */
class ApplicationTest extends TestCase
{
    /**
     * @var Application
     */
    private $application;


    public function setUp()
    {
        $this->application = new Application([]);
    }

    /**
     *
     */
    public function test_defaults()
    {
        $this->assertInstanceOf(Config::class, $this->application->config());
        $this->assertInstanceOf(BaseEventDispatcher::class, $this->application->events());
        $this->assertInstanceOf(BaseInjector::class, $this->application->injector());
        $this->assertInstanceOf(Registry::class, $this->application->registry());
    }

    /**
     *
     */
    public function test_load()
    {
        $this->application->load();

        $this->assertInstanceOf(RequestStack::class, $this->application->requestStack());
        $this->assertInstanceOf(Request::class, $this->application->baseRequest());
        $this->assertSame($this->application->baseRequest(), $this->application->request());

        $newRequest = (new RequestBuilder())->build();
        $this->application->requestStack()->push($newRequest);

        $this->assertSame($newRequest, $this->application->request());

        $this->assertInstanceOf(RequestRunner::class, $this->application->requestRunner());

        $this->assertSame($this->application->requestStack(), $this->application->requestStack());
    }

    /**
     *
     */
    public function test_run_functional_success()
    {
        $this->application->config()->setParser(new PhpConfigParser());
        $this->application->config()->load(__DIR__ . "/../_files/test_config.php");

        $_SERVER["PATH_INFO"] = "/test/index/arg1/arg2";
        $_SERVER["REQUEST_METHOD"] = "GET";
        $this->application->load();
        $this->application->register(new TestingAppModule(false));
        $this->application->run();

        $this->assertEquals("PoireauFramework\\App\\Controller\\TestController::indexAction", TestController::$method);
        $this->assertEquals(["arg1", "arg2"], TestController::$args);

        $response = TestingResponseSender::instance()->lastResponse;

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(new ArrayBag(["foo" => "bar"]), $response->getBody());
        $this->assertEquals(200, $response->getCode());
    }

    /**
     *
     */
    public function test_run_functional_error()
    {
        $this->application->config()->setParser(new PhpConfigParser());
        $this->application->config()->load(__DIR__ . "/../_files/test_config.php");

        $_SERVER["PATH_INFO"] = "/test/error";
        $_SERVER["REQUEST_METHOD"] = "GET";

        $errorListener = new ErrorListener();

        $this->application->load();
        $this->application->register(new TestingAppModule(false));

        $this->application->events()->addListener(KernelError::class, $errorListener);
        $this->application->run();

        $this->assertInstanceOf(\Exception::class, $errorListener->exception);
        $this->assertEquals(TestController::class . '::errorAction', $errorListener->exception->getMessage());
    }
}

class ErrorListener implements KernelErrorListenerInterface
{
    /**
     * @var \Exception
     */
    public $exception;

    public function onKernelError(\Exception $exception): void
    {
        $this->exception = $exception;
    }
}