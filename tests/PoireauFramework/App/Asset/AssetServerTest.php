<?php


namespace PoireauFramework\App\Asset;


use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Exception\NotFoundHttpException;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Response\Response;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Asset
 * @group ext_PoireauFramework_App_Asset_AssetServer
 */
class AssetServerTest extends TestCase
{
    /**
     * @var AssetServer
     */
    private $server;


    /**
     *
     */
    protected function setUp()
    {
        $this->server = new AssetServer("assets");
    }

    /**
     *
     */
    public function test_path()
    {
        $this->assertEquals("assets", $this->server->path());
    }

    /**
     *
     */
    public function test_handle_bad_request()
    {
        $this->expectException(NotFoundHttpException::class);

        $this->server->handle((new RequestBuilder())->build());
    }

    /**
     *
     */
    public function test_handle_handler_not_found()
    {
        $this->expectException(NotFoundHttpException::class);

        $this->server->handle((new RequestBuilder())->path("/assets/not_found/xxx")->build());
    }

    /**
     *
     */
    public function test_handle_handler_not_supports()
    {
        $this->expectException(NotFoundHttpException::class);

        $handler = new FileHandler("path", ["test" => __DIR__]);
        $this->server->register($handler);

        $this->server->handle((new RequestBuilder())->path("/assets/path/xxx")->build());
    }

    /**
     *
     */
    public function test_handle_success()
    {
        $handler = new FileHandler("path", ["test" => __DIR__]);
        $this->server->register($handler);

        $response = $this->server->handle((new RequestBuilder())->path("/assets/path/test/AssetServerTest.php")->build());

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(new TextBag(file_get_contents(__FILE__)), $response->getBody());
    }
}
