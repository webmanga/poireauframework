<?php

namespace PoireauFramework\App\Asset;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Http\Event\PrepareArgument;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Response\Response;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Asset
 * @group ext_PoireauFramework_App_Asset_ServerListener
 */
class ServerListenerTest extends TestCase
{
    /**
     * @var ServerListener
     */
    private $listener;

    /**
     * @var AssetServer
     */
    private $server;


    /**
     *
     */
    protected function setUp()
    {
        $this->server = new AssetServer("assets");

        $this->listener = new ServerListener($this->server);
    }

    /**
     *
     */
    public function test_success()
    {
        $argument = new PrepareArgument((new RequestBuilder())->path("/assets/path/test/ServerListenerTest.php")->build());
        $this->server->register(new FileHandler("path", ["test" => __DIR__]));

        $this->listener->onPrepare($argument);

        $this->assertInstanceOf(Response::class, $argument->result());
        $this->assertEquals(new TextBag(file_get_contents(__FILE__)), $argument->result()->getBody());
    }

    /**
     *
     */
    public function test_not_handled()
    {
        $argument = new PrepareArgument((new RequestBuilder())->path("/other/path")->build());

        $this->listener->onPrepare($argument);

        $this->assertNull($argument->result());
    }

    /**
     *
     */
    public function test_functional()
    {
        $app = new Application([]);
        $app->register($this->listener);
        $this->server->register(new FileHandler("path", ["test" => __DIR__]));

        $response = $app->requestRunner()->run((new RequestBuilder())->path("/assets/path/test/ServerListenerTest.php")->build());

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(new TextBag(file_get_contents(__FILE__)), $response->getBody());
    }
}
