<?php

namespace PoireauFramework\App\Asset;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Asset
 * @group ext_PoireauFramework_App_Asset_AssetServerModule
 */
class AssetServerModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;

    /**
     * @var Registry
     */
    private $registry;


    /**
     *
     */
    protected function setUp()
    {
        $this->injector = new BaseInjector($this->registry = new Registry());

        $this->injector->instance(new Config(RegistryFactory::createFromArray([
            "asset" => [
                "server_path" => "assets"
            ]
        ])));

        $this->injector->register(new AssetServerModule());
    }

    /**
     *
     */
    public function test_createServer()
    {
        $server = $this->injector->get(AssetServer::class);

        $this->assertInstanceOf(AssetServer::class, $server);
        $this->assertEquals("assets", $server->path());
    }

    /**
     *
     */
    public function test_createFileHandler()
    {
        $handler = $this->injector->create(FileHandler::class, ["path", ["foo" => "bar"]]);

        $this->assertInstanceOf(FileHandler::class, $handler);
        $this->assertEquals(new FileHandler("path", ["foo" => "bar"]), $handler);
    }

    /**
     *
     */
    public function test_createServer_with_handlers()
    {
        $this->registry->sub("asset")->set("handlers", [
            [FileHandler::class, "path", []]
        ]);
        $server = $this->injector->get(AssetServer::class);

        $expect = new AssetServer("assets");
        $expect->register(new FileHandler("path", []));

        $this->assertEquals($expect, $server);
    }
}
