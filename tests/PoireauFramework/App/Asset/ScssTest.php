<?php

namespace PoireauFramework\App\Asset;

use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Formatter\Expanded;
use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Registry\Registry;

/**
 * @group PoireauFramework
 * @group PoireauFramework_App
 * @group PoireauFramework_App_Asset
 * @group PoireauFramework_App_Asset_Scss
 */
class ScssTest extends TestCase
{
    /**
     * @var Scss
     */
    private $scss;

    /**
     * @var ConfigItem
     */
    private $config;

    /**
     *
     */
    protected function setUp()
    {
        $this->config = new ConfigItem(new Registry([
            "src" => __DIR__ . "/_files/scss/",
            "out" => __DIR__ . "/_files/css/",
            "formatter" => "expanded"
        ]));

        $compiler = new Compiler();
        $compiler->addImportPath($this->config->src);
        $compiler->setFormatter(Expanded::class);

        $this->scss = new Scss($this->config, $compiler);
    }

    /**
     *
     */
    protected function tearDown()
    {
        $iterator = new \DirectoryIterator($this->config->out);

        foreach ($iterator as $file) {
            if ($file->isDot()) {
                continue;
            }

            if ($file->getBasename() === '.keep-dir') {
                continue;
            }

            unlink($file->getPathname());
        }
    }

    /**
     *
     */
    public function test_compile()
    {
        $this->scss->compile("test.scss");

        $this->assertFileExists($this->config->out . "test.scss.css");
        $this->assertEquals(<<<CSS
.reverse {
  color: #fff;
  background: #000;
}
body {
  background: #fff;
  color: #000;
}

CSS
, file_get_contents($this->config->out . "test.scss.css"));
    }

    /**
     *
     */
    public function test_generate_no_cache()
    {
        $this->scss->compile("test.scss");

        $this->assertEquals(<<<CSS
.reverse {
  color: #fff;
  background: #000;
}
body {
  background: #fff;
  color: #000;
}

CSS
, $this->scss->generate("test.scss"));
    }
}