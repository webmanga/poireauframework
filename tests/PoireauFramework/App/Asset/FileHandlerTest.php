<?php

namespace PoireauFramework\App\Asset;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\Response;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Asset
 * @group ext_PoireauFramework_App_Asset_FileHandler
 */
class FileHandlerTest extends TestCase
{
    /**
     * @var FileHandler
     */
    private $handler;


    /**
     *
     */
    protected function setUp()
    {
        $this->handler = new FileHandler("path", ["test" => __DIR__]);
    }

    /**
     *
     */
    public function test_basePath()
    {
        $this->assertEquals("path", $this->handler->basePath());
    }

    /**
     *
     */
    public function test_supports()
    {
        $this->assertFalse($this->handler->supports("not_found", $this->createMock(RequestInterface::class)));
        $this->assertTrue($this->handler->supports("test/FileHandlerTest.php", $this->createMock(RequestInterface::class)));
    }

    /**
     *
     */
    public function test_handle()
    {
        $response = $this->handler->handle("test/FileHandlerTest.php", $this->createMock(RequestInterface::class));

        $this->assertInstanceOf(Response::class, $response);

        $this->assertEquals(new TextBag(file_get_contents(__FILE__)), $response->getBody());
    }
}
