<?php


namespace PoireauFramework\App\Asset\Cache;


use PHPUnit\Framework\TestCase;

/**
 * @group PoireauFramework
 * @group PoireauFramework_App
 * @group PoireauFramework_App_Asset
 * @group PoireauFramework_App_Asset_Cache
 * @group PoireauFramework_App_Asset_Cache_AlwaysRecompile
 */
class AlwaysRecompileTest extends TestCase
{
    /**
     * @var CacheStrategyInterface
     */
    private $strategy;


    /**
     *
     */
    protected function setUp()
    {
        $this->strategy = new AlwaysRecompile();
    }

    /**
     *
     */
    public function test_needsCompile()
    {
        $this->assertTrue($this->strategy->needsCompile(__DIR__."/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_get()
    {
        $this->assertNull($this->strategy->get(__DIR__."/../_files/scss/test.scss"));
    }
}
