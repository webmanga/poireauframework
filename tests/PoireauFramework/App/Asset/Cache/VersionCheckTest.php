<?php

namespace PoireauFramework\App\Asset\Cache;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Cache\FileCache;
use PoireauFramework\Cache\Psr\CacheItemPoolInterface;

/**
 * @group PoireauFramework
 * @group PoireauFramework_App
 * @group PoireauFramework_App_Asset
 * @group PoireauFramework_App_Asset_Cache
 * @group PoireauFramework_App_Asset_Cache_VersionCheck
 */
class VersionCheckTest extends TestCase
{
    /**
     * @var VersionCheck
     */
    private $strategy;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;


    /**
     *
     */
    protected function setUp()
    {
        $this->cache = new FileCache();
        $this->strategy = new VersionCheck($this->cache, "1.0");
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->cache->deleteItem(md5(__DIR__ . "/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_needsCompile_not_in_cache()
    {
        $this->assertTrue($this->strategy->needsCompile(__DIR__ . "/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_save()
    {
        $this->strategy->save(__DIR__ . "/../_files/scss/test.scss", "my css");
        $this->assertFalse($this->strategy->needsCompile(__DIR__ . "/../_files/scss/test.scss"));
        $this->assertEquals("my css", $this->strategy->get(__DIR__ . "/../_files/scss/test.scss"));
    }

    public function test_needsCompile_bad_version()
    {
        $strategy = new VersionCheck($this->cache, "1.1");
        $strategy->save(__DIR__ . "/../_files/scss/test.scss", "new css");

        $this->assertTrue($this->strategy->needsCompile(__DIR__ . "/../_files/scss/test.scss"));
    }
}
