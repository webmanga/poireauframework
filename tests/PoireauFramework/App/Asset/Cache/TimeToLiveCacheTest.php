<?php

namespace PoireauFramework\App\Asset\Cache;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Cache\FileCache;
use PoireauFramework\Cache\Psr\CacheItemPoolInterface;

/**
 * @group PoireauFramework
 * @group PoireauFramework_App
 * @group PoireauFramework_App_Asset
 * @group PoireauFramework_App_Asset_Cache
 * @group PoireauFramework_App_Asset_Cache_TimeToLiveCache
 */
class TimeToLiveCacheTest extends TestCase
{
    /**
     * @var TimeToLiveCache
     */
    private $strategy;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;


    /**
     *
     */
    protected function setUp()
    {
        $this->cache = new FileCache();
        $this->strategy = new TimeToLiveCache($this->cache, 120);
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->cache->deleteItem(md5(__DIR__ . "/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_needsCompile_not_in_cache()
    {
        $this->assertTrue($this->strategy->needsCompile(__DIR__ . "/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_needsCompile_cached()
    {
        $item = $this->cache->getItem(md5(__DIR__ . "/../_files/scss/test.scss"))->set("my css");
        $this->cache->save($item);

        $this->assertFalse($this->strategy->needsCompile(__DIR__ . "/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_get_cached()
    {
        $item = $this->cache->getItem(md5(__DIR__ . "/../_files/scss/test.scss"))->set("my css");
        $this->cache->save($item);

        $this->assertEquals("my css", $this->strategy->get(__DIR__ . "/../_files/scss/test.scss"));
    }

    /**
     *
     */
    public function test_save()
    {
        $this->strategy->save(__DIR__ . "/../_files/scss/test.scss", "my compiled css");

        $this->assertFalse($this->strategy->needsCompile(__DIR__ . "/../_files/scss/test.scss"));
        $this->assertEquals("my compiled css", $this->strategy->get(__DIR__ . "/../_files/scss/test.scss"));
        $this->assertEquals("my compiled css", $this->cache->getItem(md5(__DIR__ . "/../_files/scss/test.scss"))->get());
        $this->assertAttributeEquals(new \DateTime("+120seconds"), "expiration", $this->cache->getItem(md5(__DIR__ . "/../_files/scss/test.scss")));
    }
}
