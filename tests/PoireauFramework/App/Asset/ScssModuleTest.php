<?php

namespace PoireauFramework\App\Asset;

use Leafo\ScssPhp\Compiler;
use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Asset\Cache\AlwaysRecompile;
use PoireauFramework\App\Asset\Cache\CacheStrategyInterface;
use PoireauFramework\App\Asset\Cache\TimeToLiveCache;
use PoireauFramework\App\Asset\Cache\VersionCheck;
use PoireauFramework\Config\Config;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group PoireauFramework
 * @group PoireauFramework_App
 * @group PoireauFramework_App_Asset
 * @group PoireauFramework_App_Asset_ScssModule
 */
class ScssModuleTest extends TestCase
{
    /**
     *
     */
    public function test_instances()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "asset" => [
                "scss" => [
                    "src" => __DIR__ . "/_files/scss/",
                    "out" => __DIR__ . "/_files/css/",
                    "formatter" => "expanded"
                ]
            ]
        ])));
        $injector->register(new ScssModule());

        $this->assertInstanceOf(Compiler::class, $injector->get(Compiler::class));
        $this->assertInstanceOf(Scss::class, $injector->get(Scss::class));
    }

    /**
     *
     */
    public function test_cache_strategy_default()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
        ])));
        $injector->register(new ScssModule());

        $this->assertInstanceOf(AlwaysRecompile::class, $injector->get(CacheStrategyInterface::class));
    }

    /**
     *
     */
    public function test_cache_strategy_recompile()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "asset" => [
                "scss" => [
                    "strategy" => "recompile"
                ]
            ]
        ])));

        $injector->register(new ScssModule());

        $this->assertInstanceOf(AlwaysRecompile::class, $injector->get(CacheStrategyInterface::class));
    }

    /**
     *
     */
    public function test_cache_strategy_ttl()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "asset" => [
                "scss" => [
                    "strategy" => "ttl",
                    "ttl" => 120
                ]
            ]
        ])));

        $injector->register(new ScssModule());

        $this->assertInstanceOf(TimeToLiveCache::class, $injector->get(CacheStrategyInterface::class));
        $this->assertAttributeEquals(120, "ttl", $injector->get(CacheStrategyInterface::class));
    }

    /**
     *
     */
    public function test_cache_strategy_version()
    {
        $injector = new BaseInjector();
        $injector->instance(new Config(RegistryFactory::createFromArray([
            "asset" => [
                "scss" => [
                    "strategy" => "version",
                    "version" => "1.1.2"
                ]
            ]
        ])));

        $injector->register(new ScssModule());

        $this->assertInstanceOf(VersionCheck::class, $injector->get(CacheStrategyInterface::class));
        $this->assertAttributeEquals("1.1.2", "version", $injector->get(CacheStrategyInterface::class));
    }
}
