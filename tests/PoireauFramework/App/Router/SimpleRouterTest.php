<?php

namespace PoireauFramework\App\Router;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Registry\Registry;

/**
 * Class SimpleRouterTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Router
 * @group ext_PoireauFramework_App_Router_SimpleRouter
 */
class SimpleRouterTest extends TestCase
{
    /**
     * @var SimpleRouter
     */
    private $router;

    public function setUp()
    {
        $this->router = new SimpleRouter(new ConfigItem(new Registry([
            "default_controller" => "home",
            "default_action"     => "index"
        ])));
    }

    /**
     *
     */
    public function test_resolveRoute()
    {
        $request = (new RequestBuilder())->path("/user/profile/john")->method("GET")->build();

        $route = $this->router->resolveRoute($request);

        $this->assertEquals("user", $route->controller());
        $this->assertEquals("profile", $route->action());
        $this->assertEquals(["GET"], $route->methods());
    }

    /**
     *
     */
    public function test_resolveRoute_with_defaults()
    {
        $request = (new RequestBuilder())->path("/")->method("GET")->build();

        $route = $this->router->resolveRoute($request);

        $this->assertEquals("home", $route->controller());
        $this->assertEquals("index", $route->action());
        $this->assertEquals(["GET"], $route->methods());
    }

    /**
     *
     */
    public function test_route_defaults()
    {
        $route = $this->router->route([]);

        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals("home", $route->controller());
        $this->assertEquals("index", $route->action());
        $this->assertEquals([], $route->arguments());
        $this->assertEquals(["GET"], $route->methods());
    }

    /**
     *
     */
    public function test_route_with_action()
    {
        $route = $this->router->route(["news", "display", "5"]);

        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals("news", $route->controller());
        $this->assertEquals("display", $route->action());
        $this->assertEquals(["5"], $route->arguments());
        $this->assertEquals(["GET"], $route->methods());
    }

    /**
     *
     */
    public function test_route_with_method()
    {
        $route = $this->router->route(["news", "save", "_method" => "POST"]);

        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals("news", $route->controller());
        $this->assertEquals("save", $route->action());
        $this->assertEquals([], $route->arguments());
        $this->assertEquals(["POST"], $route->methods());
    }

    /**
     *
     */
    public function test_makeUrl_simple_route()
    {
        $url = $this->router->makeUrl(new Route(["GET"], "news", "index"));

        $this->assertEquals("/news/index", $url);
    }

    /**
     *
     */
    public function test_makeUrl_simple_route_with_get()
    {
        $url = $this->router->makeUrl(new Route(["GET"], "news", "index"), ["sort" => "date"]);

        $this->assertEquals("/news/index?sort=date", $url);
    }

    /**
     *
     */
    public function test_makeUrl_with_arguments()
    {
        $url = $this->router->makeUrl(new Route(["GET"], "news", "display", ["5", "my title"]), ["comments" => true]);

        $this->assertEquals("/news/display/5/my+title?comments=1", $url);
    }

    /**
     *
     */
    public function test_match_controller()
    {
        $route = new Route(["GET"], "news", "display", [5]);

        $this->assertTrue($this->router->match(["news"], $route));
        $this->assertFalse($this->router->match(["account"], $route));
    }

    /**
     *
     */
    public function test_match_method()
    {
        $route = new Route(["GET", "POST"], "news", "display", [5]);

        $this->assertFalse($this->router->match(["news", "_method" => "PUT"], $route));
        $this->assertTrue($this->router->match(["news", "_method" => "GET"], $route));
    }

    /**
     *
     */
    public function test_match_action()
    {
        $route = new Route(["GET"], "news", "display", [5]);

        $this->assertTrue($this->router->match(["news", "display"], $route));
        $this->assertFalse($this->router->match(["news", "save"], $route));
    }

    /**
     *
     */
    public function test_match_arguments()
    {
        $route = new Route(["GET"], "news", "display", [5]);

        $this->assertTrue($this->router->match(["news", "display", "5"], $route));
        $this->assertFalse($this->router->match(["news", "display", "5", "other"], $route));
        $this->assertFalse($this->router->match(["news", "display", "6"], $route));
    }

    /**
     *
     */
    public function test_match_full()
    {
        $route = new Route(["GET"], "news", "display", [5]);

        $this->assertTrue($this->router->match(["news", "display", "5", "_method" => "GET"], $route));
    }
}
