<?php

namespace PoireauFramework\App\Http;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Controller\ControllerResolverInterface;
use PoireauFramework\App\Http\Event\Prepare;
use PoireauFramework\App\Http\Event\PrepareArgument;
use PoireauFramework\App\Http\Event\PrepareListenerInterface;
use PoireauFramework\App\Http\Event\Routing;
use PoireauFramework\App\Http\Event\RoutingArgument;
use PoireauFramework\App\Http\Event\RoutingListenerInterface;
use PoireauFramework\App\Http\Event\RunError;
use PoireauFramework\App\Http\Event\RunErrorArgument;
use PoireauFramework\App\Http\Event\RunErrorListenerInterface;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Router\SimpleRouter;
use PoireauFramework\Config\ConfigItem;
use PoireauFramework\Event\Dispatcher\BaseEventDispatcher;
use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Exception\NotFoundHttpException;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Http\Response\ResponseStack;
use PoireauFramework\Http\Response\ResponseTransformer;
use PoireauFramework\Registry\Registry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Http
 * @group ext_PoireauFramework_App_Http_RequestRunner
 */
class RequestRunnerTest extends TestCase
{
    /**
     * @var RequestRunner
     */
    private $runner;

    /**
     * @var BaseEventDispatcher
     */
    private $events;

    /**
     * @var MyControllerResolver
     */
    private $resolver;

    /**
     * @var ResponseStack
     */
    private $responseStack;

    /**
     * @var RequestStack
     */
    private $requestStack;


    /**
     *
     */
    protected function setUp()
    {
        $app = new Application([]);

        $this->runner = new RequestRunner(
            $this->events = $app->events(),
            $this->requestStack = new RequestStack((new RequestBuilder())->build()),
            $this->responseStack = new ResponseStack(),
            $this->resolver = new MyControllerResolver(),
            new ResponseTransformer(),
            new SimpleRouter(new ConfigItem(new Registry()))
        );
    }

    /**
     *
     */
    public function test_run_simple()
    {
        $request = (new RequestBuilder())->build();
        $arg = new \stdClass();
        $isCalled = false;

        $this->resolver->controller = function ($a) use ($arg, &$isCalled) {
            $this->assertSame($arg, $a);
            $isCalled = true;
        };

        $this->resolver->arguments = [$arg];

        $this->assertEquals(new Response(), $this->runner->run($request));
        $this->assertTrue($isCalled);
    }

    /**
     *
     */
    public function test_run_no_controller()
    {
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage("Controller not found");

        $request = (new RequestBuilder())->build();
        $this->resolver->controller = null;

        $this->runner->run($request);
    }

    /**
     *
     */
    public function test_run_with_response()
    {
        $response = new Response();

        $request = (new RequestBuilder())->build();
        $this->resolver->controller = function () use ($response) {
            return $response;
        };

        $this->assertSame($response, $this->runner->run($request));
    }

    /**
     *
     */
    public function test_run_with_string()
    {
        $response = new Response();

        $request = (new RequestBuilder())->build();
        $this->resolver->controller = function () use ($response) {
            return "Hello World !";
        };

        $this->assertEquals(new Response(200, null, new TextBag("Hello World !")), $this->runner->run($request));
    }

    /**
     *
     */
    public function test_run_with_exception()
    {
        $listener = $this->createMock(RunErrorListenerInterface::class);
        $this->events->addListener(RunError::class, $listener);

        $exception = new \Exception("My error");
        $request = (new RequestBuilder())->build();

        $listener->expects($this->once())
            ->method("onRunError")
            ->with(new RunErrorArgument($exception, $request, new Response()))
            ->will($this->returnCallback(function (RunErrorArgument $argument) {
                $argument->setResult("handle error");
            }))
        ;

        $this->resolver->controller = function () use ($exception) {
            throw $exception;
        };

        $this->assertEquals(new Response(200, null, new TextBag("handle error")), $this->runner->run($request));
    }

    /**
     *
     */
    public function test_run_with_prepare_no_result()
    {
        $listener = $this->createMock(PrepareListenerInterface::class);
        $this->events->addListener(Prepare::class, $listener);

        $request = (new RequestBuilder())->build();

        $listener->expects($this->once())
            ->method("onPrepare")
            ->with(new PrepareArgument($request))
            ->will($this->returnCallback(function (PrepareArgument $argument) {
                $argument->request()->attach("parepared", true);
            }))
        ;

        $isCalled = false;
        $this->resolver->controller = function (Request $request) use(&$isCalled) {
            $this->assertTrue($request->attachment("parepared"));
            $isCalled = true;
        };

        $this->resolver->arguments = [$request];

        $this->runner->run($request);
        $this->assertTrue($isCalled, "controller not called");
    }

    /**
     *
     */
    public function test_run_with_prepare_result()
    {
        $listener = $this->createMock(PrepareListenerInterface::class);
        $this->events->addListener(Prepare::class, $listener);

        $request = (new RequestBuilder())->build();

        $listener->expects($this->once())
            ->method("onPrepare")
            ->with(new PrepareArgument($request))
            ->will($this->returnCallback(function (PrepareArgument $argument) {
                $argument->setResult("filtered call");
            }))
        ;

        $isCalled = false;
        $this->resolver->controller = function () use(&$isCalled) {
            $isCalled = true;
        };
        $this->resolver->arguments = [];

        $this->assertEquals("filtered call", $this->runner->run($request)->getBody()->raw());
        $this->assertFalse($isCalled, "controller should not be called");
    }

    /**
     *
     */
    public function test_run_with_routing_no_result()
    {
        $listener = $this->createMock(RoutingListenerInterface::class);
        $this->events->addListener(Routing::class, $listener);

        $request = (new RequestBuilder())
            ->path("/foo/bar/a")
            ->build();

        $listener->expects($this->once())
            ->method("onRouting")
            ->with($this->isInstanceOf(RoutingArgument::class))
            ->will($this->returnCallback(function (RoutingArgument $argument) use($request) {
                $argument->request()->attach("routing", true);

                $this->assertSame($request, $argument->request());
                $this->assertInstanceOf(Route::class, $argument->route());
                $this->assertEquals("foo", $argument->route()->controller());
                $this->assertEquals("bar", $argument->route()->action());
                $this->assertEquals(["a"], $argument->route()->arguments());
            }))
        ;

        $isCalled = false;
        $this->resolver->controller = function (Request $request) use(&$isCalled) {
            $this->assertTrue($request->attachment("routing"));
            $isCalled = true;
        };

        $this->resolver->arguments = [$request];

        $this->runner->run($request);
        $this->assertTrue($isCalled, "controller not called");
    }

    /**
     *
     */
    public function test_run_with_routing_result()
    {
        $listener = $this->createMock(RoutingListenerInterface::class);
        $this->events->addListener(Routing::class, $listener);

        $request = (new RequestBuilder())
            ->path("/foo/bar/a")
            ->build();

        $listener->expects($this->once())
            ->method("onRouting")
            ->with($this->isInstanceOf(RoutingArgument::class))
            ->will($this->returnCallback(function (RoutingArgument $argument) {
                $argument->setResult("filtered call");
            }))
        ;

        $isCalled = false;
        $this->resolver->controller = function () use(&$isCalled) {
            $isCalled = true;
        };
        $this->resolver->arguments = [];

        $this->assertEquals("filtered call", $this->runner->run($request)->getBody()->raw());
        $this->assertFalse($isCalled, "controller should not be called");
    }
}

class MyControllerResolver implements ControllerResolverInterface
{
    public $controller;
    public $arguments = [];

    public function resolveController(Route $route): ?callable
    {
        return $this->controller;
    }

    public function resolveArguments(callable $controller, Route $route, RequestInterface $request): array
    {
        return $this->arguments;
    }
}
