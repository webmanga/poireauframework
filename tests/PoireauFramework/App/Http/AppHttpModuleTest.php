<?php

namespace PoireauFramework\App\Http;


use PHPUnit\Framework\TestCase;
use PoireauFramework\Config\Config;
use PoireauFramework\Http\Cookie\Parser\JsonParser;
use PoireauFramework\Http\Cookie\Parser\MapParserResolver;
use PoireauFramework\Http\Cookie\Parser\ParserInterface;
use PoireauFramework\Http\Cookie\Parser\ParserResolverInterface;
use PoireauFramework\Http\Cookie\Parser\PlainParser;
use PoireauFramework\Http\Cookie\Parser\SignedParser;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Injector\Factory\InjectorInstantiable;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Security\Crypto\CryptoModule;
use PoireauFramework\Security\Crypto\Cypher;
use PoireauFramework\Security\Crypto\CypherSigner;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Http
 * @group ext_PoireauFramework_App_Http_AppHttpModule
 */
class AppHttpModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;

    /**
     * @var Registry
     */
    private $registry;


    /**
     *
     */
    protected function setUp()
    {
        $this->injector = new BaseInjector();

        $this->injector->instance(new Config($this->registry = new Registry()));
        $this->injector->register(new AppHttpModule());
    }

    /**
     *
     */
    public function test_get_default_parser_resolver()
    {
        /** @var MapParserResolver $resolver */
        $resolver = $this->injector->get(ParserResolverInterface::class);

        $this->assertInstanceOf(MapParserResolver::class, $resolver);

        $this->assertSame(PlainParser::instance(), $resolver->defaultParser());
    }

    /**
     *
     */
    public function test_get_map_parser_resolver_config_default()
    {
        $this->registry->sub("http")->sub("cookies")->sub("default")->set("class", MyCustomParser::class);

        /** @var MapParserResolver $resolver */
        $resolver = $this->injector->get(ParserResolverInterface::class);

        $this->assertInstanceOf(MapParserResolver::class, $resolver);
        $this->assertInstanceOf(MyCustomParser::class, $resolver->defaultParser());
        $this->assertInstanceOf(MyCustomParser::class, $resolver->parser("other"));
    }

    /**
     *
     */
    public function test_get_map_parser_resolver_config_other()
    {
        $this->registry->sub("http")->sub("cookies")->sub("other")->set("class", MyCustomParser::class);

        /** @var MapParserResolver $resolver */
        $resolver = $this->injector->get(ParserResolverInterface::class);

        $this->assertInstanceOf(MapParserResolver::class, $resolver);
        $this->assertInstanceOf(PlainParser::class, $resolver->defaultParser());
        $this->assertInstanceOf(MyCustomParser::class, $resolver->parser("other"));
    }

    /**
     *
     */
    public function test_get_map_parser_resolver_config_signed()
    {
        $this->injector->register(new CryptoModule());

        $this->registry->sub("http")->sub("cookies")->sub("default")->set("signed", true);
        $this->registry->sub("http")->sub("cookies")->sub("default")->set("key", "abcd");

        /** @var MapParserResolver $resolver */
        $resolver = $this->injector->get(ParserResolverInterface::class);

        $this->assertInstanceOf(SignedParser::class, $resolver->defaultParser());
    }

    /**
     *
     */
    public function test_get_map_parser_resolver_config_signed_json()
    {
        $this->injector->register(new CryptoModule());

        $this->registry->sub("http")->sub("cookies")->sub("default")->set("signed", true);
        $this->registry->sub("http")->sub("cookies")->sub("default")->set("key", "abcd");
        $this->registry->sub("http")->sub("cookies")->sub("default")->set("class", JsonParser::class);
        $this->registry->sub("http")->sub("cookies")->sub("default")->set("useAssoc", true);

        /** @var MapParserResolver $resolver */
        $resolver = $this->injector->get(ParserResolverInterface::class);

        $inner = new JsonParser();
        $inner->useAssoc();

        $this->assertEquals(
            new SignedParser(new CypherSigner(new Cypher(Cypher::AES256_CTR, "abcd")), $inner),
            $resolver->defaultParser()
        );
    }
}

class MyCustomParser implements ParserInterface, InjectorInstantiable {
    public function stringify($value): string
    {
        // TODO: Implement stringify() method.
    }

    public function parse(string $value)
    {
        // TODO: Implement parse() method.
    }

    static public function createInstance(\PoireauFramework\Injector\InjectorInterface $injector, array $parameters = []): InjectorInstantiable
    {
        return new static();
    }

}