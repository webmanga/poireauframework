<?php

namespace PoireauFramework\App\Security\Test;

use PoireauFramework\App\Security\UserInterface;

class SecurityUser implements UserInterface
{
    public $id;

    public $roles;

    /**
     * SecurityUser constructor.
     * @param int $id
     * @param string[] $roles
     */
    public function __construct($id, array $roles)
    {
        $this->id = $id;
        $this->roles = $roles;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function roles(): array
    {
        return $this->roles;
    }
}
