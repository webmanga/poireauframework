<?php

namespace PoireauFramework\App\Security;

require_once __DIR__."/_files/SecurityUser.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Security\Test\SecurityUser;
use PoireauFramework\Http\Exception\ForbiddenHttpException;
use PoireauFramework\Http\Exception\UnauthorizedHttpException;
use PoireauFramework\Http\Request\RequestBuilder;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Security
 * @group ext_PoireauFramework_App_Security_SecurityService
 */
class SecurityServiceTest extends TestCase
{
    /**
     * @var SecurityService
     */
    private $service;

    /**
     * @var GrantMap
     */
    private $grant;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $resolver;


    /**
     *
     */
    protected function setUp()
    {
        $this->service = new SecurityService(
            new ArrayAccessMap([
                "home" => [
                    "index" => AccessMapInterface::ANONYMOUS_ACCESS
                ],
                "account" => [
                    "*" => ["account"]
                ],
                "admin" => [
                    "*" => "admin"
                ]
            ]),
            $this->grant = new GrantMap(),
            $this->resolver = $this->createMock(UserResolverInterface::class)
        );
    }

    /**
     *
     */
    public function test_start_as_anonymous()
    {
        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->assertFalse($this->service->isLogged());
        $this->assertFalse($this->service->isMaster());
        $this->assertNull($this->service->user());

        $this->assertTrue($this->service->hasAccess($route));
        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "account", "show")));
        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "other", "other")));
    }

    /**
     *
     */
    public function test_start_as_simple_user()
    {
        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->resolver->expects($this->once())
            ->method("resolve")
            ->willReturn($user = new SecurityUser(1, []))
        ;

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->assertTrue($this->service->isLogged());
        $this->assertFalse($this->service->isMaster());
        $this->assertSame($user, $this->service->user());

        $this->assertTrue($this->service->hasAccess($route));
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));
        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "account", "show")));
    }

    /**
     *
     */
    public function test_start_as_user_with_role()
    {
        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->resolver->expects($this->once())
            ->method("resolve")
            ->willReturn($user = new SecurityUser(1, ["user"]))
        ;

        $this->grant->grant("user", ["account"]);

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->assertTrue($this->service->isLogged());
        $this->assertFalse($this->service->isMaster());
        $this->assertSame($user, $this->service->user());

        $this->assertTrue($this->service->hasAccess($route));
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "account", "show")));
        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "admin", "show")));
    }

    /**
     *
     */
    public function test_start_as_master()
    {
        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->resolver->expects($this->once())
            ->method("resolve")
            ->willReturn($user = new SecurityUser(1, ["user"]))
        ;

        $this->grant->grantMasterId(1);

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->assertTrue($this->service->isLogged());
        $this->assertTrue($this->service->isMaster());
        $this->assertSame($user, $this->service->user());

        $this->assertTrue($this->service->hasAccess($route));
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "account", "show")));
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "admin", "show")));
    }

    /**
     *
     */
    public function test_start_access_denied_anonymous()
    {
        $this->expectException(UnauthorizedHttpException::class);

        $request = (new RequestBuilder())
            ->path("/account/index")
            ->build()
        ;

        $this->service->start($request, $route = new Route(["GET"], "account", "index"));
    }

    /**
     *
     */
    public function test_start_access_denied_user()
    {
        $this->expectException(ForbiddenHttpException::class);

        $request = (new RequestBuilder())
            ->path("/admin/index")
            ->build()
        ;

        $this->resolver->expects($this->once())
            ->method("resolve")
            ->willReturn($user = new SecurityUser(1, ["user"]))
        ;

        $this->grant->grant("user", ["account"]);

        $this->service->start($request, $route = new Route(["GET"], "admin", "index"));
    }

    /**
     *
     */
    public function test_strategy_with_user()
    {
        $this->service->setDefaultStrategy(SecurityService::STRATEGY_BLOCK);
        $this->assertSame(SecurityService::STRATEGY_BLOCK, $this->service->defaultStrategy());

        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->resolver->expects($this->once())
            ->method("resolve")
            ->willReturn($user = new SecurityUser(1, []))
        ;

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "other", "other")));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_ALLOW);
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_RESTRICT);
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));
    }

    /**
     *
     */
    public function test_strategy_with_master()
    {
        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->resolver->expects($this->once())
            ->method("resolve")
            ->willReturn($user = new SecurityUser(1, []))
        ;

        $this->grant->grantMasterId(1);

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_BLOCK);
        $this->assertSame(SecurityService::STRATEGY_BLOCK, $this->service->defaultStrategy());
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_ALLOW);
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_RESTRICT);
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));
    }

    /**
     *
     */
    public function test_strategy_with_anonymous()
    {
        $request = (new RequestBuilder())
            ->path("/home/index")
            ->build()
        ;

        $this->service->start($request, $route = new Route(["GET"], "home", "index"));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_BLOCK);
        $this->assertSame(SecurityService::STRATEGY_BLOCK, $this->service->defaultStrategy());
        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "other", "other")));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_ALLOW);
        $this->assertTrue($this->service->hasAccess(new Route(["GET"], "other", "other")));

        $this->service->setDefaultStrategy(SecurityService::STRATEGY_RESTRICT);
        $this->assertFalse($this->service->hasAccess(new Route(["GET"], "other", "other")));
    }
}
