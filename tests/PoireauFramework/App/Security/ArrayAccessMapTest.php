<?php

namespace PoireauFramework\App\Security;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Router\Route;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Security
 * @group ext_PoireauFramework_App_Security_ArrayAccessMap
 */
class ArrayAccessMapTest extends TestCase
{
    /**
     *
     */
    public function test_no_access()
    {
        $map = new ArrayAccessMap([]);

        $this->assertEmpty($map->permissions(new Route(['GET'], 'home', 'index')));
    }

    /**
     *
     */
    public function test_single_permission()
    {
        $map = new ArrayAccessMap([
            "home" => [
                "index" => "my_permission"
            ]
        ]);

        $this->assertEquals(["my_permission"], $map->permissions(new Route(['GET'], 'home', 'index')));
    }

    /**
     *
     */
    public function test_array_of_permissions()
    {
        $map = new ArrayAccessMap([
            "home" => [
                "index" => ["my_permission", "other"]
            ]
        ]);

        $this->assertEquals(["my_permission", "other"], $map->permissions(new Route(['GET'], 'home', 'index')));
    }

    /**
     *
     */
    public function test_default_permission()
    {
        $map = new ArrayAccessMap([
            "*" => [
                "*" => ["default_permission"]
            ]
        ]);

        $this->assertEquals(["default_permission"], $map->permissions(new Route(['GET'], 'home', 'index')));
        $this->assertEquals(["default_permission"], $map->permissions(new Route(['GET'], 'foo', 'other')));
    }

    /**
     *
     */
    public function test_default_controller_permission()
    {
        $map = new ArrayAccessMap([
            "home" => [
                "*" => ["default_permission"]
            ]
        ]);

        $this->assertEquals(["default_permission"], $map->permissions(new Route(['GET'], 'home', 'index')));
        $this->assertEquals(["default_permission"], $map->permissions(new Route(['GET'], 'home', 'other')));
    }

    /**
     *
     */
    public function test_permission_merge()
    {

        $map = new ArrayAccessMap([
            "*" => [
                "*"     => "default",
                "index" => ["p2"]
            ],
            "home" => [
                "*"     => ["p1"],
                "index" => "final"
            ]
        ]);

        $this->assertEquals(["default", "p2", "p1", "final"], $map->permissions(new Route(['GET'], 'home', 'index')));
    }

    /**
     *
     */
    public function test_has_explicit()
    {
        $map = new ArrayAccessMap([
            "home" => ["index" => ["p1"]]
        ]);

        $this->assertTrue($map->has(new Route(["GET"], "home", "index")));
        $this->assertFalse($map->has(new Route(["GET"], "home", "other")));
    }

    /**
     *
     */
    public function test_has_default_action()
    {
        $map = new ArrayAccessMap([
            "home" => ["*" => ["p1"]]
        ]);

        $this->assertTrue($map->has(new Route(["GET"], "home", "index")));
        $this->assertTrue($map->has(new Route(["GET"], "home", "other")));
        $this->assertFalse($map->has(new Route(["GET"], "other", "index")));
    }

    /**
     *
     */
    public function test_has_default_controller()
    {
        $map = new ArrayAccessMap([
            "*" => ["index" => ["p1"]]
        ]);

        $this->assertTrue($map->has(new Route(["GET"], "home", "index")));
        $this->assertTrue($map->has(new Route(["GET"], "other", "index")));
        $this->assertFalse($map->has(new Route(["GET"], "home", "other")));
    }

    /**
     *
     */
    public function test_anonymous_on_controller()
    {
        $map = new ArrayAccessMap([
            "home" => ["*" => ArrayAccessMap::ANONYMOUS_ACCESS]
        ]);

        $this->assertTrue($map->isAnonymous(new Route(["GET"], "home", "index")));
        $this->assertTrue($map->isAnonymous(new Route(["GET"], "home", "other")));
        $this->assertFalse($map->isAnonymous(new Route(["GET"], "other", "index")));
    }

    /**
     *
     */
    public function test_anonymous_on_action()
    {
        $map = new ArrayAccessMap([
            "home" => ["index" => ArrayAccessMap::ANONYMOUS_ACCESS]
        ]);

        $this->assertTrue($map->isAnonymous(new Route(["GET"], "home", "index")));
        $this->assertFalse($map->isAnonymous(new Route(["GET"], "home", "other")));
    }

    /**
     *
     */
    public function test_anonymous_with_other_permissions()
    {
        $map = new ArrayAccessMap([
            "home" => ["index" => "p1"]
        ]);

        $this->assertFalse($map->isAnonymous(new Route(["GET"], "home", "index")));
    }

    /**
     *
     */
    public function test_anonymous_no_default_controller()
    {
        $map = new ArrayAccessMap([
            "*" => ["*" => ArrayAccessMap::ANONYMOUS_ACCESS]
        ]);

        $this->assertFalse($map->isAnonymous(new Route(["GET"], "home", "other")));
    }
}
