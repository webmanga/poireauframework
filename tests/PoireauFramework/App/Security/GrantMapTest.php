<?php

namespace PoireauFramework\App\Security;

require_once __DIR__."/_files/SecurityUser.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Security\Test\SecurityUser;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Security
 * @group ext_PoireauFramework_App_Security_GrantMap
 */
class GrantMapTest extends TestCase
{
    /**
     *
     */
    public function test_grant_role()
    {
        $map = new GrantMap();

        $this->assertSame($map, $map->grant("my_role", ["p1", "p2"]));

        $user = new SecurityUser(1, ["my_role"]);

        $this->assertTrue($map->isGranted($user, "p1"));
        $this->assertTrue($map->isGranted($user, "p2"));
        $this->assertFalse($map->isGranted($user, "other"));

        $user2 = new SecurityUser(2, ["other_role"]);

        $this->assertFalse($map->isGranted($user2, "p1"));
    }

    /**
     *
     */
    public function test_grant_id()
    {
        $map = new GrantMap();

        $this->assertSame($map, $map->grantById(1, ["p1", "p2"]));

        $user = new SecurityUser(1, ["my_role"]);

        $this->assertTrue($map->isGranted($user, "p1"));
        $this->assertTrue($map->isGranted($user, "p2"));
        $this->assertFalse($map->isGranted($user, "other"));

        $user2 = new SecurityUser(2, ["other_role"]);

        $this->assertFalse($map->isGranted($user2, "p1"));
    }

    /**
     *
     */
    public function test_grant_master_id()
    {
        $map = new GrantMap();

        $this->assertSame($map, $map->grantMasterId(1));

        $user = new SecurityUser(1, []);

        $this->assertTrue($map->isGranted($user, "p1"));
        $this->assertTrue($map->isGranted($user, "p2"));
        $this->assertTrue($map->isGranted($user, "other"));

        $user2 = new SecurityUser(2, ["other_role"]);

        $this->assertFalse($map->isGranted($user2, "p1"));

        $this->assertTrue($map->isMaster($user));
        $this->assertFalse($map->isMaster($user2));
    }

    /**
     *
     */
    public function test_grant_master_role()
    {
        $map = new GrantMap();

        $this->assertSame($map, $map->grantMasterRole("master_role"));

        $user = new SecurityUser(1, ["master_role"]);

        $this->assertTrue($map->isGranted($user, "p1"));
        $this->assertTrue($map->isGranted($user, "p2"));
        $this->assertTrue($map->isGranted($user, "other"));

        $user2 = new SecurityUser(2, ["other_role"]);

        $this->assertFalse($map->isGranted($user2, "p1"));

        $this->assertTrue($map->isMaster($user));
        $this->assertFalse($map->isMaster($user2));
    }

    /**
     *
     */
    public function test_grantAll()
    {
        $map = new GrantMap();

        $map->grant("r1", ["p1", "p2"]);
        $map->grant("r2", ["p3"]);

        $user = new SecurityUser(1,  ["r1", "r2"]);

        $this->assertTrue($map->grantAll($user, ["p1", "p2", "p3"]));

        $user2 = new SecurityUser(2, ["r1"]);

        $this->assertFalse($map->grantAll($user2, ["p1", "p2", "p3"]));
        $this->assertTrue($map->grantAll($user, ["p1", "p2"]));
    }

    /**
     *
     */
    public function test_grantOne()
    {
        $map = new GrantMap();

        $map->grant("r1", ["p1", "p2"]);
        $map->grant("r2", ["p3"]);

        $user = new SecurityUser(1,  ["r1", "r2"]);

        $this->assertTrue($map->grantOne($user, ["p2", "other"]));

        $user2 = new SecurityUser(2, []);

        $this->assertFalse($map->grantAll($user2, ["p1"]));
    }
}
