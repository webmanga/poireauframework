<?php

namespace PoireauFramework\App\Template\Listener;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Controller\ErrorController;
use PoireauFramework\App\Debug\ControllerExceptionHandler;
use PoireauFramework\App\Debug\ErrorHandlerModule;
use PoireauFramework\App\Http\RequestRunner;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Template\AppTemplateModule;
use PoireauFramework\App\Testing\Extension\ApplicationConfiguration;
use PoireauFramework\App\Testing\Extension\ResponseAssertion;
use PoireauFramework\App\Testing\TestingAppModule;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Bag\TextBag;
use PoireauFramework\Http\Exception\HttpException;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Response\Response;
use PoireauFramework\Template\TemplateService;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Listener
 * @group ext_PoireauFramework_App_Template_Listener_ViewResolverListener
 */
class ViewResolverListenerTest extends TestCase
{
    use ApplicationConfiguration;
    use ResponseAssertion;

    /**
     *
     */
    public function test_onBeforeSend_no_route()
    {
        $service = $this->createMock(TemplateService::class);
        $listener = new ViewResolverListener($service);

        $service->expects($this->never())->method('render');

        $request = (new RequestBuilder())->build();
        $response = new Response();

        $listener->onBeforeSend($response, $request);

        $this->assertNull($response->getBody());
    }

    /**
     *
     */
    public function test_onBeforeSend_not_array_response()
    {
        $service = $this->createMock(TemplateService::class);
        $listener = new ViewResolverListener($service);

        $service->expects($this->never())->method('render');

        $request = (new RequestBuilder())->build();
        $request->attach(RequestRunner::ROUTE_ATTACHMENT_KEY, new Route(['GET'], 'home', 'index'));
        $response = new Response();
        $response->body(new TextBag('Hello'));

        $listener->onBeforeSend($response, $request);

        $this->assertEquals(new TextBag('Hello'), $response->getBody());
    }

    /**
     *
     */
    public function test_onBeforeSend_will_use_route()
    {
        $service = $this->createMock(TemplateService::class);
        $listener = new ViewResolverListener($service);

        $service->expects($this->once())
            ->method('render')
            ->with('home/index', ['foo' => 'bar'])
            ->willReturn('render result')
        ;

        $request = (new RequestBuilder())->build();
        $request->attach(RequestRunner::ROUTE_ATTACHMENT_KEY, new Route(['GET'], 'home', 'index'));
        $response = new Response();
        $response->body(new ArrayBag(['foo' => 'bar']));

        $listener->onBeforeSend($response, $request);

        $this->assertEquals(new TextBag('render result'), $response->getBody());
    }

    /**
     *
     */
    public function test_functional()
    {
        $this->configureApplication(new Application([
            "config" => [
                "config"  => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ],

            "error_handler" => [
                "show_errors" => true,
                "exception_handlers" => [
                    [ControllerExceptionHandler::class, [HttpException::class, ErrorController::class]]
                ]
            ],

            "template" => [
                "paths" => [__DIR__ . "/../../_files/templates/"]
            ]
        ]), [
            new AppTemplateModule(true),
            new ErrorHandlerModule(),
            new TestingAppModule(),
        ]);

        $this->call("GET", "/showcase/auto", ["what" => "John"]);
        $this->assertResponseHtmlContent("<strong>Auto rendering John</strong>");
    }
}
