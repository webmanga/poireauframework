<?php

namespace PoireauFramework\App\Template\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Security\SecurityService;
use PoireauFramework\App\TestSecurityModule;
use PoireauFramework\App\TestUser;
use PoireauFramework\App\TestUserResolver;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Registry\Registry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Helper
 * @group ext_PoireauFramework_App_Template_Helper_Security
 */
class SecurityTest extends TestCase
{
    /**
     * @var Security
     */
    private $helper;

    /**
     * @var Application
     */
    private $application;

    /**
     *
     */
    public function setUp()
    {
        $this->application = new Application([
            "config" => [
                "config" => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ]
        ]);

        $this->application->register(new TestSecurityModule());
        $this->application->load();

        $this->helper = $this->application->injector()->get(Security::class);
    }

    /**
     *
     */
    public function test_logged_not_logged()
    {
        $this->assertFalse($this->helper->logged());
    }

    /**
     *
     */
    public function test_logged_logged()
    {
        $user = new TestUser([
            "id" => 1,
            "name" => "Paul",
            "roles" => ["user"]
        ]);

        $this->application->injector()->get(TestUserResolver::class)->user = $user;
        $this->application->injector()->get(SecurityService::class)->start((new RequestBuilder())->build(), new Route(["GET"], "test", "hello"));

        $this->assertTrue($this->helper->logged());
    }

    /**
     *
     */
    public function test_user()
    {
        $user = new TestUser([
            "id" => 1,
            "name" => "Paul",
            "roles" => ["user"]
        ]);

        $this->application->injector()->get(TestUserResolver::class)->user = $user;
        $this->application->injector()->get(SecurityService::class)->start((new RequestBuilder())->build(), new Route(["GET"], "test", "hello"));

        $this->assertSame($user, $this->helper->user());
    }
}
