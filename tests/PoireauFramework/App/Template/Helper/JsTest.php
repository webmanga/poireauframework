<?php

namespace PoireauFramework\App\Template\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Registry\Registry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Helper
 * @group ext_PoireauFramework_App_Template_Helper_Js
 */
class JsTest extends TestCase
{
    /**
     * @var Js
     */
    private $helper;

    /**
     * @var Registry
     */
    private $registry;

    /**
     *
     */
    public function setUp()
    {
        $application = new Application([
            "config" => [
                "config" => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ]
        ]);

        $application->load();

        $_SERVER['SCRIPT_NAME'] = '/document/index.php';

        $this->helper   = $application->injector()->get(Js::class);
        $this->registry = $application->registry();
    }

    /**
     *
     */
    public function test_url()
    {
        $this->assertEquals("http://webmanga.fr/document/assets/js/my_script.js", $this->helper->url("my_script.js"));
    }

    /**
     *
     */
    public function test_src___toString()
    {
        $this->assertSame($this->helper, $this->helper->src("my_script.js"));

        $this->helper->src("other.js");

        $this->assertEquals("<script lang=\"text/javascript\" src=\"http://webmanga.fr/document/assets/js/my_script.js\"></script><script lang=\"text/javascript\" src=\"http://webmanga.fr/document/assets/js/other.js\"></script>", (string) $this->helper);
    }
}
