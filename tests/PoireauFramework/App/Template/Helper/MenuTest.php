<?php

namespace PoireauFramework\App\Template\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Security\SecurityService;
use PoireauFramework\App\TestSecurityModule;
use PoireauFramework\App\TestUser;
use PoireauFramework\App\TestUserResolver;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Registry\Registry;
use PoireauFramework\Template\Engine\ViewEngineInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Helper
 * @group ext_PoireauFramework_App_Template_Helper_Menu
 */
class MenuTest extends TestCase
{
    /**
     * @var Menu
     */
    private $helper;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Application
     */
    private $application;

    /**
     *
     */
    public function setUp()
    {
        $this->application = new Application([
            "config" => [
                "config" => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ]
        ]);

        $this->application->load();

        $_SERVER['SCRIPT_NAME'] = '/document/index.php';

        $this->helper   = $this->application->injector()->get(Menu::class);
        $this->registry = $this->application->registry();
    }

    /**
     *
     */
    public function test_merge()
    {
        $this->assertSame($this->helper, $this->helper->merge("global", [
            "Foo" => "/bar",
            "Other" => "/other",
            "Extern" => "http://google.com",
            "Intern" => "#my_id"
        ]));

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
            <li><a href="http://webmanga.fr/document/other">Other</a></li>
            <li><a href="http://google.com">Extern</a></li>
            <li><a href="#my_id">Intern</a></li>
        </ul>
HTML
, $this->helper->render("global"));
    }

    /**
     *
     */
    public function test_merge_prepend()
    {
        $this->helper
            ->merge("global", [
                "Foo" => "/bar",
            ])
            ->merge("global", [
                "Other" => "/other"
            ], true)
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/other">Other</a></li>
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
        </ul>
HTML
, $this->helper->render("global"));
    }

    /**
     *
     */
    public function test_merge_will_set_current()
    {
        $this->helper
            ->merge("global", [
                "Foo" => "/bar",
            ])
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
        </ul>
HTML
, $this->helper->render());
    }

    /**
     *
     */
    public function test_run_will_set_current()
    {
        $this->helper
            ->run($this->createMock(ViewEngineInterface::class), ["global"])
            ->merge([
                "Foo" => "/bar",
            ])
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
        </ul>
HTML
, $this->helper->render());
    }

    /**
     *
     */
    public function test_run_will_merge()
    {
        $this->helper
            ->run($this->createMock(ViewEngineInterface::class), ["global", [
                "Foo" => "/bar"
            ]])
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
        </ul>
HTML
, $this->helper->render());
    }

    /**
     *
     */
    public function test_menuClass_explicit_name()
    {
        $this->helper
            ->merge("global", [
                "Foo" => "/bar",
            ])
            ->menuClass("global", "my-custom-class")
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="my-custom-class">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_menuClass_current_name()
    {
        $this->helper
            ->merge("global", [
                "Foo" => "/bar",
            ])
            ->menuClass("my-custom-class")
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="my-custom-class">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_dropdown()
    {
        $this->helper
            ->merge("global", [
                "Foo" => [
                    "menu" => [
                        "bar" => "#bar"
                    ]
                ],
            ])
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Foo</a>
                <ul class="dropdown-menu">
                    <li><a href="#bar">bar</a></li>
                </ul>
            </li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_url_with_route()
    {
        $this->helper
            ->merge("global", [
                "Foo" => [
                    "url" => [
                        "route" => ["news", "index"]
                    ]
                ],
            ])
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/news/index">Foo</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_url_with_route_and_get()
    {
        $this->helper
            ->merge("global", [
                "Foo" => [
                    "url" => [
                        "route" => ["news", "index"],
                        "get"   => ["sort" => "date"]
                    ]
                ],
            ])
        ;

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/news/index?sort=date">Foo</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_separator()
    {
        $this->assertSame($this->helper, $this->helper->merge("global", [
            "Foo" => "/bar",
            "separator",
            "Other" => "/other",
        ]));

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/bar">Foo</a></li>
            <li class="divider"></li>
            <li><a href="http://webmanga.fr/document/other">Other</a></li>
        </ul>
HTML
            , $this->helper->render("global"));
    }

    /**
     *
     */
    public function test_filter_login_no_access()
    {
        $this->application->register(new TestSecurityModule());

        $user = new TestUser([
            "id" => 1,
            "name" => "Paul",
            "roles" => ["user"]
        ]);

        $this->application->injector()->get(TestUserResolver::class)->user = $user;
        $this->application->injector()->get(SecurityService::class)->start((new RequestBuilder())->build(), new Route(["GET"], "test", "hello"));

        $helper = Menu::createInstance($this->application->injector());

        $helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ]
            ],
            "Private" => [
                "url" => [
                    "route" => ["test", "private"]
                ]
            ],
            "Hello" => [
                "url" => [
                    "route" => ["test", "hello", "john"]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/foo/index">Foo</a></li>
            <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
        </ul>
HTML
            , $helper->render());
    }

    /**
     *
     */
    public function test_filter_login_private()
    {
        $this->application->register(new TestSecurityModule());

        $user = new TestUser([
            "id" => 1,
            "name" => "Paul",
            "roles" => ["private"]
        ]);

        $this->application->injector()->get(TestUserResolver::class)->user = $user;
        $this->application->injector()->get(SecurityService::class)->start((new RequestBuilder())->build(), new Route(["GET"], "test", "hello"));

        $helper = Menu::createInstance($this->application->injector());

        $helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ]
            ],
            "Private" => [
                "url" => [
                    "route" => ["test", "private"]
                ]
            ],
            "Hello" => [
                "url" => [
                    "route" => ["test", "hello", "john"]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/foo/index">Foo</a></li>
            <li><a href="http://webmanga.fr/document/test/private">Private</a></li>
            <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
        </ul>
HTML
            , $helper->render());
    }

    /**
     *
     */
    public function test_filter_anonymous()
    {
        $this->application->register(new TestSecurityModule());

        $this->application->injector()->get(SecurityService::class)->start((new RequestBuilder())->build(), new Route(["GET"], "test", "hello"));

        $helper = Menu::createInstance($this->application->injector());

        $helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ]
            ],
            "Private" => [
                "url" => [
                    "route" => ["test", "private"]
                ]
            ],
            "Hello" => [
                "url" => [
                    "route" => ["test", "hello", "john"]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
        </ul>
HTML
            , $helper->render());
    }

    /**
     *
     */
    public function test_active_explicit()
    {
        $this->helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ],
                "active" => true
            ],
            "Private" => [
                "url" => [
                    "route" => ["test", "private"]
                ]
            ],
            "Hello" => [
                "url" => [
                    "route" => ["test", "hello", "john"]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li class="active"><a href="http://webmanga.fr/document/foo/index">Foo</a></li>
            <li><a href="http://webmanga.fr/document/test/private">Private</a></li>
            <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_active_current_request_simple()
    {
        $this->application->request()->attach("__route", new Route(["GET"], "foo", "bar"));

        $this->helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ]
            ],
            "Private" => [
                "url" => [
                    "route" => ["test", "private"]
                ]
            ],
            "Hello" => [
                "url" => [
                    "route" => ["test", "hello", "john"]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li class="active"><a href="http://webmanga.fr/document/foo/index">Foo</a></li>
            <li><a href="http://webmanga.fr/document/test/private">Private</a></li>
            <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_active_current_request_controller_action()
    {
        $this->application->request()->attach("__route", new Route(["GET"], "test", "private"));

        $this->helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ]
            ],
            "Private" => [
                "url" => [
                    "route" => ["test", "private"]
                ]
            ],
            "Hello" => [
                "url" => [
                    "route" => ["test", "hello", "john"]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/foo/index">Foo</a></li>
            <li class="active"><a href="http://webmanga.fr/document/test/private">Private</a></li>
            <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
        </ul>
HTML
            , $this->helper->render());
    }

    /**
     *
     */
    public function test_active_on_dropdown()
    {
        $this->application->request()->attach("__route", new Route(["GET"], "test", "private"));

        $this->helper->merge("global", [
            "Foo" => [
                "url" => [
                    "route" => ["foo"]
                ]
            ],
            "test" => [
                "menu" => [
                    "Private" => [
                        "url" => [
                            "route" => ["test", "private"]
                        ]
                    ],
                    "Hello" => [
                        "url" => [
                            "route" => ["test", "hello", "john"]
                        ]
                    ]
                ]
            ]
        ]);

        $this->assertXmlStringEqualsXmlString(<<<HTML
        <ul class="nav">
            <li><a href="http://webmanga.fr/document/foo/index">Foo</a></li>
            <li class="dropdown active">
                <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">test</a>
                <ul class="dropdown-menu">
                    <li class="active"><a href="http://webmanga.fr/document/test/private">Private</a></li>
                    <li><a href="http://webmanga.fr/document/test/hello/john">Hello</a></li>
                </ul>
            </li>
        </ul>
HTML
            , $this->helper->render());
    }
}
