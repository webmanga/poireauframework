<?php

namespace PoireauFramework\App\Template\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\AppModule;
use PoireauFramework\App\Asset\Scss as ScssHandler;
use PoireauFramework\App\Asset\ScssModule;
use PoireauFramework\App\Kernel\Application;

/**
 * @todo Test fonctionnel ?
 *
 * @group PoireauFramework
 * @group PoireauFramework_App
 * @group PoireauFramework_App_Template
 * @group PoireauFramework_App_Template_Helper
 * @group PoireauFramework_App_Template_Helper_Scss
 */
class ScssTest extends TestCase
{
    /**
     *
     */
    public function test_instance()
    {
        $application = new Application([]);

        $application->register(new ScssModule());
        $application->register(new AppModule());

        $scss = $application->injector()->get(Scss::class);
        $this->assertInstanceOf(Scss::class, $scss);
        $this->assertInstanceOf(Css::class, $scss->css());

        $this->assertSame($application->injector()->get(Css::class), $scss->css());
    }

    /**
     *
     */
    public function test_add_unit()
    {
        $css = $this->createMock(Css::class);
        $handler = $this->createMock(ScssHandler::class);

        $helper = new Scss($css, $handler);

        $css->expects($this->once())
            ->method("add")
            ->with("style.scss.css")
        ;

        $handler->expects($this->once())
            ->method("compile")
            ->with("style.scss")
        ;

        $this->assertSame($helper, $helper->add("style.scss"));
    }
}
