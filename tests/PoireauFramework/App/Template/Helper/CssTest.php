<?php

namespace PoireauFramework\App\Template\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Registry\Registry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Helper
 * @group ext_PoireauFramework_App_Template_Helper_Css
 */
class CssTest extends TestCase
{
    /**
     * @var Css
     */
    private $helper;

    /**
     * @var Registry
     */
    private $registry;

    /**
     *
     */
    public function setUp()
    {
        $application = new Application([
            "config" => [
                "config" => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ]
        ]);

        $application->load();

        $_SERVER['SCRIPT_NAME'] = '/document/index.php';

        $this->helper   = $application->injector()->get(Css::class);
        $this->registry = $application->registry();
    }

    /**
     *
     */
    public function test_url()
    {
        $this->assertEquals("http://webmanga.fr/document/assets/css/my_style.css", $this->helper->url("my_style.css"));
    }

    /**
     *
     */
    public function test_src___toString()
    {
        $this->assertSame($this->helper, $this->helper->add("my_style.css"));

        $this->helper->add("other.css");

        $this->assertEquals("<link rel=\"stylesheet\" lang=\"text/css\" href=\"http://webmanga.fr/document/assets/css/my_style.css\"/><link rel=\"stylesheet\" lang=\"text/css\" href=\"http://webmanga.fr/document/assets/css/other.css\"/>", (string) $this->helper);
    }
}
