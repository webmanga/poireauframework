<?php

namespace PoireauFramework\App\Template\Helper;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Registry\Registry;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Helper
 * @group ext_PoireauFramework_App_Template_Helper_Url
 */
class UrlTest extends TestCase
{
    /**
     * @var Url
     */
    private $helper;

    /**
     * @var Registry
     */
    private $registry;

    /**
     *
     */
    public function setUp()
    {
        $application = new Application([
            "config" => [
                "config" => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ]
        ]);

        $application->load();

        $_SERVER['SCRIPT_NAME'] = '/document/index.php';

        $this->helper   = $application->injector()->get(Url::class);
        $this->registry = $application->registry();
    }

    /**
     *
     */
    public function test_page()
    {
        $this->assertEquals("http://webmanga.fr/document/my/page?foo=bar", $this->helper->page("/my/page", ["foo" => "bar"]));

        $this->changeConfig("rewriting", false);

        $this->assertEquals("http://webmanga.fr/document/index.php/my/page?foo=bar", $this->helper->page("/my/page", ["foo" => "bar"]));
    }

    protected function changeConfig($key, $value)
    {
        $this->registry->sub("config")->sub("url")->set($key, $value);
    }
}
