<?php

namespace PoireauFramework\App\Template;


use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Kernel\Event\BeforeSend;
use PoireauFramework\App\Template\Listener\ViewResolverListener;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_AppTemplateModule
 */
class AppTemplateModuleTest extends TestCase
{
    /**
     *
     */
    public function test_instances()
    {
        $app = new Application([]);
        $app->register(new AppTemplateModule());

        $this->assertInstanceOf(ViewResolverListener::class, $app->injector()->get(ViewResolverListener::class));
    }

    /**
     *
     */
    public function test_boot_useResolver()
    {
        $app = new Application([]);
        $app->register(new AppTemplateModule(true));
        $app->load();

        $this->assertAttributeEquals(
            [BeforeSend::class => [$app->injector()->get(ViewResolverListener::class)]],
            'listeners',
            $app->events()
        );
    }
}
