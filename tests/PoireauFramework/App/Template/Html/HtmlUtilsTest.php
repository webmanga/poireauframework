<?php

namespace PoireauFramework\App\Template\Html;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Html
 * @group ext_PoireauFramework_App_Template_Html_HtmlElement
 */
class HtmlUtilsTest extends TestCase
{

    protected function setUp()
    {
        HtmlUtils::setIdCount(0);
    }

    /**
     *
     */
    public function test_renderAttributes_empty()
    {
        $this->assertEquals("", HtmlUtils::renderAttributes(new HtmlElement()));
    }

    /**
     *
     */
    public function test_renderAttributes_simple()
    {
        $e = new HtmlElement();

        $e
            ->addClass("btn")
            ->addClass("btn-primary")
            ->attribute("value", "Envoyer")
            ->attribute("type", "submit")
            ->property("disabled")
            ->id("my_sbt_btn")
        ;

        $this->assertEquals('id="my_sbt_btn" class="btn btn-primary" value="Envoyer" type="submit" disabled ', HtmlUtils::renderAttributes($e));
    }

    /**
     *
     */
    public function test_renderAttributes_no_xss()
    {
        $e = new HtmlElement();

        $e
            ->addClass("\"")
            ->attribute("<script>", "&")
            ->property("onclick='alert(\\'Fail !\\');'")
            ->id("<strong></strong>")
        ;

        $this->assertEquals('id="&lt;strong&gt;&lt;/strong&gt;" class="&quot;" script="&amp;" onclickalertFail ', HtmlUtils::renderAttributes($e));
    }

    /**
     *
     */
    public function test_cleanAttributeName()
    {
        $this->assertEquals("valid-attributeName.:0123456", HtmlUtils::cleanAttributeName("valid-attributeName.:0123456"));
        $this->assertEquals("onclickalertonmouseover...", HtmlUtils::cleanAttributeName("onclick='alert();' onmouseover='...;'"));
    }

    /**
     *
     */
    public function test_generateId()
    {
        $e = new HtmlElement();

        $this->assertEquals("__id_htmlelement_0__", HtmlUtils::generateId($e));
        $this->assertEquals("__id_htmlelement_0__", $e->getId());

        $this->assertEquals("__id_htmlelement_1__", HtmlUtils::generateId($e));
    }
}
