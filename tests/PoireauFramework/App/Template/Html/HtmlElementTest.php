<?php

namespace PoireauFramework\App\Template\Html;

use PHPUnit\Framework\TestCase;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Template
 * @group ext_PoireauFramework_App_Template_Html
 * @group ext_PoireauFramework_App_Template_Html_HtmlElement
 */
class HtmlElementTest extends TestCase
{
    /**
     *
     */
    public function test_get_set_attribute()
    {
        $e = new HtmlElement();

        $this->assertSame($e, $e->attribute("foo", "bar"));
        $this->assertEquals(["foo" => "bar"], $e->getAttributes());
    }

    /**
     *
     */
    public function test_get_set_class()
    {
        $e = new HtmlElement();

        $this->assertSame($e, $e->addClass("foo"));
        $this->assertEquals(["foo"], $e->getClasses());
    }

    /**
     *
     */
    public function test_get_set_property()
    {
        $e = new HtmlElement();

        $this->assertSame($e, $e->property("foo")->property("bar", false));
        $this->assertEquals(["foo"], $e->getProperties());
    }

    /**
     *
     */
    public function test_get_set_id()
    {
        $e = new HtmlElement();

        $this->assertSame($e, $e->id("foo"));
        $this->assertEquals("foo", $e->getId());
    }
}
