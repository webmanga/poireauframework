<?php

return [
    "controller_resolver" => [
        "controller_prefix" => "PoireauFramework\\App\\Controller\\",
        "controller_suffix" => "Controller",
        "method_suffix"     => "Action"
    ]
];