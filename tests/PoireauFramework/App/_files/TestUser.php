<?php


namespace PoireauFramework\App;


use PoireauFramework\App\Security\UserInterface;

class TestUser implements UserInterface
{
    public $id;
    public $name;
    public $roles = [];

    public function __construct(array $data = [])
    {
        foreach ($data as $k=>$v) {
            $this->$k = $v;
        }
    }

    /**
     * @inheritDoc
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function roles(): array
    {
        return $this->roles;
    }

}