<?php


namespace PoireauFramework\App;


use PoireauFramework\App\Security\AccessMapInterface;
use PoireauFramework\App\Security\ArrayAccessMap;
use PoireauFramework\App\Security\GrantMap;
use PoireauFramework\App\Security\GrantMapInterface;
use PoireauFramework\App\Security\SecurityService;
use PoireauFramework\App\Security\UserResolverInterface;
use PoireauFramework\Injector\InjectorConfigurable;
use PoireauFramework\Injector\InjectorInterface;
use PoireauFramework\Injector\InjectorModuleInterface;

/**
 * Class TestSecurityModule
 */
class TestSecurityModule implements InjectorModuleInterface
{
    /**
     * @inheritDoc
     */
    public function configure(InjectorConfigurable $injector): void
    {
        $injector->impl(UserResolverInterface::class, TestUserResolver::class);

        $injector->factory(SecurityService::class, [$this, "createSecurityService"]);
        $injector->factory(AccessMapInterface::class, [$this, "createAccessMap"]);
        $injector->factory(GrantMapInterface::class, [$this, "createGrantMap"]);
        $injector->factory(TestUserResolver::class, [$this, "createUserResolver"]);
    }


    /**
     * @param InjectorInterface $injector
     *
     * @return SecurityService
     */
    public function createSecurityService(InjectorInterface $injector)
    {
        return new SecurityService(
            $injector->get(AccessMapInterface::class),
            $injector->get(GrantMapInterface::class),
            $injector->get(UserResolverInterface::class)
        );
    }

    /**
     * @return ArrayAccessMap
     */
    public function createAccessMap()
    {
        return new ArrayAccessMap([
            "test" => [
                "hello" => AccessMapInterface::ANONYMOUS_ACCESS,
                "private" => ["ACCESS.private"]
            ],
        ]);
    }

    /**
     * @return GrantMap
     */
    public function createGrantMap()
    {
        $map = new GrantMap();

        $map->grant("private", ["ACCESS.private"]);

        return $map;
    }

    /**
     * @param InjectorInterface $injector
     *
     * @return TestUserResolver
     */
    public function createUserResolver()
    {
        return new TestUserResolver();
    }
}
