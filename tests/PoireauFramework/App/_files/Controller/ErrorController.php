<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\Http\Exception\NotFoundHttpException;

/**
 * Class ErrorController
 * @package PoireauFramework\App\Controller
 */
class ErrorController extends Controller
{
    /**
     * @param NotFoundHttpException $exception
     * @return string
     */
    public function notFoundHttpException(NotFoundHttpException $exception)
    {
        $this->response->code(404);

        return "Page introuvable";
    }
}