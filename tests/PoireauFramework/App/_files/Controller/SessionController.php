<?php

namespace PoireauFramework\App\Controller;

use PoireauFramework\Session\SessionService;

/**
 * Class SessionController
 */
class SessionController extends Controller
{
    /**
     * @var SessionService
     */
    private $session;

    /**
     * {@inheritdoc}
     */
    public function __construct($application)
    {
        parent::__construct($application);

        $this->session = $application->injector()->get(SessionService::class);
    }

    /**
     *
     */
    public function startAction()
    {
        $this->session->start();

        return [
            "sid"  => $this->session->session()->sid(),
            "data" => $this->session->session()->data()
        ];
    }

    /**
     *
     */
    public function setAction($key, $value)
    {
        $this->session->start();

        $this->session->session()[$key] = $value;

        return [
            "sid"  => $this->session->session()->sid(),
            "data" => $this->session->session()->data()
        ];
    }

    /**
     *
     */
    public function destroyAction()
    {
        $this->session->start();
        $this->session->destroy();
    }
}
