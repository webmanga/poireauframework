<?php

namespace PoireauFramework\App\Controller;

/**
 * Class ShowcaseController
 * @package PoireauFramework\App\Controller
 */
class ShowcaseController extends FrontEndController
{
    public function helloAction($name = "World")
    {
        return $this->render("hello", [
            "name" => $name
        ]);
    }

    public function autoAction()
    {
        return [
            "value" => $this->request->query()->get("what")
        ];
    }
}
