<!DOCTYPE html>
<html>
    <head>
        <title>PoireauFramework - <?= htmlentities($title); ?></title>
        <meta charset="UTF-8" />
    </head>
    <body>
        <?= $this->view()->getContent(); ?>
    </body>
</html>