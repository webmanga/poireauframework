<?php


namespace PoireauFramework\App;


use PoireauFramework\App\Security\UserInterface;
use PoireauFramework\App\Security\UserResolverInterface;
use PoireauFramework\Http\Request\RequestInterface;

class TestUserResolver implements UserResolverInterface
{
    /**
     * @var UserInterface
     */
    public $user;


    /**
     * @inheritDoc
     */
    public function resolve(RequestInterface $request): ?UserInterface
    {
        return $this->user;
    }
}
