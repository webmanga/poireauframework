<?php

namespace PoireauFramework\App\Session;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Http\AppHttpModule;
use PoireauFramework\Config\Config;
use PoireauFramework\Http\Cookie\CookieService;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Session\Behavior\CookieSessionIdBehavior;
use PoireauFramework\Session\Behavior\SessionBehaviorInterface;
use PoireauFramework\Session\Storage\FileSessionStorage;
use PoireauFramework\Session\Storage\SessionStorageInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Session
 * @group ext_PoireauFramework_App_Session_AppSessionModule
 */
class AppSessionModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;


    protected function setUp()
    {
        $this->injector = new BaseInjector();

        $this->injector->instance(new Config($this->injector->reg("config")));
        $this->injector->register(new AppHttpModule());
        $this->injector->register(new AppSessionModule());
    }

    /**
     *
     */
    public function test_storage_defaults()
    {
        $this->assertInstanceOf(FileSessionStorage::class, $this->injector->get(SessionStorageInterface::class));

        $this->assertEquals(new FileSessionStorage(sys_get_temp_dir()), $this->injector->get(SessionStorageInterface::class));
    }

    /**
     *
     */
    public function test_storage_configure_path()
    {
        $this->injector->reg("session")->sub("storage")->set("path", "/my/customer/path");

        $this->assertInstanceOf(FileSessionStorage::class, $this->injector->get(SessionStorageInterface::class));

        $this->assertEquals(new FileSessionStorage("/my/customer/path"), $this->injector->get(SessionStorageInterface::class));
    }

    /**
     *
     */
    public function test_behavior()
    {
        $this->assertInstanceOf(CookieSessionIdBehavior::class, $this->injector->get(SessionBehaviorInterface::class));
        $this->assertEquals(new CookieSessionIdBehavior(
            $this->injector->get(CookieService::class),
            "sid"
        ), $this->injector->get(SessionBehaviorInterface::class));
    }

    /**
     *
     */
    public function test_behavior_custom_cookie()
    {
        $this->injector->reg("session")->set("cookie_name", "my_cookie");

        $this->assertEquals(new CookieSessionIdBehavior(
            $this->injector->get(CookieService::class),
            "my_cookie"
        ), $this->injector->get(SessionBehaviorInterface::class));
    }
}
