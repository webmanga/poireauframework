<?php

namespace PoireauFramework\App\Session;

use PoireauFramework\App\Debug\ErrorHandlerModule;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Testing\AppTestCase;
use PoireauFramework\App\Testing\TestingAppModule;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Http\Cookie\CookieBuilder;
use PoireauFramework\Http\Cookie\Parser\SignedParser;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Security\Crypto\CryptoModule;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Session
 * @group ext_PoireauFramework_App_Session_Functional
 */
class FunctionalTest extends AppTestCase
{
    protected function createApplication(array $defaults = [])
    {
        return parent::createApplication([
            "config" => [
                "config"  => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../_files/functional_config.ini"]
                ]
            ],
        ]);
    }

    protected function configureApplication(Application $application = null, array $modules = [])
    {
        parent::configureApplication($application, array_merge($modules, [
            new ErrorHandlerModule(),
            new TestingAppModule(),
            new AppSessionModule(),
            new CryptoModule()
        ]));
    }

    /**
     *
     */
    public function test_start()
    {
        $response = $this->call("GET", "/session/start");

        /** @var CookieBuilder $cookie */
        $cookie = $response->headers()->getCookies()[0];

        $this->assertInstanceOf(CookieBuilder::class, $cookie);
        $this->assertEquals($response->getBody()->get("sid"), $cookie->getValue());
        $this->assertEmpty($response->getBody()->get("data"));

        $this->assertNotContains($response->getBody()->get("sid"), (string) $cookie);
    }

    /**
     *
     */
    public function test_set()
    {
        $response = $this->call("GET", "/session/start");

        /** @var CookieBuilder $cookie */
        $cookie = $response->headers()->getCookies()[0];

        preg_match("#^sid=(.*); HttpOnly#", (string) $cookie, $matches);

        $cypherSid = $matches[1];

        $builder = new RequestBuilder();

        $builder
            ->path("/session/set/foo/bar")
            ->cookies()->set("sid", $cypherSid)
        ;

        $response = $this->execute($builder->build());

        $this->assertEmpty($response->headers()->getCookies());
        $this->assertEquals([
            "sid" => $cookie->getValue(),
            "data" => ["foo" => "bar"]
        ], $response->getBody()->raw());


        $builder->path("/session/start");
        $response = $this->execute($builder->build());

        $this->assertEmpty($response->headers()->getCookies());
        $this->assertEquals([
            "sid" => $cookie->getValue(),
            "data" => ["foo" => "bar"]
        ], $response->getBody()->raw());
    }

    /**
     *
     */
    public function test_destroy()
    {
        $response = $this->call("GET", "/session/set/foo/bar");

        /** @var CookieBuilder $cookie */
        $cookie = $response->headers()->getCookies()[0];

        preg_match("#^sid=(.*); HttpOnly#", (string) $cookie, $matches);

        $cypherSid = $matches[1];

        $builder = new RequestBuilder();

        $builder
            ->path("/session/destroy")
            ->cookies()->set("sid", $cypherSid)
        ;

        $this->execute($builder->build());

        $builder->path("/session/start");
        $response = $this->execute($builder->build());

        $this->assertEmpty($response->headers()->getCookies());
        $this->assertEquals([
            "sid" => $cookie->getValue(),
            "data" => []
        ], $response->getBody()->raw());
    }
}
