<?php

namespace PoireauFramework\App\Form;

require_once __DIR__."/_files/TestRegistrationFrontForm.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Exception\FormHttpException;
use PoireauFramework\App\Form\Exception\FrontFormHttpException;
use PoireauFramework\App\Form\Template\FieldView;
use PoireauFramework\App\Form\Template\FormView;
use PoireauFramework\App\Form\Test\TestRegistrationFrontForm;
use PoireauFramework\App\Form\Test\User;
use PoireauFramework\Http\Bag\ArrayBag;
use PoireauFramework\Http\Exception\BadRequestHttpException;
use PoireauFramework\Http\Exception\MethodNotAllowedHttpException;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Form;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Http\Request\RequestBuilder;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_FrontForm
 */
class FrontFormTest extends TestCase
{
    /**
     * @var TestRegistrationFrontForm
     */
    private $form;


    /**
     *
     */
    protected function setUp()
    {
        $this->form = new TestRegistrationFrontForm();
    }

    /**
     *
     */
    public function test_create()
    {
        $form = $this->form->create();

        $this->assertInstanceOf(Form::class, $form);

        $this->assertInstanceOf(Field::class, $form->get("username"));
        $this->assertInstanceOf(Field::class, $form->get("password"));
        $this->assertInstanceOf(Field::class, $form->get("confirm"));
        $this->assertInstanceOf(Field::class, $form->get("mail"));
    }

    /**
     *
     */
    public function test_view()
    {
        $view = $this->form->view();

        $this->assertInstanceOf(FormView::class, $view);

        $this->assertInstanceOf(FieldView::class, $view["username"]);
        $this->assertInstanceOf(FieldView::class, $view["password"]);
        $this->assertInstanceOf(FieldView::class, $view["confirm"]);
        $this->assertInstanceOf(FieldView::class, $view["mail"]);

        $this->assertEquals("https://webmanga.fr/account/register", $view->getAction());
        $this->assertEquals("POST", $view->getMethod());
    }

    /**
     *
     */
    public function test_form()
    {
        $this->assertEquals($this->form->create(), $this->form->form());
        $this->assertSame($this->form->form(), $this->form->form());
    }

    /**
     *
     */
    public function test_validate_bad_method()
    {
        $this->expectException(MethodNotAllowedHttpException::class);

        $request = (new RequestBuilder())->method(Request::METHOD_GET)->build();

        $this->form->validate($request);
    }

    /**
     *
     */
    public function test_validate_no_body()
    {
        $this->expectException(BadRequestHttpException::class);
        $this->expectExceptionMessage("No data found");

        $request = (new RequestBuilder())->method(Request::METHOD_POST)->build();

        $this->form->validate($request);
    }

    /**
     *
     */
    public function test_validate_invalid_data()
    {
        $this->expectException(FrontFormHttpException::class);

        $request = (new RequestBuilder())->method(Request::METHOD_POST)->body(new ArrayBag([
            "username" => "John"
        ]))->build();

        $this->form->validate($request);
    }

    /**
     *
     */
    public function test_validate_success()
    {
        $request = (new RequestBuilder())->method(Request::METHOD_POST)->body(new ArrayBag([
            "username" => "JohnDoe",
            "password" => "MyVeryStrongPassword",
            "confirm"  => "MyVeryStrongPassword",
            "mail"     => "john.doe@webmanga.fr",
        ]))->build();

        $this->assertSame($this->form, $this->form->validate($request));

        $this->assertEquals("JohnDoe", $this->form->form()->get("username")->value());
        $this->assertEquals("MyVeryStrongPassword", $this->form->form()->get("password")->value());
        $this->assertEquals("MyVeryStrongPassword", $this->form->form()->get("confirm")->value());
        $this->assertEquals("john.doe@webmanga.fr", $this->form->form()->get("mail")->value());
    }

    /**
     *
     */
    public function test___get()
    {
        $request = (new RequestBuilder())->method(Request::METHOD_POST)->body(new ArrayBag([
            "username" => "JohnDoe",
            "password" => "MyVeryStrongPassword",
            "confirm"  => "MyVeryStrongPassword",
            "mail"     => "john.doe@webmanga.fr",
        ]))->build();

        $this->form->validate($request);

        $this->assertEquals("JohnDoe", $this->form->username);
        $this->assertEquals("MyVeryStrongPassword", $this->form->password);
        $this->assertEquals("MyVeryStrongPassword", $this->form->confirm);
        $this->assertEquals("john.doe@webmanga.fr", $this->form->mail);
    }

    /**
     *
     */
    public function test_export()
    {
        $request = (new RequestBuilder())->method(Request::METHOD_POST)->body(new ArrayBag([
            "username" => "JohnDoe",
            "password" => "MyVeryStrongPassword",
            "confirm"  => "MyVeryStrongPassword",
            "mail"     => "john.doe@webmanga.fr",
        ]))->build();

        $this->form->validate($request);

        $export = $this->form->export();

        $this->assertInstanceOf(User::class, $export);
        $this->assertEquals("JohnDoe", $export->username);
        $this->assertEquals("MyVeryStrongPassword", $export->password);
        $this->assertEquals("john.doe@webmanga.fr", $export->mail);
    }

    /**
     *
     */
    public function test_labels()
    {
        $view = $this->form->view();

        $this->assertEquals("Nom d'utilisateur", $view["username"]->getParameter("label-value"));
        $this->assertEquals("Mot de passe", $view["password"]->getParameter("label-value"));
        $this->assertEquals("Confirmation du mot de passe", $view["confirm"]->getParameter("label-value"));
        $this->assertEquals("Adresse e-mail", $view["mail"]->getParameter("label-value"));
    }
}
