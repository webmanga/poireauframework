<?php

namespace PoireauFramework\App\Form\Test;

require_once __DIR__."/TestRegistrationForm.php";

use PoireauFramework\App\Form\FrontForm;
use PoireauFramework\Http\Form\Builder\FormBuilder;
use PoireauFramework\Http\Request\Request;

/**
 * Class TestRegistrationFrontForm
 */
class TestRegistrationFrontForm extends FrontForm
{
    /**
     * @var TestRegistrationForm
     */
    private $inner;

    function __construct()
    {
        $this->inner = new TestRegistrationForm();
    }

    public function url(): string
    {
        return "https://webmanga.fr/account/register";
    }

    /**
     * @inheritDoc
     */
    public function viewUrl(): string
    {
        return "https://webmanga.fr/account/register";
    }

    public function method(): string
    {
        return Request::METHOD_POST;
    }

    public function configure(FormBuilder $builder): void
    {
        $this->inner->configure($builder);
    }

    protected function labels(): array
    {
        return [
            "username" => "Nom d'utilisateur",
            "password" => "Mot de passe",
            "confirm"  => "Confirmation du mot de passe",
            "mail"     => "Adresse e-mail"
        ];
    }
}
