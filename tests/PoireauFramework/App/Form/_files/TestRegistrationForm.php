<?php

namespace PoireauFramework\App\Form\Test;

require_once __DIR__."/User.php";

use PoireauFramework\App\Form\FormFactory;
use PoireauFramework\Http\Form\Builder\FormBuilder;
use PoireauFramework\Http\Form\Validation\Email;
use PoireauFramework\Http\Form\Validation\Length;
use PoireauFramework\Http\Form\Validation\Regex;
use PoireauFramework\Http\Form\Validation\SameAs;


class TestRegistrationForm extends FormFactory
{

    public function configure(FormBuilder $builder): void
    {
        $builder->attach(User::class);

        $builder->field("username")
            ->required()
            ->rule(new Length(4, 12))
            ->rule(new Regex("#^\\w+$#"))
            ->getSet()
        ;

        $builder->field("password")
            ->required()
            ->rule(new Length(6))
            ->getSet()
        ;

        $builder->field("confirm")
            ->required()
            ->rule(new SameAs("password"))
        ;

        $builder->field("mail")
            ->required()
            ->rule(new Email())
            ->getSet()
        ;
    }
}
