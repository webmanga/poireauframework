<?php


namespace PoireauFramework\App\Form\Template;

use PHPUnit\Framework\TestCase;
use PoireauFramework\Http\Form\Builder\FormBuilder;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Filter\Secret;
use PoireauFramework\Http\Form\Form;
use PoireauFramework\Http\Form\Operation\FormOperationRegistry;
use PoireauFramework\Http\Form\Validation\Email;
use PoireauFramework\Http\Form\Validation\Length;
use PoireauFramework\Http\Form\Validation\Regex;
use PoireauFramework\Http\Form\Validation\SameAs;


/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_GetView
 */
class GetViewTest extends TestCase
{
    /**
     *
     */
    public function test_default()
    {
        $getView = new GetView(new Form(new FormOperationRegistry(), []));

        $this->assertInstanceOf(FormView::class, $getView->get());
    }

    /**
     *
     */
    public function test_applyOnField()
    {
        $getView = new GetView(new Form(new FormOperationRegistry(), []));
        $field = new Field("foo");
        $field->setValue("bar");

        $getView->applyOnField($field);

        $form = $getView->get();

        $this->assertInstanceOf(FieldView::class, $form["foo"]);
        $this->assertEquals("bar", $form["foo"]->getValue());
    }

    /**
     *
     */
    public function test_parameters()
    {
        $getView = new GetView(new Form(new FormOperationRegistry(), []), null, [
            "foo" => [
                "param1" => "val1"
            ]
        ]);
        $field = new Field("foo");

        $getView->applyOnField($field);

        $form = $getView->get();

        $this->assertEquals("val1", $form["foo"]->getParameter("param1"));
    }

    /**
     *
     */
    public function test_with_secret()
    {
        $field = new Field("foo", [new Secret()]);

        $getView = new GetView(new Form(new FormOperationRegistry(), []));
        $getView->applyOnField($field);

        $this->assertEquals("password", $getView->get()["foo"]->getType());
    }

    /**
     *
     */
    public function test_with_email()
    {
        $field = new Field("foo", [], false, [new Email()]);

        $getView = new GetView(new Form(new FormOperationRegistry(), []));
        $getView->applyOnField($field);

        $this->assertEquals("email", $getView->get()["foo"]->getType());
    }

    /**
     *
     */
    public function test_with_max()
    {
        $field = new Field("foo", [], false, [new Length(null, 10)]);

        $getView = new GetView(new Form(new FormOperationRegistry(), []));
        $getView->applyOnField($field);

        $this->assertEquals(["maxlength" => 10], $getView->get()["foo"]->getAttributes());
    }

    /**
     * @todo gérer regex HTML ?
     */
//    public function test_with_regex()
//    {
//        $field = new Field("foo", [], false, [new Regex("my pattern")]);
//
//        $getView = new GetView(new Form(new FormOperationRegistry(), []));
//        $getView->applyOnField($field);
//
//        $this->assertEquals(["pattern" => "my pattern"], $getView->get()["foo"]->getAttributes());
//    }

    /**
     *
     */
    public function test_with_required()
    {
        $field = new Field("foo", [], true);

        $getView = new GetView(new Form(new FormOperationRegistry(), []));
        $getView->applyOnField($field);

        $this->assertEquals(["required"], $getView->get()["foo"]->getProperties());
    }

    /**
     *
     */
    public function test_functional()
    {
        $builder = new FormBuilder();

        $builder->field("username")
            ->required()
            ->rule(new Length(4, 12))
            ->rule(new Regex("#^\\w+$#"))
        ;

        $builder->field("password")
            ->required()
            ->rule(new Length(6))
            ->filter(new Secret())
        ;

        $builder->field("confirm")
            ->required()
            ->rule(new SameAs("password"))
            ->filter(new Secret())
        ;

        $builder->field("mail")
            ->required()
            ->rule(new Email())
        ;

        $form = $builder->build();

        /** @var FormView $view */
        $view = $form->apply(new GetView($form))->get();

        $this->assertInstanceOf(FieldView::class, $view["username"]);
        $this->assertInstanceOf(FieldView::class, $view["password"]);
        $this->assertInstanceOf(FieldView::class, $view["confirm"]);
        $this->assertInstanceOf(FieldView::class, $view["mail"]);

        $this->assertEquals(["required"], $view["username"]->getProperties());
        $this->assertEquals(["required"], $view["password"]->getProperties());
        $this->assertEquals(["required"], $view["confirm"]->getProperties());
        $this->assertEquals(["required"], $view["mail"]->getProperties());

        $this->assertEquals("text", $view["username"]->getType());
        $this->assertEquals("password", $view["password"]->getType());
        $this->assertEquals("password", $view["confirm"]->getType());
        $this->assertEquals("email", $view["mail"]->getType());

        $this->assertEquals(["maxlength" => 12], $view["username"]->getAttributes());
    }
}

