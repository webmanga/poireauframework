<?php


namespace PoireauFramework\App\Form\Template\Theme;


use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\Renderer\BootstrapFieldRenderer;
use PoireauFramework\App\Form\Template\Renderer\BootstrapFormRenderer;
use PoireauFramework\App\Form\Template\Renderer\FieldRenderer;
use PoireauFramework\App\Form\Template\Renderer\FormRenderer;
use PoireauFramework\App\Form\Template\Renderer\TextAreaRenderer;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_Theme
 * @group ext_PoireauFramework_App_Form_Template_Theme_BootstrapTheme
 */
class BootstrapThemeTest extends TestCase
{
    /**
     * @var BootstrapTheme
     */
    private $theme;


    /**
     *
     */
    protected function setUp()
    {
        $this->theme = new BootstrapTheme();
    }

    /**
     *
     */
    public function test_forForm()
    {
        $this->assertInstanceOf(BootstrapFormRenderer::class, $this->theme->forForm());
    }

    /**
     *
     */
    public function test_forField()
    {
        $this->assertInstanceOf(BootstrapFieldRenderer::class, $this->theme->forField());
        $this->assertInstanceOf(BootstrapFieldRenderer::class, $this->theme->forField("textarea"));
    }
}
