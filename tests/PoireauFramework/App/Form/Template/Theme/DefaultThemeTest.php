<?php


namespace PoireauFramework\App\Form\Template\Theme;


use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\Renderer\FieldRenderer;
use PoireauFramework\App\Form\Template\Renderer\FormRenderer;
use PoireauFramework\App\Form\Template\Renderer\TextAreaRenderer;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_Theme
 * @group ext_PoireauFramework_App_Form_Template_Theme_DefaultTheme
 */
class DefaultThemeTest extends TestCase
{
    /**
     * @var DefaultTheme
     */
    private $theme;


    /**
     *
     */
    protected function setUp()
    {
        $this->theme = new DefaultTheme();
    }

    /**
     *
     */
    public function test_forForm()
    {
        $this->assertInstanceOf(FormRenderer::class, $this->theme->forForm());
    }

    /**
     *
     */
    public function test_forField()
    {
        $this->assertInstanceOf(FieldRenderer::class, $this->theme->forField());
        $this->assertInstanceOf(TextAreaRenderer::class, $this->theme->forField("textarea"));
    }
}
