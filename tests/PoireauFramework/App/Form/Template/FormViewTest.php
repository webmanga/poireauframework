<?php

namespace PoireauFramework\App\Form\Template;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\Renderer\FormRendererInterface;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_FormView
 */
class FormViewTest extends TestCase
{
    /**
     *
     */
    public function test_add()
    {
        $form = new FormView();

        $field = new FieldView(new DefaultTheme());
        $field->name("foo");

        $this->assertSame($form, $form->add($field));

        $this->assertTrue(isset($form["foo"]));
        $this->assertSame($field, $form["foo"]);
        $this->assertFalse(isset($form["other"]));
    }

    /**
     *
     */
    public function test_get_set_action()
    {
        $form = new FormView();

        $this->assertSame($form, $form->action("my_action"));
        $this->assertEquals("my_action", $form->getAction());
    }

    /**
     *
     */
    public function test_get_set_method()
    {
        $form = new FormView();

        $this->assertSame($form, $form->method("GET"));
        $this->assertEquals("GET", $form->getMethod());
    }

    /**
     *
     */
    public function test_open_unit()
    {
        $renderer = $this->createMock(FormRendererInterface::class);
        $form = new FormView($renderer);

        $renderer->expects($this->once())
            ->method("renderOpenTag")
            ->with($form)
            ->willReturn("<form>")
        ;

        $this->assertEquals("<form>", $form->open());
    }

    /**
     *
     */
    public function test_close_unit()
    {
        $renderer = $this->createMock(FormRendererInterface::class);
        $form = new FormView($renderer);

        $renderer->expects($this->once())
            ->method("renderCloseTag")
            ->with($form)
            ->willReturn("</form>")
        ;

        $this->assertEquals("</form>", $form->close());
    }

    /**
     *
     */
    public function test_open_functional()
    {
        $form = new FormView();

        $form
            ->action("http://webmanga.fr/search")
            ->method("GET")
        ;

        $this->assertEquals('<form action="http://webmanga.fr/search" method="GET" >', $form->open());
    }

    /**
     *
     */
    public function test_close_functional()
    {
        $form = new FormView();

        $form
            ->action("http://webmanga.fr/search")
            ->method("GET")
        ;

        $this->assertEquals('</form>', $form->close());
    }
}
