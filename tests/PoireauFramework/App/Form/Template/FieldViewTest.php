<?php

namespace PoireauFramework\App\Form\Template;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\Renderer\FieldRendererInterface;
use PoireauFramework\App\Form\Template\Renderer\FormRendererInterface;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;
use PoireauFramework\App\Form\Template\Theme\ThemeInterface;
use PoireauFramework\App\Template\Html\HtmlUtils;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_FieldView
 */
class FieldViewTest extends TestCase
{
    /**
     * @var ThemeInterface
     */
    protected $theme;


    protected function setUp()
    {
        $this->theme = new DefaultTheme();
        HtmlUtils::setIdCount(0);
    }

    /**
     *
     */
    public function test_get_set_name()
    {
        $field = new FieldView($this->theme);

        $this->assertSame($field, $field->name("new_name"));
        $this->assertEquals("new_name", $field->getName());
    }

    /**
     *
     */
    public function test_param_constructor()
    {
        $field = new FieldView($this->theme, ["foo" => "bar"]);
        $this->assertEquals("bar", $field->getParameter("foo"));
    }

    /**
     *
     */
    public function test_get_set_type()
    {
        $field = new FieldView($this->theme);

        $this->assertSame($field, $field->type("password"));
        $this->assertEquals("password", $field->getType());
    }

    /**
     *
     */
    public function test___toString_unit()
    {
        $renderer = $this->createMock(FieldRendererInterface::class);
        $theme = $this->createMock(ThemeInterface::class);

        $field = new FieldView($theme);

        $theme->expects($this->once())
            ->method("forField")
            ->with("text")
            ->willReturn($renderer)
        ;

        $renderer->expects($this->once())
            ->method("render")
            ->with($field)
            ->willReturn("<input>")
        ;

        $this->assertEquals("<input>", (string) $field);
    }

    /**
     *
     */
    public function test___toString_functional()
    {
        $field = new FieldView($this->theme);

        $field
            ->name("pass")
            ->type("password")
        ;

        $this->assertEquals('<input type="password" name="pass" value="" />', (string) $field);
    }

    /**
     *
     */
    public function test_label()
    {
        $field = new FieldView($this->theme);

        $field
            ->name("pass")
            ->type("password")
        ;

        $this->assertEquals('<label for="__id_fieldview_0__">hello</label>', $field->label("hello"));
    }
}
