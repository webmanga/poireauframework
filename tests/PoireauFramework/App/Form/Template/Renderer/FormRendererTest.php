<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\FormView;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_Renderer
 * @group ext_PoireauFramework_App_Form_Template_Renderer_FormRenderer
 */
class FormRendererTest extends TestCase
{
    /**
     * @var FormRenderer
     */
    protected $renderer;


    /**
     *
     */
    protected function setUp()
    {
        $this->renderer = new FormRenderer();
    }

    /**
     *
     */
    public function test_renderOpenTag()
    {
        $form = new FormView();

        $form
            ->action("http://webmanga.fr/search")
            ->method("GET")
            ->id("my_form_id")
        ;

        $this->assertEquals('<form action="http://webmanga.fr/search" method="GET" id="my_form_id" >', $this->renderer->renderOpenTag($form));
    }

    /**
     *
     */
    public function test_renderCloseTag()
    {
        $form = new FormView();

        $this->assertEquals('</form>', $this->renderer->renderCloseTag($form));
    }
}
