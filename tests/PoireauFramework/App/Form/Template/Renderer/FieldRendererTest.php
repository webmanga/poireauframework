<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\FieldView;
use PoireauFramework\App\Form\Template\FormView;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;
use PoireauFramework\App\Template\Html\HtmlUtils;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_Renderer
 * @group ext_PoireauFramework_App_Form_Template_Renderer_FieldRenderer
 */
class FieldRendererTest extends TestCase
{
    /**
     * @var FieldRenderer
     */
    protected $renderer;


    /**
     *
     */
    protected function setUp()
    {
        $this->renderer = new FieldRenderer();
        HtmlUtils::setIdCount(0);
    }

    /**
     *
     */
    public function test_render()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("<script>alert('Fail !');</script>")
            ->addClass("form-control")
        ;

        $this->assertEquals('<input type="email" name="user_mail" value="&lt;script&gt;alert(\'Fail !\');&lt;/script&gt;" class="form-control" />', $this->renderer->render($field));
    }

    /**
     *
     */
    public function test_label_simple()
    {
        $field = new FieldView(new DefaultTheme());

        $this->assertEquals('<label for="__id_fieldview_0__">hello world</label>', $this->renderer->label($field, "hello world"));
    }

    /**
     *
     */
    public function test_label_param_value()
    {
        $field = new FieldView(new DefaultTheme());
        $field->name("foo");

        $field->param("label-value", "Hello World !");

        $this->assertEquals('<label for="__id_fieldview_0__">Hello World !</label>', $this->renderer->label($field, null));
    }

    /**
     *
     */
    public function test_label_param_escape()
    {
        $field = new FieldView(new DefaultTheme());
        $field->name("foo");

        $field->param("label-value", "<b>fail</b>");

        $this->assertEquals('<label for="__id_fieldview_0__">&lt;b&gt;fail&lt;/b&gt;</label>', $this->renderer->label($field, null));

        $field->param("label-escape", false);

        $this->assertEquals('<label for="__id_fieldview_0__"><b>fail</b></label>', $this->renderer->label($field, null));
    }
}
