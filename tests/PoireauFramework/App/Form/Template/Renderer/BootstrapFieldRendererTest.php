<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\FieldView;
use PoireauFramework\App\Form\Template\FormView;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;
use PoireauFramework\App\Template\Html\HtmlUtils;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_Renderer
 * @group ext_PoireauFramework_App_Form_Template_Renderer_BootstrapFieldRenderer
 */
class BootstrapFieldRendererTest extends TestCase
{
    /**
     * @var BootstrapFieldRenderer
     */
    protected $renderer;


    /**
     *
     */
    protected function setUp()
    {
        $this->renderer = new BootstrapFieldRenderer(new FieldRenderer());
        HtmlUtils::setIdCount(0);
    }

    /**
     *
     */
    public function test_render_default()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("<script>alert('Fail !');</script>")
        ;

        $this->assertEquals('<div class="form-group"><label for="__id_fieldview_0__">user_mail</label><input type="email" name="user_mail" value="&lt;script&gt;alert(\'Fail !\');&lt;/script&gt;" id="__id_fieldview_0__" class="form-control" /><div class="help-block"></div></div>', $this->renderer->render($field));
    }

    /**
     *
     */
    public function test_render_no_label()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("<script>alert('Fail !');</script>")
            ->param("bs-label", false)
        ;

        $this->assertEquals('<div class="form-group"><input type="email" name="user_mail" value="&lt;script&gt;alert(\'Fail !\');&lt;/script&gt;" class="form-control" /><div class="help-block"></div></div>', $this->renderer->render($field));
    }

    /**
     *
     */
    public function test_render_no_wrap()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("<script>alert('Fail !');</script>")
            ->param("bs-wrap", false)
        ;

        $this->assertEquals('<label for="__id_fieldview_0__">user_mail</label><input type="email" name="user_mail" value="&lt;script&gt;alert(\'Fail !\');&lt;/script&gt;" id="__id_fieldview_0__" class="form-control" /><div class="help-block"></div>', $this->renderer->render($field));
    }

    /**
     *
     */
    public function test_render_no_feedback()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("<script>alert('Fail !');</script>")
            ->param("bs-feedback", false)
        ;

        $this->assertEquals('<div class="form-group"><label for="__id_fieldview_0__">user_mail</label><input type="email" name="user_mail" value="&lt;script&gt;alert(\'Fail !\');&lt;/script&gt;" id="__id_fieldview_0__" class="form-control" /></div>', $this->renderer->render($field));
    }

    /**
     *
     */
    public function test_render_with_error()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("val")
            ->error("invalid value")
        ;

        $this->assertEquals('<div class="form-group has-error"><label for="__id_fieldview_0__">user_mail</label><input type="email" name="user_mail" value="val" id="__id_fieldview_0__" class="form-control" /><div class="help-block">invalid value</div></div>', $this->renderer->render($field));
    }
}
