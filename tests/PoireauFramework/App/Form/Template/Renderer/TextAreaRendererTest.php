<?php

namespace PoireauFramework\App\Form\Template\Renderer;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Template\FieldView;
use PoireauFramework\App\Form\Template\FormView;
use PoireauFramework\App\Form\Template\Theme\DefaultTheme;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_Template
 * @group ext_PoireauFramework_App_Form_Template_Renderer
 * @group ext_PoireauFramework_App_Form_Template_Renderer_TextAreaRenderer
 */
class TextAreaRendererTest extends TestCase
{
    /**
     * @var TextAreaRenderer
     */
    protected $renderer;


    /**
     *
     */
    protected function setUp()
    {
        $this->renderer = new TextAreaRenderer();
    }

    /**
     *
     */
    public function test_render()
    {
        $field = new FieldView(new DefaultTheme());

        $field
            ->type("email")
            ->name("user_mail")
            ->value("<script>alert('Fail !');</script>")
            ->addClass("form-control")
        ;

        $this->assertEquals('<textarea name="user_mail" class="form-control" >&lt;script&gt;alert(\'Fail !\');&lt;/script&gt;</textarea>', $this->renderer->render($field));
    }
}
