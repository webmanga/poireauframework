<?php

namespace PoireauFramework\App\Form;

require_once __DIR__."/_files/TestRegistrationForm.php";

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Form\Test\TestRegistrationForm;
use PoireauFramework\Http\Form\Element\Field;
use PoireauFramework\Http\Form\Form;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Form
 * @group ext_PoireauFramework_App_Form_FormFactory
 */
class FormFactoryTest extends TestCase
{
    /**
     *
     */
    public function test_create()
    {
        $factory = new TestRegistrationForm();

        $form = $factory->create();

        $this->assertInstanceOf(Form::class, $form);

        $this->assertInstanceOf(Field::class, $form->get("username"));
        $this->assertInstanceOf(Field::class, $form->get("password"));
        $this->assertInstanceOf(Field::class, $form->get("confirm"));
        $this->assertInstanceOf(Field::class, $form->get("mail"));
    }
}
