<?php

namespace PoireauFramework\App\Database\Console;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Database\Test\UserMapper;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Console\Console;
use PoireauFramework\Console\ConsoleModule;
use PoireauFramework\Console\Testing\ConsoleAssertions;
use PoireauFramework\Database\DatabaseHandler;
use PoireauFramework\Database\DatabaseModule;
use PoireauFramework\Database\Sql\SqlConnectionInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Database
 * @group ext_PoireauFramework_App_Database_Console
 * @group ext_PoireauFramework_App_Database_Console_CreateSchemasCommand
 */
class CreateSchemasCommandTest extends TestCase
{
    use ConsoleAssertions;

    /**
     *
     */
    protected function setUp()
    {
        $app = new Application([
            "config" => [
                "config"  => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/../../_files/functional_config.ini"]
                ]
            ],
            "database" => [
                "default_connection" => "test"
            ]
        ]);

        $app->register(new DatabaseModule());
        $app->register(new ConsoleModule());

        $this->console = new Console($app);

        $this->cleanTables();
    }

    /**
     *
     */
    protected function tearDown()
    {
        $this->cleanTables();
    }

    /**
     *
     */
    protected function cleanTables()
    {
        /** @var SqlConnectionInterface $connection */
        $connection = $this->console->injector()->get(DatabaseHandler::class)->getConnection("test");

        $connection->query("DROP TABLE IF EXISTS `USER`");
    }

    /**
     *
     */
    public function test_execute()
    {
        $result = $this->executeCommand(new CreateSchemasCommand(), [__DIR__ . "/../_files"]);

        $this->assertEquals([
            "Found mapper '" . UserMapper::class . "'",
            ""
        ], $result, "", 0, 1, true);

        /** @var SqlConnectionInterface $connection */
        $connection = $this->console->injector()->get(DatabaseHandler::class)->getConnection("test");

        $tables = $connection->query("SELECT name FROM sqlite_master WHERE type='table'")->all();

        $this->assertContains(["name" => "USER"], $tables);
    }
}
