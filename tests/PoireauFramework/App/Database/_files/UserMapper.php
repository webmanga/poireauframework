<?php

namespace PoireauFramework\App\Database\Test;

use PoireauFramework\App\Database\SqlModel;
use PoireauFramework\Database\Mapping\Metadata\FieldsBuilder;
use PoireauFramework\Database\Mapping\Metadata\Metadata;
use PoireauFramework\Database\Mapping\Query\Bag\Binding;

class UserMapper extends SqlModel
{
    /**
     * {@inheritdoc}
     */
    protected function configure(Metadata $metadata): void
    {
        $metadata->entity = User::class;
        $metadata->schema = "USER";
    }

    /**
     * {@inheritdoc}
     */
    protected function buildFields(FieldsBuilder $builder): void
    {
        $builder
            ->integer("id")->attribute("USER_ID")->autoincrement()->primary()
            ->string("username")->attribute("USER_LOGIN")
            ->string("name")->attribute("USER_NAME")
            ->string("password")->attribute("USER_PASSWORD")
            ->string("email")->attribute("USER_MAIL")
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function criterias()
    {
        return [
            "login" => [
                "username" => new Binding(),
                "password" => new Binding()
            ]
        ];
    }

    public function insert(User $user)
    {
        $this->db->query(
            $this->helper->insert(),
            $this->hydrator->toDatabase($user)
        );

        if ($user->id() === null) {
            return $user->setId($this->db->lastInsertedId());
        }

        return $user;
    }

    /**
     * @return User[]
     */
    public function all()
    {
        $entities = [];

        foreach ($this->db->query("SELECT * FROM {$this->table()}")->all() as $data) {
            $entities[] = $this->hydrator->fromDatabase($data);
        }

        return $entities;
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function get($id)
    {
        $result = $this->db->query("SELECT * FROM {$this->table()} WHERE {$this->attr("id")} = ?", [$id])->fetch();

        if ($result === false) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return User|null
     */
    public function findForLogin($username, $password)
    {
        $criteria = $this->criteria("login");
        $criteria->bind(["username" => $username, "password" => $password]);

        $query = "SELECT * FROM {$this->helper->table()} WHERE {$criteria->query()}";

        $stmt = $this->db->prepare($query);
        $stmt->execute($criteria->bindings($this->db->platform()));

        $result = $stmt->fetch();

        if (!$result) {
            return null;
        }

        return $this->hydrator->fromDatabase($result);
    }
}
