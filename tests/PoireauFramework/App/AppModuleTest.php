<?php

namespace PoireauFramework\App;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Controller\BaseControllerFactory;
use PoireauFramework\App\Controller\ControllerFactoryInterface;
use PoireauFramework\App\Controller\ControllerResolverInterface;
use PoireauFramework\App\Controller\RouterControllerResolver;
use PoireauFramework\App\Http\RequestRunner;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\RouterInterface;
use PoireauFramework\App\Router\SimpleRouter;
use PoireauFramework\Config\Config;
use PoireauFramework\Event\EventModule;
use PoireauFramework\Http\HttpModule;
use PoireauFramework\Injector\BaseInjector;

/**
 * Class AppModuleTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_AppModule
 */
class AppModuleTest extends TestCase
{
    /**
     * @var BaseInjector
     */
    private $injector;


    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->injector = new BaseInjector();
        $this->injector->instance(new Application([]));
        $this->injector->instance(new Config($this->injector->reg('config')));
        $this->injector->register(new EventModule());
        $this->injector->register(new HttpModule());
        $this->injector->register(new AppModule());
    }

    /**
     *
     */
    public function test_configure()
    {
        $this->assertInstanceOf(BaseControllerFactory::class, $this->injector->get(ControllerFactoryInterface::class));
        $this->assertInstanceOf(RouterControllerResolver::class, $this->injector->get(ControllerResolverInterface::class));
        $this->assertInstanceOf(SimpleRouter::class, $this->injector->get(RouterInterface::class));
        $this->assertInstanceOf(RequestRunner::class, $this->injector->get(RequestRunner::class));
    }
}