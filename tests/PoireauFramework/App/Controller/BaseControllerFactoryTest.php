<?php

namespace PoireauFramework\App\Controller;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;

/**
 * Class BaseControllerFactoryTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Controller
 * @group ext_PoireauFramework_App_Controller_BaseControllerFactory
 */
class BaseControllerFactoryTest extends TestCase
{
    /**
     * @var BaseControllerFactory
     */
    private $factory;

    public function setUp()
    {
        $application = new Application([]);
        $application->load();

        $this->factory = new BaseControllerFactory(
            $application
        );
    }

    /**
     *
     */
    public function test_create()
    {
        $controller = $this->factory->create(TestController::class);

        $this->assertInstanceOf(TestController::class, $controller);
    }
}
