<?php
namespace PoireauFramework\App\Controller;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Router\Route;
use PoireauFramework\App\Router\RouterInterface;
use PoireauFramework\Config\PhpConfigParser;
use PoireauFramework\Http\Request\RequestBuilder;
use PoireauFramework\Http\Request\RequestStack;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Http\Response\ResponseStack;

/**
 * Class RouterControllerResolverTest
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Controller
 * @group ext_PoireauFramework_App_Controller_RouterControllerResolver
 */
class RouterControllerResolverTest extends TestCase
{
    /**
     * @var RouterControllerResolver
     */
    private $resolver;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ResponseStack
     */
    private $responseStack;

    public function setUp()
    {
        $application = new Application([
            "config" => [
                "config" => [
                    "parsers" => [
                        new PhpConfigParser()
                    ],
                    "files" => [
                        __DIR__ . "/../_files/test_config.php"
                    ]
                ]
            ]
        ]);

        $application->load();

        $this->resolver = new RouterControllerResolver(
            $application->config()->controller_resolver,
            new BaseControllerFactory(
                $application
            ),
            $this->requestStack = new RequestStack((new RequestBuilder())->build()),
            $this->responseStack = new ResponseStack()
        );
    }

    /**
     *
     */
    public function test_resolveController_with_route()
    {
        $request = (new RequestBuilder())->build();
        $response = $this->createMock(ResponseInterface::class);

        $this->requestStack->push($request);
        $this->responseStack->push($response);

        $route = new Route(["GET"], "test", "index");

        $controller = $this->resolver->resolveController($route);

        $this->assertTrue(is_callable($controller));

        $this->assertInstanceOf(TestController::class, $controller[0]);
        $this->assertEquals("indexAction", $controller[1]);

        $this->assertSame($request, $controller[0]->request());
        $this->assertSame($response, $controller[0]->response());
    }

    /**
     *
     */
    public function test_resolveArguments()
    {
        $request = (new RequestBuilder())
            ->path("/test/index/arg1/arg2")
            ->build()
        ;

        $route = new Route(["GET"], "test", "index", ["arg1", "arg2"]);
        $controller = function () {};

        $this->assertEquals(["arg1", "arg2"], $this->resolver->resolveArguments($controller, $route, $request));
    }
}
