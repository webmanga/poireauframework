<?php
require_once __DIR__ . "/_files/Controller/TestController.php";
require_once __DIR__ . "/_files/Controller/ShowcaseController.php";
require_once __DIR__ . "/_files/Controller/ErrorController.php";
require_once __DIR__ . "/_files/Controller/SessionController.php";

require_once __DIR__. "/_files/TestSecurityModule.php";
require_once __DIR__. "/_files/TestUser.php";
require_once __DIR__. "/_files/TestUserResolver.php";