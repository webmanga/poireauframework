<?php

namespace PoireauFramework\App;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Controller\ErrorController;
use PoireauFramework\App\Debug\ControllerExceptionHandler;
use PoireauFramework\App\Debug\ErrorHandlerModule;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\App\Template\AppTemplateModule;
use PoireauFramework\App\Testing\AppTestCase;
use PoireauFramework\App\Testing\TestingAppModule;
use PoireauFramework\App\Testing\TestingResponseSender;
use PoireauFramework\Config\IniConfigParser;
use PoireauFramework\Debug\ErrorHandler;
use PoireauFramework\Http\Exception\HttpException;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Template\TemplateModule;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Functional
 */
class FunctionalTest extends AppTestCase
{

    protected function createApplication(array $defaults = [])
    {
        return parent::createApplication([
            "config" => [
                "config"  => [
                    "parsers" => [new IniConfigParser()],
                    "files"   => [__DIR__ . "/_files/functional_config.ini"]
                ]
            ],

            "error_handler" => [
                "show_errors" => true,
                "exception_handlers" => [
                    [ControllerExceptionHandler::class, [HttpException::class, ErrorController::class]]
                ]
            ],

            "template" => [
                "paths" => [__DIR__ . "/_files/templates/"]
            ]
        ]);
    }

    protected function configureApplication(Application $application = null, array $modules = [])
    {
        parent::configureApplication($application, array_merge($modules, [
            new ErrorHandlerModule(),
            new TestingAppModule(),
            new AppTemplateModule(),
        ]));
    }

    /**
     *
     */
    public function test_hello_world()
    {
        $response = $this->call('GET', '/test/hello');

        $this->assertEquals(200, $response->getCode());
        $this->assertEquals('Hello World !', $response->getBody()->raw());
    }

    /**
     *
     */
    public function test_hello_john()
    {
        $response = $this->call('GET', '/test/hello/John');

        $this->assertEquals(200, $response->getCode());
        $this->assertEquals('Hello John !', $response->getBody()->raw());
    }

    /**
     *
     */
    public function test_hello_template()
    {
        $response = $this->call('GET', '/showcase/hello/John');

        $this->assertEquals(200, $response->getCode());
        $this->assertXmlStringEqualsXmlString(<<<HTML
<!DOCTYPE html>
<html>
    <head>
        <title>PoireauFramework - Welcome</title>
        <meta charset="UTF-8" />
    </head>
    <body>
        <p>Hello <em>John</em> !</p>    
    </body>
</html>
HTML
 , $response->getBody()->raw());
    }

    /**
     *
     */
    public function test_not_found_error()
    {
        $response = $this->call('GET', '/not_found/error');

        $this->assertEquals(404, $response->getCode());
        $this->assertEquals("Page introuvable", $response->getBody()->raw());
    }
}