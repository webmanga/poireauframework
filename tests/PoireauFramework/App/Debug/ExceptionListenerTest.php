<?php

namespace PoireauFramework\App\Debug;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Http\Event\RunErrorArgument;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Debug
 * @group ext_PoireauFramework_App_Debug_ExceptionListener
 */
class ExceptionListenerTest extends TestCase
{
    /**
     *
     */
    public function test_onRunError()
    {
        $argument = new RunErrorArgument(new \Exception(), $this->createMock(RequestInterface::class), $this->createMock(ResponseInterface::class));

        $handler1 = $this->createMock(ExceptionHandlerInterface::class);
        $handler2 = $this->createMock(ExceptionHandlerInterface::class);

        $handler1->expects($this->once())
            ->method('supports')
            ->with($argument->exception())
            ->willReturn(false)
        ;

        $handler2->expects($this->once())
            ->method('supports')
            ->with($argument->exception())
            ->willReturn(true)
        ;

        $handler2->expects($this->once())
            ->method('handle')
            ->with($argument->exception(), $argument->request(), $argument->response())
            ->willReturn('return value')
        ;

        $listener = new ExceptionListener([$handler1, $handler2]);

        $listener->onRunError($argument);

        $this->assertEquals('return value', $argument->result());
    }
}