<?php

namespace PoireauFramework\App\Debug;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\Controller\BaseControllerFactory;
use PoireauFramework\App\Controller\Controller;
use PoireauFramework\App\Controller\ControllerFactoryInterface;
use PoireauFramework\App\Exception\AppException;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Http\Exception\BadRequestHttpException;
use PoireauFramework\Http\Exception\HttpException;
use PoireauFramework\Http\Exception\NotFoundHttpException;
use PoireauFramework\Http\Request\RequestInterface;
use PoireauFramework\Http\Response\ResponseInterface;
use PoireauFramework\Injector\Exception\InjectorException;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Debug
 * @group ext_PoireauFramework_App_Debug_ControllerExceptionHandler
 */
class ControllerExceptionHandlerTest extends TestCase
{
    /**
     *
     */
    public function test_supports()
    {
        $handler = new ControllerExceptionHandler(
            $this->createMock(ControllerFactoryInterface::class),
            HttpException::class,
            MyErrorController::class
        );

        $this->assertTrue($handler->supports(new NotFoundHttpException()));
        $this->assertFalse($handler->supports(new \LogicException()));
        $this->assertTrue($handler->supports(new HttpException()));
    }

    /**
     * @dataProvider handleDataProvider
     */
    public function test_handle($exception, $method)
    {
        $factory = new BaseControllerFactory(new Application([]));

        $handler = new ControllerExceptionHandler($factory, \Exception::class, MyErrorController::class);

        $handler->handle($exception, $this->createMock(RequestInterface::class), $this->createMock(ResponseInterface::class));

        $this->assertEquals($method, MyErrorController::$method);
        $this->assertSame($exception, MyErrorController::$exception);

    }

    /**
     *
     */
    public function test_method_not_found()
    {
        $this->expectException(AppException::class);

        $factory = new BaseControllerFactory(new Application([]));

        $handler = new ControllerExceptionHandler($factory, \Exception::class, MyErrorController::class);

        $handler->handle(new InjectorException(), $this->createMock(RequestInterface::class), $this->createMock(ResponseInterface::class));
    }

    public function handleDataProvider()
    {
        return [
            [new NotFoundHttpException(), 'notFoundHttpException'],
            [new BadRequestHttpException(), 'httpException'],
            [new \LogicException(), 'logicException']
        ];
    }
}

class MyErrorController extends Controller
{
    static public $method;
    static public $exception;

    public function httpException(HttpException $exception)
    {

        self::$exception = $exception;
        self::$method = "httpException";
    }

    public function notFoundHttpException(NotFoundHttpException $exception)
    {
        self::$exception = $exception;
        self::$method = "notFoundHttpException";
    }

    public function logicException(\LogicException $exception)
    {
        self::$exception = $exception;
        self::$method = "logicException";
    }
}