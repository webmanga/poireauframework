<?php

namespace PoireauFramework\App\Debug;

use PHPUnit\Framework\TestCase;
use PoireauFramework\App\AppModule;
use PoireauFramework\App\Controller\TestController;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Debug\ErrorHandler;
use PoireauFramework\Http\Exception\HttpException;
use PoireauFramework\Injector\BaseInjector;
use PoireauFramework\Registry\RegistryFactory;

/**
 * @group ext
 * @group ext_PoireauFramework
 * @group ext_PoireauFramework_App
 * @group ext_PoireauFramework_App_Debug
 * @group ext_PoireauFramework_App_Debug_ErrorHandlerModule
 */
class ErrorHandlerModuleTest extends TestCase
{
    /**
     *
     */
    public function test_configure()
    {
        $injector = new BaseInjector();

        $injector->register(new ErrorHandlerModule());

        $this->assertInstanceOf(ErrorHandler::class, $injector->get(ErrorHandler::class));
        $this->assertSame(ErrorHandler::instance(), $injector->get(ErrorHandler::class));
    }

    /**
     *
     */
    public function test_configure_with_show_errors()
    {
        $injector = new BaseInjector(RegistryFactory::createFromArray([
            "error_handler" => [
                "show_errors" => true
            ]
        ]));

        $injector->register(new ErrorHandlerModule());

        $this->assertTrue(ErrorHandler::instance()->isShowErrors());
    }

    /**
     *
     */
    public function test_create_ControllerExceptionHandler()
    {
        $injector = new BaseInjector();
        $injector->register(new ErrorHandlerModule());
        $injector->register(new AppModule()); // ControllerFactoryInterface
        $injector->instance(new Application([]));

        $handler = $injector->create(ControllerExceptionHandler::class, [HttpException::class, TestController::class]);

        $this->assertInstanceOf(ControllerExceptionHandler::class, $handler);
    }

    /**
     *
     */
    public function test_configure_with_handlers_string()
    {
        $handler = $this->createMock(ExceptionHandlerInterface::class);

        $injector = new BaseInjector(RegistryFactory::createFromArray([
            "error_handler" => [
                "exception_handlers" => [
                    get_class($handler)
                ]
            ]
        ]));
        $injector->instance($handler);

        $injector->register(new ErrorHandlerModule());

        $listener = $injector->get(ExceptionListener::class);

        $this->assertSame($handler, $this->readAttribute($listener, 'handlers')[0]);
    }

    /**
     *
     */
    public function test_configure_with_handlers_object()
    {
        $handler = $this->createMock(ExceptionHandlerInterface::class);

        $injector = new BaseInjector(RegistryFactory::createFromArray([
            "error_handler" => [
                "exception_handlers" => [
                    $handler
                ]
            ]
        ]));

        $injector->register(new ErrorHandlerModule());

        $listener = $injector->get(ExceptionListener::class);

        $this->assertSame($handler, $this->readAttribute($listener, 'handlers')[0]);
    }

    /**
     *
     */
    public function test_configure_with_handlers_array()
    {
        $injector = new BaseInjector(RegistryFactory::createFromArray([
            "error_handler" => [
                "exception_handlers" => [
                    [ControllerExceptionHandler::class, [HttpException::class, TestController::class]]
                ]
            ]
        ]));

        $injector->register(new ErrorHandlerModule());
        $injector->register(new AppModule()); // ControllerFactoryInterface
        $injector->instance(new Application([]));

        $listener = $injector->get(ExceptionListener::class);

        $this->assertInstanceOf(ControllerExceptionHandler::class, $this->readAttribute($listener, 'handlers')[0]);
    }

    /**
     *
     */
    public function test_boot_will_register_ErrorHandler()
    {
        $mock = $this->createMock(ErrorHandler::class);

        $reflection = new \ReflectionProperty(ErrorHandler::class, "instance");
        $reflection->setAccessible(true);

        $reflection->setValue(null, $mock);

        $mock->expects($this->once())->method("register");

        $application = new Application([]);

        $application->register(new ErrorHandlerModule());
        $application->load();

        $reflection->setValue(null, null);
    }
}
