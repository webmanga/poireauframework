<!DOCTYPE html>
<html>
    <head>
        <title>PoireauFramework - <?= htmlentities($title); ?></title>
        <meta charset="UTF-8" />
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="<?= $this->url()->page("/home/index") ?>">Accueil</a></li>
                <li><a href="<?= $this->url()->page("/authentication/index") ?>">Login</a></li>
                <li><a href="<?= $this->url()->page("/authentication/logout") ?>">Logout</a></li>
            </ul>
        </nav>
        <?= $this->view()->getContent(); ?>
    </body>
</html>