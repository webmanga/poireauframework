<?php $this->view()->parent("layout", ["title" => "Page introuvable"]); ?>

<h1>Erreur 404 : Page introuvable</h1>

<p>La page demandée est introuvable, ou n'existe plus</p>
