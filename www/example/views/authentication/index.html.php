<?php $this->view()->parent("layout", ["title" => "Connexion"]); ?>

<form action="<?= $this->url("/authentication/login") ?>" method="post">
    <input type="text" name="login"/>
    <input type="password" name="pass"/>
    <input type="submit" value="Connexion" />
</form>
