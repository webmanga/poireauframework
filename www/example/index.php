<?php
use Example\Controller\ErrorController;
use PoireauFramework\App\Debug\ControllerExceptionHandler;
use PoireauFramework\Http\Exception\HttpException;

error_reporting(E_ALL);
ini_set("display_errors", true);

require __DIR__ . "/../../vendor/autoload.php";

$start = microtime(true);

$application = new \PoireauFramework\App\Kernel\Application([
    "config" => [
        "config" => [
            "parsers" => [new \PoireauFramework\Config\PhpConfigParser()],
            "files"   => [__DIR__ . "/config/config.php"]
        ]
    ],
    "error_handler" => [
        "show_errors" => true,
        "exception_handlers" => [
            [ControllerExceptionHandler::class, [HttpException::class, ErrorController::class]]
        ]
    ],
    "template" => [
        "paths" => [__DIR__ . "/views/"]
    ]
]);

$application->register(new \PoireauFramework\App\Debug\ErrorHandlerModule());
$application->register(new \PoireauFramework\App\Template\AppTemplateModule());
$application->register(new \PoireauFramework\Session\SessionModule());
$application->register(new \PoireauFramework\Debug\Debug());

$application->load();
$application->run();

echo "<br/>", "Execution time : ", (microtime(true) - $start) * 1000, "ms";