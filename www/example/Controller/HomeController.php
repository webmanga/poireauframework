<?php

namespace Example\Controller;

use PoireauFramework\App\Controller\Controller;
use PoireauFramework\App\Controller\FrontEndController;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Session\SessionService;

/**
 * Class HomeController
 */
class HomeController extends FrontEndController
{
    /**
     * @var SessionService
     */
    private $session;

    /**
     * AuthenticationController constructor.
     *
     * @param SessionService $session
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->session = $application->injector()->get(SessionService::class);
    }

    public function indexAction()
    {
        return $this->render("home/index", [
            "name" => $this->session->session()->login
        ]);
    }
}