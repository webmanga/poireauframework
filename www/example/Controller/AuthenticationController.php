<?php

namespace Example\Controller;

use PoireauFramework\App\Controller\FrontEndController;
use PoireauFramework\App\Kernel\Application;
use PoireauFramework\Http\Request\Request;
use PoireauFramework\Session\SessionService;

/**
 * Class AuthenticationController
 */
class AuthenticationController extends FrontEndController
{
    /**
     * @var SessionService
     */
    private $session;

    /**
     * AuthenticationController constructor.
     *
     * @param SessionService $session
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->session = $application->injector()->get(SessionService::class);
    }


    public function indexAction()
    {
        return $this->render("authentication/index");
    }

    public function loginAction()
    {
        if (!$this->request->method() === Request::METHOD_POST) {
            //TODO throw MethodNotAllowed
        }

        $this->session->session()->login = $this->request->body()->get("login");
    }

    public function logoutAction()
    {
        unset($this->session->session()->login);
    }
}