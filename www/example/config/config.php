<?php
return [
    "controller_resolver" => [
        "controller_prefix" => "Example\\Controller\\",
        "controller_suffix" => "Controller",
        "method_suffix"     => "Action"
    ],

    "url" => [
        "base" => "http://127.0.0.1",
        "rewriting" => false
    ]
];