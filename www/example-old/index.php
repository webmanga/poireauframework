<?php

error_reporting(E_ALL);
ini_set("display_errors", true);

require __DIR__ . "/../../vendor/autoload.php";

$start = microtime(true);

PoireauFramework\App::getOrCreate(__DIR__)
    ->load()
    ->run()
;

echo "<br/>", "Execution time : ", (microtime(true) - $start) * 1000, "ms";