<?php

namespace App\Controller;

use PoireauFramework\Arch\Controller;

class Home extends Controller
{
    public function indexAction()
    {
        echo "Hello World !";
    }
}